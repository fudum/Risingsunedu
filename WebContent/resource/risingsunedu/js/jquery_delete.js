// 删除提示
function deleteConfirm(id, url, msg) {
	if (!msg)
		msg = '确定要删除？';
	art.dialog.confirm(msg, function() {
		$.ajax({
			url : url,
			type : 'post',
			dataType : 'html',
			success : function(data, status) {
				if (status == 'success') {
					var objs = jQuery.parseJSON(data);
					var timer;
					art.dialog({
						content : '<span style="color:#f00">' + objs[1]
								+ '</span>',
						init : function() {
							var that = this, i = 1;
							var fn = function() {
								!i && that.close();
								i--;
							};
							timer = setInterval(fn, 1000);
							fn();
						},
						close : function() {
							clearInterval(timer);
							location.reload();
						}
					}).show();			
					
				}
			},
			error : function(xhr, textStatus, errorThrown) {
				art.dialog.tips('<span style="color:#f00">删除失败！</span>', 1);
			}
		});
	});
}