function login(url,urlin){
	art.dialog.open(url,{
		title:'用户登入',
		width:300,
		height:150,
		okVal:'登入',
		ok:function(){
	    	var iframe = this.iframe.contentWindow;
	    	if (!iframe.document.body) {
	        	alert('iframe还没加载完毕呢')
	        	return false;
	        };
	    	var	tip = iframe.document.getElementById('login-form-tip'),
	    		username = iframe.document.getElementById('login-form-username'),
	    		password = iframe.document.getElementById('login-form-password');
	    		
	        if (check(username) && check(password)){
	        	var pwd = faultylabs.MD5(faultylabs.MD5(password.value));
	        	var stuid = username.value;
	        	$.ajax({
	        		url:urlin,
	        		type:'post',
	        		data:{stuid:stuid,password:pwd},
	        		dataType:"json",
	        		success:function(data,status){
	        			if(data == 1){
	        				window.location.reload();
	        			}
	        			else{
	        				tip.innerText = '*用户名或密码错误';
	        			}
	        		},
	        		error : function(xhr, textStatus, errorThrown) {
	        			tip.innerText = '*用户名或密码错误';
						return false;
					}
	        		
	        	});
	        }
	       	return false;
		},
		cancel: true
	});
	
	// 表单验证
	var check = function (input) {
	    if (input.value === '') {
	        input.focus();
	        return false;
	    } else {
	        return true;
	    };
	};
}

function loginStu(url,urlin,urlto){
	art.dialog.open(url,{
		title:'用户登入',
		width:300,
		height:150,
		okVal:'登入',
		ok:function(){
	    	var iframe = this.iframe.contentWindow;
	    	if (!iframe.document.body) {
	        	alert('iframe还没加载完毕呢')
	        	return false;
	        };
	    	var	tip = iframe.document.getElementById('login-form-tip'),
	    		username = iframe.document.getElementById('login-form-username'),
	    		password = iframe.document.getElementById('login-form-password');
	    		
	        if (check(username) && check(password)){
	        	var pwd = faultylabs.MD5(faultylabs.MD5(password.value));
	        	var stuid = username.value;
	        	$.ajax({
	        		url:urlin,
	        		type:'post',
	        		data:{stuid:stuid,password:pwd},
	        		dataType:"json",
	        		success:function(data,status){
	        			if(data == 1){
	        				window.location.href=urlto;
	        			}
	        			else{
	        				tip.innerText = '*用户名或密码错误';
	        			}
	        		},
	        		error : function(xhr, textStatus, errorThrown) {
	        			tip.innerText = '*用户名或密码错误';
						return false;
					}
	        		
	        	});
	        }
	       	return false;
		},
		cancel: true
	});
	
	// 表单验证
	var check = function (input) {
	    if (input.value === '') {
	        input.focus();
	        return false;
	    } else {
	        return true;
	    };
	};
}

//退出
function loginout(url,type){
	$.ajax({
		url:url,
		dataType:"json",
		success:function(data,status){
			if(data == 1){
				if(type == 2){
					window.location.href="/";
				}
				else{
					window.location.reload();	
				}
							
			}
			else{
				alert('退出失败');
			}
		},
		error : function(xhr, textStatus, errorThrown) {
			alert('退出失败');
			return false;
		}
	});
	
}
