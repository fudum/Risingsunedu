/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50542
Source Host           : localhost:3306
Source Database       : risingsunedu

Target Server Type    : MYSQL
Target Server Version : 50542
File Encoding         : 65001

Date: 2015-03-06 10:29:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for advertisement
-- ----------------------------
DROP TABLE IF EXISTS `advertisement`;
CREATE TABLE `advertisement` (
  `id` varchar(255) NOT NULL COMMENT '主键',
  `picpath` varchar(200) DEFAULT NULL COMMENT '图片路径',
  `porder` int(255) DEFAULT NULL COMMENT '显示顺序',
  `flag` varchar(2) DEFAULT NULL COMMENT '是否显示',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='首页广告';

-- ----------------------------
-- Table structure for banji
-- ----------------------------
DROP TABLE IF EXISTS `banji`;
CREATE TABLE `banji` (
  `id` varchar(32) NOT NULL COMMENT '主键编号',
  `grade` varchar(50) DEFAULT NULL COMMENT '年级',
  `subject` varchar(50) DEFAULT NULL COMMENT '科目',
  `banci` varchar(50) DEFAULT NULL COMMENT '班次',
  `bjtype` varchar(50) DEFAULT NULL COMMENT '班级类型',
  `timeframe` varchar(50) DEFAULT NULL COMMENT '时段',
  `lessonday` varchar(50) DEFAULT NULL COMMENT '上课天 周日',
  `address` varchar(200) DEFAULT NULL COMMENT '上课地点',
  `startdate` varchar(50) DEFAULT NULL COMMENT '开始日期',
  `enddate` varchar(50) DEFAULT NULL COMMENT '结束日期',
  `starttime` varchar(20) DEFAULT NULL,
  `endtime` varchar(20) DEFAULT NULL,
  `tea_id` varchar(50) DEFAULT NULL,
  `tea_name` varchar(50) DEFAULT NULL,
  `inserttime` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `year` varchar(50) DEFAULT NULL,
  `playnum` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='班级';

-- ----------------------------
-- Table structure for banjijiangci
-- ----------------------------
DROP TABLE IF EXISTS `banjijiangci`;
CREATE TABLE `banjijiangci` (
  `id` varchar(20) NOT NULL,
  `banji_id` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `jorder` int(20) DEFAULT NULL,
  `inserttime` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='班级讲次';

-- ----------------------------
-- Table structure for banjijieci
-- ----------------------------
DROP TABLE IF EXISTS `banjijieci`;
CREATE TABLE `banjijieci` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `jiangci_id` varchar(32) DEFAULT NULL COMMENT '节次主键',
  `jiangci_name` varchar(50) DEFAULT NULL COMMENT '讲次名称',
  `jieci_name` varchar(50) DEFAULT NULL COMMENT '节次名称',
  `videopath` varchar(20) DEFAULT NULL COMMENT '视频地址',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='班级节次';

-- ----------------------------
-- Table structure for baseone
-- ----------------------------
DROP TABLE IF EXISTS `baseone`;
CREATE TABLE `baseone` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='基本信息一级目录';

-- ----------------------------
-- Table structure for basetwo
-- ----------------------------
DROP TABLE IF EXISTS `basetwo`;
CREATE TABLE `basetwo` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `botype` varchar(50) DEFAULT NULL COMMENT '一级类别',
  `name` varchar(50) DEFAULT NULL COMMENT ' 名称',
  `border` int(20) DEFAULT NULL COMMENT '排序',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='基本信息二级目录';

-- ----------------------------
-- Table structure for book
-- ----------------------------
DROP TABLE IF EXISTS `book`;
CREATE TABLE `book` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `name` varchar(100) DEFAULT NULL COMMENT '书名',
  `grade` varchar(50) DEFAULT NULL COMMENT '年级',
  `subject` varchar(50) DEFAULT NULL COMMENT '科目',
  `xueqi` varchar(50) DEFAULT NULL COMMENT '学期',
  `publisher` varchar(100) DEFAULT NULL COMMENT '出版社',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='书本';

-- ----------------------------
-- Table structure for ceshi
-- ----------------------------
DROP TABLE IF EXISTS `ceshi`;
CREATE TABLE `ceshi` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `name` varchar(500) DEFAULT NULL COMMENT '测试名称',
  `subject` varchar(50) DEFAULT NULL COMMENT '测试科目',
  `originalpath` varchar(200) DEFAULT NULL COMMENT '原始测试试卷',
  `finishpath` varchar(200) DEFAULT NULL COMMENT '完成测试试卷',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  `stuid` varchar(32) DEFAULT NULL COMMENT '学生主键',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='培优测试';

-- ----------------------------
-- Table structure for dayi
-- ----------------------------
DROP TABLE IF EXISTS `dayi`;
CREATE TABLE `dayi` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `name` varchar(100) DEFAULT NULL COMMENT '名称',
  `subject` varchar(50) DEFAULT NULL COMMENT '科目',
  `grade` varchar(50) DEFAULT NULL COMMENT '年级',
  `tea_id` varchar(32) DEFAULT NULL COMMENT '教师编号',
  `tea_name` varchar(50) DEFAULT NULL COMMENT '教师名称',
  `info` text COMMENT '详情',
  `picpath` varchar(300) DEFAULT NULL COMMENT '照片地址',
  `videopath` varchar(300) DEFAULT NULL COMMENT '视频地址',
  `playNum` int(11) DEFAULT NULL COMMENT '播放次数',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  `nanyidu` varchar(20) DEFAULT NULL COMMENT '难易度',
  `chapter` varchar(100) DEFAULT NULL COMMENT '章节',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='答疑表';

-- ----------------------------
-- Table structure for excellentstudent
-- ----------------------------
DROP TABLE IF EXISTS `excellentstudent`;
CREATE TABLE `excellentstudent` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `name` varchar(50) DEFAULT NULL,
  `picpath` varchar(200) DEFAULT NULL,
  `motto` varchar(500) DEFAULT NULL,
  `stuid` varchar(32) DEFAULT NULL COMMENT '学生主键',
  `grade` varchar(50) DEFAULT NULL COMMENT '年级',
  `teaid` varchar(32) DEFAULT NULL COMMENT '教师主键',
  `teaname` varchar(50) DEFAULT NULL COMMENT '教师名称',
  `leixing` varchar(50) DEFAULT NULL COMMENT '成绩单类型',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='学霸们的成绩单';

-- ----------------------------
-- Table structure for jiangyi
-- ----------------------------
DROP TABLE IF EXISTS `jiangyi`;
CREATE TABLE `jiangyi` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `name` varchar(100) DEFAULT NULL COMMENT '讲义名称',
  `bj_id` varchar(32) DEFAULT NULL COMMENT '班级编号',
  `bj_name` varchar(100) DEFAULT NULL COMMENT '班级名称',
  `jiangyiPath` varchar(200) DEFAULT NULL COMMENT '讲义路径',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  `updatetime` varchar(20) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='讲义';

-- ----------------------------
-- Table structure for jiangyiparse
-- ----------------------------
DROP TABLE IF EXISTS `jiangyiparse`;
CREATE TABLE `jiangyiparse` (
  `id` varchar(32) NOT NULL,
  `jy_id` varchar(32) DEFAULT NULL,
  `timu` text,
  `parse` text,
  `videoPath` varchar(500) DEFAULT NULL,
  `inserttime` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for kecheng
-- ----------------------------
DROP TABLE IF EXISTS `kecheng`;
CREATE TABLE `kecheng` (
  `id` varchar(32) NOT NULL COMMENT '主键编号',
  `name` varchar(50) DEFAULT NULL COMMENT '课程名称',
  `year` varchar(50) DEFAULT NULL COMMENT '年份',
  `grade` varchar(50) DEFAULT NULL COMMENT '年级',
  `subject` varchar(50) DEFAULT NULL COMMENT '科目',
  `banci` varchar(50) DEFAULT NULL COMMENT '班次',
  `bjtype` varchar(50) DEFAULT NULL COMMENT '班级类型',
  `timeframe` varchar(50) DEFAULT NULL COMMENT '时段',
  `lessonday` varchar(50) DEFAULT NULL COMMENT '上课天 周日',
  `address` varchar(200) DEFAULT NULL COMMENT '上课地点',
  `startdate` varchar(50) DEFAULT NULL COMMENT '开始日期',
  `enddate` varchar(50) DEFAULT NULL COMMENT '结束日期',
  `starttime` varchar(20) DEFAULT NULL COMMENT '开始时间',
  `endtime` varchar(20) DEFAULT NULL COMMENT '结束时间',
  `tea_id` varchar(50) DEFAULT NULL COMMENT '教师编号',
  `tea_name` varchar(50) DEFAULT NULL COMMENT '教师姓名',
  `inserttime` varchar(50) DEFAULT NULL COMMENT '插入时间',
  `playnum` int(11) DEFAULT NULL COMMENT '展示次数',
  `price` varchar(20) DEFAULT NULL COMMENT '价格',
  `catalog` text COMMENT '目录',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='课程';

-- ----------------------------
-- Table structure for kechengjiangci
-- ----------------------------
DROP TABLE IF EXISTS `kechengjiangci`;
CREATE TABLE `kechengjiangci` (
  `id` varchar(32) NOT NULL,
  `kecheng_id` varchar(32) DEFAULT NULL COMMENT '课程编号',
  `name` varchar(200) DEFAULT NULL COMMENT '讲次名称',
  `jorder` int(11) DEFAULT NULL,
  `inserttime` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for kechengjieci
-- ----------------------------
DROP TABLE IF EXISTS `kechengjieci`;
CREATE TABLE `kechengjieci` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `jiangci_id` varchar(32) DEFAULT NULL COMMENT '节次主键',
  `jiangci_name` varchar(50) DEFAULT NULL COMMENT '讲次顺序名称',
  `jieci_name` varchar(50) DEFAULT NULL COMMENT '节次名称',
  `videopath` varchar(20) DEFAULT NULL COMMENT '视频地址',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='课程节次';

-- ----------------------------
-- Table structure for medtestcatg
-- ----------------------------
DROP TABLE IF EXISTS `medtestcatg`;
CREATE TABLE `medtestcatg` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `name` varchar(255) DEFAULT NULL COMMENT '姓名',
  `sequence` varchar(255) DEFAULT NULL COMMENT '顺序',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for openk
-- ----------------------------
DROP TABLE IF EXISTS `openk`;
CREATE TABLE `openk` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `name` varchar(200) DEFAULT NULL COMMENT '公开课名称',
  `grade` varchar(50) DEFAULT NULL COMMENT '年级',
  `subject` varchar(50) DEFAULT NULL COMMENT '科目',
  `tea_id` varchar(50) DEFAULT NULL COMMENT '教师编号',
  `tea_name` varchar(50) DEFAULT NULL COMMENT '教师姓名',
  `info` text COMMENT '公开课详情',
  `playnum` int(255) DEFAULT NULL COMMENT '播放次数',
  `inserttime` varchar(20) DEFAULT NULL,
  `kctype` varchar(100) DEFAULT NULL COMMENT '课程类型',
  `catalog` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='公开课';

-- ----------------------------
-- Table structure for openkjiangci
-- ----------------------------
DROP TABLE IF EXISTS `openkjiangci`;
CREATE TABLE `openkjiangci` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `openk_id` varchar(32) DEFAULT NULL COMMENT '公开课id',
  `name` varchar(200) DEFAULT NULL COMMENT '讲次名称',
  `jorder` int(20) DEFAULT NULL COMMENT '讲词顺序',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for openksec
-- ----------------------------
DROP TABLE IF EXISTS `openksec`;
CREATE TABLE `openksec` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `openk_id` varchar(50) DEFAULT NULL COMMENT '讲次编号',
  `secname` varchar(50) DEFAULT NULL COMMENT '讲次名称',
  `thirdname` varchar(50) DEFAULT NULL COMMENT '第几节',
  `picpath` varchar(200) DEFAULT NULL,
  `videopath` varchar(200) DEFAULT NULL COMMENT '视频路径',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for openkthird
-- ----------------------------
DROP TABLE IF EXISTS `openkthird`;
CREATE TABLE `openkthird` (
  `id` varchar(32) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for peiyoujiangyi
-- ----------------------------
DROP TABLE IF EXISTS `peiyoujiangyi`;
CREATE TABLE `peiyoujiangyi` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `name` varchar(500) DEFAULT NULL COMMENT '讲义名称',
  `subject` varchar(50) DEFAULT NULL COMMENT '讲义科目',
  `finishpath` varchar(200) DEFAULT NULL COMMENT '讲义',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  `stuid` varchar(32) DEFAULT NULL COMMENT '学生主键',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='讲义';

-- ----------------------------
-- Table structure for school
-- ----------------------------
DROP TABLE IF EXISTS `school`;
CREATE TABLE `school` (
  `id` varchar(32) NOT NULL,
  `name` varchar(100) DEFAULT NULL COMMENT '学校名称',
  `jibie` varchar(50) DEFAULT NULL COMMENT '学校级别',
  `area` varchar(32) DEFAULT NULL COMMENT '区域名称',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for schoollevel
-- ----------------------------
DROP TABLE IF EXISTS `schoollevel`;
CREATE TABLE `schoollevel` (
  `id` varchar(32) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `inserttime` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for shijuan
-- ----------------------------
DROP TABLE IF EXISTS `shijuan`;
CREATE TABLE `shijuan` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `name` varchar(200) DEFAULT NULL COMMENT '试卷名称',
  `grade` varchar(50) DEFAULT NULL COMMENT '年级',
  `subject` varchar(50) DEFAULT NULL COMMENT '科目',
  `stype` varchar(50) DEFAULT NULL COMMENT '试卷类型',
  `year` varchar(50) DEFAULT NULL COMMENT '试卷年份',
  `sjpath` varchar(200) DEFAULT NULL COMMENT '试卷存放路径',
  `wordpath` varchar(200) DEFAULT NULL,
  `xueqi` varchar(50) DEFAULT NULL COMMENT '学期',
  `downnum` int(11) DEFAULT NULL COMMENT '下载次数',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='试卷';

-- ----------------------------
-- Table structure for shitiku
-- ----------------------------
DROP TABLE IF EXISTS `shitiku`;
CREATE TABLE `shitiku` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `year` varchar(255) DEFAULT NULL COMMENT '年份',
  `subject` varchar(255) DEFAULT NULL COMMENT '科目',
  `grade` varchar(255) DEFAULT NULL COMMENT '年级',
  `name` varchar(255) DEFAULT NULL COMMENT '姓名',
  `semester` varchar(255) DEFAULT NULL COMMENT '学期',
  `rank` varchar(255) DEFAULT NULL COMMENT '等级',
  `month` varchar(255) DEFAULT NULL COMMENT '月份',
  `area` varchar(255) DEFAULT NULL COMMENT '区域',
  `school` varchar(255) DEFAULT NULL COMMENT '学校',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student` (
  `id` varchar(32) NOT NULL,
  `stuid` varchar(50) DEFAULT NULL COMMENT '用户编号',
  `name` varchar(50) NOT NULL COMMENT '姓名',
  `password` varchar(50) DEFAULT NULL COMMENT '密码',
  `sex` varchar(2) DEFAULT NULL COMMENT '性别',
  `school` varchar(100) DEFAULT NULL COMMENT '学校',
  `phone` varchar(20) DEFAULT NULL COMMENT '电话',
  `address` varchar(200) DEFAULT NULL COMMENT '地址',
  `picture` varchar(500) DEFAULT NULL COMMENT '用户头像',
  `inserttime` varchar(19) DEFAULT NULL COMMENT '插入时间',
  `zhuangtai` int(11) DEFAULT NULL COMMENT '登入状态1为可以登入，2为不可登入',
  `kaiqidangan` int(11) DEFAULT NULL COMMENT '学霸档案是否开启 1为开启，0为不开启',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for studentaddition
-- ----------------------------
DROP TABLE IF EXISTS `studentaddition`;
CREATE TABLE `studentaddition` (
  `id` varchar(32) NOT NULL,
  `stu_id` varchar(32) DEFAULT NULL,
  `kechengsheji` text,
  `shangkejilu` text,
  `tiku` text,
  `inserttime` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for studentbanji
-- ----------------------------
DROP TABLE IF EXISTS `studentbanji`;
CREATE TABLE `studentbanji` (
  `id` varchar(32) NOT NULL,
  `stu_id` varchar(32) DEFAULT NULL,
  `banji_id` varchar(32) DEFAULT NULL,
  `banjiname` varchar(200) DEFAULT NULL COMMENT '培优课堂同步班级名称',
  `teaid` varchar(32) DEFAULT NULL COMMENT '教师编号',
  `teaname` varchar(50) DEFAULT NULL COMMENT '教师姓名',
  `inserttime` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for studentkecheng
-- ----------------------------
DROP TABLE IF EXISTS `studentkecheng`;
CREATE TABLE `studentkecheng` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `grade` varchar(50) DEFAULT NULL COMMENT '年级',
  `subject` varchar(50) DEFAULT NULL COMMENT '科目',
  `catalog` varchar(500) DEFAULT NULL COMMENT '目录',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  `stuid` varchar(32) DEFAULT NULL COMMENT '学生主键',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='学生的课程';

-- ----------------------------
-- Table structure for teacher
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `name` varchar(50) DEFAULT NULL COMMENT '姓名',
  `sex` varchar(2) DEFAULT NULL COMMENT '性别',
  `tealevel` varchar(50) DEFAULT NULL COMMENT '教师级别',
  `grade` varchar(32) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL COMMENT '教学科目',
  `jiaoling` int(20) DEFAULT NULL COMMENT '实际教龄',
  `headpath` varchar(200) DEFAULT NULL,
  `picpath` varchar(200) DEFAULT NULL COMMENT '头像路径',
  `videopath` varchar(200) DEFAULT NULL COMMENT '视频路径',
  `phone` varchar(50) DEFAULT NULL COMMENT '电话',
  `inserttime` varchar(20) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL COMMENT '地址',
  `birthday` varchar(50) DEFAULT NULL COMMENT '出生年月',
  `techang` varchar(255) DEFAULT NULL COMMENT '班次类型',
  `pingfen` varchar(20) DEFAULT NULL COMMENT '教学评分',
  `stuNumber` varchar(32) DEFAULT NULL,
  `keshiNumber` varchar(32) DEFAULT NULL,
  `torder` int(11) DEFAULT NULL COMMENT '教师排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for teacheraddition
-- ----------------------------
DROP TABLE IF EXISTS `teacheraddition`;
CREATE TABLE `teacheraddition` (
  `id` varchar(32) NOT NULL,
  `tea_id` varchar(32) DEFAULT NULL COMMENT '教师表主键',
  `teainfo` text COMMENT '个人证书',
  `teachengguo` text,
  `teaweigui` text,
  `inserttime` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for teacherassess
-- ----------------------------
DROP TABLE IF EXISTS `teacherassess`;
CREATE TABLE `teacherassess` (
  `id` varchar(32) NOT NULL,
  `tea_id` varchar(255) DEFAULT NULL COMMENT '教师主键',
  `stuName` varchar(255) DEFAULT NULL COMMENT '学生姓名',
  `school` varchar(255) DEFAULT NULL COMMENT '学校姓名 ',
  `comment` varchar(255) DEFAULT NULL COMMENT '评语',
  `picPath` varchar(255) DEFAULT NULL,
  `videoPath` varchar(255) DEFAULT NULL,
  `inserttime` varchar(20) DEFAULT NULL COMMENT '教师评估',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` varchar(32) NOT NULL,
  `userId` varchar(50) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `sex` varchar(2) DEFAULT NULL,
  `age` varchar(3) DEFAULT NULL,
  `birthday` varchar(20) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `inserttime` varchar(20) DEFAULT NULL,
  `usertype` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for xinwen
-- ----------------------------
DROP TABLE IF EXISTS `xinwen`;
CREATE TABLE `xinwen` (
  `id` varchar(32) NOT NULL,
  `medtesttype` varchar(255) DEFAULT NULL COMMENT '新闻类别',
  `title` varchar(500) DEFAULT NULL COMMENT '标题',
  `neirong` text COMMENT '内容',
  `riqi` varchar(20) DEFAULT NULL COMMENT '日期',
  `hits` int(11) unsigned zerofill DEFAULT NULL COMMENT '点击量',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  `school` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for xueba
-- ----------------------------
DROP TABLE IF EXISTS `xueba`;
CREATE TABLE `xueba` (
  `id` varchar(255) NOT NULL COMMENT '主键',
  `picpath` varchar(200) DEFAULT NULL COMMENT '图片路径',
  `porder` int(255) DEFAULT NULL COMMENT '显示顺序',
  `flag` varchar(2) DEFAULT NULL COMMENT '是否显示',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='今日学霸';

-- ----------------------------
-- Table structure for xuebabaoban
-- ----------------------------
DROP TABLE IF EXISTS `xuebabaoban`;
CREATE TABLE `xuebabaoban` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `stuid` varchar(32) DEFAULT NULL COMMENT '学生主键',
  `kechengid` varchar(32) DEFAULT NULL COMMENT '课程编号',
  `kechengname` varchar(200) DEFAULT NULL COMMENT '课程名称',
  `teaid` varchar(32) DEFAULT NULL COMMENT '教师编号',
  `teaname` varchar(50) DEFAULT NULL COMMENT '教师名称',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='学霸档案培优科目';

-- ----------------------------
-- Table structure for zhangjie
-- ----------------------------
DROP TABLE IF EXISTS `zhangjie`;
CREATE TABLE `zhangjie` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `grade` varchar(50) DEFAULT NULL COMMENT '年级',
  `subject` varchar(50) DEFAULT NULL COMMENT '科目',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  `zorder` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='章节';

-- ----------------------------
-- Table structure for zuoye
-- ----------------------------
DROP TABLE IF EXISTS `zuoye`;
CREATE TABLE `zuoye` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `name` varchar(500) DEFAULT NULL COMMENT '作业名称',
  `subject` varchar(50) DEFAULT NULL COMMENT '科目',
  `originalpath` varchar(200) DEFAULT NULL COMMENT '原始作业路径',
  `finishpath` varchar(200) DEFAULT NULL COMMENT '完成作业路径',
  `inserttime` varchar(20) DEFAULT NULL COMMENT '插入时间',
  `stuid` varchar(32) DEFAULT NULL COMMENT '学生主键',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='培优作业';
