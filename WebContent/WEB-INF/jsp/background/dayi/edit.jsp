<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<%
request.setCharacterEncoding("UTF-8");
String htmlData1 = request.getParameter("info") != null ? request.getParameter("info") : "";
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>答疑编辑页面</title>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/fbackground.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
<script>
	KindEditor.ready(function(K) {
		var editor1 = K.create('textarea[name="info"]', {
			cssPath : '<%=kePath %>/plugins/code/prettify.css',
			uploadJson : '<%=kePath %>/jsp/upload_json.jsp',
			fileManagerJson : '<%=kePath %>/jsp/file_manager_json.jsp',
			allowFileManager : true,
			afterCreate : function() {
				var self = this;
				K.ctrl(document, 13, function() {
					self.sync();
					document.forms['example'].submit();
				});
				K.ctrl(self.edit.doc, 13, function() {
					self.sync();
					document.forms['example'].submit();
				});
			}
		});
		prettyPrint();
		
	});
	$(document).ready(function(){
		//上传图片
		$("#picFile").change(function(){
			var picFile = $("#picFile").val();
			if(picFile == ""){
				art.dialog.alert("请选择文件!");
				return;
			}
			else{
				var strFilter = ".jpeg|.gif|.jpg|.png|";
				if(picFile.indexOf(".") > -1){
					var p = picFile.lastIndexOf(".");
					var strPostfix = picFile.substring(p,picFile.length) + '|';
					strPostfix = strPostfix.toLowerCase();
					if(strFilter.indexOf(strPostfix) > -1){
						$.ajaxFileUpload({
							url: '<%=pre%>/bdayi/picupload<%=sufStr%>', 
							type: 'post',
							fileElementId: 'picFile',
							dataType:'json', //返回值类型，一般设置为json、application/json
							success: function(data, status){ 
					            	if(data.ret == 1){
					            		$("#picpreview").attr("src","<%=uploadPath%>/" + data.picpath);
					            		$("#picpath").val(data.picpath);
					            	}
					            	else{
					            		art.dialog.alert("上传失败！");
					            	}
					            },
					            error: function(data, status, e){ 
					            	art.dialog.tips('<span style="color:#f00">上传图片失败，请重传！</span>', 1);
					            }
						});
					}
					}else{
						art.dialog.alert("请选择：jpeg、gif、jpg、png格式的图片！");
					}
			}
		});
		
		//年级改变时候
		$(".radio_grade_subject").change(function(){
			 var grade = $("input[name='grade']:checked").val();
			 var subject = $("input[name='subject']:checked").val();
			 var h_chapter= $("#h_chapter").val();
			 if(typeof(subject) == 'undefined' ){
				 subject = '';
			 }
			 if(typeof(grade) == 'undefined'){
				 grade= '';
			 }
			 $.ajax({
					type:"post",
					url:"<%=pre%>/bdayi/checkzhangjie<%=sufStr%>",
					data:{grade:grade,subject:subject},
					dataType:"json",
					success:function(data,status){
						if(data.ret == '1'){
							var tr_zhangjie = $("#tr_zhangjie");
							tr_zhangjie.text("");
							var strHtml ='<td>章节：</td><td>';
							for(var i = 0; i < data.jos.length; i++){
								strHtml +='<input type="radio" value="'+data.jos[i].name+'" name="chapter"';
								if(data.jos[i].name == h_chapter){
									strHtml +='checked="checked"';
								}
								strHtml +=' />';
								strHtml +=data.jos[i].name;
							}
							strHtml +='</td>';
							tr_zhangjie.html(strHtml);
							tr_zhangjie.css('display','');
						}
					},
					error : function(xhr, textStatus, errorThrown) {
						art.dialog.alert('<span style="color:#f00">查找章节失败！</span>', 1);
						return false;
					}
				});
		});
		
	});
</script>
</head>
<body>
<div class="content">
		<div class="content_title">
			答疑编辑页面
		</div>
		<form id="formBack" action="<%=pre%>/bdayi/index<%=sufStr%>"></form>
		<form id="example" name="example" action="<%=pre%>/bdayi/update<%=sufStr%>" method="post" enctype="multipart/form-data" >
		<input type="hidden" id="id" name="id" value="${bean.id }" />
		<input type= "hidden" id="h_chapter" value="${bean.chapter }" />
		<div>
			<table class="tbl001">
					<tr>
						<td>年级：</td>
						<td>
							<c:forEach items="${listbt }" var="obj">
								<c:if test="${obj.botype == '年级' }">
									<input type="radio" name="grade" value="${obj.name }" class="radio_grade_subject"
										<c:if test="${obj.name == bean.grade }">checked="checked"</c:if>>${obj.name }
								</c:if>
							</c:forEach>
						</td>
					</tr>
					<tr>
						<td>科目:</td>
						<td>
							<c:forEach items="${listbt }" var="obj">
								<c:if test="${obj.botype == '科目' }">
									<input type="radio" name="subject" value="${obj.name }" class="radio_grade_subject"
									<c:if test="${obj.name == bean.subject }">checked="checked"</c:if>>${obj.name }
								</c:if>
							</c:forEach>
						</td>
					</tr>
					<tr id="tr_zhangjie">
						<td>章节：</td>
						<td>
							<c:forEach items="${listzj }" var="obj">
								<input type="radio" name="chapter" value="${obj.name }" 
								<c:if test="${obj.name == bean.chapter }">checked="checked"</c:if>>${obj.name }
							</c:forEach>
						</td>
					</tr>
					<tr>
						<td>难易度:</td>
						<td>
							<c:forEach items="${listbt }" var="obj">
								<c:if test="${obj.botype == '难易度' }">
									<input type="radio" name="nanyidu" value="${obj.name }"
									<c:if test="${obj.name == bean.nanyidu }">checked="checked"</c:if>>${obj.name }
								</c:if>
							</c:forEach>
						</td>
					</tr>
					<tr>
						<td>上课教师:</td>
						<td>
							<c:forEach items="${list}" var="obj">
								<input type="radio" name="tea_id" value="${obj.id};${obj.name}" <c:if test="${bean.tea_id == obj.id }">checked="checked"</c:if>>${obj.name }
							</c:forEach>
						</td>
					</tr>
					<tr>
						<td>答疑视频名称：</td>
						<td>
							<input type="text" id = "name" name="name" value="${bean.name }" style="width:500px"/>
						</td>
					</tr>
					<tr>
						<td>腾讯视频vid：</td>
						<td>
							<input type="text" id = "videopath" name="videopath" value="${bean.videopath }" style="width:500px"/>
						</td>
					</tr>
					<tr>
						<td>播放次数：</td>
						<td>
							<input type="text" id = "playnum" name="playnum" value="${bean.playnum }"/>
						</td>
					</tr>
					<tr>
						<td>视频展示小图：</td>
						<td>
							<input type="file" id="picFile" name="picFile"/>
							<img id="picpreview" src="<%=uploadPath%>/${bean.picpath}" />
							<span style="color:red">请选择175*100(px)大小的图像</span>
							<input type="hidden" id="picpath" name="picpath" value="${bean.picpath }"/>
						</td>
					</tr>
					<tr>
						<td>答疑详情：</td>
						<td>
							<%=htmlData1%>
							<textarea id="info" name="info" style="width:800px;height:250px;">
								<%=htmlspecialchars(htmlData1)%>${bean.info }
							</textarea>
						</td>
					</tr>
				</table>
		</div>
		<div>
			<input type="submit" value="保存" class="button" id="saveButton" /> 
			<input type="button" value="返回" class="button" id="back" />
			<span style="color:red">
				<c:if test="${tip == 0 }">添加失败！</c:if>
				<c:if test="${tip == 1 }">添加成功！</c:if>
				<c:if test="${tip == 2 }">编辑失败！</c:if>
				<c:if test="${tip == 3 }">编辑成功！</c:if>
			</span>
		</div>
		</form>
	</div>
</body>
</html>
<%!
private String htmlspecialchars(String str) {
	str = str.replaceAll("&", "&amp;");
	str = str.replaceAll("<", "&lt;");
	str = str.replaceAll(">", "&gt;");
	str = str.replaceAll("\"", "&quot;");
	return str;
}
%>