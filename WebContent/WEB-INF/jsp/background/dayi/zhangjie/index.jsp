<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>章节首页</title>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
<link rel="stylesheet"  type="text/css" href="<%=cssPath %>/fbackground.css" />
<script>
	$(document).ready(function(){
		//添加章节
		$("#btn_add_zhangjie").click(function(){
			art.dialog.open("<%=pre%>/zhangjie/add<%=sufStr%>",{
				title:"添加章节",
				width:800,
				height:400,
				cancel: true,
				ok:function(){
					var iframe = this.iframe.contentWindow;
					if (!iframe.document.body) {
			        	alert('iframe还没加载完毕呢')
			        	return false;
			        };
					var grade = iframe.document.getElementById('grade').value;
					var subject = iframe.document.getElementById('subject').value;
					var name = iframe.document.getElementById('name').value;
					var zorder = iframe.document.getElementById('zorder').value;
					$.ajax({
						type:"post",
						url:"<%=pre%>/zhangjie/insert<%=sufStr%>",
						data:{grade:grade,subject:subject,name:name,zorder:zorder},
						dataType:"json",
						success:function(data,status){
							if(data.ret == '1'){
								var timer;
								art.dialog({
									width:150,
									height:80,
									content : '<span style="color:#f00">添加成功!</span>',
									init : function() {
										var that = this, i = 1;
										var fn = function() {
											!i && that.close();
											i--;
										};
										timer = setInterval(fn, 1000);
										fn();
									},
									close : function() {
										clearInterval(timer);
										location.reload();
									}
									
								}).show();			
							}
							else{
								art.dialog.alert("添加失败！");
								return false;
							}
						},
						error : function(xhr, textStatus, errorThrown) {
							art.dialog.alert('<span style="color:#f00">添加失败！</span>', 1);
							return false;
						}
					});
				}
			});
		});
	});
	
	function editPage(url){
		art.dialog.open(url,{
			title:"修改章节",
			width:800,
			height:300,
			cancel: true,
			ok:function(){
				var iframe = this.iframe.contentWindow;
				if (!iframe.document.body) {
		        	alert('iframe还没加载完毕呢')
		        	return false;
		        };
		        var id = iframe.document.getElementById('id').value;
				var grade = iframe.document.getElementById('grade').value;
				var subject = iframe.document.getElementById('subject').value;
				var old_chapter = iframe.document.getElementById('old_chapter').value;
				var name = iframe.document.getElementById('name').value;
				var zorder = iframe.document.getElementById('zorder').value;
				$.ajax({
					type:"post",
					url:"<%=pre%>/zhangjie/update<%=sufStr%>",
					data:{id:id,grade:grade,subject:subject,name:name,zorder:zorder,old_chapter:old_chapter},
					dataType:"json",
					success:function(data,status){
						if(data.ret == '1'){
							var timer;
							art.dialog({
								width:150,
								height:80,
								content : '<span style="color:#f00">编辑成功!</span>',
								init : function() {
									var that = this, i = 1;
									var fn = function() {
										!i && that.close();
										i--;
									};
									timer = setInterval(fn, 1000);
									fn();
								},
								close : function() {
									clearInterval(timer);
									location.reload();
								}
								
							}).show();			
						}
						else{
							art.dialog.alert("编辑失败！");
							return false;
						}
					},
					error : function(xhr, textStatus, errorThrown) {
						art.dialog.alert('<span style="color:#f00">编辑失败！</span>', 1);
						return false;
					}
				});
			}
		});
	}
</script>
</head>
<body>
	<div class="content">
		<div>
			<form name="fom" id="fom" method="post"></form>
			<input type="button" class="btn_add" id="btn_add_zhangjie" value="添加章节" onclick="link();"/>
		</div>
		<div>
			<table class="tbl002" >
				<tr>
					<td>序号</td>
					<td>年级</td>
					<td>科目</td>
					<td>章节名称</td>
					<td>顺序</td>
					<td>插入时间</td>
					<td>操作</td>
					<td>序号</td>
				</tr>
				<c:forEach items="${pager.pageList}" var="obj" varStatus="stauts">
					<tr bgcolor="#FFFFFF" align="center">
						<td>${stauts.index +1}</td>
						<td>${obj.grade}</td>
						<td>${obj.subject}</td>
						<td>${obj.name }</td>
						<td>${obj.zorder }</td>
						<td>${obj.inserttime }</td>
						<td>
							<a href="javascript:void(0)" onclick="javascript:editPage('<%=pre%>/zhangjie/edit<%=sufStr%>?id=${obj.id }')">编辑详情</a> | 
							<a href="javascript:deleteConfirm('${obj.id }','<%=pre%>/zhangjie/delete<%=sufStr%>?id=${obj.id}');">删除</a>
						</td>
						<td>${stauts.index +1}</td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>
</body>
</html>