<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>添加章节</title>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/fbackground.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
<script>
$(document).ready(function(){
	$(".radio_grade").change(function(){
		 var grade = $("input[name='grade']:checked").val();
		$("#grade").val(grade);
	});
	
	$(".radio_subject").change(function(){
		 var subject = $("input[name='subject']:checked").val();
		$("#subject").val(subject);
	});
	
});
</script>
</head>
<body>
	<div class="content">
		<div style="margin-top:10px;">
			<input type="hidden" id="grade" />
			<input type="hidden" id="subject" />
			<table class="tbl001">
			<tr>
				<td>年级：</td>
				<td>
					<c:forEach items="${listbt }" var="obj">
						<c:if test="${obj.botype == '年级' }">
							<input type="radio" name="grade" value="${obj.name}"  class="radio_grade"/>${obj.name }
						</c:if>
					</c:forEach>
				</td>
			</tr>
			<tr>
				<td>科目：</td>
				<td>
					<c:forEach items="${listbt }" var="obj">
						<c:if test="${obj.botype == '科目' }"> 
							<input type="radio" name="subject" value="${obj.name}" class="radio_subject"  />${obj.name }
						</c:if>
					</c:forEach>
				</td>
			</tr>
			<tr>
				<td>名称：</td>
				<td>
					<input  type="text" id="name" style="width:300px"/>
				</td>
			</tr>
			<tr>
				<td>顺序：</td>
				<td>
					<input  type="text" id="zorder"/>
				</td>
			</tr>
			</table>
		</div>
	</div>
</body>
</html>