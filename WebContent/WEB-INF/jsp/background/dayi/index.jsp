<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>答疑模块后台</title>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
<link rel="stylesheet"  type="text/css" href="<%=cssPath %>/fbackground.css" />
<script>
	function link() {
		document.getElementById("fom").action = "<%=pre%>/bdayi/add<%=sufStr%>";
		document.getElementById("fom").submit();
	}
</script>
</head>
<body>
	<div class="content">
		<div>
			<form name="fom" id="fom" method="post"></form>
			<input type="button" class="btn_add" id="btn_add_dayi" value="添加答疑" onclick="link();"/>
		</div>
		<div>
			<table class="tbl002" >
				<tr>
					<td>序号</td>
					<td>名称</td>
					<td>科目</td>
					<td>年级</td>
					<td>老师姓名</td>
					<td>插入时间</td>
					<td>操作</td>
					<td>序号</td>
				</tr>
				<c:forEach items="${pager.pageList}" var="obj" varStatus="stauts">
					<tr bgcolor="#FFFFFF" align="center">
						<td>${stauts.index +1}</td>
						<td>${obj.name}</td>
						<td>${obj.subject}</td>
						<td>${obj.grade }</td>
						<td>${obj.tea_name }</td>
						<td>${obj.inserttime }</td>
						<td>
							<a href="<%=pre%>/bdayi/edit<%=sufStr%>?id=${obj.id }">编辑详情</a> | 
							<a href="javascript:deleteConfirm('${obj.id }','<%=pre%>/bdayi/delete<%=sufStr%>?id=${obj.id}');">删除</a>
						</td>
						<td>${stauts.index +1}</td>
					</tr>
				</c:forEach>
			</table>
		</div>
		
		<div><!-- 分页开始 -->
			<div class="manu">
				<c:if test="${pager.totalPage >0}">
					<!-- prev -->
					<c:choose>
						<c:when test="${pager.curtPage == 1 }">
							<span class="disabled">&lt;Prev</span>
						</c:when>
						<c:otherwise>
							<a href="<%=pre %>/bdayi/index<%=sufStr%>?curtPage=${pager.curtPage - 1}">&lt; Prev </a>
						</c:otherwise>
					</c:choose>
					
					<!-- content -->
					<c:if test="${pager.totalPage < 7 }">
						<c:forEach var = "item" begin="1" end = "${pager.totalPage }">
							<c:choose>
								<c:when test="${pager.curtPage == item }">
								  	<span class="current">${item }</span>
								</c:when>
								<c:otherwise>
									<a href="<%=pre %>/bdayi/index<%=sufStr%>?curtPage=${item}">${item }</a>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:if>
					<c:if test="${pager.totalPage >= 7 }">
						<c:if test="${pager.curtPage <= 4 }">
							<c:forEach var = "item" begin="1" end = "5">
								<c:choose>
									<c:when test="${pager.curtPage == item }">
									  	<span class="current">${item }</span>
									</c:when>
									<c:otherwise>
										<a href="<%=pre %>/bdayi/index<%=sufStr%>?curtPage=${item}">${item }</a>
									</c:otherwise>
								</c:choose>
							</c:forEach>
							...
							<a href="<%=pre %>/bdayi/index<%=sufStr%>?curtPage=${pager.totalPage}">${pager.totalPage}</a>
						</c:if>
						<c:if test="${pager.curtPage > 4 }">
							<a href="<%=pre %>/bdayi/index<%=sufStr%>?curtPage=1">1</a>
							...
							<c:choose>
									
									<c:when test="${pager.curtPage < pager.totalPage - 3 }">
										<c:forEach var = "item" begin="${pager.curtPage - 2 }" end = "${pager.curtPage + 2 }">
											<c:choose>
												<c:when test="${pager.curtPage == item }">
												  	<span class="current">${item }</span>
												</c:when>
												<c:otherwise>
													<a href="<%=pre %>/bdayi/index<%=sufStr%>?curtPage=${item}">${item }</a>
												</c:otherwise>
											</c:choose>
										</c:forEach>
										...
										<a href="<%=pre %>/bdayi/index<%=sufStr%>?curtPage=${pager.totalPage}">${pager.totalPage}</a>
									</c:when>
									<c:otherwise>
										<c:forEach var = "item" begin="${pager.totalPage - 4 }" end = "${pager.totalPage }">
											<c:choose>
												<c:when test="${pager.curtPage == item }">
												  	<span class="current">${item }</span>
												</c:when>
												<c:otherwise>
													<a href="<%=pre %>/bdayi/index<%=sufStr%>?curtPage=${item}">${item }</a>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</c:otherwise>
								</c:choose>
						</c:if>
					</c:if>
					<!-- next -->
					<c:choose>
						<c:when test="${pager.curtPage == pager.totalPage }">
							<span class="disabled">Next &gt;</span>
						</c:when>
						<c:otherwise>
							<a href="<%=pre %>/bdayi/index<%=sufStr%>?curtPage=${pager.curtPage + 1}">Next  &gt; </a>
						</c:otherwise>
					</c:choose>	
				</c:if>
			</div>
		</div><!-- 分页结束 -->
	</div>
</body>
</html>