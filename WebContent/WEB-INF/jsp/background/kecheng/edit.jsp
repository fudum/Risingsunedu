<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<%
request.setCharacterEncoding("UTF-8");
String htmlData = request.getParameter("catalog") != null ? request.getParameter("catalog") : "";
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>课程编辑页面</title>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/fbackground.css" />
<script type="text/javascript" src="<%=jsPath%>/baddedit.js"></script>
<script>
$(document).ready(function(){
	//保存班级信息
	$("#saveButton").click(function() {
		var id = $("#id").val();
		var name = $("#name").val();
		var grade = '';
		$("input[name='grade']:checked").each(function(){
			grade +=$(this).val();
			grade +=',';
		});
		grade = grade.substring(0,grade.length-1);
		var subject = $("input[name='subject']:checked").val();
		var banci = '';
		$("input[name='banci']:checked").each(function(){
			banci +=$(this).val();
			banci +=',';
		});
		banci = banci.substring(0,banci.length-1);
		var bjtype = '';
		$("input[name='bjtype']:checked").each(function(){
			bjtype +=$(this).val();
			bjtype +=',';
		});
		bjtype = bjtype.substring(0,bjtype.length-1);
		var timeframe = $("input[name='timeframe']:checked").val();
		var lessonday = $("input[name='lessonday']:checked").val();
		var startdate = $("#startdate").val();
		var enddate = $("#enddate").val();
		var starttime = $("#starttime").val();
		var endtime = $("#endtime").val();
		var tea_id = $("input[name='tea_id']:checked").val();
		var address = $("#address").val();
		var year = $("#year").val();
		var playnum = $("#playnum").val();
		var price = $("#price").val();
		
		//ajax提交
		$.ajax({
			url : "<%=pre%>/b_kecheng/update<%=sufStr%>",
			type : 'post',
			dataType : 'html',
			data : 'grade='+ grade + '&subject=' + subject + '&banci=' +banci
			+ '&bjtype=' + bjtype + '&timeframe=' + timeframe + '&lessonday=' + lessonday 
			+ '&startdate=' + startdate + '&enddate=' + enddate + '&starttime=' + starttime
			+ '&endtime=' + endtime + '&tea_id=' + tea_id + '&address=' + address
			+ '&id=' + id + '&year=' + year +'&playnum=' +playnum +'&name=' +name
			+ '&price=' + price,
			success : function(data, status) {
				if (status == 'success') {
					if (data == '1') {
						var timer;
						art.dialog({
							content : '<span style="color:#f00">编辑成功！</span>',
							init : function() {
								var that = this, i = 1;
								var fn = function() {
									!i && that.close();
									i--;
								};
								timer = setInterval(fn, 1000);
								fn();
							},
							close : function() {
								clearInterval(timer);
								
							}
						}).show();	
					} else {
						art.dialog.tips('<span style="color:#f00">编辑失败！</span>', 1);
					}
				}
			},
			error : function(xhr, textStatus,
					errorThrown) {
				art.dialog.tips('<span style="color:#f00">编辑失败！</span>', 1);
			}
		});
	});
	
	//添加讲次
	$("#add_jiangci").click(function(){
		var kecheng_id = $("#id").val();
		art.dialog.open("<%=pre%>/b_kechengjiangci/add<%=sufStr%>?kecheng_id=" + kecheng_id,{
			title:"添加讲次",
			width:500,
			height:200,
			cancel: true,
			ok:function(){
				var iframe = this.iframe.contentWindow;
				if (!iframe.document.body) {
		        	alert('iframe还没加载完毕呢')
		        	return false;
		        };
				var kecheng_id = $("#id").val();
				var name = iframe.document.getElementById('name').value;
				var jorder = iframe.document.getElementById('jorder').value;
				$.ajax({
					type:"post",
					url:"<%=pre%>/b_kechengjiangci/insert<%=sufStr%>",
					data:{kecheng_id:kecheng_id,name:name,jorder:jorder},
					dataType:"json",
					success:function(data,status){
						if(data.ret == '1'){
							var timer;
							art.dialog({
								width:150,
								height:80,
								content : '<span style="color:#f00">添加成功!</span>',
								init : function() {
									var that = this, i = 1;
									var fn = function() {
										!i && that.close();
										i--;
									};
									timer = setInterval(fn, 1000);
									fn();
								},
								close : function() {
									clearInterval(timer);
									location.reload();
								}
							}).show();			
						}
						else{
							art.dialog.alert("添加失败！");
							return false;
						}
					},
					error : function(xhr, textStatus, errorThrown) {
						art.dialog.alert('<span style="color:#f00">添加失败！</span>', 1);
						return false;
					}
				});
			}
		});
	});
	
	//添加视频
	$("#add_video").click(function(){
		var kecheng_id = $("#id").val();
		art.dialog.open("<%=pre%>/b_kechengjieci/add<%=sufStr%>?kecheng_id=" + kecheng_id,{
			title:"添加视频",
			width:800,
			height:300,
			cancel: true,
			ok:function(){
				var iframe = this.iframe.contentWindow;
				if (!iframe.document.body) {
		        	alert('iframe还没加载完毕呢')
		        	return false;
		        };
				var jiangci_name = iframe.document.getElementById('jiangci').value;
				var jieci_name = iframe.document.getElementById('jieci').value;
				var videopath = iframe.document.getElementById('vidopath').value;
				$.ajax({
					type:"post",
					url:"<%=pre%>/b_kechengjieci/insert<%=sufStr%>",
					data:{jiangci_name:jiangci_name,jieci_name:jieci_name,videopath:videopath},
					dataType:"json",
					success:function(data,status){
						if(data.ret == '1'){
							var timer;
							art.dialog({
								width:150,
								height:80,
								content : '<span style="color:#f00">添加成功!</span>',
								init : function() {
									var that = this, i = 1;
									var fn = function() {
										!i && that.close();
										i--;
									};
									timer = setInterval(fn, 1000);
									fn();
								},
								close : function() {
									clearInterval(timer);
									location.reload();
								}
								
							}).show();			
						}
						else{
							art.dialog.alert("添加失败！");
							return false;
						}
					},
					error : function(xhr, textStatus, errorThrown) {
						art.dialog.alert('<span style="color:#f00">添加失败！</span>', 1);
						return false;
					}
				});
			}
		});
	});	
	
	
});


KindEditor.ready(function(K) {
	var editor = K.create('textarea[name="catalog"]', {
		cssPath : '<%=kePath %>/plugins/code/prettify.css',
		uploadJson : '<%=kePath %>/jsp/upload_json.jsp',
		fileManagerJson : '<%=kePath %>/jsp/file_manager_json.jsp',
		allowFileManager : true,
		afterCreate : function() {
			var self = this;
			K.ctrl(document, 13, function() {
				self.sync();
				document.forms['example'].submit();
			});
			K.ctrl(self.edit.doc, 13, function() {
				self.sync();
				document.forms['example'].submit();
			});
		}
	});
	
	//课程目录提交保存
	$("#btn_catalog").click(function(){
		var catalog = editor.text();
		var id = $("#id").val();
		$.ajax({
			type:"post",
			url:"<%=pre%>/b_kecheng/updateCatalog<%=sufStr%>",
			data:{id:id,catalog:catalog},
			dataType:"json",
			success:function(data,status){
				if(data == 1){
					art.dialog.tips("编辑成功！");
				}
				else{
					art.dialog.tips("编辑失败！");
				}
			},
			error : function(xhr, textStatus, errorThrown) {
				art.dialog.alert('<span style="color:#f00">编辑失败！</span>', 1);
				return false;
			}
		});
	});
	prettyPrint();
});

/**编辑页面**/
function editPage(url){
	art.dialog.open(url,{
		title:"编辑讲次信息",
		width:500,
		height:200,
		cancel: true,
		ok:function(){
			var iframe = this.iframe.contentWindow;
			if (!iframe.document.body) {
	        	alert('iframe还没加载完毕呢')
	        	return false;
	        };
			var name = iframe.document.getElementById('name').value;
			var jorder = iframe.document.getElementById('jorder').value;
			var id = iframe.document.getElementById('id').value;
			$.ajax({
				type:"post",
				url:"<%=pre%>/b_kechengjiangci/update<%=sufStr%>",
				data:{id:id,name:name,jorder:jorder},
				dataType:"json",
				success:function(data,status){
					if(data.ret == '1'){
						var timer;
						art.dialog({
							width:150,
							height:80,
							content : '<span style="color:#f00">编辑成功!</span>',
							init : function() {
								var that = this, i = 1;
								var fn = function() {
									!i && that.close();
									i--;
								};
								timer = setInterval(fn, 1000);
								fn();
							},
							close : function() {
								clearInterval(timer);
								location.reload();
							}
							
						}).show();			
					}
					else{
						art.dialog.alert("编辑失败！");
						return false;
					}
				},
				error : function(xhr, textStatus, errorThrown) {
					art.dialog.alert('<span style="color:#f00">编辑失败！</span>', 1);
					return false;
				}
			});
		}
	});
}

//视频编辑
function editVideo(url){
	art.dialog.open(url,{
		title:"编辑视频信息",
		width:800,
		height:300,
		cancel: true,
		ok:function(){
			var iframe = this.iframe.contentWindow;
			if (!iframe.document.body) {
	        	alert('iframe还没加载完毕呢')
	        	return false;
	        };
	        var jiangci_name = iframe.document.getElementById('jiangci').value;
			var jieci_name = iframe.document.getElementById('jieci').value;
			var videopath = iframe.document.getElementById('vidopath').value;
			var id = iframe.document.getElementById('id').value;
			$.ajax({
				type:"post",
				url:"<%=pre%>/b_kechengjieci/update<%=sufStr%>",
				data:{id:id,jiangci_name:jiangci_name,jieci_name:jieci_name,videopath:videopath},
				dataType:"json",
				success:function(data,status){
					if(data.ret == '1'){
						var timer;
						art.dialog({
							width:150,
							height:80,
							content : '<span style="color:#f00">编辑成功!</span>',
							init : function() {
								var that = this, i = 1;
								var fn = function() {
									!i && that.close();
									i--;
								};
								timer = setInterval(fn, 1000);
								fn();
							},
							close : function() {
								clearInterval(timer);
								location.reload();
							}
							
						}).show();			
					}
					else{
						art.dialog.alert("编辑失败！");
						return false;
					}
				},
				error : function(xhr, textStatus, errorThrown) {
					art.dialog.alert('<span style="color:#f00">编辑失败！</span>', 1);
					return false;
				}
			});
		}
	});
}
</script>
</head>
<body>
	<div class="content">
		<div class="content_title">
			课程编辑页面
		</div>
		<div>
			<input type="hidden" value="${bean.id }" id="id" />
			<table class="tbl001">
				<tr>
					<td>名称：</td>
					<td><input type="text" id="name" name="name" value="${bean.name }" style="width:400px;" ></td>
				</tr>
				<tr>
					<td>年级：</td>
					<td>
						<c:forEach items="${listGrade }" var="obj">
							<input type="checkbox" name="grade" value="${obj.name }"
								<c:if test="${fn:contains(bean.grade, obj.name)}">checked="checked"</c:if>>${obj.name }
						</c:forEach>
					</td>
				</tr>
				<tr>
					<td>科目:</td>
					<td>
						<c:forEach items="${listSubject }" var="obj">
							<input type="radio" name="subject" value="${obj.name }" 
							<c:if test="${bean.subject == obj.name }">checked="checked"</c:if>>${obj.name }
						</c:forEach>
					</td>
				</tr>
				<tr>
					<td>班次：</td>
					<td>
						<c:forEach items="${listBanci }" var="obj">
							<input type="checkbox" name="banci" value="${obj.name }" 
							<c:if test="${fn:contains(bean.banci,obj.name)}">checked="checked"</c:if>>${obj.name }
						</c:forEach>
					</td>
				</tr>
				<tr>
					<td>班级类型：</td>
					<td>
						<c:forEach items="${listBjtype }" var="obj">
							<input type="checkbox" name="bjtype" value="${obj.name }" 
							<c:if test="${fn:contains(bean.bjtype,obj.name) }">checked="checked"</c:if>>${obj.name }
						</c:forEach>
					</td>
				</tr>
				<tr>
					<td>年份：</td>
					<td>
						<input id="year" class="Wdate" type="text" onfocus="WdatePicker({dateFmt:'yyyy'})" 
						style="width:100px" value="${bean.year}"/>
					</td>
				</tr>
				<tr>
					<td>开课日期:</td>
					<td>
						<input id="startdate" class="Wdate" type="text" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})"
						style="width: 100px" value="${bean.startdate }"/>
						-
						<input id="enddate" class="Wdate" type="text" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})"
						style="width: 100px" value="${bean.enddate }" />
					</td>
				</tr>
				<tr>
					<td>周几：</td>
					<td>
						<input type="radio" name="lessonday" value="周一" <c:if test="${bean.lessonday == '周一'}">checked="checked"</c:if>>周一
						<input type="radio" name="lessonday" value="周二" <c:if test="${bean.lessonday == '周二'}">checked="checked"</c:if>>周二
						<input type="radio" name="lessonday" value="周三" <c:if test="${bean.lessonday == '周三'}">checked="checked"</c:if>>周三
						<input type="radio" name="lessonday" value="周四" <c:if test="${bean.lessonday == '周四'}">checked="checked"</c:if>>周四
						<input type="radio" name="lessonday" value="周五" <c:if test="${bean.lessonday == '周五'}">checked="checked"</c:if>>周五
						<input type="radio" name="lessonday" value="周六" <c:if test="${bean.lessonday == '周六'}">checked="checked"</c:if>>周六
						<input type="radio" name="lessonday" value="周日" <c:if test="${bean.lessonday == '周日'}">checked="checked"</c:if>>周日
					</td>
				</tr>
				<tr>
					<td>上课时间:</td>
					<td>
						<input id="starttime" class="Wdate" type="text" onfocus="WdatePicker({dateFmt:'HH:mm'})"
						style="width: 100px" value="${bean.starttime }" />
						-
						<input id="endtime" class="Wdate" type="text" onfocus="WdatePicker({dateFmt:'HH:mm'})"
						style="width: 100px" value="${bean.endtime }"/>
					</td>
				</tr>
				<tr>
					<td>上课教师:</td>
					<td>
						<c:forEach items="${list}" var="obj">
							<input type="radio" name="tea_id" value="${obj.id};${obj.name}" 
							<c:if test="${bean.tea_id == obj.id}">checked="checked"</c:if>>${obj.name }
						</c:forEach>
					</td>
				</tr>
				<tr>
					<td>上课时段：</td>
					<td>
						<input type="radio" name="timeframe" value="上午" <c:if test="${bean.timeframe == '上午'}">checked="checked"</c:if>>上午
						<input type="radio" name="timeframe" value="下午" <c:if test="${bean.timeframe == '下午'}">checked="checked"</c:if>>下午
						<input type="radio" name="timeframe" value="晚上" <c:if test="${bean.timeframe == '晚上'}">checked="checked"</c:if>>晚上
					</td>
				</tr>
				<tr>
					<td>价格：</td>
					<td>
						<input type="text" name = "price" id="price" value="${bean.price }" style="width:100px"/> 元
					</td>
				</tr>
				<tr>
					<td>播放次数：</td>
					<td>
						<input type="text" name = "playnum" id="playnum"  value="${bean.playnum }"/>
					</td>
				</tr>
				<tr>
					<td>上课地点：</td>
					<td>
						<input type="text" name = "address" id="address" value="${bean.address }"/>
					</td>
				</tr>
				<tr>
					<td>
						<input type="button" value="保存" class="button" id="saveButton" /> 
					</td>
					<td>
						<span style="color:red">
							<c:choose>
								<c:when test="${tip == 'i1' }">插入成功！</c:when>
								<c:when test="${tip == 'i0' }">插入失败！</c:when>
								<c:when test="${tip == 'u1' }">编辑成功！</c:when>
								<c:when test="${tip == 'u0' }">编辑失败！</c:when>
							</c:choose>
						</span>
					</td>
				</tr>
			</table>
		</div>
		
		<!-- 课程目录 -->
		<div class="content_title" style="margin-top:10px;">
			课程目录
		</div>
		<div>
			<form id="example" name="example" action="<%=pre%>/b_kecheng/updateCatalog<%=sufStr%>" method="post">
				<%=htmlData%>
				<div class="teabody">
					<textarea id="catalog" name="catalog" style="width:100%;height:250px;">
						<%=htmlspecialchars(htmlData)%>${bean.catalog}
					</textarea>
				</div>
			</form>
		</div>
		<div class="teabody">
			<input type="button" value="保存" class="button" id="btn_catalog" />
		</div>
		
		<!-- 讲次 -->
		<div class="content_title" style="margin-top:10px;">
			课程讲次
		</div>
		<div>
			<input id="add_jiangci" type="button" class="btn_add" value="添加讲次" />
		</div>
		<div>
			<table class="tbl002" >
				<tr>
					<td>序号</td>
					<td>讲次名称</td>
					<td>顺序</td>
					<td>插入时间</td>
					<td>操作</td>
					<td>序号</td>
				</tr>
				<c:forEach items="${listjiangci}" var="obj1" varStatus="stauts">
					<tr bgcolor="#FFFFFF" align="center">
						<td>${stauts.index +1}</td>
						<td>${obj1.name}</td>
						<td>${obj1.jorder}</td>
						<td>${obj1.inserttime }</td>
						<td>
							<a href="javascript:void(0)" onclick="javascript:editPage('<%=pre%>/b_kechengjiangci/edit<%=sufStr%>?id=${obj1.id }')">编辑详情</a> | 
							<a href="javascript:deleteConfirm('${obj1.id }','<%=pre%>/b_kechengjiangci/delete<%=sufStr%>?id=${obj1.id}');">删除</a>
						</td>
						<td>${stauts.index +1}</td>
					</tr>
				</c:forEach>
			</table>
		</div>
		
		<!-- 视频 -->
		<div class="content_title" style="margin-top:10px;">
			课程视频
		</div>
		<div>
			<input id="add_video" type="button" class="btn_add" value="添加视频" />
		</div>
		<div>
			<table class="tbl002">
				<tr>
					<td>序号</td>
					<td>讲次</td>
					<td>节次</td>
					<td>插入时间</td>
					<td>预览播放</td>
					<td>操作</td>
					<td>序号</td>
				</tr>
				<c:forEach items="${listjieci}" var="obj" varStatus="status">
					<tr>
						<td>${status.index +1 }</td>
						<td>${obj.jiangci_name}</td>
						<td>${obj.jieci_name}</td>
						<td>${obj.inserttime }</td>
						<td>
							<a href="javascript:void(0)" onclick="javascript:videodisplay('${obj.videopath}')">视频播放</a>
						</td>
						<td>
							<a href="javascript:void(0)" onclick="javascript:editVideo('<%=pre%>/b_kechengjieci/edit<%=sufStr%>?id=${obj.id }&kecheng_id=${bean.id }')">编辑详情</a> |
							<a href="javascript:deleteConfirm('${obj.id }','<%=pre%>/b_kechengjieci/delete<%=sufStr%>?id=${obj.id}');">删除</a>
						</td>
						<td>${status.index +1 }</td>
					</tr>
				</c:forEach>
			</table>
		</div>
		
	</div>
</body>
</html>
<%!
private String htmlspecialchars(String str) {
	str = str.replaceAll("&", "&amp;");
	str = str.replaceAll("<", "&lt;");
	str = str.replaceAll(">", "&gt;");
	str = str.replaceAll("\"", "&quot;");
	return str;
}
%>