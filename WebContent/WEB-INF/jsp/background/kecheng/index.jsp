<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>课程管理</title>
<link rel="stylesheet" type="text/css" href="<%=cssPath %>/fbackground.css"/>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css"  />
<script>
	function link() {
		document.getElementById("fom").action = "<%=pre%>/b_kecheng/add<%=sufStr%>";
		document.getElementById("fom").submit();
	}
</script>
</head>
<body>
	<div class="content">
		<div>
			<form name="fom" id="fom" method="post" action=""></form>
		</div>
		<div>
			<input type="button" class="btn_add" value="添加课程" onclick="link();" />
		</div>
		<div>
			<table class="tbl002">
				<tr>
					<td>序号</td>		
					<td>名称</td>		
					<td>年级</td>
					<td>科目</td>
					<td>班次</td>
					<td>班级类型</td>
					<td>教师</td>
					<td>上课时间</td>
					<td>插入时间</td>
					<td>操作</td>
					<td>序号</td>
				</tr>
				<c:forEach items="${pager.pageList}" var="obj" varStatus="stauts">
					<tr>
						<td>${stauts.index +1}</td>
						<td>${obj.name}</td>
						<td>${obj.grade}</td>
						<td>${obj.subject}</td>
						<td>${obj.banci}</td>
						<td>${obj.bjtype}</td>
						<td>${obj.tea_name}</td>
						<td>${obj.lessonday}${obj.starttime }-${obj.endtime }</td>
						<td>${obj.inserttime }</td>
						<td>
							<a href="<%=pre%>/b_kecheng/edit<%=sufStr%>?id=${obj.id }">编辑详情</a> | 
							<a href="javascript:deleteConfirm('${obj.id }','<%=pre%>/b_kecheng/delete<%=sufStr%>?id=${obj.id}');">删除</a>
						</td>
						<td>${stauts.index +1}</td>
					</tr>
				</c:forEach>
			</table>
		</div>
		
		<!-- 分页条 -->
		<div>
			<div class="manu">
				<c:if test="${pager.totalPage >0}">
					<!-- prev -->
					<c:choose>
						<c:when test="${pager.curtPage == 1 }">
							<span class="disabled">&lt;Prev</span>
						</c:when>
						<c:otherwise>
							<a href="<%=pre %>/banji/index<%=sufStr%>?curtPage=${pager.curtPage - 1}">&lt; Prev </a>
						</c:otherwise>
					</c:choose>
					
					<!-- content -->
					<c:if test="${pager.totalPage < 7 }">
						<c:forEach var = "item" begin="1" end = "${pager.totalPage }">
							<c:choose>
								<c:when test="${pager.curtPage == item }">
								  	<span class="current">${item }</span>
								</c:when>
								<c:otherwise>
									<a href="<%=pre %>/banji/index<%=sufStr%>?curtPage=${item}">${item }</a>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:if>
					<c:if test="${pager.totalPage >= 7 }">
						<c:if test="${pager.curtPage <= 4 }">
							<c:forEach var = "item" begin="1" end = "5">
								<c:choose>
									<c:when test="${pager.curtPage == item }">
									  	<span class="current">${item }</span>
									</c:when>
									<c:otherwise>
										<a href="<%=pre %>/banji/index<%=sufStr%>?curtPage=${item}">${item }</a>
									</c:otherwise>
								</c:choose>
							</c:forEach>
							...
							<a href="<%=pre %>/banji/index<%=sufStr%>?curtPage=${pager.totalPage}">${pager.totalPage}</a>
						</c:if>
						<c:if test="${pager.curtPage > 4 }">
							<a href="<%=pre %>/banji/index<%=sufStr%>?curtPage=1">1</a>
							...
							<c:choose>
									
									<c:when test="${pager.curtPage < pager.totalPage - 3 }">
										<c:forEach var = "item" begin="${pager.curtPage - 2 }" end = "${pager.curtPage + 2 }">
											<c:choose>
												<c:when test="${pager.curtPage == item }">
												  	<span class="current">${item }</span>
												</c:when>
												<c:otherwise>
													<a href="<%=pre %>/banji/index<%=sufStr%>?curtPage=${item}">${item }</a>
												</c:otherwise>
											</c:choose>
										</c:forEach>
										...
										<a href="<%=pre %>/banji/index<%=sufStr%>?curtPage=${pager.totalPage}">${pager.totalPage}</a>
									</c:when>
									<c:otherwise>
										<c:forEach var = "item" begin="${pager.totalPage - 4 }" end = "${pager.totalPage }">
											<c:choose>
												<c:when test="${pager.curtPage == item }">
												  	<span class="current">${item }</span>
												</c:when>
												<c:otherwise>
													<a href="<%=pre %>/banji/index<%=sufStr%>?curtPage=${item}">${item }</a>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</c:otherwise>
								</c:choose>
						</c:if>
					</c:if>
					<!-- next -->
					<c:choose>
						<c:when test="${pager.curtPage == pager.totalPage }">
							<span class="disabled">Next &gt;</span>
						</c:when>
						<c:otherwise>
							<a href="<%=pre %>/banji/index<%=sufStr%>?curtPage=${pager.curtPage + 1}">Next  &gt; </a>
						</c:otherwise>
					</c:choose>	
				</c:if>
			</div>
		</div><!-- 分页结束 -->
		
	</div>
</body>
</html>