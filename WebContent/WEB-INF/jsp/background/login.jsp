<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<!DOCTYPE HTML >
<html>
<head>
<meta charset=UTF-8>
<title>旭日阳光后台登入页面</title>
<link href="<%=cssPath%>/css.css" rel="stylesheet" type="text/css" />
<link href="<%=cssPath%>/login.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=jsPath%>/md5.js"></script>
<script>
	$(document).ready(function(){
		$("#btn_sub").click(function(){
			var userid = trim($("#userid").val());
			var password = $("#password").val();
			if(userid == '' || userid ==null ){
				$("#useridtip").html("请输入用户名！");
				return;
			}
			if(password == '' || password == null){
				$("#pwdtip").html("请输入密码！");
				return;
			}
			password = faultylabs.MD5(password);
			password = faultylabs.MD5(password);
			$.ajax({
				url : "<%=pre%>/login/login<%=sufStr%>",
				type : 'post',
				dataType : 'html',
				data:{userid:userid,password:password},
				success:function(data, status){
					if(data == 1) {
						window.location.href = '<%=pre%>/login/index<%=sufStr%>';
					}
					else{
						$("#tip").html("用户名或密码错误！");
					}
				},
				error : function(xhr, textStatus,
						errorThrown){
					$("#tip").html("用户名或密码错误！");
				}
			});
			
		});
		
		$("#form1").keypress(function(e) { 
			if(e.which == 13){ 
				$("#btn_sub").click();
			} 
		}); 
	});
</script>
</head>
<body>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td height="147" background="<%=imagePath%>/top02.gif"><img src="<%=imagePath%>/top03.png" width="776" height="147" /></td>
		</tr>
	</table>
	<table width="562" border="0" align="center" cellpadding="0" cellspacing="0" class="right-table03">
		<tr>
			<td width="221">
				<table width="95%" border="0" cellpadding="0" cellspacing="0" class="login-text01">
					<tr>
						<td>
							<table width="100%" border="0" cellpadding="0" cellspacing="0" class="login-text01">
								<tr>
									<td align="center"><img src="<%=imagePath%>/ico13.gif" width="107" height="97" /></td>
								</tr>
								<tr>
									<td height="40" align="center">
										<a style="text-decoration:none;color:#111" href="<%=pre%>/client/index<%=sufStr%>">旭日阳光教育前台</a>
									</td>
								</tr>
							</table>
						</td>
						<td><img src="<%=imagePath%>/line01.gif" width="5" height="292" /></td>
					</tr>
				</table>
			</td>
			<td>
				<form id="form1" name="from1" method="post" action="<%=pre %>/login/login<%=sufStr%>">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="31%" height="35" class="login-text02">用户名称：<br /></td>
							<td width="69%">
								<input id="userid" name="userid" type="text" size="15" />
								<span style="color:red;font-size:12px;" id="useridtip"></span>
							</td>
						</tr>
						<tr>
							<td height="35" class="login-text02">密 码：<br /></td>
							<td>
								<input id="password" name="password" type="password" size="15" />
								<span style="color:red;font-size:12px;" id="pwdtip"></span>
							</td>
						</tr>
						<tr>
							<td height="35">&nbsp;</td>
							<td>
								<input id="btn_sub" type="button" class="right-button01" value="确认登陆" />
								<span id="tip" style="color:red"></span>
							</td>
						</tr>
					</table>
				</form>
			</td>
		</tr>
	</table>
</body>
</html>