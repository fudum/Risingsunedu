<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML >
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
<meta charset=UTF-8>
<title>添加优秀学员</title>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/fbackground.css" />
<script type="text/javascript" src="<%=jsPath%>/fileupload.js"></script>
<script>
$(document).ready(function(){
	//保存按钮
	$("#saveButton").click(function() {
		var name = $("#name").val();
		var teaname = $("#teaname").val();
		var picpath= $("#picpath").val();
		var motto = $("#motto").val();
		var grade = $("input[name='grade']:checked").val();
		var leixing = $("#leixing").val();
		$.ajax({
			url : "<%=pre%>/b_excstu/insert<%=suf%>",
			type : 'post',
			dataType : 'html',
			data : {name:name,teaname:teaname,picpath:picpath,motto:motto,grade:grade,leixing:leixing},
			success : function(data, status) {
				if (status == 'success') {
					if (data == '1') {
						var timer;
						art.dialog({
							content : '<span style="color:#f00">插入成功！</span>',
							init : function() {
								var that = this, i = 1;
								var fn = function() {
									!i && that.close();
									i--;
								};
								timer = setInterval(fn, 1000);
								fn();
							},
							close : function() {
								clearInterval(timer);
								window.location.href="<%=pre%>/b_excstu/index<%=suf%>";
							}
						}).show();	
					} else {
						art.dialog.tips('<span style="color:#f00">插入失败！</span>', 1);
					}
				}
			},
			error : function(xhr, textStatus,
					errorThrown) {
				art.dialog.tips('<span style="color:#f00">插入失败！</span>', 1);
			}
		});
		
	});
	
	$("#uploadFile").live('change',function(){
		var wjjname = 'excstu';
		var imgId = 'pic_img';
		var inputId = 'picpath';
		var url = '<%=pre%>/b_com/fileupload<%=sufStr%>';
		var uploadPath = '<%=uploadPath%>/';
		uploadFile(url,uploadPath,wjjname,imgId,inputId);
	});
	
});
</script>
</head>
<body>
<div class="content">
	<div class="content_title">
		学霸成绩单添加页面
	</div>
	<table class="tbl001">
		<tr>
			<td>学生姓名：</td>
			<td>
				<select id="name">
					<c:forEach items="${listStudent}" var="obj">
						<option value="${obj.id};${obj.name}">${obj.name }</option>
					</c:forEach>
				</select>
			</td>
		</tr>
		<tr>
			<td>学霸成绩单年级：</td>
			<td>
				<c:forEach items="${listGrade}" var="obj">
					<input type="radio" name="grade" value="${obj.name }">${obj.name }
				</c:forEach>
			</td>
		</tr>
		<tr>
			<td>感谢的老师姓名：</td>
			<td>
				<select id="teaname">
					<c:forEach items="${listTeacher}" var="obj">
						<option value="${obj.id};${obj.name}">${obj.name }</option>
					</c:forEach>
				</select>
			</td>
		</tr>
		<tr>
			<td>成绩单类型：</td>
			<td>
				<input type="text" id="leixing" >
			</td>
		</tr>
		<tr>
			<td>感言：</td>
			<td>
				<input type="text" id="motto" style="width:600px" >
			</td>
		</tr>
		<tr>
			<td>图片文件：</td>
			<td>
				<input type="file" name="uploadFile" id="uploadFile">
				<img alt="" src="" id="pic_img" width="300px" height="100px">
				<input type="hidden" id="picpath">
			</td>
			
		</tr>
		<tr>
			<td>
				<input type="submit" value="保存" class="button" id="saveButton" />
				<input type="submit" value="返回" class="button" onClick="javascript:history.go(-1);" />
			</td>
			<td></td>
		</tr>
	</table>
</div>
</body>
</html>