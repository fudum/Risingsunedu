<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML >
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
<meta charset=UTF-8>
<title>学霸们的成绩单</title>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/fbackground.css"/>
<script>
	function link() {
		document.getElementById("fom").submit();
	}
</script>
</head>
<body>
<div class="content">
	<div style="margin-bottom:10px;">
		<form name="fom" id="fom" method="post" action="<%=pre%>/b_excstu/add<%=suf%>"></form>
	</div>
	<div>
		<input type="button" class="btn_add" value="添加学霸成绩单" onclick="link();" />
	</div>
	<div>
		<table class="tbl002">
			<tr>
				<td>序号</td>
				<td>学霸姓名</td>
				<td>感谢教师名称</td>
				<td>年级</td>
				<td>学霸成绩单类型</td>
				<td>感言</td>
				<td>操作时间</td>
				<td>操作</td>
				<td>序号</td>
			</tr>
			<c:forEach items="${pager.pageList}" var="obj" varStatus="stauts">
				<tr>
					<td>${stauts.index +1}</td>
					<td>${obj.name }</td>
					<td>${obj.teaname }</td>
					<td>${obj.grade }</td>
					<td>${obj.leixing}</td>
					<td>
						<c:choose>
          					<c:when test="${fn:length(obj.motto) > 20}">
              					<c:out value="${fn:substring(obj.motto, 0, 20)}..." />
          					</c:when>
         					<c:otherwise>
            					<c:out value="${obj.motto}" />
          					</c:otherwise>
      					</c:choose>
      				</td>
      				<td>${obj.inserttime }</td>
      				<td>
      					<a href="<%=pre%>/b_excstu/edit<%=suf%>?id=${obj.id }">编辑详情</a> | 
						<a href="javascript:deleteConfirm('${obj.id }','<%=pre%>/b_excstu/delete<%=suf%>?id=${obj.id}');">删除</a>
      				</td>
      				<td>${stauts.index +1}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</div>
</body>
</html>
