<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/fbackground.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
<script type="text/javascript" src="<%=jsPath%>/fileupload.js"></script>
<title>后台-优秀学员-今日学霸</title>
<script>
$(document).ready(function(){
	//保存按钮
	$("#saveButton").click(function() {
		var picpath = $("#picpath").val();
		var porder = $("#porder").val();
		//ajax提交
		$.ajax({
			url : "<%=pre%>/b_xueba/insert<%=sufStr%>",
			type : 'post',
			dataType : 'html',
			data : {picpath:picpath,porder:porder},
			success : function(data, status) {
				if (status == 'success') {
					if (data == '1') {
						var timer;
						art.dialog({
							content : '<span style="color:#f00">插入成功！</span>',
							init : function() {
								var that = this, i = 1;
								var fn = function() {
									!i && that.close();
									i--;
								};
								timer = setInterval(fn, 1000);
								fn();
							},
							close : function() {
								clearInterval(timer);
								window.location.href="<%=pre%>/b_xueba/index<%=sufStr%>";
							}
						}).show();	
					} else {
						art.dialog.tips('<span style="color:#f00">插入失败！</span>', 1);
					}
				}
			},
			error : function(xhr, textStatus,
					errorThrown) {
				art.dialog.tips('<span style="color:#f00">插入失败！</span>', 1);
			}
		});
		
	});
	
	$("#uploadFile").live('change',function(){
		var wjjname = 'excstu';
		var imgId = 'pic_img';
		var inputId = 'picpath';
		var url = '<%=pre%>/b_com/fileupload<%=sufStr%>';
		var uploadPath = '<%=uploadPath%>/';
		uploadFile(url,uploadPath,wjjname,imgId,inputId);
	});
	
});
</script>
</head>
<body>
<div class="content">
	<div class="content_title">
		首页今日学霸添加页面
	</div>
	<table class="tbl001">
		<tr>
			<td>显示顺序：</td>
			<td><input type="text" name="porder" id="porder"></td>
		</tr>
		<tr>
			<td>图片文件：</td>
			<td>
				<input type="file" name="uploadFile" id="uploadFile">
				<img alt="" src="" id="pic_img" width="300px" height="100px">
				<input type="hidden" id="picpath">
			</td>
			
		</tr>
		<tr>
			<td>
				<input type="submit" value="保存" class="button" id="saveButton" />
				<input type="submit" value="返回" class="button" onClick="javascript:history.go(-1);" />
			</td>
			<td></td>
		</tr>
	</table>
</div>
</body>
</html>