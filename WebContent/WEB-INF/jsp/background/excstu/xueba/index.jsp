<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>后台-优秀学员-今日学霸</title>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath %>/fbackground.css" />
<script>
	function link() {
		document.getElementById("fom").action = "<%=pre%>/b_xueba/add<%=sufStr%>";
		document.getElementById("fom").submit();
	}
</script>
</head>
<body>
<div class="content">
	<div>
		<form name="fom" id="fom" method="post" action=""></form>
	</div>
	<div style="float:left">
		<input type="button" class="btn_add" value="添加图片" onclick="link();" />
	</div>
	<div style="clear:both;"></div>
	<table class="tbl002" >
		<tr>
			<td>序号</td>
			<td>图片路径</td>
			<td>排序</td>
			<td>插入时间</td>
			<td>操作</td>
			<td>序号</td>
		</tr>
		<c:forEach items="${pager.pageList}" var="obj" varStatus="stauts">
			<tr bgcolor="#FFFFFF" align="center">
				<td>${stauts.index +1}</td>
				<td><img src="<%=uploadPath%>/${obj.picpath}" style="width:200px;heigth:50px"></td>
				<td>${obj.porder}</td>
				
				<td>${obj.inserttime }</td>
				<td>
					<a href="<%=pre%>/b_xueba/edit<%=suf%>?id=${obj.id }">编辑详情</a> | 
					<a href="javascript:deleteConfirm('${obj.id }','<%=pre%>/b_xueba/delete<%=suf%>?id=${obj.id}');">删除</a></td>
				<td>${stauts.index +1}</td>
			</tr>
		</c:forEach>
	</table>
</div>
</body>
</html>