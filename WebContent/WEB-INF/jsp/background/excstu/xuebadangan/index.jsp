<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>学霸档案</title>
<!-- 样式表 -->
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css"/>
<link rel="stylesheet" type="text/css" href="<%=cssPath %>/fbackground.css" />
</head>
<body>
<div class="content">
	<div class="content_title">
		学霸档案页面
	</div>
	<table class="tbl002">
		<tr>
			<td>序号</td>	
			<td>用户名</td>
			<td>姓名</td>
			<td>性别</td>
			<td>学校</td>
			<td>插入时间</td>
			<td>操作</td>
			<td>序号</td>
		</tr>
		<c:forEach items="${pager.pageList}" var="obj" varStatus="status">
			<tr>
				<td>${status.index +1}</td>
				<td>${obj.stuid}</td>
				<td>${obj.name}</td>
				<td>${obj.sex}</td>
				<td>${obj.school }</td>
				<td>${obj.inserttime }</td>
				<td>
					<a href="<%=pre%>/b_xuebabaoban/index<%=suf%>?id=${obj.id}">编辑档案</a>
				</td>
				<td>${status.index +1}</td>
			</tr>
		</c:forEach>
	</table>
	
	<!-- 分页条 -->
	<div>
		<div class="manu">
			<c:if test="${pager.totalPage >0}">
				<!-- prev -->
				<c:choose>
					<c:when test="${pager.curtPage == 1 }">
						<span class="disabled">&lt;Prev</span>
					</c:when>
					<c:otherwise>
						<a href="<%=pre %>/b_excstu/xuebadangan<%=sufStr%>?curtPage=${pager.curtPage - 1}">&lt; Prev </a>
					</c:otherwise>
				</c:choose>
				
				<!-- content -->
				<c:if test="${pager.totalPage < 7 }">
					<c:forEach var = "item" begin="1" end = "${pager.totalPage }">
						<c:choose>
							<c:when test="${pager.curtPage == item }">
							  	<span class="current">${item }</span>
							</c:when>
							<c:otherwise>
								<a href="<%=pre %>/b_excstu/xuebadangan<%=sufStr%>?curtPage=${item}">${item }</a>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</c:if>
				<c:if test="${pager.totalPage >= 7 }">
					<c:if test="${pager.curtPage <= 4 }">
						<c:forEach var = "item" begin="1" end = "5">
							<c:choose>
								<c:when test="${pager.curtPage == item }">
								  	<span class="current">${item }</span>
								</c:when>
								<c:otherwise>
									<a href="<%=pre %>/b_excstu/xuebadangan<%=sufStr%>?curtPage=${item}">${item }</a>
								</c:otherwise>
							</c:choose>
						</c:forEach>
						...
						<a href="<%=pre %>/b_excstu/xuebadangan<%=sufStr%>?curtPage=${pager.totalPage}">${pager.totalPage}</a>
					</c:if>
					<c:if test="${pager.curtPage > 4 }">
						<a href="<%=pre %>/b_excstu/xuebadangan<%=sufStr%>?curtPage=1">1</a>
						...
						<c:choose>
								
								<c:when test="${pager.curtPage < pager.totalPage - 3 }">
									<c:forEach var = "item" begin="${pager.curtPage - 2 }" end = "${pager.curtPage + 2 }">
										<c:choose>
											<c:when test="${pager.curtPage == item }">
											  	<span class="current">${item }</span>
											</c:when>
											<c:otherwise>
												<a href="<%=pre %>/b_excstu/xuebadangan<%=sufStr%>?curtPage=${item}">${item }</a>
											</c:otherwise>
										</c:choose>
									</c:forEach>
									...
									<a href="<%=pre %>/b_excstu/xuebadangan<%=sufStr%>?curtPage=${pager.totalPage}">${pager.totalPage}</a>
								</c:when>
								<c:otherwise>
									<c:forEach var = "item" begin="${pager.totalPage - 4 }" end = "${pager.totalPage }">
										<c:choose>
											<c:when test="${pager.curtPage == item }">
											  	<span class="current">${item }</span>
											</c:when>
											<c:otherwise>
												<a href="<%=pre %>/b_excstu/xuebadangan<%=sufStr%>?curtPage=${item}">${item }</a>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</c:otherwise>
							</c:choose>
					</c:if>
				</c:if>
				<!-- next -->
				<c:choose>
					<c:when test="${pager.curtPage == pager.totalPage }">
						<span class="disabled">Next &gt;</span>
					</c:when>
					<c:otherwise>
						<a href="<%=pre %>/b_excstu/xuebadangan<%=sufStr%>?curtPage=${pager.curtPage + 1}">Next  &gt; </a>
					</c:otherwise>
				</c:choose>	
			</c:if>
		</div>
	</div><!-- 分页结束 -->
	
</div>
</body>
</html>