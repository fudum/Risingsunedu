<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>学霸报班-编辑页面</title>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css"/>
<link rel="stylesheet" type="text/css" href="<%=cssPath %>/fbackground.css" />
<script>
$(document).ready(function(){
	//添加课程
	$("#btn_add_xuebabaoban").click(function(){
		var stuid = $("#stuid").val();
		art.dialog.open("<%=pre%>/b_xuebabaoban/add<%=sufStr%>?stuid=" + stuid,{
			title:"添加学霸报班",
			width:800,
			height:400,
			cancel: true,
			ok:function(){
				var iframe = this.iframe.contentWindow;
				if (!iframe.document.body) {
		        	alert('iframe还没加载完毕呢')
		        	return false;
		        };
				var stuid = iframe.document.getElementById('stuid').value;
				var myselect=iframe.document.getElementById("kechengname");
				var index=myselect.selectedIndex;
				var kechengname =  myselect.options[index].value;
				$.ajax({
					type:"post",
					url:"<%=pre%>/b_xuebabaoban/insert<%=sufStr%>",
					data:{stuid:stuid,kechengname:kechengname},
					dataType:"json",
					success:function(data,status){
						if(data == 1){
							var timer;
							art.dialog({
								width:150,
								height:80,
								content : '<span style="color:#f00">添加成功!</span>',
								init : function() {
									var that = this, i = 1;
									var fn = function() {
										!i && that.close();
										i--;
									};
									timer = setInterval(fn, 1000);
									fn();
								},
								close : function() {
									clearInterval(timer);
									location.reload();
								}
							}).show();			
						}
						else{
							art.dialog.alert("添加失败！");
							return false;
						}
					},
					error : function(xhr, textStatus, errorThrown) {
						art.dialog.alert('<span style="color:#f00">添加失败！</span>', 1);
						return false;
					}
				});
			}
		});
	});
	
});
function link() {
	$("#fom").submit();
}

function editPage(url){
	art.dialog.open(url,{
		title:"编辑学霸报班",
		width:800,
		height:400,
		cancel: true,
		ok:function(){
			var iframe = this.iframe.contentWindow;
			if (!iframe.document.body) {
	        	alert('iframe还没加载完毕呢')
	        	return false;
	        };
	        var id = iframe.document.getElementById('id').value;
			var stuid = iframe.document.getElementById('stuid').value;
			var myselect=iframe.document.getElementById("kechengname");
			var index=myselect.selectedIndex;
			var kechengname =  myselect.options[index].value;

			$.ajax({
				type:"post",
				url:"<%=pre%>/b_xuebabaoban/update<%=sufStr%>",
				data:{id:id,stuid:stuid,kechengname:kechengname},
				dataType:"json",
				success:function(data,status){
					if(data == 1){
						var timer;
						art.dialog({
							width:150,
							height:80,
							content : '<span style="color:#f00">编辑成功!</span>',
							init : function() {
								var that = this, i = 1;
								var fn = function() {
									!i && that.close();
									i--;
								};
								timer = setInterval(fn, 1000);
								fn();
							},
							close : function() {
								clearInterval(timer);
								location.reload();
							}
						}).show();			
					}
					else{
						art.dialog.alert("编辑失败！");
						return false;
					}
				},
				error : function(xhr, textStatus, errorThrown) {
					art.dialog.alert('<span style="color:#f00">编辑失败！</span>', 1);
					return false;
				}
			});
		}
	});
}

</script>
</head>
<body>
<div class="content">
	<div>
		<input type="button" class="btn_add" value="添加学霸报班" id="btn_add_xuebabaoban" />
		<span>学生姓名：${bean.name }</span>
		<input type="hidden" id="stuid"  value="${bean.id }">
		<a href="<%=pre%>/b_excstu/xuebadangan<%=suf%>">退出到学霸档案界面</a>
	</div>
	<table class="tbl002">
		<tr>
			<td>序号</td>	
			<td>教师名称</td>
			<td>报班名称</td>
			<td>插入时间</td>
			<td>操作</td>
			<td>序号</td>
		</tr>
		<c:forEach items="${list}" var="obj" varStatus="status">
			<tr>
				<td>${status.index +1}</td>
				<td>${obj.teaname}</td>
				<td>${obj.kechengname}</td>
				<td>${obj.inserttime }</td>
				<td>
					<a href="javascript:editPage('<%=pre%>/b_xuebabaoban/edit<%=suf%>?id=${obj.id }');">编辑详情</a> | 
					<a href="javascript:deleteConfirm('${obj.id }','<%=pre%>/b_xuebabaoban/delete<%=suf%>?id=${obj.id}');">删除</a>
				</td>
				<td>${status.index +1}</td>
			</tr>
		</c:forEach>
	</table>
</div>
</body>
</html>