<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>学霸档案-我要报班-编辑页面</title>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/fbackground.css" />
</head>
<body>
<div class="content" style="margin-top:10px;">
	<table class="tbl001">
		<tr>
			<td style="width:150px"> 我要报班名称：</td>
			<td>
				<input type="hidden" value="${bean.stuid}" id="stuid">
				<input type="hidden" value="${bean.id }" id="id">
				<select id="kechengname">
					<c:forEach items="${listKc }" var="obj">
						<option value="${obj.tea_id };${obj.tea_name};${obj.id};${obj.name}"
							<c:if test="${obj.id == bean.kechengid }">selected='selected'</c:if>
						>${obj.name}</option>
					</c:forEach>
				</select>
			</td>
		</tr>
	</table>
</div>
</body>
</html>