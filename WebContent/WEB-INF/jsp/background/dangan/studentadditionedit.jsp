<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<%
	request.setCharacterEncoding("UTF-8");
	String htmlData1 = request.getParameter("teainfo") != null ? request.getParameter("teainfo") : "";
	String htmlData2 = request.getParameter("teaachive") != null ? request.getParameter("teaachive") : "";
	String htmlData3 = request.getParameter("teainfoweigui") != null ? request.getParameter("teainfoweigui") : "";
%>
<!DOCTYPE HTML >
<html>
<head>
	<meta charset=UTF-8>
	<title>学生档案附加信息</title>
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
	<link rel="stylesheet" href="<%=kePath %>/themes/default/default.css" />
	<link rel="stylesheet" href="<%=kePath %>/plugins/code/prettify.css" />
	<script charset="utf-8" src="<%=kePath %>/kindeditor.js"></script>
	<script charset="utf-8" src="<%=kePath %>/lang/zh_CN.js"></script>
	<script charset="utf-8" src="<%=kePath %>/plugins/code/prettify.js"></script>
	<script>
		KindEditor.ready(function(K) {
			var editor1 = K.create('textarea[name="kechengsheji"]', {
				cssPath : '<%=kePath %>/plugins/code/prettify.css',
				uploadJson : '<%=kePath %>/jsp/upload_json.jsp',
				fileManagerJson : '<%=kePath %>/jsp/file_manager_json.jsp',
				allowFileManager : true,
				afterCreate : function() {
					var self = this;
					K.ctrl(document, 13, function() {
						self.sync();
						document.forms['example'].submit();
					});
					K.ctrl(self.edit.doc, 13, function() {
						self.sync();
						document.forms['example'].submit();
					});
				}
			});
			
			var editor2 = K.create('textarea[name="shangkejilu"]', {
				cssPath : '<%=kePath %>/plugins/code/prettify.css',
				uploadJson : '<%=kePath %>/jsp/upload_json.jsp',
				fileManagerJson : '<%=kePath %>/jsp/file_manager_json.jsp',
				allowFileManager : true,
				afterCreate : function() {
					var self = this;
					K.ctrl(document, 13, function() {
						self.sync();
						document.forms['example'].submit();
					});
					K.ctrl(self.edit.doc, 13, function() {
						self.sync();
						document.forms['example'].submit();
					});
				}
			});
			
			var editor3 = K.create('textarea[name="tiku"]', {
				cssPath : '<%=kePath %>/plugins/code/prettify.css',
				uploadJson : '<%=kePath %>/jsp/upload_json.jsp',
				fileManagerJson : '<%=kePath %>/jsp/file_manager_json.jsp',
				allowFileManager : true,
				afterCreate : function() {
					var self = this;
					K.ctrl(document, 13, function() {
						self.sync();
						document.forms['example'].submit();
					});
					K.ctrl(self.edit.doc, 13, function() {
						self.sync();
						document.forms['example'].submit();
					});
				}
			});
			prettyPrint();
			
		});
	</script>
	<style>
		.teatitle{
			font-size:24px;
			margin:20px 10px 10px 20px;
		}
		.teabody{
			margin-left:20px;
		}
	</style>
</head>
<body>
	<form id="example" name="example" action="../stuaddn/stuaddnupdate<%=sufStr%>" method="post" enctype="multipart/form-data">
		<div class="MainDiv">
			<input type="hidden" name="id" value="${bean.id}" />
			<input type="hidden" name="stu_id" value="${bean.stu_id }" />
			<table width="99%" border="0" cellpadding="0" cellspacing="0" class="CContent">
				<tr>
					<th class="tablestyle_title">学生档案附加信息详情页面</th>
				</tr>
				<tr>
					<td class="CPanel">
						<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
							<TR>
								<td width="100%">
									<fieldset style="height: 100%;">
										<legend>学生编辑</legend>
										<div class="teatitle">课程设计</div>
										<%=htmlData1%>
										<div class="teabody">
											<textarea id="kechengsheji" name="kechengsheji" style="width:900px;height:250px;">
												<%=htmlspecialchars(htmlData1)%>${bean.kechengsheji}
											</textarea>
										</div>
										<div class="teatitle">上课记录</div>
										<%=htmlData2%>
										<div class="teabody">
											<textarea id="shangkejilu" name="shangkejilu" style="width:900px;height:250px;">
												<%=htmlspecialchars(htmlData2)%>${bean.shangkejilu}
											</textarea>
										</div>
										<div class="teatitle">题库</div>
										<%=htmlData3%>
										<div class="teabody">
											<textarea id="tiku" name="tiku" style="width:900px;height:250px;">
												<%=htmlspecialchars(htmlData3)%>${bean.tiku}
											</textarea>											
										</div>
									</fieldset>
									<div>${type}</div>
								</td>
							</TR>
						</table>
					</td>
				</tr>
				<TR>
					<td colspan="2" align="center" height="50px">
						
						<input type="submit" value="保存" class="button" id="saveButton" />
						<input type="button" value="返回" class="button" id="goBack" />
					</td>
				</TR>
			</table>
		</div>
	</form>
</body>
</html>

<%!
	private String htmlspecialchars(String str) {
		str = str.replaceAll("&", "&amp;");
		str = str.replaceAll("<", "&lt;");
		str = str.replaceAll(">", "&gt;");
		str = str.replaceAll("\"", "&quot;");
		return str;
	}
%>
