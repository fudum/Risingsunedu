<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>基本信息管理</title>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath %>/fbackground.css" />
<script>
$(document).ready(function(){
	//添加一级
	$("#btn_add_one").click(function(){
		art.dialog.open("<%=pre%>/baseone/add<%=sufStr%>",{
			title:"添加一级",
			width:500,
			height:200,
			cancel: true,
			ok:function(){
				var iframe = this.iframe.contentWindow;
				if (!iframe.document.body) {
		        	alert('iframe还没加载完毕')
		        	return false;
		        };
				var name = iframe.document.getElementById('name').value;
				$.ajax({
					type:"post",
					url:"<%=pre%>/baseone/insert<%=sufStr%>",
					data:{name:name},
					dataType:"json",
					success:function(data,status){
						if(data.ret == '1'){
							var timer;
							art.dialog({
								width:150,
								height:80,
								content : '<span style="color:#f00">添加成功!</span>',
								init : function() {
									var that = this, i = 1;
									var fn = function() {
										!i && that.close();
										i--;
									};
									timer = setInterval(fn, 1000);
									fn();
								},
								close : function() {
									clearInterval(timer);
									location.reload();
								}
								
							}).show();			
						}
						else{
							art.dialog.alert("添加失败！");
							return false;
						}
					},
					error : function(xhr, textStatus, errorThrown) {
						art.dialog.alert('<span style="color:#f00">添加失败！</span>', 1);
						return false;
					}
				});
			}
		});
	});
	
	
	//添加二级
	$("#btn_add_two").click(function(){
		art.dialog.open("<%=pre%>/basetwo/add<%=sufStr%>",{
			title:"添加一级",
			width:500,
			height:200,
			cancel: true,
			ok:function(){
				var iframe = this.iframe.contentWindow;
				if (!iframe.document.body) {
		        	alert('iframe还没加载完毕')
		        	return false;
		        };
		        var botype = iframe.document.getElementById('botype').value;
				var name = iframe.document.getElementById('name').value;
				var border = iframe.document.getElementById('border').value;
				$.ajax({
					type:"post",
					url:"<%=pre%>/basetwo/insert<%=sufStr%>",
					data:{botype:botype,name:name,border:border},
					dataType:"json",
					success:function(data,status){
						if(data.ret == '1'){
							var timer;
							art.dialog({
								width:150,
								height:80,
								content : '<span style="color:#f00">添加成功!</span>',
								init : function() {
									var that = this, i = 1;
									var fn = function() {
										!i && that.close();
										i--;
									};
									timer = setInterval(fn, 1000);
									fn();
								},
								close : function() {
									clearInterval(timer);
									location.reload();
								}
								
							}).show();			
						}
						else{
							art.dialog.alert("添加失败！");
							return false;
						}
					},
					error : function(xhr, textStatus, errorThrown) {
						art.dialog.alert('<span style="color:#f00">添加失败！</span>', 1);
						return false;
					}
				});
			}
		});
	});
	
	
});


/**编辑一级页面**/
function editOnePage(url){
	art.dialog.open(url,{
		title:"编辑基本一级目录",
		width:500,
		height:200,
		cancel: true,
		ok:function(){
			var iframe = this.iframe.contentWindow;
			if (!iframe.document.body) {
	        	alert('iframe还没加载完毕呢')
	        	return false;
	        };
			var name = iframe.document.getElementById('name').value;
			var id = iframe.document.getElementById('id').value;
			$.ajax({
				type:"post",
				url:"<%=pre%>/baseone/update<%=sufStr%>",
				data:{id:id,name:name},
				dataType:"json",
				success:function(data,status){
					if(data.ret == '1'){
						var timer;
						art.dialog({
							width:150,
							height:80,
							content : '<span style="color:#f00">编辑成功!</span>',
							init : function() {
								var that = this, i = 1;
								var fn = function() {
									!i && that.close();
									i--;
								};
								timer = setInterval(fn, 1000);
								fn();
							},
							close : function() {
								clearInterval(timer);
								location.reload();
							}
							
						}).show();			
					}
					else{
						art.dialog.alert("编辑失败！");
						return false;
					}
				},
				error : function(xhr, textStatus, errorThrown) {
					art.dialog.alert('<span style="color:#f00">编辑失败！</span>', 1);
					return false;
				}
			});
		}
	});
}

/**编辑二级页面**/
function editTwoPage(url){
	art.dialog.open(url,{
		title:"编辑基本二级目录",
		width:500,
		height:200,
		cancel: true,
		ok:function(){
			var iframe = this.iframe.contentWindow;
			if (!iframe.document.body) {
	        	alert('iframe还没加载完毕呢')
	        	return false;
	        };
	        var id = iframe.document.getElementById('id').value;
	        var old_name = iframe.document.getElementById('old_name').value;
			var name = iframe.document.getElementById('name').value;
			var botype = iframe.document.getElementById('botype').value;
			var border = iframe.document.getElementById('border').value;
			$.ajax({
				type:"post",
				url:"<%=pre%>/basetwo/update<%=sufStr%>",
				data:{id:id,name:name,botype:botype,border:border,old_name:old_name},
				dataType:"json",
				success:function(data,status){
					if(data.ret == '1'){
						var timer;
						art.dialog({
							width:150,
							height:80,
							content : '<span style="color:#f00">编辑成功!</span>',
							init : function() {
								var that = this, i = 1;
								var fn = function() {
									!i && that.close();
									i--;
								};
								timer = setInterval(fn, 1000);
								fn();
							},
							close : function() {
								clearInterval(timer);
								location.reload();
							}
							
						}).show();			
					}
					else{
						art.dialog.alert("编辑失败！");
						return false;
					}
				},
				error : function(xhr, textStatus, errorThrown) {
					art.dialog.alert('<span style="color:#f00">编辑失败！</span>', 1);
					return false;
				}
			});
		}
	});
}
</script>
</head>
<body>
<div class="content">
	<div class="content_title">
		基本信息一级
	</div>
	<div>
		<input type="button" class="btn_add" id="btn_add_one" value="添加基本信息一级" />
	</div>
	<div>
		<table class="tbl002" >
			<tr>
				<td>序号</td>
				<td>基本信息名称</td>
				<td>插入时间</td>
				<td>操作</td>
				<td>序号</td>
			</tr>
			<c:forEach items="${listone}" var="obj" varStatus="stauts">
				<tr bgcolor="#FFFFFF" align="center">
					<td>${stauts.index +1}</td>
					<td>${obj.name}</td>
					<td>${obj.inserttime }</td>
					<td>
						<a href="javascript:void(0)" onclick="javascript:editOnePage('<%=pre%>/baseone/edit<%=sufStr%>?id=${obj.id }')">编辑详情</a> | 
						<a href="javascript:deleteConfirm('${obj.id }','<%=pre%>/baseone/delete<%=sufStr%>?id=${obj.id}');">删除</a></td>
					<td>${stauts.index +1}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
	
	<!-- 二级基本信息 -->
	<div class="content_title" style="margin-top:10px;">
		基本信息二级
	</div>
	<div>
		<input  type="button" class="btn_add" id="btn_add_two" value="添加二级基本信息" />
	</div>
	
	<div>
		<table class="tbl002" >
			<tr>
				<td>序号</td>
				<td>所属一级名称</td>
				<td>名称</td>
				<td>排序</td>
				<td>插入时间</td>
				<td>操作</td>
				<td>序号</td>
			</tr>
			<c:forEach items="${listtwo}" var="obj" varStatus="stauts">
				<tr bgcolor="#FFFFFF" align="center">
					<td>${stauts.index +1}</td>
					<td>${obj.botype}</td>
					<td>${obj.name}</td>
					<td>${obj.border}</td>
					<td>${obj.inserttime }</td>
					<td>
						<a href="javascript:void(0)" onclick="javascript:editTwoPage('<%=pre%>/basetwo/edit<%=sufStr%>?id=${obj.id }')">编辑详情</a> | 
						<a href="javascript:deleteConfirm('${obj.id }','<%=pre%>/basetwo/delete<%=sufStr%>?id=${obj.id}');">删除</a></td>
					<td>${stauts.index +1}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</div>
</body>
</html>