<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>添加二级目录</title>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/fbackground.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
</head>
<body>
	<div class="content">
		<div style="margin-top:10px;">
			<table class="tbl001">
				<tr>
					<td>一级目录名称：</td>
					<td>
						<select id="botype">
							<c:forEach items="${listone }" var="obj">
								<option>${obj.name }</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr>
					<td>二级名称：</td>
					<td>
						<input type="text" id="name" style="width:300px"/>
					</td>
				</tr>
				<tr>
					<td>显示顺序：</td>
					<td>
						<input type="text" id="border" style="width:300px"/>
					</td>
				</tr>
			</table>
		</div>
	</div>
</body>
</html>