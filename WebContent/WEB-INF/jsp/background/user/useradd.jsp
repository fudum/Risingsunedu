<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<!DOCTYPE HTML >
<html>
<head>
<meta charset=UTF-8>
<title>添加管理员页面</title>

<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />

<style type="text/css">
.atten {
	font-size: 12px;
	font-weight: normal;
	color: #F00;
}
</style>

<script type="text/javascript">
	//点击提交按钮时候
	$(document).ready(
			function() {
				$("#saveButton").click(
						function() {
							var userid = $("#userid").val();
							var name = $("#name").val();
							var password = $("#password").val();
							var password2 = $("#password2").val();
							var sex = $("#sex").val();
							var usertype = $("#usertype").val();
							var phone = $("#phone").val();
							var address = $("#address").val();

							//用户编号是否为空
							if (userid == "") {
								$("#useridtip").html("*请输入管理员编号！");
								$("#userid")[0].focus();
								return false;
							}

							//用户名是否为空
							if (name == "") {
								$("#nametip").html("*请输入管理员名！");
								$("#name")[0].focus();
								return false;
							}

							//用户密码是否为空
							if (password == "") {
								$("#passwordtip").html("请输入管理员密码！");
								$("#password")[0].focus();
							}

							//用户二次密码
							if (password2 == "") {
								$("#password2tip").html("请再次输入管理员密码!");
								$("#password2")[0].focus();
							}

							//判断输入的密码是否一致
							if (password != password2) {
								$("#password2tip").html("*输入的密码不一致,请重新输入！");
								$("#password2")[0].focus();
								return false;
							}

							//ajax提交
							$.ajax({
								url : "../user/userinsert<%=sufStr%>",
								type : 'post',
								dataType : 'html',
								data : 'userid=' + userid + '&name=' + name
										+ '&password=' + password + '&sex='
										+ sex + '&usertype=' + usertype
										+ '&phone=' + phone + '&address='
										+ address,
								success : function(data, status) {
									if (status == 'success') {
										if (data == '1') {
											art.dialog.alert("插入成功！");
										} else {
											art.dialog.alert("插入失败！");
										}
									}
								},
								error : function(xhr, textStatus, errorThrown) {
									art.dialog.alert("插入失败，请重试！");
								}
							});

						});
			});
</script>
</head>
<body class="ContentBody">
	<form id="form1" action="../user/userinsert<%=sufStr%>" method="post">
		<div class="MainDiv">
			<table width="99%" border="0" cellpadding="0" cellspacing="0" class="CContent">
				<tr>
					<th class="tablestyle_title">用户管理员页面</th>
				</tr>
				<tr>
					<td class="CPanel">
						<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
							<TR>
								<td width="100%">
									<fieldset style="height: 100%;">
										<legend>管理员添加</legend>
										<div>
											<table border="0" cellpadding="2" cellspacing="1" style="width: 100%">
												<tr>
													<td nowrap align="right" width="40%">管理员编号:</td>
													<td width="60%"><input id="userid" name="userid" class="validate[required]" data-errormessage-value-missing="请输入用户编号！"
														style="width: 150px" type="text" /> <span class="red" id="useridtip"> *</span></td>
												</tr>
												<tr>
													<td nowrap align="right" width="5%">管理员名:</td>
													<td width="20%"><input id="name" name="name" class="validate[required]" data-errormessage-value-missing="请输入用户名！"
														style="width: 150px" type="text" /> <span class="red" id="nametip"> *</span></td>
												</tr>
												<tr>
													<td nowrap align="right" width="5%">管理员密码:</td>
													<td width="20%"><input id="password" name="password" class="validate[required]" data-errormessage-value-missing="请输入用户密码！"
														style="width: 150px" type="text" /> <span class="red" id="passwordtip"> *</span></td>
												</tr>
												<tr>
													<td nowrap align="right" width="5%">再次输入密码:</td>
													<td width="20%"><input id="password2" name="password2" class="validate[required]"
														data-errormessage-value-missing="请再次输入用户密码！" style="width: 150px" type="text" /> <span class="red" id="password2tip">*</span></td>
												</tr>
												<tr>
													<td nowrap align="right" width="5%">性别:</td>
													<td width="20%"><select id="sex" name="sex" width="50px">
															<option>男</option>
															<option>女</option>
													</select></td>
												</tr>
												<tr>
													<td nowrap align="right" width="5%">类别:</td>
													<td width="20%"><select id="usertype" name="usertype">
															<option>管理员</option>															
													</select></td>
												</tr>
												<tr>
													<td nowrap align="right" width="5%">电话:</td>
													<td width="20%"><input id="phone" name="phone" class="validate[custom[phone]]" style="width: 150px" type="text" /></td>
												</tr>
												<tr>
													<td nowrap align="right" width="5%">地址:</td>
													<td width="20%"><input id="address" name="address" style="width: 250px" type="text" /></td>
												</tr>
											</table>
										</div>
										<br />
									</fieldset>
								</td>
							</TR>
						</TABLE>
					</td>
				</tr>
				<TR>
					<td colspan="2" align="center" height="50px"><input type="button" value="保存" class="button" id="saveButton" /> <input
						type="button" value="返回" class="button" name="goback" onclick="window.history.go(-1);" /></td>
				</TR>
			</TABLE>
		</div>
	</form>
</body>
</html>