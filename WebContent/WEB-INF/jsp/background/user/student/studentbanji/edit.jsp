<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>编辑培优课堂同步</title>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/fbackground.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
</head>
<body>
<div class="content">
	<div style="margin-top:10px;">
		<input type="hidden" value="${bean.id }" id="id">
		<input type="hidden" value="${bean.stu_id}" id="stuid">
		<table class="tbl001">
			<tr>
				<td>培优课堂同步名称：</td>
				<td>
					<select id="banjiname" name="banjiname" varStatus="status">
						<c:forEach items="${list}" var="obj" >
							<option value="${obj.tea_id};${obj.tea_name};${obj.id};${obj.year}${obj.grade}${obj.subject}${obj.banci}${obj.bjtype}"
							<c:if test="${bean.teaid == obj.tea_id }">selected='selected'</c:if>>
							${obj.tea_name}${obj.year}${obj.grade}${obj.subject}${obj.banci}${obj.bjtype}  ${obj.lessonday }(${obj.starttime }-${obj.endtime })</option>
						</c:forEach>
					</select>
				</td>
			</tr>
		</table>
	</div>
</div>
</body>
</html>