<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML >
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
<meta charset=UTF-8>
<title>添加学生页面</title>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/fbackground.css" />
<script type="text/javascript" src="<%=commonPath%>/jquery/md5.js"></script>
<script type="text/javascript" src="<%=jsPath%>/fileupload.js"></script>
<script>
$(document).ready(function(){
	$("#saveButton").click(function() {
		var stuid = $("#stuid").val();
		var name = $("#name").val();
		var password  = $("#password").val();
		var password2 = $("#password2").val();
		//判断账号是否一致
		password = faultylabs.MD5(faultylabs.MD5(password));
		var sex = $("#sex").val();
		var phone = $("#phone").val();
		var zhuangtai = $("#zhuangtai").val();
		var kaiqidangan = $("#kaiqidangan").val();
		var picture = $("#picture").val();
		var school = $("#school").val();
		//ajax提交
		$.ajax({
			url : "<%=pre%>/b_student/insert<%=sufStr%>",
			type : 'post',
			dataType : 'html',
			data : {stuid:stuid,name:name,password:password,sex:sex,phone:phone,zhuangtai:zhuangtai,picture:picture,
				school:school,kaiqidangan:kaiqidangan},
			success : function(data, status) {
				if (status == 'success') {
					if (data == '1') {
						var timer;
						art.dialog({
							content : '<span style="color:#f00">插入成功！</span>',
							init : function() {
								var that = this, i = 1;
								var fn = function() {
									!i && that.close();
									i--;
								};
								timer = setInterval(fn, 1000);
								fn();
							},
							close : function() {
								clearInterval(timer);
								window.location.href="<%=pre%>/b_student/index<%=sufStr%>";
							}
						}).show();	
					} else {
						art.dialog.tips('<span style="color:#f00">插入失败！</span>', 1);
					}
				}
			},
			error : function(xhr, textStatus,
					errorThrown) {
				art.dialog.tips('<span style="color:#f00">插入失败！</span>', 1);
			}
		});
	});
	
	$("#uploadFile").live('change',function(){
		var wjjname = 'student';
		var imgId = 'pic_img';
		var inputId = 'picture';
		var url = '<%=pre%>/b_com/fileupload<%=sufStr%>';
		var uploadPath = '<%=uploadPath%>/';
		uploadFile(url,uploadPath,wjjname,imgId,inputId);
	});
});
</script>
</head>
<body>
<div class="content">
	<div class="content_title">
		学生添加页面
	</div>
	<div>
		<table class="tbl001">
			<tr>
				<td style="width:150px;">用户编号：</td>
				<td>
					<input type="text" name="stuid" id="stuid" />
					<span style="color:red">*</span>
				</td>
			</tr>
			<tr>
				<td>姓名：</td>
				<td>
					<input type="text" name="name" id="name"/>
					<span style="color:red">*</span>
				</td>
			</tr>
			<tr>
				<td>密码:</td>
				<td>
					<input type="password" name="password" id="password"/>
					<span style="color:red">*</span>
				</td>
			</tr>
			<tr>
				<td>确认密码:</td>
				<td>
					<input type="password" name="password2" id="password2"/>
					<span style="color:red">*</span>
				</td>
			</tr>
			
			<tr>
				<td>性别：</td>
				<td>
					<select name="sex" id="sex">
						<option>男</option>
						<option>女</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>电话：</td>
				<td>
					<input type="text" name="phone" id="phone" />
				</td>
			</tr>
			<tr>
				<td>学校：</td>
				<td>
					<input type="text" name="school" id="school" />
				</td>
			</tr>
			<tr>
				<td>登入系统状态：</td>
				<td>
					<select id="zhuangtai">
						<option value="1">允许</option>
						<option value="2">拒绝</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>开放学霸档案：</td>
				<td>
					<select id="kaiqidangan">
						<option value="0">拒绝</option>
						<option value="1">开放</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>头像：</td>
				<td>
					<input type="file" name="uploadFile" id="uploadFile">
					<img alt="" src="" id="pic_img" width="100px" height="100px">
					<input type="hidden" id="picture">
				</td>
			</tr>
		</table>
	</div>
	<div>
		<input type="button" value="保存" class="button" id="saveButton" /> 
		<input type="button" value="返回" class="button" name="goback" onclick="window.history.go(-1);" />
	</div>
</div>
</body>
</html>