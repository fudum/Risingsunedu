<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML >
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
<meta charset=UTF-8>
<title>学生编辑页面</title>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/fbackground.css" />
<script type="text/javascript" src="<%=commonPath%>/jquery/md5.js"></script>
<script type="text/javascript" src="<%=jsPath%>/fileupload.js"></script>
<script>
$(document).ready(function(){
	$("#saveButton").click(function() {
		var id = $("#id").val();
		var stuid = $("#stuid").val();
		var name = $("#name").val();
		var password  = $("#password").val();
		var password2 = $("#password2").val();
		var opwd = $("#opwd").val();
		if(opwd != password){
			password = faultylabs.MD5(faultylabs.MD5(password));
		}
		//判断账号是否一致
		var sex = $("#sex").val();
		var phone = $("#phone").val();
		var zhuangtai = $("#zhuangtai").val();
		var kaiqidangan = $("#kaiqidangan").val();
		var picture = $("#picture").val();
		var school = $("#school").val();
		//ajax提交
		$.ajax({
			url : "<%=pre%>/b_student/update<%=sufStr%>",
			type : 'post',
			dataType : 'html',
			data : {id:id,stuid:stuid,name:name,password:password,sex:sex,phone:phone,zhuangtai:zhuangtai,
				picture:picture,school:school,kaiqidangan:kaiqidangan},
			success : function(data, status) {
				if (status == 'success') {
					if (data == '1') {
						var timer;
						art.dialog({
							content : '<span style="color:#f00">修改成功！</span>',
							init : function() {
								var that = this, i = 1;
								var fn = function() {
									!i && that.close();
									i--;
								};
								timer = setInterval(fn, 1000);
								fn();
							},
							close : function() {
								clearInterval(timer);
								window.location.href="<%=pre%>/b_student/edit<%=sufStr%>?id=" + id;
							}
						}).show();	
					} else {
						art.dialog.tips('<span style="color:#f00">修改失败！</span>', 1);
					}
				}
			},
			error : function(xhr, textStatus,
					errorThrown) {
				art.dialog.tips('<span style="color:#f00">修改失败！</span>', 1);
			}
		});
	});
	
	//添加课程
	$("#btn_add_kc").click(function(){
		var stuid = $("#id").val();
		art.dialog.open("<%=pre%>/b_student_kc/add<%=sufStr%>?stuid=" + stuid,{
			title:"添加学生课程",
			width:800,
			height:400,
			cancel: true,
			ok:function(){
				var iframe = this.iframe.contentWindow;
				if (!iframe.document.body) {
		        	alert('iframe还没加载完毕呢')
		        	return false;
		        };
				var stuid = iframe.document.getElementById('stuid').value;
				var name = iframe.document.getElementById('name').value;
				var subject = iframe.document.getElementById('subject').value;
				var catalog = iframe.document.getElementById('catalog').value;
				var grade = iframe.document.getElementById('grade').value;
	
				$.ajax({
					type:"post",
					url:"<%=pre%>/b_student_kc/insert<%=sufStr%>",
					data:{stuid:stuid,name:name,grade:grade,subject:subject,catalog:catalog},
					dataType:"json",
					success:function(data,status){
						if(data.ret == '1'){
							var timer;
							art.dialog({
								width:150,
								height:80,
								content : '<span style="color:#f00">添加成功!</span>',
								init : function() {
									var that = this, i = 1;
									var fn = function() {
										!i && that.close();
										i--;
									};
									timer = setInterval(fn, 1000);
									fn();
								},
								close : function() {
									clearInterval(timer);
									location.reload();
								}
							}).show();			
						}
						else{
							art.dialog.alert("添加失败！");
							return false;
						}
					},
					error : function(xhr, textStatus, errorThrown) {
						art.dialog.alert('<span style="color:#f00">添加失败！</span>', 1);
						return false;
					}
				});
			}
		});
	});
	
	//添加课程
	$("#btn_add_pykttb").click(function(){
		var stuid = $("#id").val();
		art.dialog.open("<%=pre%>/b_studentbanji/add<%=sufStr%>?stuid=" + stuid,{
			title:"添加学生培优课堂同步",
			width:800,
			height:400,
			cancel: true,
			ok:function(){
				var iframe = this.iframe.contentWindow;
				if (!iframe.document.body) {
		        	alert('iframe还没加载完毕呢')
		        	return false;
		        };
				var stu_id = iframe.document.getElementById('stuid').value;
				var myselect=iframe.document.getElementById("banjiname");
				var index=myselect.selectedIndex;
				var banjiname =  myselect.options[index].value;
				$.ajax({
					type:"post",
					url:"<%=pre%>/b_studentbanji/insert<%=sufStr%>",
					data:{stu_id:stu_id,banjiname:banjiname},
					dataType:"json",
					success:function(data,status){
						if(data == 1){
							var timer;
							art.dialog({
								width:150,
								height:80,
								content : '<span style="color:#f00">添加成功!</span>',
								init : function() {
									var that = this, i = 1;
									var fn = function() {
										!i && that.close();
										i--;
									};
									timer = setInterval(fn, 1000);
									fn();
								},
								close : function() {
									clearInterval(timer);
									location.reload();
								}
							}).show();			
						}
						else{
							art.dialog.alert("添加失败！");
							return false;
						}
					},
					error : function(xhr, textStatus, errorThrown) {
						art.dialog.alert('<span style="color:#f00">添加失败！</span>', 1);
						return false;
					}
				});
			}
		});
	});
	
	
	
	
	$("#uploadFile").live('change',function(){
		var wjjname = 'student';
		var imgId = 'pic_img';
		var inputId = 'picture';
		var url = '<%=pre%>/b_com/fileupload<%=sufStr%>';
		var uploadPath = '<%=uploadPath%>/';
		uploadFile(url,uploadPath,wjjname,imgId,inputId);
	});
});

function editPage(url){
	art.dialog.open(url,{
		title:"编辑学生课程",
		width:800,
		height:400,
		cancel: true,
		ok:function(){
			var iframe = this.iframe.contentWindow;
			if (!iframe.document.body) {
	        	alert('iframe还没加载完毕呢')
	        	return false;
	        };
	        var id = iframe.document.getElementById('id').value;
			var stuid = iframe.document.getElementById('stuid').value;
			var name = iframe.document.getElementById('name').value;
			var subject = iframe.document.getElementById('subject').value;
			var catalog = iframe.document.getElementById('catalog').value;
			var grade = iframe.document.getElementById('grade').value;
			$.ajax({
				type:"post",
				url:"<%=pre%>/b_student_kc/update<%=sufStr%>",
				data:{id:id,stuid:stuid,name:name,grade:grade,subject:subject,catalog:catalog},
				dataType:"json",
				success:function(data,status){
					if(data.ret == '1'){
						var timer;
						art.dialog({
							width:150,
							height:80,
							content : '<span style="color:#f00">编辑成功!</span>',
							init : function() {
								var that = this, i = 1;
								var fn = function() {
									!i && that.close();
									i--;
								};
								timer = setInterval(fn, 1000);
								fn();
							},
							close : function() {
								clearInterval(timer);
								location.reload();
							}
						}).show();			
					}
					else{
						art.dialog.alert("编辑失败！");
						return false;
					}
				},
				error : function(xhr, textStatus, errorThrown) {
					art.dialog.alert('<span style="color:#f00">编辑失败！</span>', 1);
					return false;
				}
			});
		}
	});
}

function editStudentBanji(url){
	art.dialog.open(url,{
		title:"编辑我的培优课堂同步",
		width:800,
		height:400,
		cancel: true,
		ok:function(){
			var iframe = this.iframe.contentWindow;
			if (!iframe.document.body) {
	        	alert('iframe还没加载完毕呢')
	        	return false;
	        };
	        var id = iframe.document.getElementById('id').value;
	        var stu_id = iframe.document.getElementById('stuid').value;
			var myselect=iframe.document.getElementById("banjiname");
			var index=myselect.selectedIndex;
			var banjiname =  myselect.options[index].value;
			$.ajax({
				type:"post",
				url:"<%=pre%>/b_studentbanji/update<%=sufStr%>",
				data:{id:id,stu_id:stu_id,banjiname:banjiname},
				dataType:"json",
				success:function(data,status){
					if(data == '1'){
						var timer;
						art.dialog({
							width:150,
							height:80,
							content : '<span style="color:#f00">编辑成功!</span>',
							init : function() {
								var that = this, i = 1;
								var fn = function() {
									!i && that.close();
									i--;
								};
								timer = setInterval(fn, 1000);
								fn();
							},
							close : function() {
								clearInterval(timer);
								location.reload();
							}
						}).show();			
					}
					else{
						art.dialog.alert("编辑失败！");
						return false;
					}
				},
				error : function(xhr, textStatus, errorThrown) {
					art.dialog.alert('<span style="color:#f00">编辑失败！</span>', 1);
					return false;
				}
			});
		}
	});
}
</script>
</head>
<body>
<div class="content">
	<div class="content_title">
		学生编辑页面
	</div>
	<div>
		<table class="tbl001">
			<tr>
				<td style="width:150px;">用户编号：</td>
				<td>
					<input type="hidden" value="${bean.id }" id="id"/>
					<input type="text" name="stuid" id="stuid"  value="${bean.stuid }"/>
					<span style="color:red">*</span>
				</td>
			</tr>
			<tr>
				<td>姓名：</td>
				<td>
					<input type="text" name="name" id="name" value="${bean.name }"/>
					<span style="color:red">*</span>
				</td>
			</tr>
			<tr>
				<td>密码:</td>
				<td>
					<input type="password" name="password" id="password" value="${bean.password }"/>
					<input type="hidden" id="opwd" value="${bean.password }">
					<span style="color:red">*</span>
				</td>
			</tr>
			<tr>
				<td>确认密码:</td>
				<td>
					<input type="password" name="password2" id="password2" value="${bean.password }"/>
					<span style="color:red">*</span>
				</td>
			</tr>
			
			<tr>
				<td>性别：</td>
				<td>
					<select name="sex" id="sex">
						<option <c:if test="${bean.sex == '男' }">selected="selected"</c:if>>男</option>
						<option <c:if test="${bean.sex == '女' }">selected="selected"</c:if>>女</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>电话：</td>
				<td>
					<input type="text" name="phone" id="phone" value="${bean.phone }" />
				</td>
			</tr>
			<tr>
				<td>学校：</td>
				<td>
					<input type="text" name="school" id="school" value="${bean.school }" />
				</td>
			</tr>
			<tr>
				<td>登入系统状态：</td>
				<td>
					<select id="zhuangtai">
						<option value="1" <c:if test="${bean.zhuangtai == 1 }">selected='selected'</c:if>>允许</option>
						<option value="2" <c:if test="${bean.zhuangtai == 2 }">selected='selected'</c:if>>拒绝</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>开放学霸档案：</td>
				<td>
					<select id="kaiqidangan">
						<option value="0" <c:if test="${bean.kaiqidangan == 0 }">selected='selected'</c:if>>拒绝</option>
						<option value="1" <c:if test="${bean.kaiqidangan == 1 }">selected='selected'</c:if>>开放</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>头像：</td>
				<td>
					<input type="file" name="uploadFile" id="uploadFile">
					<img src="<%=uploadPath %>/${bean.picture}" id="pic_img" width="100px" height="100px">
					<input type="hidden" id="picture" value="${bean.picture}">
				</td>
			</tr>
			<tr>
				<td>
					<input type="button" value="保存" class="button" id="saveButton" /> 
					<input type="button" value="返回" class="button" name="goback" onclick="window.history.go(-1);" />
				</td>
				<td></td>
			</tr>
		</table>
	</div>
	
	<!-- 我的课程 -->
	<div class="content_title" style="margin-top:10px;">
		我的课程
	</div>
	<div>
		<input  type="button" class="btn_add" id="btn_add_kc" value="添加我的课程" />
	</div>
	<div>
		<table class="tbl002" >
			<tr>
				<td>序号</td>
				<td>课程名称</td>
				<td>年级</td>
				<td>科目</td>
				<td>插入时间</td>
				<td>操作</td>
			</tr>
			<c:forEach items="${listKc}" var="obj" varStatus="stauts">
				<tr bgcolor="#FFFFFF" align="center">
					<td>${stauts.index +1}</td>
					<td>${obj.name}</td>
					<td>${obj.grade}</td>
					<td>${obj.subject }</td>
					<td>${obj.inserttime }</td>
					<td>
						<a href="javascript:void(0);" onclick="javascript:editPage('<%=pre%>/b_student_kc/edit<%=sufStr%>?id=${obj.id }')">编辑详情</a> | 
						<a href="javascript:deleteConfirm('${obj.id }','<%=pre%>/b_student_kc/delete<%=sufStr%>?id=${obj.id}');">删除</a></td>
				</tr>
			</c:forEach>
		</table>
	</div>
	
	<!-- 我的培优课堂同步 -->
	<div class="content_title" style="margin-top:10px;">
		我的培优课堂同步
	</div>
	<div>
		<input  type="button" class="btn_add" id="btn_add_pykttb" value="添加我的培优课堂同步" />
	</div>
	<div>
		<table class="tbl002" >
			<tr>
				<td>序号</td>
				<td>教师名称</td>
				<td>培优课堂同步名称</td>
				<td>插入时间</td>
				<td>操作</td>
			</tr>
			<c:forEach items="${listSB}" var="obj" varStatus="stauts">
				<tr bgcolor="#FFFFFF" align="center">
					<td>${stauts.index +1}</td>
					<td>${obj.teaname }</td>
					<td>${obj.banjiname}</td>
					<td>${obj.inserttime }</td>
					<td>
						<a href="javascript:void(0);" onclick="javascript:editStudentBanji('<%=pre%>/b_studentbanji/edit<%=sufStr%>?id=${obj.id }')">编辑详情</a> | 
						<a href="javascript:deleteConfirm('${obj.id }','<%=pre%>/b_studentbanji/delete<%=sufStr%>?id=${obj.id}');">删除</a></td>
				</tr>
			</c:forEach>
		</table>
	</div>
</div>
<div style="margin-bottom:50px;"></div>
</body>
</html>