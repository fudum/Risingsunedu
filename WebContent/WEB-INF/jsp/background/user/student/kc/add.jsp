<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>学生档案-课程添加</title>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/fbackground.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
<script type="text/javascript" src="<%=jsPath%>/fileupload.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#uploadFile").live('change',function(){
		var wjjname = 'student';
		var imgId = 'pic_img';
		var inputId = 'catalog';
		var url = '<%=pre%>/b_com/fileupload<%=sufStr%>';
		var uploadPath = '<%=uploadPath%>/';
		uploadFile(url,uploadPath,wjjname,imgId,inputId);
	});
	
	$("input[name='subject']").click(function(){
		$("#subject").val($("input[name=subject]:checked").val());
	});
	
	$("input[name='grade']").click(function(){
		$("#grade").val($("input[name=grade]:checked").val());
	});
});
</script>
</head>
<body>
	<div class="content">
		<div style="margin-top:10px;">
			<input type="hidden" value="${stuid}" id="stuid">
			<table class="tbl001">
			<tr>
				<td>课程名称：</td>
				<td>
					<input type="text" id="name" style="width:600px"/>
				</td>
			</tr>
			<tr>
				<td>年级：</td>
				<td>
					<input type="hidden" id="grade">					
					<c:forEach items="${listGrade}" var="obj">
						<input type="radio" name="grade" value="${obj.name }"/>${obj.name}
					</c:forEach>
				</td>
			</tr>
			<tr>
				<td>科目：</td>
				<td>
					<input type="hidden" id="subject"> 
					<c:forEach items="${listSubject }" var="obj">
						<input type="radio" name="subject" value="${obj.name}"/>${obj.name}
					</c:forEach>
				</td>
			</tr>
			<tr>
				<td>课程目录：</td>
				<td>
					<input type="file" id="uploadFile" name='uploadFile'/>
					<img alt="" src="" id="pic_img">
					<input type="hidden" id="catalog">
				</td>
			</tr>
			</table>
		</div>
	</div>
</body>
</html>