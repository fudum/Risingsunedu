<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>后台管理-培优作业列表</title>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css"/>
<link rel="stylesheet" type="text/css" href="<%=cssPath %>/fbackground.css" />
<script>
$(document).ready(function(){
	//添加课程
	$("#btn_add_py").click(function(){
		var stuid = $("#stuid").val();
		art.dialog.open("<%=pre%>/b_ceshi/add<%=sufStr%>?stuid=" + stuid,{
			title:"添加培优测试",
			width:800,
			height:400,
			cancel: true,
			ok:function(){
				var iframe = this.iframe.contentWindow;
				if (!iframe.document.body) {
		        	alert('iframe还没加载完毕呢')
		        	return false;
		        };
				var stuid = iframe.document.getElementById('stuid').value;
				var name = iframe.document.getElementById('name').value;
				var subject = iframe.document.getElementById('subject').value;
				var finishpath = iframe.document.getElementById('finishpath').value;
	
				$.ajax({
					type:"post",
					url:"<%=pre%>/b_ceshi/insert<%=sufStr%>",
					data:{stuid:stuid,name:name,subject:subject,finishpath:finishpath},
					dataType:"json",
					success:function(data,status){
						if(data.ret == '1'){
							var timer;
							art.dialog({
								width:150,
								height:80,
								content : '<span style="color:#f00">添加成功!</span>',
								init : function() {
									var that = this, i = 1;
									var fn = function() {
										!i && that.close();
										i--;
									};
									timer = setInterval(fn, 1000);
									fn();
								},
								close : function() {
									clearInterval(timer);
									location.reload();
								}
							}).show();			
						}
						else{
							art.dialog.alert("添加失败！");
							return false;
						}
					},
					error : function(xhr, textStatus, errorThrown) {
						art.dialog.alert('<span style="color:#f00">添加失败！</span>', 1);
						return false;
					}
				});
			}
		});
	});
	
});
function link() {
	$("#fom").submit();
}

function editPage(url){
	art.dialog.open(url,{
		title:"编辑培优测试",
		width:800,
		height:400,
		cancel: true,
		ok:function(){
			var iframe = this.iframe.contentWindow;
			if (!iframe.document.body) {
	        	alert('iframe还没加载完毕呢')
	        	return false;
	        };
	        var id = iframe.document.getElementById('id').value;
			var stuid = iframe.document.getElementById('stuid').value;
			var name = iframe.document.getElementById('name').value;
			var subject = iframe.document.getElementById('subject').value;
			var finishpath = iframe.document.getElementById('finishpath').value;

			$.ajax({
				type:"post",
				url:"<%=pre%>/b_ceshi/update<%=sufStr%>",
				data:{id:id,stuid:stuid,name:name,subject:subject,finishpath:finishpath},
				dataType:"json",
				success:function(data,status){
					if(data.ret == '1'){
						var timer;
						art.dialog({
							width:150,
							height:80,
							content : '<span style="color:#f00">编辑成功!</span>',
							init : function() {
								var that = this, i = 1;
								var fn = function() {
									!i && that.close();
									i--;
								};
								timer = setInterval(fn, 1000);
								fn();
							},
							close : function() {
								clearInterval(timer);
								location.reload();
							}
						}).show();			
					}
					else{
						art.dialog.alert("编辑失败！");
						return false;
					}
				},
				error : function(xhr, textStatus, errorThrown) {
					art.dialog.alert('<span style="color:#f00">编辑失败！</span>', 1);
					return false;
				}
			});
		}
	});
}

function showZuoye(zuoyePath){
	art.dialog.open("<%=pre%>/b_ceshi/showZuoye<%=suf%>?zuoyePath=" +zuoyePath,{
		title:'测试试卷',
		width:840,
		height:430
	});
}
</script>
</head>
<body>
<div class="content">
	<div>
		<form name="fom" id="fom" method="post" action="<%=pre%>/b_ceshi/add<%=suf%>"></form>
	</div>
	<div>
		<input type="button" class="btn_add" value="添加培优测试" id="btn_add_py" />
		<input type= "hidden" value="${studentBean.id }" id="stuid"/>
		&nbsp;&nbsp;学生编号：${studentBean.stuid }&nbsp;&nbsp;学生姓名：${studentBean.name}
		<a href="<%=pre%>/b_student/index<%=suf%>">退出到学生界面</a>
	</div>
	<div>
		<table class="tbl002">
			<tr>
				<td>序号</td>	
				<td>测试名称</td>
				<td>科目</td>
				<td>测试试卷文件</td>
				<td>插入时间</td>
				<td>操作</td>
				<td>序号</td>
			</tr>
			<c:forEach items="${list}" var="obj" varStatus="stauts">
				<tr>
					<td>${stauts.index +1}</td>
					<td>${obj.name}</td>
					<td>${obj.subject}</td>
					<td>
						<c:choose>
							<c:when test="${fn:length(obj.finishpath) gt 0}">
								<a href="javascript:void(0);" onclick="showZuoye('${obj.finishpath}')">查看</a>
							</c:when>
							<c:otherwise>
								未上传
							</c:otherwise>
						</c:choose>
					</td>
					<td>${obj.inserttime }</td>
					<td>
						<a href="javascript:editPage('<%=pre%>/b_ceshi/edit<%=suf%>?id=${obj.id}');">编辑详情</a> | 
						<a href="javascript:deleteConfirm('${obj.id }','<%=pre%>/b_ceshi/delete<%=suf%>?id=${obj.id}');">删除</a>
					</td>
					<td>${stauts.index +1}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</div>
</body>
</html>