<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/fbackground.css" />
<title>后台管理-作业编辑</title>
<script>
$(document).ready(function(){
	//上传作业
	$("#originalFile").live('change',function(){
		var picFile = $("#originalFile").val();
		if(picFile == ""){
			art.dialog.alert("请选择文件!");
			return;
		}
		else{
			var strFilter = ".pdf|.PDF|";
			if(picFile.indexOf(".") > -1){
				var p = picFile.lastIndexOf(".");
				var strPostfix = picFile.substring(p,picFile.length) + '|';
				strPostfix = strPostfix.toLowerCase();
				if(strFilter.indexOf(strPostfix) > -1){
					$.ajaxFileUpload({
						url: '<%=pre%>/b_zuoye/uploadOriginal<%=suf%>', 
						type: 'post',
						fileElementId: 'originalFile',
						dataType:'json', 
						success: function(data, status){ 
				            	if(data.ret == 1){
				            		$("#tip_originalpath").html("文件："+data.path);
				            		$("#originalpath").val("student/peiyouzuoye/" +data.path);
				            	}
				            	else{
				            		art.dialog.alert("添加失败，请重新上传！");
				            	}
				            },
				            error: function(data, status, e){ 
				            	art.dialog.tips('<span style="color:#f00">添加失败，请重新上传！</span>', 1);
				            }
					});
				}else{
					art.dialog.alert("请选择：pdf格式的文件！");
				}
			}
		}
	});
	
	//上传学生完成情况
	$("#finishFile").live('change',function(){
		var picFile = $("#finishFile").val();
		if(picFile == ""){
			art.dialog.alert("请选择文件!");
			return;
		}
		else{
			var strFilter = ".pdf|.PDF|";
			if(picFile.indexOf(".") > -1){
				var p = picFile.lastIndexOf(".");
				var strPostfix = picFile.substring(p,picFile.length) + '|';
				strPostfix = strPostfix.toLowerCase();
				if(strFilter.indexOf(strPostfix) > -1){
					$.ajaxFileUpload({
						url: '<%=pre%>/b_zuoye/uploadFinish<%=suf%>', 
						type: 'post',
						fileElementId: 'finishFile',
						dataType:'json', 
						success: function(data, status){ 
				            	if(data.ret == 1){
				            		$("#tip_finishpath").html("文件："+data.path);
				            		$("#finishpath").val("student/peiyouzuoye/" + data.path);
				            	}
				            	else{
				            		art.dialog.alert("上传失败，请重新上传！");
				            	}
				            },
				            error: function(data, status, e){ 
				            	art.dialog.tips('<span style="color:#f00">上传失败，请重新上传！</span>', 1);
				            }
					});
				}else{
					art.dialog.alert("请选择：pdf格式的文件！");
				}
			}
		}
	});
	
	$("input[name=subject]").click(function(){
		$("#subject").val($("input[name=subject]:checked").val());
	});
});
</script>
</head>
<body>
	<div class="content">
		<div style="margin-top:10px;">
			<input type="hidden" value="${bean.id}" id="id">
			<input type="hidden" value="${bean.stuid}" id="stuid">
			<table class="tbl001">
			<tr>
				<td>测试名称：</td>
				<td>
					<input type="text" id="name" style="width:600px" value="${bean.name }"/>
				</td>
			</tr>
			<tr>
				<td>科目：</td>
				<td>
					<input type="hidden" id="subject" value="${bean.subject }"> 
					<c:forEach items="${listSubject }" var="obj">
						<input type="radio" name="subject" value="${obj.name}" 
						  <c:if test="${bean.subject == obj.name }">checked="checked"</c:if> />${obj.name}
					</c:forEach>
				</td>
			</tr>
			
			<tr>
				<td>测试试卷(pdf)：</td>
				<td>
					<input type="file" id="finishFile" name='finishFile'/>
					<span id="tip_finishpath">${bean.finishpath}</span>
					<input type="hidden" id="finishpath" value="${bean.finishpath }" />
				</td>
			</tr>
			</table>
		</div>
	</div>
</body>
</html>