<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/include/b_common_include.jsp" %>
<!DOCTYPE HTML >
<html>
<head>
	<meta charset=UTF-8>
	<title>中考类别管理</title>
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css"/>
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css"/>
	<script>
		function link() {
			document.getElementById("fom").action = "../medtestcatg/add<%=sufStr%>";
			document.getElementById("fom").submit();
		}
	</script>
</head>
<body>
	<div>
		<form id="fom" method="post"></form>
	</div>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td height="30">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="62" background="<%=imagePath%>/nav04.gif">
							<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
								<tr>
									<td width="21"><img src="<%=imagePath%>/ico07.gif" width="20" height="18" /></td>
									<td width="538">查看方式： <select>
											<option value="all">全部</option>
											<option value="userid">教师编号</option>
											<option value="username">教师姓名</option>
											<option value="usersex">性别</option>
									</select><input name="textfield" type="text" size="12" /> <input name="Submit4" type="button" class="right-button02" value="查 询" /></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table id="subtree1" style="DISPLAY:" width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
								<tr>
									<td height="20"><input type="button" class="right-button08" value="添加类别" onclick="link();" /></td>
								</tr>
								<tr>
									<td height="40" class="font42">
										<div>
											<table width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#464646" class="newfont03">
												<tr class="CTitle">
													<td height="22" colspan="7" align="center" style="font-size: 16px">中考信息详细列表</td>
												</tr>
												<tr bgcolor="#EEEEEE" align="center">
													<td width="4%">序号</td>
													<td width="10%">类别名称</td>
													<td width="10%">排列顺序</td>
													<td width="12%">操作</td>
												</tr>
												<c:forEach items="${list}" var="obj" varStatus="stauts">
													<tr bgcolor="#FFFFFF" align="center">
														<td>${stauts.index +1}</td>
														<td>${obj.name }</td>
														<td>${obj.sequence}</td>														
														<td>
															<a href="../medtestcatg/edit<%=sufStr%>?id=${obj.id }">编辑详情</a> | 
															<a href="javascript:deleteConfirm('${obj.id }','../medtestcatg/delete<%=sufStr%>?id=${obj.id}');">删除</a></td>
													</tr>
												</c:forEach>
											</table>
										</div>
									</td>
								</tr>
							</table>
							<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
								<tr>
									<td height="6"><img src="<%=imagePath%>/spacer.gif" width="1" height="1" /></td>
								</tr>
								<tr>
									<td height="33"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="right-font08">
											<tr>
												<td width="50%">
													共 <span class="right-text09">${totalPage}</span>
													 页 | 第 <span class="right-text09">${countPerPage }</span> 页
												</td>
												<td width="49%" align="right">
													[<a href="../user/usermanage.do" class="right-font08">首页</a> | 
													<a href="../user/frontpage.do?pageNum=${countPerPage}&&totalPage=${totalPage}" class="right-font08">上一页</a> | 
													<a href="../user/nextpage.do?pageNum=${countPerPage}&&totalPage=${totalPage}" class="right-font08">下一页</a> | 
													<a href="../user/lastpage.do?totalPage=${totalPage}" class="right-font08">末页</a>] 
												</td>
												<td width="1%"></td>
											</tr>
										</table></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>