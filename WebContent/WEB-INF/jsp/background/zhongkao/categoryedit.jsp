<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<!DOCTYPE HTML >
<html>
<head>
	<meta charset=UTF-8>
	<title>中考类别编辑</title>
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
	<script type="text/javascript">
		$(document).ready(
			//点击提交按钮时候
			function() {
				$("#saveButton").click(
					function() {
						//教师姓名是否为空
						var name = $("#name").val();
						if (name == "") {
							$("#nametip").html("*请输入类别名称！");
							$("#name")[0].focus();
							return false;
						}
						//序号是否为空
						var sequence = $("#sequence").val();
						if (sequence == ""){
							$("#sequencetip").html("*请输入序号");
							$("sequence")[0].focus();
							return false;
						}
						$("#form1").submit();
					});
				
				//点击返回按钮
				$("#goBack").click(
					function(){
						$("#form2").submit();
					});
		});
	</script>
</head>
<body class="ContentBody">
<form id="form2" action="../medtestcatg/manage.do" method="post"></form>
	<form id="form1" action="../medtestcatg/update.do" method="post">
		<div class="MainDiv">
			<input type="hidden" name="id" value="${bean.id }">
			<table width="99%" border="0" cellpadding="0" cellspacing="0" class="CContent">
				<tr>
					<th class="tablestyle_title">编辑类别页面</th>
				</tr>
				<tr>
					<td class="CPanel">
						<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
							<TR>
								<td width="100%">
									<fieldset style="height: 100%;">
										<legend>类别编辑</legend>
										<div>
											<table class="tb_1" border="0" cellpadding="2" cellspacing="1" style="width: 100%">
												<tr>
													<td nowrap align="right" width="40%">类别名称：</td>
													<td width="60%">
														<input id="name" name="name" style="width: 150px" type="text" value="${bean.name }" />
														<span class="red" id="nametip">*</span>
													</td>
												</tr>
												<tr>
													<td align="right" >类别排序：</td>
													<td>
														<input id="sequence" name="sequence" type="text" style="width:150px" value="${bean.sequence }" />
														<span class="red" id="sequencetip">*</span>
													</td>													
												</tr>
												<tr>
													<td align="right" >
														<label style="color:red">
																${retVal}
														</label>
													</td>
																									
												</tr>
											</table>
										</div>
										<br />
									</fieldset>
								</td>
							</TR>
						</TABLE>
					</td>
				</tr>
				<TR>
					<td colspan="2" align="center" height="50px">
						<input type="button" value="保存" class="button" id="saveButton" />
						<input type="button" value="返回" class="button" name="goback" id="goBack"/>
					</td>
				</TR>
			</TABLE>
		</div>
	</form>
</body>
</html>