<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<%
request.setCharacterEncoding("UTF-8");
String hdValue = request.getParameter("neirong") != null ? request.getParameter("neirong") : "";
%>
<!DOCTYPE HTML >
<html>
<head>
	<meta charset=UTF-8>
	<title>中考发布新闻</title>
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
	<link rel="stylesheet" href="<%=kePath %>/themes/default/default.css" />
	<link rel="stylesheet" href="<%=kePath %>/plugins/code/prettify.css" />
	<script charset="utf-8" src="<%=kePath %>/kindeditor.js"></script>
	<script charset="utf-8" src="<%=kePath %>/lang/zh_CN.js"></script>
	<script charset="utf-8" src="<%=kePath %>/plugins/code/prettify.js"></script>
	<script type="text/javascript">
		$(document).ready(
			//点击提交按钮时候
			function() {
				$("#saveButton").click(
					function() {
						//教师姓名是否为空
						var name = $("#name").val();
						if (name == "") {
							$("#nametip").html("*请输入类别名称！");
							$("#name")[0].focus();
							return false;
						}
						//序号是否为空
						var sequence = $("#sequence").val();
						if (sequence == ""){
							$("#sequencetip").html("*请输入序号");
							$("sequence")[0].focus();
							return false;
						}
						$("#form1").submit();
					});
				
				//点击返回按钮
				$("#goBack").click(
					function(){
						$("#form2").submit();
					});
				
				$("input[name='medtesttype']").change(
					function(){
						var flag = false;
						$("input[name='medtesttype']:checked").each(function(){
							if($(this).val() == '名校动态'){
								flag = true;
							}
						});
						if(flag == true){
							$('#trschool').removeClass('dn');
						}
						else{
							$('#trschool').addClass('dn');
						}
					});
		});
		
		KindEditor.ready(function(K) {
			var editor1 = K.create('textarea[name="neirong"]', {
				cssPath : '<%=kePath %>/plugins/code/prettify.css',
				uploadJson : '<%=kePath %>/jsp/upload_json.jsp',
				fileManagerJson : '<%=kePath %>/jsp/file_manager_json.jsp',
				allowFileManager : true,
				afterCreate : function() {
					var self = this;
					K.ctrl(document, 13, function() {
						self.sync();
						document.forms['form1'].submit();
					});
					K.ctrl(self.edit.doc, 13, function() {
						self.sync();
						document.forms['form1'].submit();
					});
				}
			});
			prettyPrint();
		});
	</script>
	<style>
	.dn{
		display:none;
	}
	</style>
</head>
<body class="ContentBody">
	<form id="form2" action="../xinwen/manage<%=sufStr%>" method="post"></form>
	<form id="form1" action="../xinwen/insert<%=sufStr%>" method="post" enctype="multipart/form-data">
		<div class="MainDiv">
			<table width="99%" border="0" cellpadding="0" cellspacing="0" class="CContent">
				<tr>
					<th class="tablestyle_title">新闻添加页面</th>
				</tr>
				<tr>
					<td class="CPanel">
						<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
							<TR>
								<td width="100%">
									<fieldset style="height: 100%;">
										<legend>新闻添加</legend>
										<div>
											<table class="tb_1" border="0" cellpadding="2" cellspacing="1" style="width: 100%">
												<tr>
													<td nowrap align="right" width="8%">新闻标题:</td>
													<td width="90%">
														<input id="name" name="title" style="width: 400px" type="text" />
														<span class="red" id="nametip">*</span>
													</td>
												</tr>
												<tr>
													<td align="right" >新闻类别：</td>
													<td>
														<c:forEach items="${xinwenTypeList}" var="obj">
															<input type="checkbox" name="medtesttype" value="${obj.name}" />${obj.name}
														</c:forEach>
													</td>													
												</tr>
												<tr id="trschool" class="dn" >
													<td align="right">学校名称：</td>
													<td>
														<c:forEach items="${schoolList }" var="obj">
															<input type="radio" name="school" value="${obj.name }"/>${obj.name }
														</c:forEach>
													</td>
												</tr>
												<tr>
													<td align="right">新闻内容：</td>
													<td>
														<%=hdValue %>
														<div>
															<textarea id="neirong" name="neirong" style="width:900px;height:800px;"><%=htmlspecialchars(hdValue)%></textarea>
														</div>
													</td>
												</tr>
											</table>
										</div>
										<br />
									</fieldset>
								</td>
							</TR>
						</TABLE>
					</td>
				</tr>
				<TR>
					<td colspan="2" align="center" height="50px">
						<input type="submit" value="保存" class="button" id="saveButton" />
						<input type="button" value="返回" class="button" name="goback" id="goBack"/>
					</td>
				</TR>
			</TABLE>
		</div>
	</form>
</body>
</html>
<%!
private String htmlspecialchars(String str) {
	str = str.replaceAll("&", "&amp;");
	str = str.replaceAll("<", "&lt;");
	str = str.replaceAll(">", "&gt;");
	str = str.replaceAll("\"", "&quot;");
	return str;
}
%>