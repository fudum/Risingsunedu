<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>添加试卷</title>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/fbackground.css" />
<script>
$(document).ready(function(){
	//上传图片
	$("#uploadFile").live('change',function(){
		var picFile = $("#uploadFile").val();
		var wjjname = "shijuan";
		if(picFile == ""){
			art.dialog.alert("请选择文件!");
			return;
		}
		else{
			var strFilter = ".pdf|.PDF|";
			if(picFile.indexOf(".") > -1){
				var p = picFile.lastIndexOf(".");
				var strPostfix = picFile.substring(p,picFile.length) + '|';
				strPostfix = strPostfix.toLowerCase();
				if(strFilter.indexOf(strPostfix) > -1){
					$.ajaxFileUpload({
						url: '<%=pre%>/b_com/fileupload<%=sufStr%>', 
						data:{wjjname:wjjname},
						type: 'post',
						fileElementId: 'uploadFile',
						dataType:'json', //返回值类型，一般设置为json、application/json
						success: function(data, status){ 
				            	if(data.ret == 1){
				            		$("#sjpathtip").html("文件："+data.path);
				            		$("#sjpath").val(data.path);
				            	}
				            	else{
				            		art.dialog.alert("上传失败！");
				            	}
				            },
				            error: function(data, status, e){ 
				            	art.dialog.tips('<span style="color:#f00">上传失败，请重传！</span>', 1);
				            }
					});
				}else{
					art.dialog.alert("请选择：pdf格式的文件！");
				}
			}
		}
	});
	
	//上传图片
	$("#wordFile").live('change',function(){
		var picFile = $("#wordFile").val();
		var wjjname = "shijuan";
		if(picFile == ""){
			art.dialog.alert("请选择文件!");
			return;
		}
		else{
			var strFilter = ".doc|.docx|";
			if(picFile.indexOf(".") > -1){
				var p = picFile.lastIndexOf(".");
				var strPostfix = picFile.substring(p,picFile.length) + '|';
				strPostfix = strPostfix.toLowerCase();
				if(strFilter.indexOf(strPostfix) > -1){
					$.ajaxFileUpload({
						url: '<%=pre%>/b_shijuan/wordupload<%=sufStr%>', 
						data:{wjjname:wjjname},
						type: 'post',
						fileElementId: 'wordFile',
						dataType:'json', //返回值类型，一般设置为json、application/json
						success: function(data, status){ 
				            	if(data.ret == 1){
				            		$("#wordpathtip").html("文件："+data.path);
				            		$("#wordpath").val(data.path);
				            	}
				            	else{
				            		art.dialog.alert("上传失败！");
				            	}
				            },
				            error: function(data, status, e){ 
				            	art.dialog.tips('<span style="color:#f00">上传失败，请重传！</span>', 1);
				            }
					});
				}else{
					art.dialog.alert("请选择：word格式的文件！");
				}
			}
		}
	});
	
	$("#saveButton").click(function() {
		var grade = $("input[name='grade']:checked").val();
		var subject = $("input[name='subject']:checked").val();
		var year  = $("input[name='year']:checked").val();
		var stype = $("input[name='stype']:checked").val();
		var xueqi = $("input[name='xueqi']:checked").val();
		var downnum = $("#downnum").val();
		var name = $("#name").val();
		var sjpath = $("#sjpath").val();
		var wordpath = $("#wordpath").val();
		
		//ajax提交
		$.ajax({
			url : "<%=pre%>/b_shijuan/insert<%=sufStr%>",
			type : 'post',
			dataType : 'html',
			data : {grade:grade,subject:subject,year:year,stype:stype,xueqi:xueqi,downnum:downnum,name:name,sjpath:sjpath,wordpath:wordpath},
			success : function(data, status) {
				if (status == 'success') {
					if (data == '1') {
						var timer;
						art.dialog({
							content : '<span style="color:#f00">插入成功！</span>',
							init : function() {
								var that = this, i = 1;
								var fn = function() {
									!i && that.close();
									i--;
								};
								timer = setInterval(fn, 1000);
								fn();
							},
							close : function() {
								clearInterval(timer);
								window.location.href="<%=pre%>/b_shijuan/index<%=sufStr%>";
							}
						}).show();	
					} else {
						art.dialog.tips('<span style="color:#f00">插入失败！</span>', 1);
					}
				}
			},
			error : function(xhr, textStatus,
					errorThrown) {
				art.dialog.tips('<span style="color:#f00">插入失败！</span>', 1);
			}
		});
	});
	
});
</script>
</head>
<body>
<div class="content">
	<div class="content_title">
		试卷添加页面
	</div>
	<div>
		<table class="tbl001">
			<tr>
				<td style="width:150px;">试卷年份：</td>
				<td>
					<c:forEach items="${listNianFen }" var="obj">
						<input type="radio" name="year" value="${obj.name }" >${obj.name }
					</c:forEach>
				</td>
			</tr>
			<tr>
				<td>年级：</td>
				<td>
					<c:forEach items="${listGrade }" var="obj">
						<input type="radio" name="grade" value="${obj.name }" >${obj.name }
					</c:forEach>
				</td>
			</tr>
			<tr>
				<td>科目:</td>
				<td>
					<c:forEach items="${listSubject }" var="obj">
						<input type="radio" name="subject" value="${obj.name }" >${obj.name }
					</c:forEach>
				</td>
			</tr>
			<tr>
				<td>试卷类型：</td>
				<td>
					<c:forEach items="${listLeixing }" var="obj">
						<input type="radio" name="stype" value="${obj.name }" >${obj.name }
					</c:forEach>
				</td>
			</tr>
			<tr>
				<td>学期：</td>
				<td>
					<input type="radio" name="xueqi" value="上学期" >上学期
					<input type="radio" name="xueqi" value="下学期" >下学期
				</td>
			</tr>
			<tr>
				<td>试卷名称：</td>
				<td>
					<input type="text" id="name" name="name" style="width:400px;"/>
				</td>
			</tr>
			<tr>
				<td>下载次数：</td>
				<td>
					<input type="text" id="downnum" name = "downnum"/>
				</td>
			</tr>
			<tr>
				<td>试卷pdf：</td>
				<td>
					<input type="file" name="uploadFile" id="uploadFile">
					<span id="sjpathtip" style="color:red"></span>
					<input type="hidden" name = "sjpath" id="sjpath" > 
				</td>
			</tr>
			<tr>
				<td>试卷word：</td>
				<td>
					<input type="file" name="wordFile" id="wordFile">
					<span id="wordpathtip" style="color:red"></span>
					<input type="hidden" name="wordpath" id="wordpath" > 
				</td>
			</tr>
		</table>
	</div>
	<div>
		<input type="button" value="保存" class="button" id="saveButton" /> 
		<input type="button" value="返回" class="button" name="goback" onclick="window.history.go(-1);" />
	</div>
</div>
</body>
</html>