<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>试卷首页</title>
<!-- 样式表 -->
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css"  />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css"  />
<link rel="stylesheet" type="text/css" href="<%=cssPath %>/fbackground.css" />
<script>
	function link() {
		$("#fom").submit();
	}
</script>
</head>
<body>
<div class="content">
	<div>
		<form name="fom" id="fom" method="post" action="<%=pre%>/b_shijuan/add<%=sufStr%>"></form>
	</div>
	<div>
		<input type="button" class="btn_add" value="添加试卷" onclick="link();" />
	</div>
	<div>
		<table class="tbl002">
			<tr>
				<td>序号</td>	
				<td>年份</td>			
				<td>年级</td>
				<td>科目</td>
				<td>试卷类型</td>
				<td>学期</td>
				<td>试卷名称</td>
				<td>下载次数</td>
				<td>插入时间</td>
				<td>操作</td>
				<td>序号</td>
			</tr>
			<c:forEach items="${pager.pageList}" var="obj" varStatus="stauts">
				<tr>
					<td>${stauts.index +1}</td>
					<td>${obj.year}</td>
					<td>${obj.grade}</td>
					<td>${obj.subject}</td>
					<td>${obj.stype}</td>
					<td>${obj.xueqi}</td>
					<td>${obj.name}</td>
					<td>${obj.downnum }</td>
					<td>${obj.inserttime }</td>
					<td>
						<a href="<%=pre%>/b_shijuan/edit<%=sufStr%>?id=${obj.id }">编辑详情</a> | 
						<a href="javascript:deleteConfirm('${obj.id }','<%=pre%>/b_shijuan/delete<%=sufStr%>?id=${obj.id}');">删除</a>
					</td>
					<td>${stauts.index +1}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
	
	<!-- 分页条 -->
	<div>
		<div class="manu">
			<c:if test="${pager.totalPage >0}">
				<!-- prev -->
				<c:choose>
					<c:when test="${pager.curtPage == 1 }">
						<span class="disabled">&lt;Prev</span>
					</c:when>
					<c:otherwise>
						<a href="<%=pre %>/b_shijuan/index<%=sufStr%>?curtPage=${pager.curtPage - 1}">&lt; Prev </a>
					</c:otherwise>
				</c:choose>
				
				<!-- content -->
				<c:if test="${pager.totalPage < 7 }">
					<c:forEach var = "item" begin="1" end = "${pager.totalPage }">
						<c:choose>
							<c:when test="${pager.curtPage == item }">
							  	<span class="current">${item }</span>
							</c:when>
							<c:otherwise>
								<a href="<%=pre %>/b_shijuan/index<%=sufStr%>?curtPage=${item}">${item }</a>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</c:if>
				<c:if test="${pager.totalPage >= 7 }">
					<c:if test="${pager.curtPage <= 4 }">
						<c:forEach var = "item" begin="1" end = "5">
							<c:choose>
								<c:when test="${pager.curtPage == item }">
								  	<span class="current">${item }</span>
								</c:when>
								<c:otherwise>
									<a href="<%=pre %>/b_shijuan/index<%=sufStr%>?curtPage=${item}">${item }</a>
								</c:otherwise>
							</c:choose>
						</c:forEach>
						...
						<a href="<%=pre %>/b_shijuan/index<%=sufStr%>?curtPage=${pager.totalPage}">${pager.totalPage}</a>
					</c:if>
					<c:if test="${pager.curtPage > 4 }">
						<a href="<%=pre %>/b_shijuan/index<%=sufStr%>?curtPage=1">1</a>
						...
						<c:choose>
								
								<c:when test="${pager.curtPage < pager.totalPage - 3 }">
									<c:forEach var = "item" begin="${pager.curtPage - 2 }" end = "${pager.curtPage + 2 }">
										<c:choose>
											<c:when test="${pager.curtPage == item }">
											  	<span class="current">${item }</span>
											</c:when>
											<c:otherwise>
												<a href="<%=pre %>/b_shijuan/index<%=sufStr%>?curtPage=${item}">${item }</a>
											</c:otherwise>
										</c:choose>
									</c:forEach>
									...
									<a href="<%=pre %>/b_shijuan/index<%=sufStr%>?curtPage=${pager.totalPage}">${pager.totalPage}</a>
								</c:when>
								<c:otherwise>
									<c:forEach var = "item" begin="${pager.totalPage - 4 }" end = "${pager.totalPage }">
										<c:choose>
											<c:when test="${pager.curtPage == item }">
											  	<span class="current">${item }</span>
											</c:when>
											<c:otherwise>
												<a href="<%=pre %>/b_shijuan/index<%=sufStr%>?curtPage=${item}">${item }</a>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</c:otherwise>
							</c:choose>
					</c:if>
				</c:if>
				<!-- next -->
				<c:choose>
					<c:when test="${pager.curtPage == pager.totalPage }">
						<span class="disabled">Next &gt;</span>
					</c:when>
					<c:otherwise>
						<a href="<%=pre %>/b_shijuan/index<%=sufStr%>?curtPage=${pager.curtPage + 1}">Next  &gt; </a>
					</c:otherwise>
				</c:choose>	
			</c:if>
		</div>
	</div><!-- 分页结束 -->
	
</div>
</body>
</html>