<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<!DOCTYPE HTML >
<html>
<head>
	<meta charset=UTF-8>
	<title>编辑学校信息</title>
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
	<script type="text/javascript">
		$(document).ready(
			//点击提交按钮时候
			function() {
				$("#saveButton").click(
					function() {
						//教师姓名是否为空
						var name = $("#name").val();
						if (name == "") {
							$("#nametip").html("*请输入类别名称！");
							$("#name")[0].focus();
							return false;
						}
						//序号是否为空
						var sequence = $("#sequence").val();
						if (sequence == ""){
							$("#sequencetip").html("*请输入序号");
							$("sequence")[0].focus();
							return false;
						}
						$("#form1").submit();
					});
				
				//点击返回按钮
				$("#goBack").click(
					function(){
						$("#form2").submit();
					});
		});
	</script>
</head>
<body class="ContentBody">
<form id="form2" action="../school/manage<%=sufStr%>" method="post"></form>
	<form id="form1" action="../school/update<%=sufStr%>" method="post">
		<div class="MainDiv">
			<input type="hidden" name="id" value="${bean.id }" />
			<table width="99%" border="0" cellpadding="0" cellspacing="0" class="CContent">
				<tr>
					<th class="tablestyle_title">编辑学校页面</th>
				</tr>
				<tr>
					<td class="CPanel">
						<table border="0" cellpadding="0" cellspacing="0" style="width: 100%">
							<TR>
								<td width="100%">
									<fieldset style="height: 100%;">
										<legend>学校编辑页面</legend>
										<div>
											<table class="tb_1" border="0" cellpadding="2" cellspacing="1" style="width: 100%">
												<tr>
													<td nowrap align="right" width="40%">学校名称：</td>
													<td width="60%">
														<input id="name" name="name" style="width: 150px" type="text" value="${bean.name}" />
														<span class="red" id="nametip">*</span>
													</td>
												</tr>
												<tr>
													<td align="right" >学校级别：</td>
													<td>
														<select name="jibie">
															<c:forEach items="${list}" var="obj">
																<option <c:if test="${bean.jibie == obj.name }">selected="selected"</c:if>>${obj.name }</option>
															</c:forEach>
														</select>
													</td>													
												</tr>
												<tr>
													<td align="right" >所属区域：</td>
													<td>
														<input id="area" name="area" style="width: 150px" type="text" value="${bean.area }" />
													</td>													
												</tr>
												<tr>
													<td align="right" >
														<label style="color:red">${retVal}</label>
													</td>
													<td>
													</td>													
												</tr>
											</table>
										</div>
										<br />
									</fieldset>
								</td>
							</TR>
						</TABLE>
					</td>
				</tr>
				<TR>
					<td colspan="2" align="center" height="50px">
						<input type="button" value="保存" class="button" id="saveButton" />
						<input type="button" value="返回" class="button" name="goback" id="goBack"/>
					</td>
				</TR>
			</TABLE>
		</div>
	</form>
</body>
</html>