<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>添加节次</title>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/fbackground.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
<script>
	$(document).ready(function(){
		$(".radio_jiang").change(function(){
			 var jiang = $("input[name='secname']:checked").val();
			$("#jiangci").val(jiang);
		});
		
		$(".radio_jie").change(function(){
			 var jie = $("input[name='thirdname']:checked").val();
			$("#jieci").val(jie);
		});
	});
</script>
</head>
<body>
<div class="content">
		<input type="hidden" id="jiangci" />
		<input type="hidden" id="jieci" value="第1节"/>
			<div style="margin-top:10px">
				<table class="tbl001">
					<tr>
						<td width="100px">第几讲：</td>
						<td>
							<c:forEach items="${listjiangci }" var="obj" varStatus="status">
								<input class="radio_jiang" type="radio" value= "${obj.id};第${obj.jorder}讲" name="secname"/>第${obj.jorder}讲
							</c:forEach>
						</td>
					</tr>
					<tr>
						<td>第几节：</td>
						<td>
							<c:forEach var= "obj" begin="1" end = "30">
								<input class="radio_jie" type="radio" value= "第${obj}节" name="thirdname" 
								<c:if test="${obj == 1 }">
									checked="checked"
								</c:if>  />第${obj}节
							</c:forEach>
						</td>
					</tr>
					
					<tr>
						<td>腾讯视频vid：</td>
						<td>
							<input type="text" id="vidopath"/>
							<span id="videopath"></span>
						</td>
					</tr>
				</table>
			</div>
	</div>
</body>
</html>