<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML >
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
<meta charset=UTF-8>
<title>班级添加页面</title>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/fbackground.css" />
<script type="text/javascript">
	//点击提交按钮时候
	$(document).ready(function() {
		$("#saveButton").click(function() {
			var grade = $("input[name='grade']:checked").val();
			var subject = $("input[name='subject']:checked").val();
			var banci  = $("input[name='banci']:checked").val();
			var bjtype = $("input[name='bjtype']:checked").val();
			var timeframe = $("input[name='timeframe']:checked").val();
			var lessonday = $("input[name='lessonday']:checked").val();
			var startdate = $("#startdate").val();
			var enddate = $("#enddate").val();
			var starttime = $("#starttime").val();
			var endtime = $("#endtime").val();
			var tea_id = $("input[name='tea_id']:checked").val();
			var address = $("#address").val();
			var year  = $("#year").val();
			var playnum = $("#playnum").val();
			
			//ajax提交
			$.ajax({
				url : "../banji/banjiinsert<%=sufStr%>",
				type : 'post',
				dataType : 'html',
				data : 'grade='+ grade + '&subject=' + subject + '&banci=' +banci
						+ '&bjtype=' + bjtype + '&timeframe=' + timeframe + '&lessonday=' + lessonday 
						+ '&startdate=' + startdate + '&enddate=' + enddate + '&starttime=' + starttime
						+ '&endtime=' + endtime + '&tea_id=' + tea_id + '&address=' + address
						+ '&year=' + year + '&playnum=' + playnum,
				success : function(data, status) {
					if (status == 'success') {
						if (data == '1') {
							var timer;
							art.dialog({
								content : '<span style="color:#f00">插入成功！</span>',
								init : function() {
									var that = this, i = 1;
									var fn = function() {
										!i && that.close();
										i--;
									};
									timer = setInterval(fn, 1000);
									fn();
								},
								close : function() {
									clearInterval(timer);
									window.location.href="<%=pre%>/banji/index<%=sufStr%>";
								}
							}).show();	
						} else {
							art.dialog.tips('<span style="color:#f00">插入失败！</span>', 1);
						}
					}
				},
				error : function(xhr, textStatus,
						errorThrown) {
					art.dialog.tips('<span style="color:#f00">插入失败！</span>', 1);
				}
			});
		});
	});
</script>
</head>
<body>
	<div class="content">
		<div class="content_title">
			班级添加页面
		</div>
		<div>
			<table class="tbl001">
				<tr>
					<td>年级：</td>
					<td>
						<c:forEach items="${listGrade }" var="obj">
							<input type="radio" name="grade" value="${obj.name }" >${obj.name }
						</c:forEach>
					</td>
				</tr>
				<tr>
					<td>科目:</td>
					<td>
						<c:forEach items="${listSubject }" var="obj">
							<input type="radio" name="subject" value="${obj.name }" >${obj.name }
						</c:forEach>
					</td>
				</tr>
				<tr>
					<td>班次：</td>
					<td>
						<c:forEach items="${listBanci }" var="obj">
							<input type="radio" name="banci" value="${obj.name }" >${obj.name }
						</c:forEach>
					</td>
				</tr>
				<tr>
					<td>班级类型：</td>
					<td>
						<c:forEach items="${listBjtype }" var="obj">
							<input type="radio" name="bjtype" value="${obj.name }" >${obj.name }
						</c:forEach>
					</td>
				</tr>
				<tr>
					<td>年份：</td>
					<td>
						<input id="year" class="Wdate" type="text" onfocus="WdatePicker({dateFmt:'yyyy'})" style="width: 100px"/>
					</td>
				</tr>
				<tr>
					<td>开课日期:</td>
					<td>
						<input id="startdate" class="Wdate" type="text" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})"
						style="width: 100px"/>
						-
						<input id="enddate" class="Wdate" type="text" onfocus="WdatePicker({dateFmt:'yyyy-MM-dd'})"
						style="width: 100px" />
					</td>
				</tr>
				<tr>
					<td>周几：</td>
					<td>
						<input type="radio" name="lessonday" value="周一" checked="checked">周一
						<input type="radio" name="lessonday" value="周二">周二
						<input type="radio" name="lessonday" value="周三">周三
						<input type="radio" name="lessonday" value="周四">周四
						<input type="radio" name="lessonday" value="周五">周五
						<input type="radio" name="lessonday" value="周六">周六
						<input type="radio" name="lessonday" value="周日">周日
					</td>
				</tr>
				<tr>
					<td>上课时间:</td>
					<td>
						<input id="starttime" class="Wdate" type="text" onfocus="WdatePicker({dateFmt:'HH:mm'})"
						style="width: 100px" />
						-
						<input id="endtime" class="Wdate" type="text" onfocus="WdatePicker({dateFmt:'HH:mm'})"
						style="width: 100px" />
					</td>
				</tr>
				<tr>
					<td>上课教师:</td>
					<td>
						<c:forEach items="${list}" var="obj">
							<input type="radio" name="tea_id" value="${obj.id};${obj.name}">${obj.name }
						</c:forEach>
					</td>
				</tr>
				<tr>
					<td>上课时段：</td>
					<td>
						<input type="radio" name="timeframe" value="上午" checked="checked">上午
						<input type="radio" name="timeframe" value="下午">下午
						<input type="radio" name="timeframe" value="晚上">晚上
					</td>
				</tr>
				<tr>
					<td>播放次数：</td>
					<td>
						<input type="text" name = "playnum" id="playnum" />
					</td>
				</tr>
				<tr>
					<td>上课地点：</td>
					<td>
						<input type="text" name = "address" id="address" />
					</td>
				</tr>
			</table>
		</div>
		<div>
			<input type="button" value="保存" class="button" id="saveButton" /> 
			<input type="button" value="返回" class="button" name="goback" onclick="window.history.go(-1);" />
		</div>
	</div>
</body>
</html>