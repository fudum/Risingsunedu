<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML >
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<%
request.setCharacterEncoding("UTF-8");
String htmlData1 = request.getParameter("info") != null ? request.getParameter("info") : "";
%>
<html>
<head>
<meta charset=UTF-8>
	<title>添加公开课</title>
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/fbackground.css" />
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
	<script>
		KindEditor.ready(function(K) {
			var editor1 = K.create('textarea[name="info"]', {
				cssPath : '<%=kePath %>/plugins/code/prettify.css',
				uploadJson : '<%=kePath %>/jsp/upload_json.jsp',
				fileManagerJson : '<%=kePath %>/jsp/file_manager_json.jsp',
				allowFileManager : true,
				afterCreate : function() {
					var self = this;
					K.ctrl(document, 13, function() {
						self.sync();
						document.forms['example'].submit();
					});
					K.ctrl(self.edit.doc, 13, function() {
						self.sync();
						document.forms['example'].submit();
					});
				}
			});
			prettyPrint();
			
		});
		
		$(document).ready(function(){
			//上传图片
			$("#uploadFile").live('change',function(){
				var picFile = $("#uploadFile").val();
				var wjjname = "openk";
				if(picFile == ""){
					art.dialog.alert("请选择文件!");
					return;
				}
				else{
					var strFilter = ".pdf|.PDF|";
					if(picFile.indexOf(".") > -1){
						var p = picFile.lastIndexOf(".");
						var strPostfix = picFile.substring(p,picFile.length) + '|';
						strPostfix = strPostfix.toLowerCase();
						if(strFilter.indexOf(strPostfix) > -1){
							$.ajaxFileUpload({
								url: '<%=pre%>/b_com/fileupload<%=sufStr%>', 
								data:{wjjname:wjjname},
								type: 'post',
								fileElementId: 'uploadFile',
								dataType:'json', //返回值类型，一般设置为json、application/json
								success: function(data, status){ 
						            	if(data.ret == 1){
						            		$("#catalogtip").html("文件："+data.path);
						            		$("#catalog").val(data.path);
						            	}
						            	else{
						            		art.dialog.alert("上传失败！");
						            	}
						            },
						            error: function(data, status, e){ 
						            	art.dialog.tips('<span style="color:#f00">上传失败，请重传！</span>', 1);
						            }
							});
						}else{
							art.dialog.alert("请选择：pdf格式的文件！");
						}
					}
				}
			});
		});
	</script>
</head>
<body>
	<div class="content">
		<div class="content_title">
			公开课添加页面
		</div>
		<form id="example" name="example" action="<%=pre%>/openk/insert<%=sufStr%>" method="post" enctype="multipart/form-data" >
		<div>
			<table class="tbl001">
					<tr>
						<td>年级：</td>
						<td>
							<c:forEach items="${listGrade }" var="obj">
								<input type="checkbox" name="grade" value="${obj.name }" >${obj.name }
							</c:forEach>
						</td>
					</tr>
					<tr>
						<td>科目:</td>
						<td>
							<c:forEach items="${listSubject }" var="obj">
								<input type="radio" name="subject" value="${obj.name }" >${obj.name }
							</c:forEach>
						</td>
					</tr>
					<tr>
						<td>课程类型:</td>
						<td>
							<c:forEach items="${listKctype }" var="obj">
								<input type="checkbox" name="kctype" value="${obj.name }" >${obj.name }
							</c:forEach>
						</td>
					</tr>
					<tr>
						<td>上课教师:</td>
						<td>
							<c:forEach items="${list}" var="obj">
								<input type="radio" name="tea_id" value="${obj.id};${obj.name}">${obj.name }
							</c:forEach>
						</td>
					</tr>
					<tr>
						<td>播放次数：</td>
						<td>
							<input type="text" id = "playnum" name="playnum" style="width:300px"/>
						</td>
					</tr>
					<tr>
						<td>公开课名称：</td>
						<td>
							<input type="text" id = "name" name="name" style="width:500px"/>
						</td>
					</tr>
					<tr>
						<td>课程目录：</td>
						<td>
							<input type="file" name="uploadFile" id="uploadFile">
							<span id="catalogtip" style="color:red"></span>
							<input type="hidden" name = "catalog" id="catalog" > 
						</td>
					</tr>	
					<tr>
						<td>课程详情：</td>
						<td>
							<%=htmlData1%>
							<textarea id="info" name="info" style="width:800px;height:250px;">
								<%=htmlspecialchars(htmlData1)%>
							</textarea>
						</td>
					</tr>
				</table>
		</div>
		<div>
			<input type="submit" value="保存" class="button" id="saveButton" /> 
			<input type="button" value="返回" class="button" name="goback" onclick="window.history.go(-1);" />
		</div>
		</form>
	</div>
</body>
</html>
<%!
private String htmlspecialchars(String str) {
	str = str.replaceAll("&", "&amp;");
	str = str.replaceAll("<", "&lt;");
	str = str.replaceAll(">", "&gt;");
	str = str.replaceAll("\"", "&quot;");
	return str;
}
%>