<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML >
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
	<meta charset=UTF-8>
	<title>公开课讲次管理</title>
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
	<link rel="stylesheet"  type="text/css" href="<%=cssPath %>/fbackground.css" />
	<script>
		$(document).ready(function(){
			$("#btn_add_jc").click(function(){
				var openk_id = $("#openk_id").val();
				art.dialog.open("<%=pre%>/openkjc/add<%=sufStr%>?openk_id=" + openk_id,{
					title:"添加讲次",
					width:500,
					height:200,
					cancel: true,
					ok:function(){
						var iframe = this.iframe.contentWindow;
						if (!iframe.document.body) {
				        	alert('iframe还没加载完毕呢')
				        	return false;
				        };
						var openk_id = $("#openk_id").val();
						var name = iframe.document.getElementById('name').value;
						var jorder = iframe.document.getElementById('jorder').value;
						$.ajax({
							type:"post",
							url:"<%=pre%>/openkjc/insert<%=sufStr%>",
							data:{openk_id:openk_id,name:name,jorder:jorder},
							dataType:"json",
							success:function(data,status){
								if(data.ret == '1'){
									var timer;
									art.dialog({
										width:150,
										height:80,
										content : '<span style="color:#f00">添加成功!</span>',
										init : function() {
											var that = this, i = 1;
											var fn = function() {
												!i && that.close();
												i--;
											};
											timer = setInterval(fn, 1000);
											fn();
										},
										close : function() {
											clearInterval(timer);
											location.reload();
										}
										
									}).show();			
								}
								else{
									art.dialog.alert("添加失败！");
									return false;
								}
							},
							error : function(xhr, textStatus, errorThrown) {
								art.dialog.alert('<span style="color:#f00">添加失败！</span>', 1);
								return false;
							}
						});
					}
				});
			});
		});
		
		/**编辑页面**/
		function editPage(url){
			art.dialog.open(url,{
				title:"编辑讲次信息",
				width:500,
				height:200,
				cancel: true,
				ok:function(){
					var iframe = this.iframe.contentWindow;
					if (!iframe.document.body) {
			        	alert('iframe还没加载完毕呢')
			        	return false;
			        };
					var openk_id = $("#openk_id").val();
					var name = iframe.document.getElementById('name').value;
					var jorder = iframe.document.getElementById('jorder').value;
					var id = iframe.document.getElementById('id').value;
					$.ajax({
						type:"post",
						url:"<%=pre%>/openkjc/update<%=sufStr%>",
						data:{id:id,openk_id:openk_id,name:name,jorder:jorder},
						dataType:"json",
						success:function(data,status){
							if(data.ret == '1'){
								var timer;
								art.dialog({
									width:150,
									height:80,
									content : '<span style="color:#f00">编辑成功!</span>',
									init : function() {
										var that = this, i = 1;
										var fn = function() {
											!i && that.close();
											i--;
										};
										timer = setInterval(fn, 1000);
										fn();
									},
									close : function() {
										clearInterval(timer);
										location.reload();
									}
									
								}).show();			
							}
							else{
								art.dialog.alert("编辑失败！");
								return false;
							}
						},
						error : function(xhr, textStatus, errorThrown) {
							art.dialog.alert('<span style="color:#f00">编辑失败！</span>', 1);
							return false;
						}
					});
				}
			});
		}
	</script>
</head>
<body>
	<div class="content">
		<div>
			<form name="fom" id="fom" method="post" action="">
				<input type="hidden" id="openk_id" name="openk_id" value="${bean.openk_id }"/>
			</form>
		</div>
		<div>
			<input type="button" class="btn_add" id="btn_add_jc" value="添加讲次" />
		</div>
		<div>
			<table class="tbl002" >
				<tr>
					<td >序号</td>
					<td>讲次名称</td>
					<td>顺序</td>
					<td>插入时间</td>
					<td>操作</td>
				</tr>
				<c:forEach items="${list}" var="obj" varStatus="stauts">
					<tr bgcolor="#FFFFFF" align="center">
						<td>${stauts.index +1}</td>
						<td>${obj.name}</td>
						<td>${obj.jorder}</td>
						<td>${obj.inserttime }</td>
						<td>
							<a href="#" onclick="javascript:editPage('<%=pre%>/openkjc/edit<%=sufStr%>?id=${obj.id }')">编辑详情</a> | 
							<a href="javascript:deleteConfirm('${obj.id }','<%=pre%>/openkjc/delete<%=sufStr%>?id=${obj.id}');">删除</a></td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>
</body>
</html>