<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML >
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
<meta charset=UTF-8>
<title>添加讲次</title>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/fbackground.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
</head>
<body>
	<div class="content">
		<div style="margin-top:10px;">
			<input type="hidden" value="${openk_id}" id="openk_id">
			<table class="tbl001">
			<tr>
				<td>讲次名称：</td>
				<td>
					<input type="text" id="name" style="width:300px"/>
				</td>
			</tr>
			<tr>
				<td>讲次顺序：</td>
				<td>
					<select id="jorder">
						<c:forEach var="item" begin="1" end = "30">
							<option>${item }</option>
						</c:forEach>
					</select>
					
				</td>
			</tr>
			</table>
		</div>
	</div>
</body>
</html>