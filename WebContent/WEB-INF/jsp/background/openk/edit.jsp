<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML >
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<%
request.setCharacterEncoding("UTF-8");
String htmlData1 = request.getParameter("info") != null ? request.getParameter("info") : "";
%>
<html>
<head>
<meta charset=UTF-8>
<title>添加公开课</title>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/fbackground.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
<script>
	KindEditor.ready(function(K) {
		var editor1 = K.create('textarea[name="info"]', {
			cssPath : '<%=kePath %>/plugins/code/prettify.css',
			uploadJson : '<%=kePath %>/jsp/upload_json.jsp',
			fileManagerJson : '<%=kePath %>/jsp/file_manager_json.jsp',
			allowFileManager : true,
			afterCreate : function() {
				var self = this;
				K.ctrl(document, 13, function() {
					self.sync();
					document.forms['example'].submit();
				});
				K.ctrl(self.edit.doc, 13, function() {
					self.sync();
					document.forms['example'].submit();
				});
			}
		});
		prettyPrint();
	});
</script>
<script>
	$(document).ready(function(){
		
		//上传图片
		$("#uploadFile").live('change',function(){
			var picFile = $("#uploadFile").val();
			var wjjname = "openk";
			if(picFile == ""){
				art.dialog.alert("请选择文件!");
				return;
			}
			else{
				var strFilter = ".pdf|.PDF|";
				if(picFile.indexOf(".") > -1){
					var p = picFile.lastIndexOf(".");
					var strPostfix = picFile.substring(p,picFile.length) + '|';
					strPostfix = strPostfix.toLowerCase();
					if(strFilter.indexOf(strPostfix) > -1){
						$.ajaxFileUpload({
							url: '<%=pre%>/b_com/fileupload<%=sufStr%>', 
							data:{wjjname:wjjname},
							type: 'post',
							fileElementId: 'uploadFile',
							dataType:'json', //返回值类型，一般设置为json、application/json
							success: function(data, status){ 
					            	if(data.ret == 1){
					            		$("#catalogtip").html("文件："+data.path);
					            		$("#catalog").val(data.path);
					            	}
					            	else{
					            		art.dialog.alert("上传失败！");
					            	}
					            },
					            error: function(data, status, e){ 
					            	art.dialog.tips('<span style="color:#f00">上传失败，请重传！</span>', 1);
					            }
						});
					}else{
						art.dialog.alert("请选择：pdf格式的文件！");
					}
				}
			}
		});
		
		//添加讲次
		$("#btn_add_jc").click(function(){
			var openk_id = $("#id").val();
			art.dialog.open("<%=pre%>/openkjc/add<%=sufStr%>?openk_id=" + openk_id,{
				title:"添加讲次",
				width:500,
				height:200,
				cancel: true,
				ok:function(){
					var iframe = this.iframe.contentWindow;
					if (!iframe.document.body) {
			        	alert('iframe还没加载完毕呢')
			        	return false;
			        };
					var openk_id = $("#id").val();
					var name = iframe.document.getElementById('name').value;
					var jorder = iframe.document.getElementById('jorder').value;
					$.ajax({
						type:"post",
						url:"<%=pre%>/openkjc/insert<%=sufStr%>",
						data:{openk_id:openk_id,name:name,jorder:jorder},
						dataType:"json",
						success:function(data,status){
							if(data.ret == '1'){
								var timer;
								art.dialog({
									width:150,
									height:80,
									content : '<span style="color:#f00">添加成功!</span>',
									init : function() {
										var that = this, i = 1;
										var fn = function() {
											!i && that.close();
											i--;
										};
										timer = setInterval(fn, 1000);
										fn();
									},
									close : function() {
										clearInterval(timer);
										location.reload();
									}
									
								}).show();			
							}
							else{
								art.dialog.alert("添加失败！");
								return false;
							}
						},
						error : function(xhr, textStatus, errorThrown) {
							art.dialog.alert('<span style="color:#f00">添加失败！</span>', 1);
							return false;
						}
					});
				}
			});
		});
		
		//添加视频
		$("#add_video").click(function(){
			var openk_id = $("#id").val();
			art.dialog.open("<%=pre%>/openksec/add<%=sufStr%>?openk_id=" + openk_id,{
				title:"添加视频",
				width:800,
				height:300,
				cancel: true,
				ok:function(){
					var iframe = this.iframe.contentWindow;
					if (!iframe.document.body) {
			        	alert('iframe还没加载完毕呢')
			        	return false;
			        };
					var openk_id = $("#id").val();
					var secname = iframe.document.getElementById('jiangci').value;
					var thirdname = iframe.document.getElementById('jieci').value;
					var videopath = iframe.document.getElementById('vidopath').value;
					$.ajax({
						type:"post",
						url:"<%=pre%>/openksec/insertnew<%=sufStr%>",
						data:{secname:secname,thirdname:thirdname,videopath:videopath},
						dataType:"json",
						success:function(data,status){
							if(data.ret == '1'){
								var timer;
								art.dialog({
									width:150,
									height:80,
									content : '<span style="color:#f00">添加成功!</span>',
									init : function() {
										var that = this, i = 1;
										var fn = function() {
											!i && that.close();
											i--;
										};
										timer = setInterval(fn, 1000);
										fn();
									},
									close : function() {
										clearInterval(timer);
										location.reload();
									}
									
								}).show();			
							}
							else{
								art.dialog.alert("添加失败！");
								return false;
							}
						},
						error : function(xhr, textStatus, errorThrown) {
							art.dialog.alert('<span style="color:#f00">添加失败！</span>', 1);
							return false;
						}
					});
				}
				
			});
		});
		
	});
	
	/**编辑页面**/
	function editPage(url){
		art.dialog.open(url,{
			title:"编辑讲次信息",
			width:500,
			height:200,
			cancel: true,
			ok:function(){
				var iframe = this.iframe.contentWindow;
				if (!iframe.document.body) {
		        	alert('iframe还没加载完毕呢')
		        	return false;
		        };
				var openk_id = $("#id").val();
				var name = iframe.document.getElementById('name').value;
				var jorder = iframe.document.getElementById('jorder').value;
				var id = iframe.document.getElementById('id').value;
				$.ajax({
					type:"post",
					url:"<%=pre%>/openkjc/update<%=sufStr%>",
					data:{id:id,openk_id:openk_id,name:name,jorder:jorder},
					dataType:"json",
					success:function(data,status){
						if(data.ret == '1'){
							var timer;
							art.dialog({
								width:150,
								height:80,
								content : '<span style="color:#f00">编辑成功!</span>',
								init : function() {
									var that = this, i = 1;
									var fn = function() {
										!i && that.close();
										i--;
									};
									timer = setInterval(fn, 1000);
									fn();
								},
								close : function() {
									clearInterval(timer);
									location.reload();
								}
								
							}).show();			
						}
						else{
							art.dialog.alert("编辑失败！");
							return false;
						}
					},
					error : function(xhr, textStatus, errorThrown) {
						art.dialog.alert('<span style="color:#f00">编辑失败！</span>', 1);
						return false;
					}
				});
			}
		});
	}
	
	//视频播放
	function videodisplay(videopath){
		alert(videopath);
		art.dialog.open("<%=pre%>/openk/vedioPreview<%=sufStr%>?vp=" + videopath,{
			width:600,
		    height:400,
			title:"公开课播放"
		});
	}
	
	//视频编辑
	function editVideo(url){
		art.dialog.open(url,{
			title:"编辑视频信息",
			width:800,
			height:300,
			cancel: true,
			ok:function(){
				var iframe = this.iframe.contentWindow;
				if (!iframe.document.body) {
		        	alert('iframe还没加载完毕呢')
		        	return false;
		        };
		        var secname = iframe.document.getElementById('jiangci').value;
				var thirdname = iframe.document.getElementById('jieci').value;
				var videopath = iframe.document.getElementById('vidopath').value;
				var id = iframe.document.getElementById('id').value;
				$.ajax({
					type:"post",
					url:"<%=pre%>/openksec/update<%=sufStr%>",
					data:{id:id,secname:secname,thirdname:thirdname,videopath:videopath},
					dataType:"json",
					success:function(data,status){
						if(data.ret == '1'){
							var timer;
							art.dialog({
								width:150,
								height:80,
								content : '<span style="color:#f00">编辑成功!</span>',
								init : function() {
									var that = this, i = 1;
									var fn = function() {
										!i && that.close();
										i--;
									};
									timer = setInterval(fn, 1000);
									fn();
								},
								close : function() {
									clearInterval(timer);
									location.reload();
								}
								
							}).show();			
						}
						else{
							art.dialog.alert("编辑失败！");
							return false;
						}
					},
					error : function(xhr, textStatus, errorThrown) {
						art.dialog.alert('<span style="color:#f00">编辑失败！</span>', 1);
						return false;
					}
				});
			}
		});
	}
</script>
</head>
<body>
	<div class="content">
		<div class="content_title">
			公开课编辑页面
		</div>
		<form id="formBack" action="<%=pre%>/openk/index<%=sufStr%>"></form>
		<form id="example" name="example" action="<%=pre%>/openk/update<%=sufStr%>" method="post" enctype="multipart/form-data" >
		<div>
			<input type="hidden" value="${bean.id }" name="id" id="id" />
			<table class="tbl001">
				<tr>
					<td>年级：</td>
					<td>
						<c:forEach items="${listGrade }" var="obj">
							<input type="checkbox" name="grade" value="${obj.name }" 
								<c:if test="${fn:contains(bean.grade,obj.name) }">checked="checked"</c:if>>${obj.name }
						</c:forEach>
						
					</td>
				</tr>
				<tr>
					<td>科目:</td>
					<td>
						<c:forEach items="${listSubject }" var="obj">
							<input type="radio" name="subject" value="${obj.name }" 
							<c:if test="${bean.subject == obj.name }">checked="checked"</c:if>>${obj.name }
						</c:forEach>
						
					</td>
				</tr>
				<tr>
					<td>课程类型:</td>
					<td>
						<c:forEach items="${listKctype }" var="obj">
							<input type="checkbox" name="kctype" value="${obj.name }" 
							<c:if test="${fn:contains(bean.kctype,obj.name) }">checked="checked"</c:if>>${obj.name }
						</c:forEach>
					</td>
				</tr>
				<tr>
					<td>上课教师:</td>
					<td>
						<c:forEach items="${list}" var="obj">
							<input type="radio" name="tea_id" value="${obj.id};${obj.name}"
							<c:if test="${bean.tea_id == obj.id}">checked="checked"</c:if>
							>${obj.name }
						</c:forEach>
					</td>
				</tr>
				<tr>
					<td>播放次数：</td>
					<td>
						<input type="text" id = "playnum" name="playnum" style="width:300px" value="${bean.playnum }"/>
					</td>
				</tr>
				<tr>
					<td>公开课名称：</td>
					<td>
						<input type="text" id = "name" name="name" style="width:500px" value="${bean.name}"/>
					</td>
				</tr>
				<tr>
					<td>课程目录：</td>
					<td>
						<input type="file" name="uploadFile" id="uploadFile">
						<span id="catalogtip" style="color:red">${bean.catalog }</span>
						<input type="hidden" name = "catalog" id="catalog" value="${bean.catalog }" > 
					</td>
				</tr>	
				<tr>
					<td>课程详情：</td>
					<td>
						<%=htmlData1%>
						<textarea id="info" name="info" style="width:800px;height:250px;">
							<%=htmlspecialchars(htmlData1)%>${bean.info }
						</textarea>
					</td>
				</tr>
				<tr>
					<td>
						<input type="submit" value="保存" class="button" id="saveButton" /> 
					</td>
					<td>
						<span style="color:red">
							<c:choose>
								<c:when test="${tip == 'i1' }">插入成功！</c:when>
								<c:when test="${tip == 'i0' }">插入失败！</c:when>
								<c:when test="${tip == 'u1' }">编辑成功！</c:when>
								<c:when test="${tip == 'u0' }">编辑失败！</c:when>
							</c:choose>
						</span>
					</td>
				</tr>
			</table>
		</div>
		</form>
		
		<!-- 讲次 -->
		<div class="content_title" style="margin-top:10px;">
			公开课讲次
		</div>
		<div>
			<input type="button" class="btn_add" id="btn_add_jc" value="添加讲次" />
		</div>
		<div>
			<table class="tbl002" >
				<tr>
					<td>序号</td>
					<td>讲次名称</td>
					<td>顺序</td>
					<td>插入时间</td>
					<td>操作</td>
					<td>序号</td>
				</tr>
				<c:forEach items="${listjc}" var="obj1" varStatus="stauts">
					<tr bgcolor="#FFFFFF" align="center">
						<td>${stauts.index +1}</td>
						<td>${obj1.name}</td>
						<td>${obj1.jorder}</td>
						<td>${obj1.inserttime }</td>
						<td>
							<a href="javascript:void(0)" onclick="javascript:editPage('<%=pre%>/openkjc/edit<%=sufStr%>?id=${obj1.id }')">编辑详情</a> | 
							<a href="javascript:deleteConfirm('${obj1.id }','<%=pre%>/openkjc/delete<%=sufStr%>?id=${obj1.id}');">删除</a></td>
						<td>${stauts.index +1}</td>
					</tr>
				</c:forEach>
			</table>
		</div>
		
		<!-- 视频 -->
		<div class="content_title" style="margin-top:10px;">
			公开课视频
		</div>
		<div>
			<input id="add_video" type="button" class="btn_add" value="添加视频" />
		</div>
		<div>
			<table class="tbl002">
				<tr>
					<td>序号</td>
					<td>讲次</td>
					<td>节次</td>
					<td>预览播放</td>
					<td>插入时间</td>
					<td>操作</td>
					<td>序号</td>
				</tr>
				<c:forEach items="${listvideo}" var="obj" varStatus="status">
					<tr>
						<td>${status.index +1 }</td>
						<td>${obj.secname}</td>
						<td>${obj.thirdname}</td>
						<td>
							<a href="javascript:void(0)" onclick="javascript:videodisplay('${obj.videopath}')">视频播放</a>
						</td>
						<td>${obj.inserttime }</td>
						<td>
							<a href="javascript:void(0)" onclick="javascript:editVideo('<%=pre%>/openksec/edit<%=sufStr%>?id=${obj.id }&openkId=${bean.id}')">编辑</a> |
							<a href="javascript:deleteConfirm('${obj.id }','<%=pre%>/openksec/delete<%=sufStr%>?id=${obj.id}');">删除</a>
						</td>
						<td>${status.index +1 }</td>
					</tr>
					
				</c:forEach>
			</table>
		</div>
		
		
		<div style="padding-bottom:20px;">
			<input type="button" value="返回" class="button" id="back"/>
		</div>
		
	</div>
</body>
</html>
<%!
private String htmlspecialchars(String str) {
	str = str.replaceAll("&", "&amp;");
	str = str.replaceAll("<", "&lt;");
	str = str.replaceAll(">", "&gt;");
	str = str.replaceAll("\"", "&quot;");
	return str;
}
%>