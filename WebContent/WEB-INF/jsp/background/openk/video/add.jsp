<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML >
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
	<meta charset=UTF-8>
	<title></title>
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/fbackground.css" />
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
	<script>
		$(document).ready(function(){
			$("#btnUpload").click(function(){
				var secname = $("input[name='secname']:checked").val();
				if(typeof(secname) == "undefined") {
					art.dialog.alert("请选择第几讲！");
					return;
				}
				var thirdname = $("input[name='thirdname']:checked").val();
				if(typeof(thirdname) == "undefined") {
					art.dialog.alert("请选择第几节！");
					return;
				}
				//判断视频文件是否为空
				var videoFile = $("#videoFile").val();
				if(videoFile == ""){
					art.dialog.alert('请选择视频文件！');
					return;
				}
				var picpath = $("#picpath").val();
				
				$.ajaxFileUpload({
		            url: '<%=pre%>/openksec/insert<%=sufStr%>', 
		            type: 'post',
		            data:{'secname':secname, 'thirdname':thirdname,'picpath':picpath},
		            fileElementId: 'videoFile', //上传文件的id、name属性名
		            dataType:'json', //返回值类型，一般设置为json、application/json
		            success: function(data, status){ 
		            	if(data.ret == 1){
							$("#tip").html("上传成功！");
							$("#videopath").html(data.videopath);
							$("#btnUpload").attr("disabled", true); 
		            	}
		            	else{
		            		art.dialog.alert("上传失败！");
		            	}
		            },
		            error: function(data, status, e){ 
		            	art.dialog.tips('<span style="color:#f00">上传失败！</span>', 1);
		            }
				});
			});
			
			//上传图片
			$("#picFile").change(function(){
				var openk_id = $("#openk_id").val();
				var picFile = $("#picFile").val();
				if(picFile == ""){
					art.dialog.alert("请选择文件!");
					return;
				}
				else{
					var strFilter = ".jpeg|.gif|.jpg|.png|";
					if(picFile.indexOf(".") > -1){
						var p = picFile.lastIndexOf(".");
						var strPostfix = picFile.substring(p,picFile.length) + '|';
						strPostfix = strPostfix.toLowerCase();
						if(strFilter.indexOf(strPostfix) > -1){
							$.ajaxFileUpload({
								url: '<%=pre%>/openksec/picupload<%=sufStr%>', 
								type: 'post',
								fileElementId: 'picFile',
								data:{'openk_id':openk_id},
								dataType:'json', //返回值类型，一般设置为json、application/json
								success: function(data, status){ 
						            	if(data.ret == 1){
						            		$("#picpreview").attr("src","<%=uploadPath%>/" + data.picpath);
						            		$("#picpath").val(data.picpath);
						            	}
						            	else{
						            		art.dialog.alert("上传失败！");
						            	}
						            },
						            error: function(data, status, e){ 
						            	art.dialog.tips('<span style="color:#f00">上传图片失败，请重传！</span>', 1);
						            }
							});
						}
 					}else{
 						art.dialog.alert("请选择：jpeg、gif、jpg、png格式的图片！");
 					}
				}
			});
			
		});
	</script>
</head>
<body>
	<div class="content">
		
		<form id="form1" action="" method="post" enctype="mutipart/form-data">
			<div style="margin-top:10px">
				<table class="tbl001">
					<tr>
						<td width="100px">第几讲：</td>
						<td>
							<c:forEach items="${listsec }" var="obj" varStatus="status">
								<input type="radio" value= "${obj.id};第${obj.jorder}讲" name="secname"/>第${obj.jorder}讲
							</c:forEach>
						</td>
					</tr>
					<tr>
						<td>第几节：</td>
						<td>
							<c:forEach items="${thirdList }" var="obj" varStatus="status">
								<input type="radio" value= "${obj}" name="thirdname"/>${obj}
							</c:forEach>
						</td>
					</tr>
					<tr>
						<td>视频首页图片：</td>
						<td>
							<input type="file" name="picFile" id="picFile" />
							<img style="width:160px;" id="picpreview" />
							<input type="hidden" id="picpath" />
						</td>
					</tr>
					<tr>
						<td>视频文件：</td>
						<td>
							<input type="file" name="videoFile" id="videoFile" />
							<span id="videopath"></span>
						</td>
					</tr>
				</table>
			</div>
			<div style="text-align:center">
				<span id="tip" style="color:red"></span>
			</div>
			<div style="text-align:center">
				<input type="button" value="上传视频" id="btnUpload" /> 
			</div>
		</form>
	</div>
</body>
</html>