<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML >
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
<meta charset=UTF-8>
<title></title>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath %>/fbackground.css" />
<script>
	$(document).ready(function(){
		$("#add_video").click(function(){
			var openk_id = $("#id").val();
			art.dialog.open("<%=pre%>/openksec/add<%=sufStr%>?openk_id=" + openk_id,{
				title:"添加视频",
				width:800,
				height:400
			});
		});
		
		//点击刷新按钮
		$("#refresh").click(function(){
			$("#form_refresh").submit();
		});
	});
	
	//视频播放
	function videodisplay(videopath){
		art.dialog.open("<%=pre%>/teacher/vedioPreview<%=sufStr%>?vp=" + videopath,{
			width:650,
		    height:370,
			title:"公开课播放"
		});
	}
</script>
</head>
<body>
	<div class="content">
		<div>
			<form id="form_refresh" action = "<%=pre%>/openksec/manage<%=sufStr%>" method="post">
				<input type="hidden" value="${bean.id }" id="id" />
			</form>
		</div>
		<div>
			<input id="add_video" type="button" class="btn_add" value="添加视频" />
			<input id="refresh" type="button" class="btn_add" value="刷新页面" />
		</div>
		<div style="line-height:36px;height:36px;font-size:24px;padding:10px 0 5px">
			公开课：${bean.name }
		</div>
		<div style="line-height:24px;height:24px;font-size:18px;padding:5px 0">
			 年级：${bean.grade } 科目：${bean.subject } 教师：${bean.tea_name }
		</div>
		<div>
			<table class="tbl002">
				<tr>
					<td>讲次</td>
					<td>节次</td>
					<td>视频文件</td>
					<td>预览播放</td>
					<td>操作</td>
				</tr>
				<c:forEach items="${list}" var="obj" varStatus="status">
					<tr>
						<td>${obj.secname}</td>
						<td>${obj.thirdname}</td>
						<td>${fn:substring(obj.videopath,25,fn:length(obj.videopath))}</td>
						<td>
							<a href="#" onclick="javascript:videodisplay('${obj.videopath}')">视频播放</a>
						</td>
						<td>
							<a href="#">编辑</a> |
							<a href="#">删除</a>
						</td>
					</tr>
				</c:forEach>
			</table>
			
		</div>
	</div>
</body>
</html>