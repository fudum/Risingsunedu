<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>视频编辑页面</title>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/fbackground.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
<script>
	$(document).ready(function(){
		$(".radio_jiang").change(function(){
			 var jiang = $("input[name='secname']:checked").val();
			$("#jiangci").val(jiang);
		});
		
		$(".radio_jie").change(function(){
			 var jie = $("input[name='thirdname']:checked").val();
			$("#jieci").val(jie);
		});
	});
</script>
</head>
<body>
<div class="content">
	<form id="form1" action="" method="post" >
		<input type="hidden" id="id" value="${bean.id }">
		<input type="hidden" id="jiangci" value="${bean.openk_id };${bean.secname}"  />
		<input type="hidden" id="jieci" value="${bean.thirdname }" />
		<div style="margin-top:10px">
			<table class="tbl001">
				<tr>
					<td width="100px">第几讲：</td>
					<td>
						<c:forEach items="${listsec }" var="obj" varStatus="status">
							<input class="radio_jiang" type="radio" value= "${obj.id};第${obj.jorder}讲" name="secname"
							<c:if test="${bean.openk_id == obj.id }">checked="checked"</c:if> />第${obj.jorder}讲
						</c:forEach>
					</td>
				</tr>
				<tr>
					<td>第几节：</td>
					<td>
						<c:forEach items="${thirdList }" var="obj" varStatus="status">
							<input class="radio_jie" type="radio" value= "${obj}" name="thirdname"
							<c:if test="${bean.thirdname == obj }">checked="checked"</c:if>
							/>${obj}
						</c:forEach>
					</td>
				</tr>
				
				<tr>
					<td>腾讯视频vid：</td>
					<td>
						<input type="text" id="vidopath" value="${bean.videopath }"/>
						<span id="videopath"></span>
					</td>
				</tr>
			</table>
		</div>
	</form>
</div>
</body>
</html>