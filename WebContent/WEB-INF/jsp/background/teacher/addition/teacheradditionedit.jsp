<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML >
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<%
request.setCharacterEncoding("UTF-8");
String htmlData1 = request.getParameter("teainfo") != null ? request.getParameter("teainfo") : "";
String htmlData2 = request.getParameter("teaachive") != null ? request.getParameter("teaachive") : "";
String htmlData3 = request.getParameter("teainfoweigui") != null ? request.getParameter("teainfoweigui") : "";
%>
<html>
<head>
<meta charset=UTF-8>
	<title>教师附加信息编辑</title>
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
	<script>
		KindEditor.ready(function(K) {
			var editor1 = K.create('textarea[name="teainfo"]', {
				cssPath : '<%=kePath %>/plugins/code/prettify.css',
				uploadJson : '<%=kePath %>/jsp/upload_json.jsp',
				fileManagerJson : '<%=kePath %>/jsp/file_manager_json.jsp',
				allowFileManager : true,
				afterCreate : function() {
					var self = this;
					K.ctrl(document, 13, function() {
						self.sync();
						document.forms['example'].submit();
					});
					K.ctrl(self.edit.doc, 13, function() {
						self.sync();
						document.forms['example'].submit();
					});
				}
			});
			
			var editor2 = K.create('textarea[name="teachengguo"]', {
				cssPath : '<%=kePath %>/plugins/code/prettify.css',
				uploadJson : '<%=kePath %>/jsp/upload_json.jsp',
				fileManagerJson : '<%=kePath %>/jsp/file_manager_json.jsp',
				allowFileManager : true,
				afterCreate : function() {
					var self = this;
					K.ctrl(document, 13, function() {
						self.sync();
						document.forms['example'].submit();
					});
					K.ctrl(self.edit.doc, 13, function() {
						self.sync();
						document.forms['example'].submit();
					});
				}
			});
			
			var editor3 = K.create('textarea[name="teaweigui"]', {
				cssPath : '<%=kePath %>/plugins/code/prettify.css',
				uploadJson : '<%=kePath %>/jsp/upload_json.jsp',
				fileManagerJson : '<%=kePath %>/jsp/file_manager_json.jsp',
				allowFileManager : true,
				afterCreate : function() {
					var self = this;
					K.ctrl(document, 13, function() {
						self.sync();
						document.forms['example'].submit();
					});
					K.ctrl(self.edit.doc, 13, function() {
						self.sync();
						document.forms['example'].submit();
					});
				}
			});
			prettyPrint();
			
		});
	</script>
	<style>
		.teatitle{
			font-size:24px;
			margin:10px 10px 10px 10px;
		}
		.teabody{
			margin-left:10px;
		}
	</style>
</head>
<body>
	<form id="example" name="example" action="<%=pre%>/teacheraddition/teacheradditionupdate<%=sufStr%>" method="post" enctype="multipart/form-data">
		<div class="MainDiv">
			<input type="hidden" name="id" value="${bean.id}" />
			<input type="hidden" name="tea_id" value="${bean.tea_id }" />
			<div>
				<div class="teatitle">个人证书</div>
				<%=htmlData1%>
				<div class="teabody">
					<textarea id="teainfo" name="teainfo" style="width:900px;height:250px;">
						<%=htmlspecialchars(htmlData1)%>${bean.teainfo}
					</textarea>
				</div>
				<div class="teatitle">违规记录</div>
				<%=htmlData3%>
				<div class="teabody">
					<textarea id="teaweigui" name="teaweigui" style="width:900px;height:250px;">
						<%=htmlspecialchars(htmlData3)%>${bean.teaweigui}
					</textarea>											
				</div>
				<div class="teatitle">教学成果</div>
				<%=htmlData2%>
				<div class="teabody">
					<textarea id="teachengguo" name="teachengguo" style="width:900px;height:250px;">
						<%=htmlspecialchars(htmlData2)%>${bean.teachengguo}
					</textarea>
				</div>
				<div class="teabody">
					<input type="submit" value="保存" class="button" id="saveButton" />
					<input type="button" value="返回" class="button" id="goBack" />
				</div>
			</div>
			
		</div>
	</form>
</body>
</html>

<%!
private String htmlspecialchars(String str) {
	str = str.replaceAll("&", "&amp;");
	str = str.replaceAll("<", "&lt;");
	str = str.replaceAll(">", "&gt;");
	str = str.replaceAll("\"", "&quot;");
	return str;
}
%>