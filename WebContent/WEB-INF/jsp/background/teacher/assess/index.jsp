<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML >
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
<meta charset=UTF-8>
<title></title>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css"/>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css"/>
<link rel="stylesheet" type="text/css" href="<%=cssPath %>/fbackground.css"/>
</head>
<body>
	<div class="content_title">
		学生评估
	</div>
	<div>
		<table class="tbl002">
			<tr>
				<td>序号</td>
				<td>教师姓名</td>
				<td>性别</td>
				<td>教龄</td>
				<td>教师级别</td>
				<td>教学科目</td>
				<td>操作</td>
				<td>序号</td>
			</tr>
			<c:forEach items="${pager.pageList}" var="obj" varStatus="status">
				<tr>
					<td>${status.index +1}</td>
					<td>${obj.name}</td>
					<td>${obj.sex}</td>
					<td>${obj.jiaoling}年</td>
					<td>${obj.tealevel}</td>
					<td>${obj.subject }</td>
      				<td><a href="<%=pre%>/b_ta/edit<%=sufStr%>?id=${obj.id }">编辑评估学员</a></td>
					<td>${status.index +1}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
	
	<!-- 分页条 -->
		<div>
			<div class="manu">
				<c:if test="${pager.totalPage >0}">
					<!-- prev -->
					<c:choose>
						<c:when test="${pager.curtPage == 1 }">
							<span class="disabled">&lt;Prev</span>
						</c:when>
						<c:otherwise>
							<a href="<%=pre %>/b_ta/index<%=sufStr%>?curtPage=${pager.curtPage - 1}">&lt; Prev </a>
						</c:otherwise>
					</c:choose>
					
					<!-- content -->
					<c:if test="${pager.totalPage < 7 }">
						<c:forEach var = "item" begin="1" end = "${pager.totalPage }">
							<c:choose>
								<c:when test="${pager.curtPage == item }">
								  	<span class="current">${item }</span>
								</c:when>
								<c:otherwise>
									<a href="<%=pre %>/b_ta/index<%=sufStr%>?curtPage=${item}">${item }</a>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</c:if>
					<c:if test="${pager.totalPage >= 7 }">
						<c:if test="${pager.curtPage <= 4 }">
							<c:forEach var = "item" begin="1" end = "5">
								<c:choose>
									<c:when test="${pager.curtPage == item }">
									  	<span class="current">${item }</span>
									</c:when>
									<c:otherwise>
										<a href="<%=pre %>/b_ta/index<%=sufStr%>?curtPage=${item}">${item }</a>
									</c:otherwise>
								</c:choose>
							</c:forEach>
							...
							<a href="<%=pre %>/b_ta/index<%=sufStr%>?curtPage=${pager.totalPage}">${pager.totalPage}</a>
						</c:if>
						<c:if test="${pager.curtPage > 4 }">
							<a href="<%=pre %>/b_ta/index<%=sufStr%>?curtPage=1">1</a>
							...
							<c:choose>
									
									<c:when test="${pager.curtPage < pager.totalPage - 3 }">
										<c:forEach var = "item" begin="${pager.curtPage - 2 }" end = "${pager.curtPage + 2 }">
											<c:choose>
												<c:when test="${pager.curtPage == item }">
												  	<span class="current">${item }</span>
												</c:when>
												<c:otherwise>
													<a href="<%=pre %>/b_ta/index<%=sufStr%>?curtPage=${item}">${item }</a>
												</c:otherwise>
											</c:choose>
										</c:forEach>
										...
										<a href="<%=pre %>/b_ta/index<%=sufStr%>?curtPage=${pager.totalPage}">${pager.totalPage}</a>
									</c:when>
									<c:otherwise>
										<c:forEach var = "item" begin="${pager.totalPage - 4 }" end = "${pager.totalPage }">
											<c:choose>
												<c:when test="${pager.curtPage == item }">
												  	<span class="current">${item }</span>
												</c:when>
												<c:otherwise>
													<a href="<%=pre %>/b_ta/index<%=sufStr%>?curtPage=${item}">${item }</a>
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</c:otherwise>
								</c:choose>
						</c:if>
					</c:if>
					<!-- next -->
					<c:choose>
						<c:when test="${pager.curtPage == pager.totalPage }">
							<span class="disabled">Next &gt;</span>
						</c:when>
						<c:otherwise>
							<a href="<%=pre %>/b_ta/index<%=sufStr%>?curtPage=${pager.curtPage + 1}">Next  &gt; </a>
						</c:otherwise>
					</c:choose>	
				</c:if>
			</div>
		</div><!-- 分页结束 -->
	
</body>
</html>