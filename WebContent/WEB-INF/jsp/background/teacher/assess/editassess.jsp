<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML >
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>学员评估编辑</title>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/fbackground.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
<script>
$(document).ready(function(){
	//上传图片
	$("#picFile").change(function(){
		alert('1');
		var picFile = $("#picFile").val();
		if(picFile == ""){
			art.dialog.alert("请选择文件!");
			return;
		}
		else{
			var strFilter = ".jpeg|.gif|.jpg|.png|";
			if(picFile.indexOf(".") > -1){
				var p = picFile.lastIndexOf(".");
				var strPostfix = picFile.substring(p,picFile.length) + '|';
				strPostfix = strPostfix.toLowerCase();
				if(strFilter.indexOf(strPostfix) > -1){
					$.ajaxFileUpload({
						url: '<%=pre%>/b_ta/picupload<%=sufStr%>', 
						type: 'post',
						fileElementId: 'picFile',
						dataType:'json', //返回值类型，一般设置为json、application/json
						success: function(data, status){ 
				            	if(data.ret == 1){
				            		$("#picpreview").attr("src","<%=uploadPath%>/" + data.picpath);
				            		$("#picpath").val(data.picpath);
				            	}
				            	else{
				            		art.dialog.alert("上传失败！");
				            	}
				            },
				            error: function(data, status, e){ 
				            	art.dialog.tips('<span style="color:#f00">上传图片失败，请重传！</span>', 1);
				            }
					});
				}
				}else{
					art.dialog.alert("请选择：jpeg、gif、jpg、png格式的图片！");
				}
		}
	});
	
});
</script>
</head>
<body>
	<div>
		<input type="hidden" id="id" name="id" value="${bean.id }" />
		<table class="tbl001">
			<tr>
				<td>学员姓名：</td>
				<td><input type="text" name="stuname" id="stuname" value="${bean.stuname }" /></td>
			</tr>
			<tr>
				<td>毕业学校：</td>
				<td><input type="text" name="school" id="school" value="${bean.school }"/></td>
			</tr>
			<tr>
				<td>对教师评价：</td>
				<td>
					<textarea cols="70" rows="10" id="comment" name="comment">${bean.comment }</textarea>
				</td>
			</tr>
			<tr>
				<td>头像图片：</td>
				<td>
					<input type="file" name="picFile" id="picFile">
					<img src="<%=uploadPath %>/${bean.picpath}" width="100px" height="100px"/>
					<span style="color:red">选择140*140(px)大小的图片</span>
					<input type="hidden" id="picpath" name="picpath" value="${bean.picpath }">
				</td>
			</tr>
			<tr>
				<td>学生视频vid：</td>
				<td>
					<input type="text" value="${bean.videopath }" id="videopath" />
				</td>
			</tr>
		</table>
	</div>
</body>
</html>