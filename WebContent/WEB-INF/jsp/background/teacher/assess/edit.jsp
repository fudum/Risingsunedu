<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML >
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
<meta charset=UTF-8>
<title></title>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/fbackground.css" />
<style>
	.customerList_item{
		margin-top:20px;
	}
	
	.customerPhoto
	{
		float:left;
		width:140px;
		margin:0 20px;
	}
	
	.customerDesWrap {
		margin-right:20px;
	}
	
	.customerDesWrap .cdw_name{
		color:#333;
		font-size:18px;
		line-height: 1.5;
	}
	
	.customerDesWrap .cdw_des {
		color:#333;
		font-size:14px;
		line-height:1.5;
		margin:10px 0px;
	}
	.clear{
		clear:both;
	}
	
	.fenjie {
		margin:20px 20px 0;
		
	}
	.fenjie hr{
		height:1px;
		border:none;
		border-top:1px dashed #22b4b8;
	}
	.edit_delete{
		float:right;
		width:100px;
	}
	.edit_delete a{
		font-size:12px;
		text-decoration:none;
	}
	.tea_info{
		padding:10px 0;
		font-size:18px;
	}
</style>
<script>
	$(document).ready(function(){
		//添加学员评估
		$("#addAssess").click(function(){
			var teaId = $('#tea_id').val();
			art.dialog.open('<%=pre%>/b_ta/add<%=sufStr%>?tea_id=' + teaId,{
				title:'学员评估',
				width:'800px',
				height:'400px',
				cancel: true,
				ok:function(){
					var iframe = this.iframe.contentWindow;
					if (!iframe.document.body) {
			        	alert('iframe还没加载完毕');
			        	return false;
			        };
			        var stuname = iframe.document.getElementById('stuname').value;
					var school = iframe.document.getElementById('school').value;
					var comment = iframe.document.getElementById('comment').value;
					var picpath = iframe.document.getElementById('picpath').value;
					var videopath = iframe.document.getElementById('videopath').value;
					var tea_id = iframe.document.getElementById('tea_id').value;
					$.ajax({
						type:"post",
						url:"<%=pre%>/b_ta/insert<%=sufStr%>",
						data:{tea_id:tea_id,stuname:stuname,school:school,comment:comment,picpath:picpath,videopath:videopath},
						dataType:"json",
						success:function(data,status){
							if(data.ret == '1'){
								var timer;
								art.dialog({
									width:150,
									height:80,
									content : '<span style="color:#f00">添加成功!</span>',
									init : function() {
										var that = this, i = 1;
										var fn = function() {
											!i && that.close();
											i--;
										};
										timer = setInterval(fn, 1000);
										fn();
									},
									close : function() {
										clearInterval(timer);
										location.reload();
									}
									
								}).show();			
							}
							else{
								art.dialog.alert("添加失败！");
								return false;
							}
						},
						error : function(xhr, textStatus, errorThrown) {
							art.dialog.alert('<span style="color:#f00">添加失败！</span>', 1);
							return false;
						}
					});
				}
			});
		});		
	});
	
	function edit(id){
		art.dialog.open('<%=pre%>/b_ta/editassess<%=sufStr%>?id=' + id,{
			title:'学员评估',
			width:'800px',
			height:'400px',
			cancel: true,
			ok:function(){
				var iframe = this.iframe.contentWindow;
				if (!iframe.document.body) {
		        	alert('iframe还没加载完毕');
		        	return false;
		        };
		        var id = iframe.document.getElementById('id').value;
		        var stuname = iframe.document.getElementById('stuname').value;
				var school = iframe.document.getElementById('school').value;
				var comment = iframe.document.getElementById('comment').value;
				var picpath = iframe.document.getElementById('picpath').value;
				var videopath = iframe.document.getElementById('videopath').value;
				$.ajax({
					type:"post",
					url:"<%=pre%>/b_ta/update<%=sufStr%>",
					data:{id:id,stuname:stuname,school:school,comment:comment,picpath:picpath,videopath:videopath},
					dataType:"json",
					success:function(data,status){
						if(data.ret == '1'){
							var timer;
							art.dialog({
								width:150,
								height:80,
								content : '<span style="color:#f00">添加成功!</span>',
								init : function() {
									var that = this, i = 1;
									var fn = function() {
										!i && that.close();
										i--;
									};
									timer = setInterval(fn, 1000);
									fn();
								},
								close : function() {
									clearInterval(timer);
									location.reload();
								}
								
							}).show();			
						}
						else{
							art.dialog.alert("添加失败！");
							return false;
						}
					},
					error : function(xhr, textStatus, errorThrown) {
						art.dialog.alert('<span style="color:#f00">添加失败！</span>', 1);
						return false;
					}
				});
			}
			
		});
		
	}
</script>
</head>
<body>
	<div>
		<input type="button" class="btn_add" id="addAssess" value="添加学员评估" />
		<input type="hidden" value="${tBean.id }" id="tea_id" />
		
	</div>
	
	<!-- 教师信息 -->
	<div class="tea_info">
		教师姓名：${tBean.name} 性别：${tBean.sex} 教师级别：${tBean.tealevel }
	</div>
	
	<!-- 学员评估列表 -->
	<div>
		<c:forEach items="${list}" var="obj">
			<div class="customerList_item">
				<div class="customerPhoto">
					<a href="#">
						<img src="<%=uploadPath%>/${obj.picpath}" />
					</a>
				</div>
				<div class="customerDesWrap">
					<div class="cdw_name">
						${obj.school}&nbsp;&nbsp;${obj.stuname}
					</div>
					<div class="cdw_des">
						${obj.comment}
					</div>
				</div>
				<div class="edit_delete">
					<a href="#" onclick="javascript:edit('${obj.id}')">编辑</a>|
					<a href="javascript:deleteConfirm('${obj.id }','<%=pre%>/b_ta/delete<%=sufStr%>?id=${obj.id}');">删除</a>
				</div>
				<div class="clear"></div>
			</div>
			<div class="fenjie"><hr/></div>
		</c:forEach>
	</div>
</body>
</html>