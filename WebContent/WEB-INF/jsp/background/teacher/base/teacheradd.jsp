<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<!DOCTYPE HTML >
<html>
<head>
<meta charset=UTF-8>
<title>添加教师页面</title>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/fbackground.css" />

<script type="text/javascript">
$(document).ready(function() {
	$("#saveButton").click(function() {
		var name = $("#name").val();
		if (name == "") {
			$("#nametip").html("*请输入教师姓名！");
			$("#name")[0].focus();
			return false;
		}
		var videopath = $("#videopath").val();
		if(videopath == ""){
			$("#videopathtip").html("请输入腾讯视频vid！");
			$("#videopath")[0].focus();
			return false;
		}
		$("#form1").submit();
	});
		
	$("#goBack").click(function(){
		$("#form2").submit();
	});
	
});
</script>
</head>
<body>
	<form id="form2" action="<%=pre%>/teacher/index<%=sufStr%>" method="post"></form>
	<form id="form1" action="<%=pre%>/teacher/insert<%=sufStr%>" method="post" enctype="multipart/form-data">
		<div class="content">
			<div class="content_title">
				教师添加页面
			</div>
			<div >
				<table class="tbl001">
					<tr>
						<td style="width:10%">教师名称:</td>
						<td style="width:40%">
							<input id="name" name="name" style="width: 150px" type="text" />
							<span class="red" id="nametip">*</span>
						</td>
						<td style="width:10%">性别:</td>
						<td style="width:40%">
							<select id="sex" name="sex">
								<option>男</option>
								<option>女</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>教龄：</td>
						<td>
							<input id="jiaoling" name="jiaoling" type="text" style="width:150px" />
							<span>年</span>
						</td>
						<td>教师级别：</td>
						<td>
							<select id="tealevel" name="tealevel">
								<option>金牌讲师</option>
								<option>银牌讲师</option>
								<option>铜牌讲师</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>腾讯视频vid：</td>
						<td>
							<input type="text" name="videopath" id="videopath" style="width:150px" />
							<span style="color:red" id="videopathtip">*</span>
						</td>
						<td>显示顺序：</td>
						<td>
							<input type="text" name="torder" id="torder" style="width:150px" />
						</td>
					</tr>
					<tr>
						<td>电话：</td>
						<td>
							<input type="text" name="phone" id="phone" style="width:150px" />
						</td>
						<td>单位：</td>
						<td>
							<input type="text" name="danwei" id="danwei" style="width:300px" />
						</td>
					</tr>
					<tr>
						<td>职称：</td>
						<td>
							<input type="text" name="zhicheng" id="zhicheng" style="width:300px" />
						</td>
						<td>职教科目：</td>
						<td>
							<input type="text" name="zhijiaokemu" id="zhijiaokemu" style="width:300px" />
						</td>
					</tr>
					<tr>
						<td>教学科目：</td>
						<td colspan="3">
							<c:forEach items="${listSubject}" var="obj">
								<input name="subject" type="checkbox" value="${obj.name }" />${obj.name }
							</c:forEach>
						</td>
					</tr>
					<tr>
						<td>教学年级：</td>
						<td colspan="3">
							<c:forEach items="${listGrade}" var="obj">
								<input name="grade" type="checkbox" value="${obj.name }" />${obj.name }
							</c:forEach>
						</td>
					</tr>
					<tr>
						<td>带班类型：</td>
						<td colspan="3">
							<c:forEach items="${listTechang}" var="obj">
								<input name="techang" type="checkbox" value="${obj.name }" <c:if test="${obj.name == '基础' }">checked='checked'</c:if>  />${obj.name }
							</c:forEach>
						</td>
					</tr>
					
					<tr>
						<td>选择头像：</td>
						<td colspan="3">
							<input id="headFile" name="headFile" type="file" />
							<span style="color:red">(建议图片大小：220*220px)</span>
						</td>
					</tr>
					<tr>
						<td>选择照片：</td>
						<td colspan="3">
							<input id="picFile" name= "picFile" type="file" />
							
							<span style="color:red">(建议图片高度：300px)</span>
						</td>
					</tr>
					
				</table>
			</div>
			<div>
				<input type="button" value="保存" class="button" id="saveButton" />
				<input type="button" value="返回" class="button" name="goback" id="goBack"/>
			</div>
		</div>
	</form>
</body>
</html>