<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<!DOCTYPE HTML >
<html>
<head>
<meta charset=UTF-8>
<title>教师详情页面</title>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/fbackground.css" />
<script type="text/javascript" src="<%=jmPath %>/build/mediaelement-and-player.min.js" ></script>
<link rel="stylesheet" type="text/css" href="<%=jmPath%>/build/mediaelementplayer.min.css" />
<script type="text/javascript">
	$(document).ready(
		function() {
			//点击提交按钮时候
			$("#saveButton").click(
				function() {
					//教师姓名是否为空
					var name = $("#name").val();
					if (name == "") {
						$("#nametip").html("*请输入教师姓名！");
						$("#name")[0].focus();
						return false;
					}
					
					//腾讯视频vid是否为空
					var videopath = $("#videopath").val();
					if(videopath == ""){
						$("#videopathtip").html("请输入腾讯视频vid！");
						$("#videopath")[0].focus();
						return false;
					}
					
					$("#form1").submit();
				});
			
			//返回按钮
			$("#goBack").click(
				function(){
					$("#form2").submit();
				});
			
			//视频预览功能
			$("#videoPreview").click(
				function(){
					var videoPath = $("#videopath").val();
					art.dialog.open("<%=pre%>/teacher/vedioPreview<%=sufStr%>?vp=" + videoPath,{
						width:650,
					    height:370,
						title:"教师视频简介"
					});
			});
		});
</script>
</head>
<body>
	<form id="form2" action="<%=pre%>/teacher/index<%=sufStr%>" method="post"></form>
	<form id="form1" action="<%=pre%>/teacher/update<%=sufStr%>" method="post" enctype="multipart/form-data">
		<div class="content">
			<div class="content_title">
				教师编辑页面
			</div>
			<div>
				<input type="hidden" name="id" value="${tea.id}" />
			</div>
			<div>
				<table class="tbl001">
					<tr>
						<td style="width:10%">教师姓名：</td>
						<td style="width:40%">
							<input id="name" name="name" type="text" style="width:150px" value="${tea.name }" />
							<span class="red" id="nametip">*</span>
						</td>
						<td style="width:10%">性别:</td>
						<td style="width:40%">
							<select id="sex" name="sex">
								<option <c:if test="${tea.sex == '男'}">selected="selected"</c:if>>男</option>
								<option <c:if test="${tea.sex == '女'}">selected="selected"</c:if>>女</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>教龄：</td>
						<td>
							<input id="jiaoling" name="jiaoling" type="text" style="width:150px" value="${tea.jiaoling }" />
							<span>年</span>
						</td>
						<td>教师级别：</td>
						<td>
							<select id="tealevel" name="tealevel">
								<option <c:if test="${tea.tealevel == '金牌讲师' }">selected='selected'</c:if>>金牌讲师</option>
								<option <c:if test="${tea.tealevel == '银牌讲师' }">selected='selected'</c:if>>银牌讲师</option>
								<option <c:if test="${tea.tealevel == '铜牌讲师' }">selected='selected'</c:if>>铜牌讲师</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>腾讯视频vid：</td>
						<td>
							<input type="text" name="videopath" value="${tea.videopath }" style="width:150px"/>
							<span style="color:red">*</span>
							<input type="button" value="视频预览" id="videoPreview" />
						</td>
						<td>显示顺序：</td>
						<td>
							<input type="text" name="torder" id="torder" style="width:150px" value="${tea.torder }" />
						</td>
					</tr>
					<tr>
						<td>电话：</td>
						<td>
							<input type="text" name="phone" id="phone" style="width:150px" value="${tea.phone }"/>
						</td>
						<td>单位：</td>
						<td>
							<input type="text" name="danwei" id="danwei" style="width:300px" value="${tea.danwei }"/>
						</td>
					</tr>
					<tr>
						<td>职称：</td>
						<td>
							<input type="text" name="zhicheng" id="zhicheng" style="width:300px" value="${tea.zhicheng }" />
						</td>
						<td>职教科目：</td>
						<td>
							<input type="text" name="zhijiaokemu" id="zhijiaokemu" style="width:300px" value="${tea.zhijiaokemu }" />
						</td>
					</tr>
					<tr>
						<td>教学科目：</td>
						<td colspan="3">
							<c:forEach items="${listSubject }" var="obj">
								<input name="subject" type="checkbox" value="${obj.name}" 
									<c:if test="${fn:contains(tea.subject,obj.name)}">checked="checked"</c:if> />${obj.name }
							</c:forEach>
						</td>
					</tr>
					<tr>
						<td>教学年级：</td>
						<td colspan="3">
							<c:forEach items="${listGrade }" var="obj">
								<input name="grade" type="checkbox" value="${obj.name}"
									<c:if test="${fn:contains(tea.grade,obj.name)}">checked="checked"</c:if> />${obj.name }
							</c:forEach>
						</td>
					</tr>
					<tr>
						<td>带班类型：</td>
						<td colspan="3">
							<c:forEach items="${listTechang}" var="obj">
								<input name="techang" type="checkbox" value="${obj.name }"
									<c:if test="${fn:contains(tea.techang,obj.name)}">checked="checked"</c:if> />${obj.name }
							</c:forEach>
						</td>
					</tr>
					
					<tr>
						<td>选择头像：</td>
						<td colspan=3>
							<div style="float:left;width:240px;height:140px;vertical-align:center;">
								<input id="headFile" name= "headFile" type="file" style="line-height:140px" /> 
							</div>
							<div>
								<img src="<%=uploadPath%>/${tea.headpath}" width="120px" height="120px"/>
								<span style="color:red">请选择220*220(px)大小的图像</span>
							</div>
						</td>
					</tr>	
					<tr>
						<td>选择照片：</td>
						<td colspan=3>
							<div style="float:left;width:240px;height:140px;vertical-align:center;">
								<input id="picFile" name= "picFile" type="file" style="line-height:140px" /> 
							</div>
							<div>
								<img src="<%=uploadPath%>/${tea.picpath}" width="180px" height="120px"/>
								<span style="color:red">请选择高度为300px的照片</span>
							</div>
						</td>
					</tr>	
					
					<tr>
						<td>
							<input type="button" value="保存" class="button" id="saveButton" />
							<input type="button" value="返回" class="button" id="goBack" />
						</td>
						<td colspan=3>
							<label style="color:red">
								<c:if test="${tip == 'ok' }">保存成功！</c:if>
								<c:if test="${tip == 'error' }">保存失败！</c:if>
							</label>
						</td>
					</tr>										
				</table>
			</div>
			
		</div>
	</form>
</body>
</html>