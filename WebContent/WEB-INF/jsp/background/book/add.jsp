<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>旭日阳光教育后台-添加书目</title>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/fbackground.css" />
<script>
$(document).ready(function(){
	$("#saveButton").click(function() {
		var grade = $("input[name='grade']:checked").val();
		var subject = $("input[name='subject']:checked").val();
		var xueqi = $("input[name='xueqi']:checked").val();
		var name = $("#name").val();
		var publisher = $("#publisher").val();
		
		$.ajax({
			url : "<%=pre%>/b_book/insert<%=sufStr%>",
			type : 'post',
			dataType : 'html',
			data :{grade:grade,subject:subject,xueqi:xueqi,name:name,publisher:publisher},
			success : function(data, status) {
				if (status == 'success') {
					if (data == '1') {
						var timer;
						art.dialog({
							content : '<span style="color:#f00">插入成功！</span>',
							init : function() {
								var that = this, i = 1;
								var fn = function() {
									!i && that.close();
									i--;
								};
								timer = setInterval(fn, 1000);
								fn();
							},
							close : function() {
								clearInterval(timer);
								window.location.href="<%=pre%>/b_book/index<%=sufStr%>";
							}
						}).show();	
					} else {
						art.dialog.tips('<span style="color:#f00">插入失败！</span>', 1);
					}
				}
			},
			error : function(xhr, textStatus,
					errorThrown) {
				art.dialog.tips('<span style="color:#f00">插入失败！</span>', 1);
			}
		});
	});
});
</script>
</head>
<body>
<div class="content">
	<div class="content_title">
		书本添加页面
	</div>
	<div>
		<form id="frm">
			<table class="tbl001">
				<tr>
					<td>年级：</td>
					<td>
						<c:forEach items="${listGrade}" var="obj">
							<input type="radio" name="grade" value="${obj.name }" >${obj.name }
						</c:forEach>
					</td>
				</tr>
				<tr>
					<td>科目:</td>
					<td>
						<c:forEach items="${listSubject }" var="obj">
							<input type="radio" name="subject" value="${obj.name }" >${obj.name }
						</c:forEach>
					</td>
				</tr>
				<tr>
					<td>学期：</td>
					<td>
						<input type="radio" name="xueqi" value="上学期" >上学期
						<input type="radio" name="xueqi" value="下学期" >下学期
					</td>
				</tr>
				<tr>
					<td>试卷名称：</td>
					<td>
						<input type="text" id="name" name="name" style="width:400px;" />
					</td>
				</tr>
				<tr>
					<td>出版社名称：</td>
					<td>
						<input type="text" id="publisher" name="publisher" style="width:400px;" />
					</td>
				</tr>
			</table>
		</form>
		<div>
			<input type="button" value="保存" class="button" id="saveButton" /> 
			<input type="button" value="返回" class="button" name="goback" onclick="window.history.go(-1);" />
		</div>
	</div>
	
</div>
</body>
</html>