<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>旭日阳光教育后台-书目管理</title>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath %>/fbackground.css" />
<script>
function link() {
	$("#frm").submit();
}
</script>
</head>
<body>
<div class="content">
	<div>
		<form name="frm" id="frm" method="post" action="<%=pre%>/b_book/add<%=sufStr%>"></form>
	</div>
	<div style="float:left">
		<input type="button" class="btn_add" value="添加书本" onclick="link();" />
	</div>
	<div style="clear:both;"></div>
	<div>
		<table class="tbl002" >
			<tr>
				<td>序号</td>
				<td>书本名称</td>
				<td>年级</td>
				<td>科目</td>
				<td>学期</td>
				<td>插入时间</td>
				<td>操作</td>
				<td>序号</td>
			</tr>
			<c:forEach items="${pager.pageList}" var="obj" varStatus="stauts">
				<tr bgcolor="#FFFFFF" align="center">
					<td>${stauts.index +1}</td>
					<td>${obj.name}</td>
					<td>${obj.grade}</td>
					<td>${obj.subject}</td>
					<td>${obj.xueqi}</td>
					<td>${obj.inserttime }</td>
					<td>
						<a href="<%=pre%>/openk/edit<%=sufStr%>?id=${obj.id }">编辑详情</a> | 
						<a href="javascript:deleteConfirm('${obj.id }','<%=pre%>/openk/delete<%=sufStr%>?id=${obj.id}');">删除</a></td>
					<td>${stauts.index +1}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</div>
</body>
</html>