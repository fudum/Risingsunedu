<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<!DOCTYPE HTML >
<html>
<head>
	<meta charset=UTF-8>
	<title>讲义附加信息编辑</title>
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
	<link rel="stylesheet" href="<%=cssPath %>/jiangyiedit.css" />
	<script>
		function showParse(parse){
			art.dialog({
				title:'文字解析',
				content:parse
			});
		}
		
		function showVideo(videoPath){
			alert(videoPath);
			art.dialog.open("../jyparse/vedioPreview<%=sufStr%>?vp=" + videoPath,{title:'视频解析',width:'660px',height:'380px'});
		}
	
		$("document").ready(function(){
			
			//添加题目点击触发事件
			$("#addBttn").click(function(){
				var jyid = $("#jyid").val();
				art.dialog.open(
					"../jyparse/jyparseadd<%=sufStr%>?jy_id="+jyid,
					{
						 width:1000,
						 height: 500,
						 title: '添加讲义解析题目',
						 lock:false
					},
					false);
			});
			
			
		});
	</script>
</head>
<body>
	<div class="MainDiv">
		<input type="hidden" name="id" value="${bean.id}" />
		<table width="99%" border="0" cellpadding="0" cellspacing="0" class="CContent">
			<tr>
				<th class="tablestyle_title">讲义解析详情页面</th>
			</tr>
			<tr>
				<td class="CPanel">
					<div class="jiangyititle">
						<h2>${jyBean.name }</h2>
						<input type="hidden" value="${jyBean.id }" id="jyid"/>
					</div>
					<div class="jiangyiinfo">
						<c:forEach items="${jyParseList}" var="obj">
							<div class="ti_all">
								<div class="divtimu">
									${obj.timu}
								</div>
								<div class="divjiexi">
									<a href="#" onclick="showParse('${obj.parse}')">显示解析</a>									
									<a href="#" onclick="showVideo('${obj.videopath}')">视频讲解</a>
									<a href="#" onclick="javascript:deleteConfirm('${obj.id }','../jyparse/delete<%=sufStr%>?id=${obj.id}');">删除此题</a>
								</div>
							</div>
						</c:forEach>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center" height="50px">
					<input type="button" value="添加题目" class="button" id="addBttn" onclick="addTitle();" />
					<input type="button" value="返回" class="button" id="goBack" />
				</td>
			</tr>
		</table>
	</div>
</body>
</html>