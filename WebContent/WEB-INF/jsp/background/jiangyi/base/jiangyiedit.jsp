<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<!DOCTYPE HTML >
<html>
<head>
<meta charset=UTF-8>
<title>讲义详情页面</title>

<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/fbackground.css" />

<script type="text/javascript">
	$(document).ready(function() {
			//点击提交按钮时候
			$("#saveButton").click(function() {
					var name = $("#name").val();
					//讲义姓名是否为空
					if (name == "") {
						$("#nametip").html("*请输入讲义名称！");
						$("#name")[0].focus();
						return false;
					}
					
					$("#form1").submit();
				});
			
			//返回按钮
			$("#goBack").click(function(){
					$("#form2").submit();
				});
		});
</script>
</head>
<body>
	<form id="form2" action="../jiangyi/jiangyimanage<%=sufStr%>" method="post"></form>
	<form id="form1" action="../jiangyi/jiangyiupdate<%=sufStr%>" method="post" enctype="multipart/form-data">
		<div class="content">
			<div class="content_title">讲义编辑页面</div>
			<div>
				<input type="hidden" value="${bean.id }" name="id"/>
				<table class="tbl001">
					<tr>
						<td>讲义名称：</td>
						<td>
							<input id="name" name="name" style="width: 150px" type="text" value="${bean.name}" />
							<span class="red" id="nametip">*</span>
						</td>
					</tr>
					
					<tr>
						<td>选择班级：</td>
						<td>
							<c:forEach items="${blist }" var="obj" varStatus="status">
							<input type="radio" name="bj_id" 
								value="${obj.id};${obj.grade}${obj.subject}${obj.banci}${obj.bjtype}${obj.lessonday}${obj.starttime}-${obj.endtime}" 
								<c:if test="${obj.id == bean.bj_id }">checked="checked"</c:if>/>
								${obj.grade}${obj.subject}${obj.banci}${obj.bjtype}${obj.lessonday}${obj.starttime}-${obj.endtime} 教师：${obj.tea_name }
								<c:if test="${status.index mod 2 == 1 }"><br/></c:if>
							</c:forEach>
						</td>
					</tr>
					<tr>
						<td>选择要上传的讲义：</td>
						<td>
							<input id="jiangyiFile" name= "jiangyiFile" type="file" />
							<span class="red" id="jiangyiFileTip">*</span>
							<a href="#">${bean.name}</a>
						</td>
					</tr>							
				</table>
			</div>
			<div style="text-align:center" >
				<span style="color:red">${tip}</span>
			</div>
			<div>
				<input type="button" value="保存" class="button" id="saveButton" />
				<input type="button" value="返回" class="button" id="goBack" />
			</div>
		</div>
	</form>
</body>
</html>