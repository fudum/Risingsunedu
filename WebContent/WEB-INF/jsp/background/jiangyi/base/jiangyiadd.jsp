<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<!DOCTYPE HTML >
<html>
<head>
<meta charset=UTF-8>
<title>讲义添加页面</title>

<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/fbackground.css" />

<script type="text/javascript">
	//点击提交按钮时候
	$(document).ready(function() {
			$("#saveButton").click(function(){
					var name = $("#name").val();
					var bj_id = $("input[name='bj_id']:checked").val();
					//讲义姓名是否为空
					if (name == "") {
						$("#nametip").html("*请输入讲义名称！");
						$("#name")[0].focus();
						return false;
					}
					//判断是否选择班级
					if (bj_id == "") {
						alert("请选择班级。");
						return false;
					}
					
					//上传讲义文件
					var jiangyiFile = $("#jiangyiFile").val();
					if(jiangyiFile == "") {
						$("#jiangyiFileTip").html("*请选择要上传的讲义文件！");
						return false;
					}
					
					//提交表单
					$("#form1").submit();
				});
			
			//点击返回按钮
			$("#goBack").click(function(){
				$("#form2").submit();
			});
		});
</script>
</head>
<body>
	<form id="form2" action="/risingsunedu/jiangyi/jiangyimanage<%=sufStr%>" method="post"></form>
	<form id="form1" action="/risingsunedu/jiangyi/jiangyiinsert<%=sufStr%>" method="post" enctype="multipart/form-data">
	<div class="content">
		<div class="content_title">讲义添加页面</div>
		<div>			
			<table class="tbl001">
				<tr>
					<td>讲义名称：</td>
					<td>
						<input id="name" name="name" style="width: 300px" type="text" />
						<span class="red" id="nametip">*</span>
					</td>
				</tr>
				<tr>
					<td>选择班级：</td>
					<td>
						<c:forEach items="${blist }" var="obj" varStatus="status">
							<input type="radio" name="bj_id" 
								value="${obj.id};${obj.grade}${obj.subject}${obj.banci}${obj.bjtype}${obj.lessonday}${obj.starttime}-${obj.endtime}" />
								${obj.grade}${obj.subject}${obj.banci}${obj.bjtype}${obj.lessonday}${obj.starttime}-${obj.endtime} 教师：${obj.tea_name }
							<c:if test="${status.index mod 2 == 1 }"><br/></c:if>
						</c:forEach>
					</td>
				</tr>
				<tr>
					<td>选择要上传的讲义：</td>
					<td>
						<input id="jiangyiFile" name= "jiangyiFile" type="file" />
						<span class="red" id="jiangyiFileTip">*&nbsp;注：请上传pdf格式文件。</span>
					</td>
				</tr>
			</table>
		</div>
		<div>
			<input type="button" value="保存" class="button" id="saveButton" /> 
			<input type="button" value="返回" class="button" name="goback" onclick="window.history.go(-1);" />
		</div>
	</div>
	</form>
</body>
</html>