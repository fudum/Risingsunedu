<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<!DOCTYPE HTML >
<html>
<head>
<meta charset=UTF-8>
<title>讲义管理</title>

<!-- 样式表 -->
<link rel="stylesheet" href="<%=cssPath%>/css.css" type="text/css" />
<link href="<%=cssPath%>/style.css" rel="stylesheet" type="text/css" />
<link href="<%=cssPath%>/user/usermange.css" rel="stylesheet" type="text/css" />
<link href="<%=cssPath %>/fbackground.css" rel="stylesheet"  type="text/css"/>
<script>
	function link() {
		document.getElementById("fom").action = "../jiangyi/jiangyiadd<%=sufStr%>";
		document.getElementById("fom").submit();
	}
</script>

</head>
<body >
	<div class="margin_10">
		<div>
			<form name="fom" id="fom" method="post" action=""></form>
		</div>
		<div>
			<input type="button" value="添加讲义" onclick="link();" class="btn_add" />
		</div>
		<div>
			<table class="tbl002" >
				<tr>
					<td>序号</td>
					<td>讲义名称</td>
					<td>班级名称</td>
					<td>更新时间</td>
					<td>操作</td>
				</tr>
				<c:forEach items="${pager.pageList}" var="obj" varStatus="stauts">
					<tr>
						<td>${stauts.index +1}</td>
						<td>${obj.name}</td>
						<td>${obj.bj_name}</td>
						<td>${obj.updatetime}</td>
						<td>
							<a href="../jiangyi/jiangyiedit<%=sufStr%>?id=${obj.id }">编辑|</a> 
							<a href="javascript:deleteConfirm('${obj.id }','../jiangyi/delete<%=sufStr%>?id=${obj.id}');">删除</a></td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>
</body>
</html>