<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<%
	request.setCharacterEncoding("UTF-8");
	String hdtimu = request.getParameter("timu") != null ? request.getParameter("timu") : "";
	String hdparse = request.getParameter("parse") != null ? request.getParameter("parse") : "";
%>
<!DOCTYPE HTML >
<html>
<head>
	<meta charset=UTF-8>
	<title>讲义附加信息编辑</title>
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
	<link rel="stylesheet" href="<%=kePath %>/themes/default/default.css" />
	<link rel="stylesheet" href="<%=kePath %>/plugins/code/prettify.css" />
	<script charset="utf-8" src="<%=kePath %>/kindeditor.js"></script>
	<script charset="utf-8" src="<%=kePath %>/lang/zh_CN.js"></script>
	<script charset="utf-8" src="<%=kePath %>/plugins/code/prettify.js"></script>
	<script>
		KindEditor.ready(function(K){
			var editortimu = K.create('textarea[name="timu"]', {
				cssPath : '<%=kePath %>/plugins/code/prettify.css',
				uploadJson : '<%=kePath %>/jsp/upload_json.jsp',
				fileManagerJson : '<%=kePath %>/jsp/file_manager_json.jsp',
				allowFileManager : true,
				afterCreate : function() {
					var self = this;
					K.ctrl(document, 13, function() {
						self.sync();
						document.forms['example'].submit();
					});
					K.ctrl(self.edit.doc, 13, function() {
						self.sync();
						document.forms['example'].submit();
					});
				}
			});
			var editorparse = K.create('textarea[name="parse"]', {
				cssPath : '<%=kePath %>/plugins/code/prettify.css',
				uploadJson : '<%=kePath %>/jsp/upload_json.jsp',
				fileManagerJson : '<%=kePath %>/jsp/file_manager_json.jsp',
				allowFileManager : true,
				afterCreate : function() {
					var self = this;
					K.ctrl(document, 13, function() {
						self.sync();
						document.forms['example'].submit();
					});
					K.ctrl(self.edit.doc, 13, function() {
						self.sync();
						document.forms['example'].submit();
					});
				}
			});
			prettyPrint();
		});
	</script>
</head>
<body>
	<form id="example" action="../jyparse/jyparseinsert<%=sufStr%>" method="post" enctype="multipart/form-data">
		<div class="MainDiv">
			<input type="hidden" name="id" value="${bean.id}" />
			<input type="hidden" name="jy_id" value="${bean.jy_id }" />
			<div>
				<div>
					<div>
						<div style="background-color:#6795B4;color:#FFF;height:20px;line-height:20px;font-size: 16px;padding-left: 10px;">
							题目
						</div>
						<textarea id="timu" name="timu" style="width:1000px;height:150px;">
							<%=htmlspecialchars(hdtimu)%>
						</textarea>
					</div>
				</div>
				<div>
					<div>
						<div style="background-color:#6795B4;color:#FFF;height:20px;line-height:20px;font-size: 16px;padding-left: 10px;">
							文字解析
						</div>
						<textarea id="parse" name="parse" style="width:1000px;height:150px;">
							<%=htmlspecialchars(hdparse)%>
						</textarea>
					</div>
				</div>
				<div>
					<div style="background-color:#6795B4;color:#FFF;height:20px;line-height:20px;font-size: 16px;padding-left: 10px;">
						视频解析
					</div>
					<div>
						<input type="file" name="videoFile"/>
						<a href="#">视频查看</a>
					</div>
				</div>
			</div>
			<div>
				<input type="submit" value="保存" class="button" id="saveButton" />
				<input type="button" value="返回" class="button" id="goBack" />
			</div>
		</div>
	</form>
</body>
<%!
	private String htmlspecialchars(String str) {
		str = str.replaceAll("&", "&amp;");
		str = str.replaceAll("<", "&lt;");
		str = str.replaceAll(">", "&gt;");
		str = str.replaceAll("\"", "&quot;");
		return str;
	}
%>
</html>