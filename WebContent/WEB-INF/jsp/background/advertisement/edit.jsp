<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/b_common_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/fbackground.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
<title>首页广告-编辑</title>
<script>
$(document).ready(function(){
	//保存按钮
	$("#saveButton").click(function() {
		var picpath = $("#picpath").val();
		var porder = $("#porder").val();
		var id = $("#id").val();
		//ajax提交
		$.ajax({
			url : "../b_adv/update<%=sufStr%>",
			type : 'post',
			dataType : 'html',
			data : {id:id,picpath:picpath,porder:porder},
			success : function(data, status) {
				if (status == 'success') {
					if (data == '1') {
						var timer;
						art.dialog({
							content : '<span style="color:#f00">编辑成功！</span>',
							init : function() {
								var that = this, i = 1;
								var fn = function() {
									!i && that.close();
									i--;
								};
								timer = setInterval(fn, 1000);
								fn();
							},
							close : function() {
								clearInterval(timer);
								window.location.href="<%=pre%>/b_adv/index<%=sufStr%>";
							}
						}).show();	
					} else {
						art.dialog.tips('<span style="color:#f00">编辑失败！</span>', 1);
					}
				}
			},
			error : function(xhr, textStatus,
					errorThrown) {
				art.dialog.tips('<span style="color:#f00">插入失败！</span>', 1);
			}
		});
	});
	
	//上传图片
	$("#uploadFile").live('change',function(){
		var picFile = $("#uploadFile").val();
		var wjjname = "advertisement";
		if(picFile == ""){
			art.dialog.alert("请选择文件!");
			return;
		}
		else{
			var strFilter = ".jpeg|.gif|.jpg|.png|";
			if(picFile.indexOf(".") > -1){
				var p = picFile.lastIndexOf(".");
				var strPostfix = picFile.substring(p,picFile.length) + '|';
				strPostfix = strPostfix.toLowerCase();
				if(strFilter.indexOf(strPostfix) > -1){
					$.ajaxFileUpload({
						url: '<%=pre%>/b_com/fileupload<%=sufStr%>', 
						data:{wjjname:wjjname},
						type: 'post',
						fileElementId: 'uploadFile',
						dataType:'json', //返回值类型，一般设置为json、application/json
						success: function(data, status){ 
				            	if(data.ret == 1){
				            		$("#catalogtip").html("文件："+data.path);
				            		$("#picpath").val(data.path);
				            	}
				            	else{
				            		art.dialog.alert("上传失败！");
				            	}
				            },
				            error: function(data, status, e){ 
				            	art.dialog.tips('<span style="color:#f00">上传失败，请重传！</span>', 1);
				            }
					});
				}else{
					art.dialog.alert("请选择：图片格式的文件！");
				}
			}
		}
	});
	
});
</script>
</head>
<body>
<div class="content">
	<div class="content_title">
		首页图片添加页面
	</div>
	<input type="hidden" id="id" value="${bean.id }">
	<table class="tbl001">
		<tr>
			<td>显示顺序：</td>
			<td><input type="text" name="porder" id="porder" value="${bean.porder}"></td>
		</tr>
		<tr>
			<td>图片文件：</td>
			<td>
				<input type="file" name="uploadFile" id="uploadFile">
				<span id="catalogtip" style="color:red">${bean.picpath }</span>
				<input id="picpath" type="hidden" value="${bean.picpath}">
				<img src="<%=uploadPath%>/${bean.picpath}" width="200px" height="100px"/>
			</td>
		</tr>
		<tr>
			<td><input type="submit" value="保存" class="button" id="saveButton" /></td>
			<td></td>
		</tr>
	</table>
</div>
</body>
</html>