<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="framework.base.common.BaseConstants"%>
<%@page import="framework.base.utils.PublicCacheUtil"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@include file="/WEB-INF/jsp/include/include_common.jsp"%>
<%
	String backgroundPath = path + "/resource/background";
	String commonPath = path + "/resource/common";
	String jsPath = path + "/resource/background/js";
	String cssPath = path + "/resource/background/css";
	String imagePath = path + "/resource/background/image";
	String uploadPath = path + "/upload";
	String jmPath = commonPath + "/johndyer-mediaelement";
	String kePath = commonPath + "/kindeditor-4.1.10";
	String artDialogPath = commonPath + "/artDialog";
	String validationPath = commonPath + "/jquery-validation-1.13.1";
	String sufStr =".html";
%>


<link rel="SHORTCUT ICON" HREF="<%=imagePath%>/logo.ico">
<script type="text/javascript" src="<%=commonPath%>/jquery/jquery.min.js"></script>
<link rel="stylesheet" href="<%=artDialogPath%>/skins/default.css?4.1.7">
<script type="text/javascript" src="<%=artDialogPath%>/jquery.artDialog.js?skin=default"></script>
<script type="text/javascript" src="<%=artDialogPath%>/plugins/iframeTools.js"></script>
<script type="text/javascript" src="<%=jsPath%>/jquery_delete.js"></script>
<script type="text/javascript" src="<%=jsPath%>/ajaxfileupload.js"></script>

<!--引入my97datepicker控件  -->
<link rel="stylesheet" type="text/css" href="<%=backgroundPath%>/My97DatePicker/skin/WdatePicker.css">
<script type="text/javascript" src="<%=backgroundPath%>/My97DatePicker/calendar.js"></script>
<script type="text/javascript" src="<%=backgroundPath%>/My97DatePicker/WdatePicker.js"></script>

<link rel="stylesheet" href="<%=kePath %>/themes/default/default.css" />
<link rel="stylesheet" href="<%=kePath %>/plugins/code/prettify.css" />
<script charset="utf-8" src="<%=kePath %>/kindeditor.js"></script>
<script charset="utf-8" src="<%=kePath %>/lang/zh_CN.js"></script>
<script charset="utf-8" src="<%=kePath %>/plugins/code/prettify.js"></script>
<script>
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "//hm.baidu.com/hm.js?6ee6f002fd402f593e90cdaa93f8ed7c";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
</script>
