<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="framework.base.common.BaseConstants"%>
<%@page import="framework.base.utils.PublicCacheUtil"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@include file="/WEB-INF/jsp/include/include_common.jsp"%>
<%
	String js = path + "/resource/front001/js";
	String css = path + "/resource/front001/css";
	String images = path + "/resource/front001/images";	
	String jm = path + "/resource/common/johndyer-mediaelement";
	String artDialog = path + "/resource/common/artDialog";
	String uploadPath = path + "/upload";
%>
<link rel="SHORTCUT ICON" HREF="<%=images%>/logo.ico">
<script type="text/javascript" src="<%=js%>/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<%=artDialog%>/jquery.artDialog.js?skin=risingsunedu"></script>
<script type="text/javascript" src="<%=artDialog%>/plugins/iframeTools.js"></script>
<script type="text/javascript" src="<%=common%>/jquery/md5.js"></script>
<script>
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "//hm.baidu.com/hm.js?2abe4e27bf7d228f051df86f1faf399a";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
</script>
