<%@page import="framework.base.common.BaseConstants"%>
<%@page import="framework.base.utils.PublicCacheUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	String path = request.getContextPath();
	String backgroundPath = path + "/resource/risingsunedu";
	String commonPath = path + "/resource/common";
	String jsPath = path + "/resource/risingsunedu/js";
	String cssPath = path + "/resource/risingsunedu/css";
	String imagePath = path + "/resource/risingsunedu/image";
	String uploadPath = path + "/upload";
	String jmPath = path + "/resource/common/johndyer-mediaelement";
	String artDialogPath = path + "/resource/common/artDialog";
	String sufStr = ".html";
%>


<script type="text/javascript" src="<%=commonPath%>/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<%=artDialogPath%>/jquery.artDialog.js?skin=default"></script>
<script type="text/javascript" src="<%=artDialogPath%>/plugins/iframeTools.js"></script>

<link rel="SHORTCUT ICON" HREF="<%=imagePath%>/logo.ico">
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/risingsunedu.css">
<link rel="stylesheet" type="text/css" href="<%=jsPath%>/artDialog/skins/default.css">