<%@page import="framework.base.common.BaseConstants"%>
<%@page import="framework.base.utils.PublicCacheUtil"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@include file="/WEB-INF/jsp/include/include_common.jsp"%>
<%
	String js = path + "/resource/front001/js";
	String css = path + "/resource/front001/css";
	String images = path + "/resource/front001/images";	
	String jm = path + "/resource/common/johndyer-mediaelement";
	String artDialog = path + "/resource/common/artDialog";
	String uploadPath = path + "/upload";
%>


<link rel="SHORTCUT ICON" HREF="<%=images%>/logo.ico">
<link rel="stylesheet" type="text/css" href="<%=css%>/responsiveslides.css">
<link rel="stylesheet" type="text/css" href="<%=css%>/style.css" media="all" />
<link rel="stylesheet" type="text/css" href="<%=css %>/f.css" />
<script type="text/javascript" src="<%=js%>/jquery-1.8.3.min.js"></script>
<script src="<%=js%>/responsiveslides.min.js"></script>
<script type="text/javascript" src="<%=common%>/jquery/md5.js"></script>
<script type="text/javascript" src="<%=artDialog%>/jquery.artDialog.js?skin=risingsunedu"></script>
<script type="text/javascript" src="<%=artDialog%>/plugins/iframeTools.js"></script>

