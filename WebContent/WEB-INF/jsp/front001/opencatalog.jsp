<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/front001_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="renderer" content="webkit" />
<title>旭日阳光教育-公开课目录</title>
<script type="text/javascript" src="<%=js%>/jquery.media.js"></script>
<style>
.catalogpdf{
	border:1px solid #f5f5f5;
	display:block;
}
.sjname{
	color:#333;
	font-size:24px;
	float:left;
}
.t_downpdf{
	line-height:18px;
}
.downloadbtn{
	padding:5px 8px;
	color: #FFF;
	background: #22B4B8;
	display: inline-block;
	font-size:14px;
	text-transform: uppercase;
	-webkit-transition: all 0.3s ease-out;
	-moz-transition: all 0.3s ease-out;
	-ms-transition: all 0.3s ease-out;
	-o-transition: all 0.3s ease-out;
	transition: all 0.3s ease-out;
}
</style>
<script>
function toDownload(storeName, realName) {
	document.getElementById('storeName').value = storeName;
	document.getElementById('realName').value = realName;
	document.getElementById('downForm').submit();
}
</script>

</head>
<body>
	<div>
		<div class="t_downpdf">
			<a href="javascript:void();" class="downloadbtn" onclick="javascript:toDownload('${bean.catalog}','${bean.name}');">下载详情</a>
		</div>
	</div>
	<div class="catalogpdf">
		<a class="media" href="<%=uploadPath%>/${bean.catalog}"></a> 
	</div>
	<div>
		<form id="downForm" action="<%=pre%>/open/download<%=suf%>" method="post">
			<input type="hidden" name="storeName" id="storeName" value="" /> 
			<input type="hidden" name="realName" id="realName" value="" />
		</form>
	</div>	
</body>
</html>
<script>
$("document").ready(function(){
	$('a.media').media({width:830, height:400});
});
</script>