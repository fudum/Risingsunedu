<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML >
<%@include file="/WEB-INF/jsp/include/front001_include.jsp"%>
<html>
<head>
<meta charset=UTF-8>
<meta name="renderer" content="webkit" />
<title>旭日阳光教育-公开课</title>
<link rel="stylesheet" type="text/css" href="<%=css %>/fenye.css" />
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css">
<style>

/**课程详情**/
.course_item {
	overflow: hidden;
	border: 1px solid #ededed;
	background: #fcfcfc;
	margin-bottom: 10px;
	padding-bottom: 10px;
}
.ui_courses .ui_avatar {
	float: left;
	display: inline;
	margin: 10px 10px 0 10px;
}
.ui_avatar {
	width: 50px;
	position: relative;
	overflow: hidden;
}

.ui_avatar ul.avatar_items {
	width:60px;
	height: 78px;
	overflow: hidden;
}
.ui_avatar ul.avatar_items li {
	float: left;
	margin-right: 10px;
}

.ui_avatar ul.avatar_items li img {
	width: 50px;
	height: 50px;
}

.ui_avatar ul.avatar_items li a {
	display: block;
	text-align: center;
	line-height: 180%;
	color: #666666;
	font-size:0.7em;
}
.ui_avatar ul.avatar_items li a:hover {
	text-decoration:underline;
}

.course_detail {
	float: left;
	display: inline;
}
.course_detail .course_title {
	font-size: 1em;
	font-family: "Microsoft YaHei";
	color: #333333;
	font-weight: normal;
	padding: 7px 0;
}

.course_detail .course_title em{
	float:left;
	display:inline-block;
	width:40px;
	line-height:20px;
	background-image:url("<%=images%>/icon_subject.png");
	background-repeat: no-repeat;
	color:#ffffff;
	font-size:12px;
	text-indent:3px;
}
.course_detail .course_info {
	margin-bottom: 7px;
	color: #999999;
	font-size: 12px;
}
.course_detail .course_info a {
	margin-left: 10px;
	color:#22b4b8;
}
.course_detail .course_info a:hover {
	text-decoration:underline;
}
.course_detail .course_title a {
	color: #333333;
	padding-left:5px;
}
.course_detail .course_func {
	margin-bottom: 5px;
	overflow: hidden;
	line-height: 25px;
	height: 25px;
	display: block;
}
.course_detail .course_func .btn_audition {
	float:left;
	width:75px;
	height:25px;
	line-height:25px;
	text-decoration:none;
	margin-right:20px;
	color:#22B4B8;
	background:url("<%=images%>/icon_play1.png");
	text-indent:20px;
	font-size:0.7em;
}
.course_detail .course_func a.btn_audition:hover {
	background:url("<%=images%>/icon_play3.png");
	color:#fff;
}
.icon {
	display:inline-block;
	width:25px;
	height:25px;
	line-height:25px;
	text-align: center;
	background-image: url("<%=images%>/icon_play.png");
}

.course_detail .course_func span.num_videos strong {
	display:inline-block;
	height:25px;
	line-height:25px;
	vertical-align: top;
	color:#FF6700;
}
	
.course_detail .course_title a:hover {
	text-decoration:underline;
}
</style>
<script>
	$(document).ready(function(){
		$("#top_6").addClass("active");
	});
	function changeGrade(border,grade){
		$("#o_grade").val(grade);
		$("#ddgrade a").removeClass("current");
		$("#o_page").val(1);
		$("#g"+border).addClass("current");
		myAjax();
	}
	
	function changeSubject(border,subject){
		$("#o_subject").val(subject);
		$("#o_page").val(1);
		$("#ddsubject a").removeClass("current");
		$("#s"+border).addClass("current");
		myAjax();
	}
	
	function changeKctype(border,kctype){
		$("#o_kctype").val(kctype);
		$("#o_page").val(1);
		$("#ddkctype a").removeClass("current");
		$("#k" + border).addClass("current");
		myAjax();
	}
	
	function changePage(page){
		$("#o_page").val(page);
		myAjax();
	}
	
	function myAjax(){
		$("#form").submit();
	}
	
	function showCatalog(id){
		art.dialog.open("../open/showcatalog.html?id=" +id,{
			title:'目录详情',
			width:840,
			height:430
		});
	}
</script>
</head>
<body>
	<!-- 头部 -->
	<%@include file="/WEB-INF/jsp/front001/top.jsp"%>
	<div class="content">
		<div class="wrap">
			<div class="top-head">
				<ul>
					<li><a href="index.html">首页 / </a></li>
					<li><a href="#"><span>公开课</span></a></li>
				</ul>
			</div>
			<div class="clear"></div>
			<div class="container">
				<div>
					<form id="form" action="<%=pre%>/open/index<%=suf%>" method="get" >
						<input type="hidden" id = "o_grade" name="grade" value="${bean.grade }"/>
						<input type="hidden" id="o_subject" name="subject" value="${bean.subject }" />
						<input type="hidden" id="o_kctype" name="kctype" value="${bean.kctype }"/>
						<input type="hidden" id="o_page" name="curtPage" value="${pager.curtPage }" />
					</form>
				</div>
				<div class="ui_filter">
					<dl class="filter_item border_bottom">
						<dt>年级</dt>
						<dd id="ddgrade">
							<a href="javascript:void(0)" id="g0" onclick="javascrpit:changeGrade('0','')" <c:if test="${bean.grade == '' || bean.grade == null }">class="current"</c:if>>全部</a>
							<c:forEach items="${listbt }" var="obj">
								<c:if test="${obj.botype == '年级' }">
									<a href="javascript:void(0)" id="g${obj.border }" onclick="javascript:changeGrade('${obj.border}','${obj.name }')" <c:if test="${bean.grade == obj.name }">class="current"</c:if>>${obj.name }</a>
								</c:if>
							</c:forEach>
						</dd>
					</dl>
					<dl class="filter_item border_bottom">
						<dt>科目</dt>
						<dd id="ddsubject">
							<a href="javascript:void(0)" id="s0" onclick="javascript:changeSubject('0','')" <c:if test="${bean.subject == '' || bean.subject == null}">class="current"</c:if>>全部</a>
							<c:forEach items="${listbt }" var="obj">
								<c:if test="${obj.botype == '科目' }">
									<a href="javascript:void(0)" id="s${obj.border }" onclick="javascript:changeSubject('${obj.border}','${obj.name }')" <c:if test="${bean.subject == obj.name }">class="current"</c:if>>${obj.name }</a>
								</c:if>
							</c:forEach>
						</dd>
					</dl>
					<dl class="filter_item">
						<dt>课程类型</dt>
						<dd id="ddkctype">
							<a href="javascript:void(0)" id="k0" onclick="javascript:changeKctype('0','')" <c:if test="${bean.kctype == '' || bean.kctype == null}">class="current"</c:if>>全部</a>
							<c:forEach items="${listbt }" var="obj">
								<c:if test="${obj.botype == '课程类型' }">
									<a href="javascript:void(0)" id="k${obj.border }" onclick="javascript:changeKctype('${obj.border}','${obj.name }')" <c:if test="${bean.kctype == obj.name }">class="current"</c:if>>${obj.name }</a>
								</c:if>
							</c:forEach>
						</dd>
					</dl>
				</div>
				<!-- 课程详情 -->
				<div class="ui_courses" id="ui_courses">
					<c:forEach items="${pager.pageList}" var="obj">
						<div class="course_item">
							<div class="ui_avatar">
								<ul class="avatar_items">
									<li class="ui_userinfo">
										<a href="#" class="userpic">
											<img src="<%=uploadPath %>/${obj.teacherBean.headpath}" />
										</a>
										<a href="<%=pre%>/tea/info<%=suf%>?id=${obj.tea_id }" class="username" target="_blank">${obj.teacherBean.name }</a>
									</li>
								</ul>
							</div>
							<div class="course_detail">
								<p class="course_title">
									<em>${obj.subject }</em>
									<a href="<%=pre%>/opensec/index<%=suf %>?id=${obj.id }" target="_blank">${obj.name }</a>
								</p>
								<p class="course_info">
									<span>适用于：${obj.grade }学生</span>
									<span>课程类型：${obj.kctype }</span>
									<a href="javascript:void();" onclick="javascript:showCatalog('${obj.id}')">目录详情</a>
								</p>
								<p class="course_func">
									<a href="<%=pre%>/opensec/index<%=suf %>?id=${obj.id }" target="_blank" class="btn_audition">立即听课</a>
									<span class="num_videos">
										<em class="icon"></em>
										<strong>${obj.playnum }</strong>
									</span>
								</p>
							</div>
						</div>
					</c:forEach>
				</div>
				
				<!-- 分页 -->
				<div id="divFenye"><!-- 分页开始 -->
					<div class="manu">
						<c:if test="${pager.totalPage >0}">
							<!-- prev -->
							<c:choose>
								<c:when test="${pager.curtPage == 1 }">
									<span class="disabled">&lt;上一页</span>
								</c:when>
								<c:otherwise>
									<a href="javascript:void();" onclick="changePage('${pager.curtPage - 1}')">&lt; 上一页 </a>
								</c:otherwise>
							</c:choose>
							<c:if test="${pager.totalPage < 7 }">
								<c:forEach var = "item" begin="1" end = "${pager.totalPage }">
									<c:choose>
										<c:when test="${pager.curtPage == item }">
										  	<span class="current">${item }</span>
										</c:when>
										<c:otherwise>
											<a href="javascript:void();" onclick="changePage('${item}')" >${item }</a>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</c:if>
							<c:if test="${pager.totalPage >= 7 }">
								<c:if test="${pager.curtPage <= 4 }">
									<c:forEach var = "item" begin="1" end = "5">
										<c:choose>
											<c:when test="${pager.curtPage == item }">
											  	<span class="current">${item }</span>
											</c:when>
											<c:otherwise>
												<a href="javascript:void();" onclick="changePage('${item}')" >${item }</a>
											</c:otherwise>
										</c:choose>
									</c:forEach>
									...
									<a href="javascript:void();" onclick="changePage('${pager.totalPage}')" >${pager.totalPage}</a>
								</c:if>
								<c:if test="${pager.curtPage > 4 }">
									<a href="javascript:void();" onclick="changePage('1')" >1</a>
									...
									<c:choose>
											<c:when test="${pager.curtPage < pager.totalPage - 3 }">
												<c:forEach var = "item" begin="${pager.curtPage - 2 }" end = "${pager.curtPage + 2 }">
													<c:choose>
														<c:when test="${pager.curtPage == item }">
														  	<span class="current">${item }</span>
														</c:when>
														<c:otherwise>
															<a href="javascript:void();" onclick="changePage('${item}')" >${item }</a>
														</c:otherwise>
													</c:choose>
												</c:forEach>
												...
												<a href="javascript:void();" onclick="changePage('${pager.totalPage}')" >${pager.totalPage}</a>
											</c:when>
											<c:otherwise>
												<c:forEach var = "item" begin="${pager.totalPage - 4 }" end = "${pager.totalPage }">
													<c:choose>
														<c:when test="${pager.curtPage == item }">
														  	<span class="current">${item }</span>
														</c:when>
														<c:otherwise>
															<a href="javascript:void();" onclick="changePage('${item}')" >${item }</a>
														</c:otherwise>
													</c:choose>
												</c:forEach>
											</c:otherwise>
										</c:choose>
								</c:if>
							</c:if>
							<!--next-->
							<c:choose>
								<c:when test="${pager.curtPage == pager.totalPage }">
									<span class="disabled">下一页 &gt;</span>
								</c:when>
								<c:otherwise>
									<a href="javascript:void();" onclick="changePage('${pager.curtPage + 1}')" >下一页  &gt; </a>
								</c:otherwise>
							</c:choose>	
						</c:if>
					</div>
				</div><!--分页结束-->
				
			</div>
		</div>
	</div>
	
	<!--底部-->
	<%@include file="/WEB-INF/jsp/front001/footer.jsp"%>
</body>
</html>