<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/front001_include.jsp"%>
<html>
<head>
<meta name="renderer" content="webkit" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>旭日阳光教育-课程详细信息</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/kechenginfo.css" />
<script>
$(document).ready(function(){
	$("#top_8").addClass("active");
});
</script>
</head>
<body>
	<!-- 头部 -->
	<%@include file="/WEB-INF/jsp/front001/top.jsp"%>
	<div class="content">
		<div class="wrap">
			<div class="top-head">
				<ul>
					<li><a href="#">首页 / </a></li>
					<li><a href="#"><span>我要报名</span>/</a></li>
					<li><a href="#"><span>课程详情</span></a></li>
				</ul>
			</div>
			<div class="clear"></div>
			
			<div class="coures_content">
				<div class="content_title">
					<h2>${bean.name }</h2>
				</div>
				<div class="content_player">
					<div class="player_list">
						<div class="list_title">
							<span style="padding-left:20px;">课程内容</span>
						</div>
						<div class="ui_scrolls scrolls_free">
							<div class="scrolls_item jspScrollable" >
								<div class="jspContainer" style="width: 230px; height: 465px;">
									<div class="jspPane">
										<div class="scrolls_con">
											<ul class="select_list">
												<c:forEach items ="${myMap}" var = "objMap" varStatus="statusMap" >
													<li class="scrolls_title">
														<c:set var="str" value="${fn:split(objMap.mykey, ';')}" />
														第${str[0]}讲 ${str[1] }
													</li>
													<c:forEach items="${objMap.mylist}" var="obj" varStatus="status">
														<li id="${obj.id }" <c:if test="${status.index ==0 &&  statusMap.index == 0 }">class="current"</c:if>>
															<p>
																<em class="icon icon_live_gary"></em>
																<c:choose>
																	<c:when test="${statusMap.index < 2 }">
																		<a href="javascript:void(0)" onclick="javascript:changeVideo('${obj.videopath }','${obj.id}')">${obj.jieci_name }</a>
																	</c:when>
																	<c:otherwise>
																		<a href="javascript:void(0)" onclick="javascript:changeVideo('','${obj.id}')">${obj.jieci_name }</a>
																	</c:otherwise>
																</c:choose>
															
															</p>
														</li>
													</c:forEach>	
												</c:forEach>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="player_live">
						<div id="videoPlay">
							<embed id="embed1" wmode="opaque" src="http://static.video.qq.com/TPout.swf?auto=1&vid=${videoStr }" 
								quality="high" width="745" height="465" align="middle" allowScriptAccess="sameDomain" 
								allowFullscreen="true" type="application/x-shockwave-flash"></embed>
						</div>
					</div>
					<div class="player_info">
						<dl>
							<dt>授课老师：</dt>
							<dd>
								<div class="ui_avatar_con">
									<div class="ui_avatar">
										<ul class="avatar_items">
											<li>
												<a href="<%=pre %>/tea/info.html?id=${teacherBean.id}" class="userpic" target="_blank">
													<img src="<%=uploadPath %>/${teacherBean.headpath}" alt="${teacherBean.name }">
												</a>
											</li>
										</ul>
									</div>
								</div>
							</dd>
						</dl>
						<div class="listen_num">
							<span>价格：￥${bean.price}</span>
						</div>
					</div>
				</div>
			</div>
			
			<div class="clear"></div>
			<div class="region">
				<div class="mod-tabCon">
					<div class="mhd">
						<span>课程详情</span>
					</div>
					<div class="mbd">
						${bean.catalog }
					</div>
				</div>
			</div>
			
			
		</div><!-- wrap -->
	</div>
	<!--底部-->
	<%@include file="/WEB-INF/jsp/front001/footer.jsp"%>
</body>
<script>
	function changeVideo(videopath,id){
		$(".select_list li").each(function(){
			if($(this).attr("class") == 'current'){
				$(this).attr("class","");
			}
		});
		$("#" + id).attr("class",'current');
		
		if(videopath == ''){
			$.ajax({
				type:'post',
				url:'<%=pre%>/home/check<%=suf%>',
				dataType:"json",
				success:function(data,status){
					if(data == 0){
						login('<%=pre%>/home/login<%=suf%>','<%=pre%>/home/loginin<%=suf%>');
					}
					else{
						$.ajax({
							type:'post',
							url:'<%=pre%>/course/vid<%=suf%>',
							data:{id:id},
							dataType:"json",
							success:function(data,status){
								if(data[0] == 1){
									videopath = data[1];
									var strHtml = "<embed src='http://static.video.qq.com/TPout.swf?auto=1&vid="+ videopath +"'";
									strHtml += 'quality="high" width="745" height="465" align="middle" allowScriptAccess="sameDomain"'; 
									strHtml += 'allowFullscreen="true" type="application/x-shockwave-flash" wmode="opaque"></embed>';
									$("#videoPlay").html(strHtml);
								}
								else{
									changeVideo(videopath,id);
								}
							},
							error : function(xhr, textStatus, errorThrown) {
								return false;
							}
							
						});
					}
					
				},
				error : function(xhr, textStatus, errorThrown) {
					return false;
				}
			});
		}
		else{
			var strHtml = "<embed src='http://static.video.qq.com/TPout.swf?auto=1&vid="+ videopath +"'";
			strHtml += 'quality="high" width="745" height="465" align="middle" allowScriptAccess="sameDomain"'; 
			strHtml += 'allowFullscreen="true" type="application/x-shockwave-flash" wmode="opaque"></embed>';
			$("#videoPlay").html(strHtml);
		}		
	}
</script>
</html>