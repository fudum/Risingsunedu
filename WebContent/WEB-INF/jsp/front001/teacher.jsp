<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/front001_include.jsp"%>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="renderer" content="webkit" />
<meta name="author" content="www.fudum.com"/>
<meta name="description" content="旭日阳光教育-教师基本信息"/>
<title>旭日阳光教育-教师信息</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css">
<link rel="stylesheet" type="text/css" href="<%=css %>/fenye.css" />
<style>
.team-member .teainfo ul{font-size: 0.875em;color: rgba(85, 85, 85, 0.53);line-height: 1.5em;font-family: Arial, Helvetica, sans-serif;padding: 6px 0px 5px 0px;				}
.tea-member .teainfo ul li {float:left;}
</style>
<script>
    $(function(){
      $("#slider1").responsiveSlides({
        maxwidth: 1600,
        speed: 600
      });
});

jQuery(document).ready(function($) {
	$("#top_5").addClass("active");
	$(".scroll").click(function(event){		
		event.preventDefault();
		$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
	});
});

function changeGrade(border,name){
	$("#o_grade").val(name);
	$("#o_page").val(1);
	$("#ddgrade a").removeClass("current");
	$("#g"+border).addClass("current");
	myAjax();
}

function changeSubject(border,name){
	$("#o_subject").val(name);
	$("#o_page").val(1);
	$("#ddsubject a").removeClass("current");
	$("#s"+border).addClass("current");
	myAjax();
}

function changeBanci(border,name){
	$("#o_techang").val(name);
	$("#o_page").val(1);
	$("#ddbanci a").removeClass("current");
	$("#bc"+border).addClass("current");
	myAjax();
}

function changeJiaoling(border,name){
	$("#o_jiaoling").val(name);
	$("#o_page").val(1);
	$("#ddjiaoling a").removeClass("current");
	$("#jl"+border).addClass("current");
	myAjax();
}

function changePage(page){
	$("#o_page").val(page);
	myAjax();
}

function myAjax(){
	$("#form").submit();
}

</script>
</head>
<body>
	<!-- 头部 -->
	<%@include file="/WEB-INF/jsp/front001/top.jsp"%>
	<div class="content">
		<div class="wrap">
			<div class="top-head">
				<ul>
					<li><a href="index.html">首页 / </a></li>
					<li><a href="#"><span>教师信息</span></a></li>
				</ul>
			</div>
			<div class="clear"></div>
			<div class="container">
				<div>
					<form id="form" action="<%=pre%>/tea/index<%=suf%>" method="get" >
						<input type="hidden" id = "o_grade" name="grade" value="${bean.grade }"/>
						<input type="hidden" id="o_subject" name="subject" value="${bean.subject }" />
						<input type="hidden" id="o_techang" name="techang" value="${bean.techang }"/>
						<input type="hidden" id="o_jiaoling" name="jiaoling" value="${bean.jiaoling }"/>
						<input type="hidden" id="o_page" name="curtPage" value="${pager.curtPage }" />
					</form>
				</div>
				<div class="ui_filter">
					<dl class="filter_item border_bottom">
						<dt>年级</dt>
						<dd id="ddgrade">
							<a href="javascript:void(0)" id="g0" onclick="javascrpit:changeGrade('0','')" <c:if test="${bean.grade == '' || bean.grade == null }">class="current"</c:if> >全部</a>
							<c:forEach items="${listGrade }" var="obj">
								<a href="javascript:void(0)" id="g${obj.id }" onclick="javascript:changeGrade('${obj.id}','${obj.name }')" <c:if test="${bean.grade == obj.name }">class="current"</c:if> >${obj.name }</a>
							</c:forEach>
						</dd>
					</dl>
					<dl class="filter_item border_bottom">
						<dt>科目</dt>
						<dd id="ddsubject">
							<a href="javascript:void(0)" id="s0" onclick="javascript:changeSubject('0','')" <c:if test="${bean.subject == '' || bean.subject == null }">class="current"</c:if>>全部</a>
							<c:forEach items="${listSubject }" var="obj">
								<a href="javascript:void(0)" id="s${obj.id }" onclick="javascript:changeSubject('${obj.id}','${obj.name }')" <c:if test="${bean.subject == obj.name }">class="current"</c:if> >${obj.name }</a>
							</c:forEach>
						</dd>
					</dl>
					<dl class="filter_item border_bottom">
						<dt>类型</dt>
						<dd id="ddbanci">
							<a href="javascript:void(0)" id="bc0" onclick="javascript:changeBanci('0','')" <c:if test="${bean.techang == '' || bean.techang == null }">class="current"</c:if>>全部</a>
							<c:forEach items="${listBanci }" var="obj">
								<a href="javascript:void(0)" id="bc${obj.id }" onclick="javascript:changeBanci('${obj.id}','${obj.name }')" <c:if test="${bean.techang == obj.name }">class="current"</c:if> >${obj.name }</a>
							</c:forEach>
						</dd>
					</dl>
					<dl class="filter_item">
						<dt>教龄</dt>
						<dd id="ddjiaoling">
							<a href="javascript:void(0)" id="bc0" onclick="javascript:changeJiaoling('0','')" <c:if test="${bean.jiaoling == '' || bean.jiaoling == null }">class="current"</c:if>>全部</a>
							<c:forEach items="${listJiaoling }" var="obj">
								<a href="javascript:void(0)" id="jl${obj.id }" onclick="javascript:changeJiaoling('${obj.id}','${obj.name }')" <c:if test="${bean.jiaoling == obj.name }">class="current"</c:if>>${obj.name }年</a>
							</c:forEach>
						</dd>
					</dl>
				</div>
			</div>
			
			<div class="team">
				<div style="margin:20px;"></div>
					<c:forEach items="${pager.pageList}" var="obj" varStatus="status"> 
						<div class="team-member" style="margin-bottom:1em;">
							<a href="<%=pre%>/tea/info<%=suf%>?id=${obj.id}" class="flipLightBox">
								<img src="<%=uploadPath%>/${obj.headpath}"  alt="" />
							</a>
							<h3>${obj.name }</h3>						
							<div class="teainfo">
								<ul>
									<li>教学科目：${obj.subject}</li>
									<li>教龄：${obj.jiaoling }</li>
									<li>教学级别：${obj.tealevel}</li>
								</ul>
							</div>			
							<div class="sub-about-grid-social">
								<ul>
									<li>
										<a href="<%=pre%>/tea/info<%=suf%>?id=${obj.id}" target="_blank">
											<img src="<%=images%>/checkinfo.png" title="查看详情">
										</a>
									</li>
								</ul>
							</div>
						</div>
					</c:forEach>
					<div class="clear"></div>
			</div>
			
			<!-- 分页 -->
				<div id="divFenye"><!-- 分页开始 -->
					<div class="manu">
						<c:if test="${pager.totalPage >0}">
							<!-- prev -->
							<c:choose>
								<c:when test="${pager.curtPage == 1 }">
									<span class="disabled">&lt;上一页</span>
								</c:when>
								<c:otherwise>
									<a href="javascript:void();" onclick="changePage('${pager.curtPage - 1}')">&lt; 上一页 </a>
								</c:otherwise>
							</c:choose>
							<c:if test="${pager.totalPage < 7 }">
								<c:forEach var = "item" begin="1" end = "${pager.totalPage }">
									<c:choose>
										<c:when test="${pager.curtPage == item }">
										  	<span class="current">${item }</span>
										</c:when>
										<c:otherwise>
											<a href="javascript:void();" onclick="changePage('${item}')" >${item }</a>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</c:if>
							<c:if test="${pager.totalPage >= 7 }">
								<c:if test="${pager.curtPage <= 4 }">
									<c:forEach var = "item" begin="1" end = "5">
										<c:choose>
											<c:when test="${pager.curtPage == item }">
											  	<span class="current">${item }</span>
											</c:when>
											<c:otherwise>
												<a href="javascript:void();" onclick="changePage('${item}')" >${item }</a>
											</c:otherwise>
										</c:choose>
									</c:forEach>
									...
									<a href="javascript:void();" onclick="changePage('${pager.totalPage}')" >${pager.totalPage}</a>
								</c:if>
								<c:if test="${pager.curtPage > 4 }">
									<a href="javascript:void();" onclick="changePage('1')" >1</a>
									...
									<c:choose>
											<c:when test="${pager.curtPage < pager.totalPage - 3 }">
												<c:forEach var = "item" begin="${pager.curtPage - 2 }" end = "${pager.curtPage + 2 }">
													<c:choose>
														<c:when test="${pager.curtPage == item }">
														  	<span class="current">${item }</span>
														</c:when>
														<c:otherwise>
															<a href="javascript:void();" onclick="changePage('${item}')" >${item }</a>
														</c:otherwise>
													</c:choose>
												</c:forEach>
												...
												<a href="javascript:void();" onclick="changePage('${pager.totalPage}')" >${pager.totalPage}</a>
											</c:when>
											<c:otherwise>
												<c:forEach var = "item" begin="${pager.totalPage - 4 }" end = "${pager.totalPage }">
													<c:choose>
														<c:when test="${pager.curtPage == item }">
														  	<span class="current">${item }</span>
														</c:when>
														<c:otherwise>
															<a href="javascript:void();" onclick="changePage('${item}')" >${item }</a>
														</c:otherwise>
													</c:choose>
												</c:forEach>
											</c:otherwise>
										</c:choose>
								</c:if>
							</c:if>
							<!--next-->
							<c:choose>
								<c:when test="${pager.curtPage == pager.totalPage }">
									<span class="disabled">下一页 &gt;</span>
								</c:when>
								<c:otherwise>
									<a href="javascript:void();" onclick="changePage('${pager.curtPage + 1}')" >下一页  &gt; </a>
								</c:otherwise>
							</c:choose>	
						</c:if>
					</div>
				</div><!--分页结束-->
			
		</div>
	</div>
	<!--底部-->
	<%@include file="/WEB-INF/jsp/front001/footer.jsp"%>
</body>
</html>