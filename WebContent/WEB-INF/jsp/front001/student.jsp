<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/front001_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="renderer" content="webkit" />
<title>旭日阳光教育-个人档案模块</title>
<style>
.partLeft{width: 100%;float: left;margin-right:21px;margin-top:10px;border: 1px solid #dedede;}
.region{overflow: hidden;zoom:1;background-color: #fff;padding:30px;}
.mod-tabCon{zoom:1;overflow: hidden;}
.mod-tabCon .mhd {height: 47px;line-height: 47px;border-bottom: 1px solid #eeeeee;position: relative;}
.mod-tabCon .mhd span {padding: 0 24px;float: left;height: 46px;line-height: 46px;font-size: 20px;color: #333;border-bottom: 2px solid #22b4b8;}

.bordered {margin:10px 0px;*border-collapse: collapse;border-spacing: 0;width:100%;font-family: 'trebuchet MS', 'Lucida sans', Arial;border: solid #ccc 1px;-moz-border-radius: 6px;-webkit-border-radius: 6px;border-radius: 6px;}
.bordered td a {color:#22B4B8;}
.bordered td a.xq {color:#22B4B8;}
.bordered td a:hover {text-decoration:underline;color:#22B4B8;}
.bordered tr:hover {background:  #ddf2f2;-o-transition: all 0.1s ease-in-out;-webkit-transition: all 0.1s ease-in-out;-moz-transition: all 0.1s ease-in-out;-ms-transition: all 0.1s ease-in-out;transition: all 0.1s ease-in-out;}    
.bordered td, .bordered th {border-left: 1px solid #ccc;border-top: 1px solid #ccc;padding: 10px;text-align: left;}
.bordered th {
    background-color: #f5f5f5;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#f5f5f5), to(#f5f5f5));
    background-image: -webkit-linear-gradient(top, #f5f5f5, #f5f5f5);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #f5f5f5);
    background-image:     -ms-linear-gradient(top, #f5f5f5, #f5f5f5);
    background-image:      -o-linear-gradient(top, #f5f5f5, #f5f5f5);
    background-image:         linear-gradient(top, #f5f5f5, #f5f5f5);
    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset;  
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}
.bordered td:first-child, .bordered th:first-child {border-left: none;}
.bordered th:first-child {-moz-border-radius: 6px 0 0 0;-webkit-border-radius: 6px 0 0 0;border-radius: 6px 0 0 0;}
.bordered th:last-child {-moz-border-radius: 0 6px 0 0;-webkit-border-radius: 0 6px 0 0;border-radius: 0 6px 0 0;}
.bordered th:only-child{-moz-border-radius: 6px 6px 0 0;-webkit-border-radius: 6px 6px 0 0;border-radius: 6px 6px 0 0;}
.bordered tr:last-child td:first-child {-moz-border-radius: 0 0 0 6px;-webkit-border-radius: 0 0 0 6px;border-radius: 0 0 0 6px;}
.bordered tr:last-child td:last-child {-moz-border-radius: 0 0 6px 0;-webkit-border-radius: 0 0 6px 0;border-radius: 0 0 6px 0;}

/****/
.ulfilter li{float:left;}
.ulfilter li a{padding:5px 10px}
.ulfilter li a.current{background: #22B4B8;color: #fff;text-decoration: none;}
.ulfilter li a:hover{text-decoration:underline}
.notnow{display:none}
.now{display:block}
</style>
<script>
function showKcCatalog(id){
	art.dialog.open("<%=pre%>/student/showcatalog<%=suf%>?id=" +id,{
		title:'课程目录',
		height:400,
		width:600
	});
}
$(document).ready(function(){
	$("#top_2").addClass("active");
	$("#ul_pyjy li a").click(function(){
		$("#ul_pyjy li a").removeClass('current');
		$(this).addClass('current');
		var subject = $(this).attr('title');
		$("[id^='div_pyjy_']").removeClass('now');
		$("#div_pyjy_" + subject).addClass('now');
	});
	
	$("#ul_zuoye li a").click(function(){
		$("#ul_zuoye li a").removeClass('current');
		$(this).addClass('current');
		var subject = $(this).attr('title');
		$("[id^='div_zuoye_']").removeClass('now');
		$("#div_zuoye_" + subject).addClass('now');
	});
	
	$("#ul_ceshi li a").click(function(){
		$("#ul_ceshi li a").removeClass('current');
		$(this).addClass('current');
		var subject = $(this).attr('title');
		$("[id^='div_ceshi_']").removeClass('now');
		$("#div_ceshi_" + subject).addClass('now');
	});
});
</script>
</head>
<body>
	<!-- 头部 -->
	<%@include file="/WEB-INF/jsp/front001/top.jsp"%>
	<div class="content">
		<div class="wrap">
			<div class="top-head">
				<ul>
					<li><a href="<%=pre%>/home/index<%=suf%>">首页 / </a></li>
					<li><a href="#"><span>我的档案</span></a></li>
				</ul>
			</div>
			<div class="clear"></div>
			
			<div class="partLeft">
				<div class="region">
					<div class="mod-tabCon">
						<div class="mhd">
							<span>我的课程</span>
						</div>
						<div class="mbd">
							<div class="div_sj">
								<table class="bordered" id="tbl_sj">
									<tr>
										<td style="width:10%">序号</td>
										<td>课程名称</td>
										<td style="width:10%">科目</td>
										<td style="width:15%">时间</td>
										<td style="width:15%">课程目录</td>
									</tr>
									<c:forEach items="${listKc}" var="obj" varStatus="status">
										<tr>
											<td>${status.index + 1 }</td>
											<td>${obj.name}</td>
											<td>${obj.subject}</td>
											<td>${fn:substring(obj.inserttime,0,10) }</td>
											<td><a href="javascript:void();" onclick="javascript:showKcCatalog('${obj.id}')">课程目录</a></td>
										</tr>
									</c:forEach>
								</table>
							</div>
						</div>
					</div>
				</div>
				
				<div class="region">
					<div class="mod-tabCon">
						<div class="mhd">
							<span>我的培优讲义</span>
							<ul class="ulfilter" id="ul_pyjy">
								<c:forEach items="${strListPyjy }" var="obj" varStatus="status">
									<li> <a title="${status.index }" href="javascript:void(0);"<c:if test="${status.index == 0 }">class="current"</c:if>>${obj.strkey}</a></li>
								</c:forEach>
							</ul>
						</div>
						<div class="mbd">
							<c:forEach items="${strListPyjy }" var="obj" varStatus="statusOut">
								<div id="div_pyjy_${statusOut.index }"
									<c:if test="${statusOut.index == 0 }">class="notnow now"</c:if>
									<c:if test="${statusOut.index > 0 }">class="notnow"</c:if> >
									<table class="bordered" id="tbl_sj">
										<tr>
											<td style="width:10%">序号</td>
											<td>讲义名称</td>
											<td style="width:10%">科目</td>
											<td style="width:15%">时间</td>
											<td style="width:15%">讲义详情</td>
										</tr>
										<c:forEach items="${obj.list }" var="obj" varStatus="statusIn">
											<tr>
												<td>${statusIn.index + 1 }</td>
												<td>${obj.name}</td>
												<td>${obj.subject}</td>
												<td>${fn:substring(obj.inserttime,0,10) }</td>
												<td><a href="<%=pre%>/peiyoujy/info<%=suf%>?id=${obj.id}" target="_blank">讲义详情</a></td>
											</tr>
										</c:forEach>
									</table>
								</div>
							</c:forEach>
						</div>
					</div>
				</div>
				
				<div class="region">
					<div class="mod-tabCon">
						<div class="mhd">
							<span>我的培优作业</span>
							<ul class="ulfilter" id="ul_zuoye">
								<c:forEach items="${strListZuoye }" var="obj" varStatus="status">
									<li> <a title="${status.index }" href="javascript:void(0);"<c:if test="${status.index == 0 }">class="current"</c:if>>${obj.strkey}</a></li>
								</c:forEach>
							</ul>
						</div>
						<div class="mbd">
							<c:forEach items="${strListZuoye }" var="obj" varStatus="statusOut">
								<div id="div_zuoye_${statusOut.index }"
									<c:if test="${statusOut.index == 0 }">class="notnow now"</c:if>
									<c:if test="${statusOut.index > 0 }">class="notnow"</c:if> >
									<table class="bordered" id="tbl_sj">
										<tr>
											<td style="width:10%">序号</td>
											<td>作业名称</td>
											<td style="width:10%">科目</td>
											<td style="width:15%">时间</td>
											<td style="width:15%">作业内容</td>
											<td style="width:15%">完成情况</td>
										</tr>
										<c:forEach items="${obj.list}" var="obj" varStatus="statusIn">
											<tr>
												<td>${statusIn.index + 1 }</td>
												<td>${obj.name}</td>
												<td>${obj.subject}</td>
												<td>${fn:substring(obj.inserttime,0,10) }</td>
												<td><a href="<%=pre%>/zuoye/info<%=suf%>?id=${obj.id}&type=1" target="_blank">作业内容</a></td>
												<td><a href="<%=pre%>/zuoye/info<%=suf%>?id=${obj.id}&type=2" target="_blank">完成情况</a></td>
											</tr>
										</c:forEach>
									</table>
								</div>
							</c:forEach>
						</div>
					</div>
				</div>
				
				<div class="region">
					<div class="mod-tabCon">
						<div class="mhd">
							<span>我的培优测试</span>
							<ul class="ulfilter" id="ul_ceshi">
								<c:forEach items="${strListCeshi }" var="obj" varStatus="status">
									<li> <a title="${status.index }" href="javascript:void(0);"<c:if test="${status.index == 0 }">class="current"</c:if>>${obj.strkey}</a></li>
								</c:forEach>
							</ul>
						</div>
						<div class="mbd">
							<c:forEach items="${strListCeshi }" var="obj" varStatus="statusOut">
								<div id="div_ceshi_${statusOut.index }"
									<c:if test="${statusOut.index == 0 }">class="notnow now"</c:if>
									<c:if test="${statusOut.index > 0 }">class="notnow"</c:if> >
									<table class="bordered" id="tbl_sj">
										<tr>
											<td style="width:10%">序号</td>
											<td>测试名称</td>
											<td style="width:10%">科目</td>
											<td style="width:15%">时间</td>
											<td style="width:15%">测试详情</td>
										</tr>
										<c:forEach items="${obj.list}" var="obj" varStatus="statusIn">
											<tr>
												<td>${statusIn.index + 1 }</td>
												<td>${obj.name}</td>
												<td>${obj.subject}</td>
												<td>${fn:substring(obj.inserttime,0,10) }</td>
												<td>
													<a href="<%=pre%>/ceshi/info<%=suf%>?id=${obj.id}" target="_blank">测试详情</a></td>
											</tr>
										</c:forEach>
									</table>
								</div>
							</c:forEach>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
	<!--底部-->
	<%@include file="/WEB-INF/jsp/front001/footer.jsp"%>
</body>
</html>