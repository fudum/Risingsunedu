<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/front001_include.jsp"%>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="renderer" content="webkit" />
	<title>旭日阳光教育-教师信息</title>
	<style>
		.team-member .teainfo ul{
			font-size: 0.875em;
			color: rgba(85, 85, 85, 0.53);
			line-height: 1.5em;
			font-family: Arial, Helvetica, sans-serif;
			padding: 6px 0px 5px 0px;				
		}
		.tea-member .teainfo ul li {
			float:left;
		}
		
		.jyinfo{
			margin:20px;
		}
		.jyinfo .jyname{
			font-size:18px;
			height:27px;
			line-height:27px;
			color:#333;
		}
		.jyinfo .jytag{
			font-size:14px;
			line-height:21px;
			color:#333;
			margin-bottom:10px;
		}
		.jypdf {
			width:800px;
			border:1px solid #f5f5f5;
			display:block;
		}
		
		.downloadbtn{
			color: #FFF;
			padding: 0.8em 2em;
			background: #22B4B8;
			display: inline-block;
			font-size: 0.875em;
			text-transform: uppercase;
			margin-top: 0.5em;
			-webkit-transition: all 0.3s ease-out;
			-moz-transition: all 0.3s ease-out;
			-ms-transition: all 0.3s ease-out;
			-o-transition: all 0.3s ease-out;
			transition: all 0.3s ease-out;
			font-weight:700;
		}
		
		.clear {
			clear:both;
		}
	</style>
	<script>
		$(function () {
		      $("#slider1").responsiveSlides({
		        maxwidth: 1600,
		        speed: 600
		      });
		});
		
		//下载功能
		function toDownload(storeName, realName) {
			document.getElementById('storeName').value = storeName;
			document.getElementById('realName').value = realName;
			document.getElementById('downForm').submit();
		}
	</script>
	   <script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
			});
		});
	</script>
	<script type="text/javascript" src="<%=js%>/jquery.media.js"></script>
	<script type="text/javascript">
		$("document").ready(function(){
			$("#top_5").addClass("active");
			$('a.media').media({width:800, height:600});
		});
	</script>
</head>
<body>
	<!-- 头部 -->
	<%@include file="/WEB-INF/jsp/front001/top.jsp"%>
	<div class="content">
		<div class="wrap">
			<div class="top-head">
				<h3>教师信息</h3>
				<ul>
					<li><a href="index.html">首页 / </a></li>
					<li><a href="#"><span>教师信息</span></a> /</li>
					<li><a href="#"><span>教师详情</span></a> /</li>
					<li><a href="#"><span>讲义详情</span></a></li>
				</ul>
			</div>
			<div class="jyinfo">
				<div class="jyname">${bean.name }</div>
				<div class="jytag">
					教师：${bjBean.tea_name} | 班级：${bean.bj_name }
				</div>
				<div class="jypdf">
					<a class="media" href="<%=uploadPath%>/${bean.jiangyipath}"></a> 
				</div>
				<div class="downpdf">
					<a href="#" class="downloadbtn" onclick="javascript:toDownload('${bean.jiangyipath}','${bean.name}');">下载文档</a>
				</div>			
				<div>
					<form id="downForm" action="<%=pre%>/jy/download<%=suf%>" method="post">
						<input type="hidden" name="storeName" id="storeName" value="" /> 
						<input type="hidden" name="realName" id="realName" value="" />
					</form>
				</div>	
				<div class="clear"></div>
			</div>
		</div>
	</div>
	
	<!--底部-->
	<%@include file="/WEB-INF/jsp/front001/footer.jsp"%>
</body>
</html>