<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/front001_include.jsp"%>
<html>
<head>
<meta name="renderer" content="webkit" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>旭日阳光教育-我要报班</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/kecheng.css">
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css">
<link rel="stylesheet" type="text/css" href="<%=css %>/fenye.css" />
<script src="<%=js%>/kecheng.js"></script>
<script type="text/javascript" src="<%=js%>/kecheng.js"></script>

<script>
$(document).ready(function(){
	$("#top_8").addClass("active");
});

function changeGrade(border,grade){
	$("#o_grade").val(grade);
	$("#o_page").val(1);
	$("#ddgrade a").removeClass("current");
	$("#g"+border).addClass("current");
	myAjax();
}

function changeSubject(border,subject){
	$("#o_subject").val(subject);
	$("#o_page").val(1);
	$("#ddsubject a").removeClass("current");
	$("#s"+border).addClass("current");
	myAjax();
}

function changeBanci(border,banci){
	$("#o_banci").val(banci);
	$("#o_page").val(1);
	$("#ddbanci a").removeClass("current");
	$("#bc" + border).addClass("current");
	myAjax();
}

function changeBjtype(border,bjtype){
	$("#o_bjtype").val(bjtype);
	$("#o_page").val(1);
	$("#ddbjtype a").removeClass("current");
	$("#bt" + border).addClass("current");
	myAjax();
}

function changePage(page){
	$("#o_page").val(page);
	myAjax();
}

function myAjax(){
	$("#form").submit();
}
</script>

</head>
<body>
	<!-- 头部 -->
	<%@include file="/WEB-INF/jsp/front001/top.jsp"%>
	<!-- 内容 -->
	<div class="content">
		<div class="wrap">
			<div class="top-head">
				<ul>
					<li><a href="#">首页 / </a></li>
					<li><a href="#"><span>我要报班</span></a></li>
				</ul>
			</div>
			<div class="clear"></div>
			<div class="container">
				<form id="form" action="<%=pre%>/course/index<%=suf%>" method="get" >
					<input type="hidden" id="o_grade" name="grade" value="${bean.grade }"/>
					<input type="hidden" id="o_subject" name="subject" value="${bean.subject }" />
					<input type="hidden" id="o_banci" name="banci" value="${bean.banci }" />
					<input type="hidden" id="o_bjtype" name="bjtype" value="${bean.bjtype }" />
					<input type="hidden" id="o_page" name="curtPage" value="${pager.curtPage }" />
				</form>
				<div class="ui_filter">
					<dl class="filter_item border_bottom">
						<dt>年级</dt>
						<dd id="ddgrade">
							<a href="javascript:void(0)" id="g0" onclick="javascrpit:changeGrade('0','')" <c:if test="${bean.grade == '' || bean.grade == null }">class="current"</c:if> >全部</a>
							<c:forEach items="${listGrade }" var="obj">
								<a href="javascript:void(0)" id="g${obj.id }" onclick="javascript:changeGrade('${obj.id}','${obj.name }')" <c:if test="${bean.grade == obj.name }">class="current"</c:if>>${obj.name }</a>
							</c:forEach>
						</dd>
					</dl>
					<dl class="filter_item border_bottom">
						<dt>科目</dt>
						<dd id="ddsubject">
							<a href="javascript:void(0)" id="s0" onclick="javascript:changeSubject('0','')" <c:if test="${bean.subject == '' || bean.subject == null}">class="current"</c:if> >全部</a>
							<c:forEach items="${listSubject }" var="obj">
								<a href="javascript:void(0)" id="s${obj.id }" onclick="javascript:changeSubject('${obj.id}','${obj.name }')" <c:if test="${bean.subject == obj.name }">class="current"</c:if> >${obj.name }</a>
							</c:forEach>
						</dd>
					</dl>
					<dl class="filter_item border_bottom">
						<dt>课程类型</dt>
						<dd id="ddbanci">
							<a href="javascript:void(0)" id="bc0" onclick="javascript:changeBanci('0','')" <c:if test="${bean.banci == '' || bean.banci == null }">class="current"</c:if> >全部</a>
							<c:forEach items="${listBanci }" var="obj">
								<a href="javascript:void(0)" id="bc${obj.id }" onclick="javascript:changeBanci('${obj.id}','${obj.name }')" <c:if test="${bean.banci == obj.name }">class="current"</c:if>>${obj.name }</a>
							</c:forEach>
						</dd>
					</dl>
					<dl class="filter_item">
						<dt>上课时间</dt>
						<dd id="ddbjtype">
							<a href="javascript:void(0)" id="bt0" onclick="javascript:changeBjtype('0','')" <c:if test="${bean.bjtype == ''  || bean.bjtype == null }">class="current"</c:if>>全部</a>
							<c:forEach items="${listBjtype }" var="obj">
								<a href="javascript:void(0)" id="bt${obj.id }" onclick="javascript:changeBjtype('${obj.id}','${obj.name }')" <c:if test="${bean.bjtype == obj.name }">class="current"</c:if>>${obj.name }</a>
							</c:forEach>
						</dd>
					</dl>
				</div>
			</div>
			<div class="clear"></div>
			<div class="s-box">
				<section class="s-main-box">
					<c:forEach items="${pager.pageList }" var="obj">
						<div class="s-r-list">
							<div class="s-r-list-photo">
								<a href="<%=pre%>/tea/info<%=suf %>?id=${obj.tea_id}" target="_blank">
									<img src="<%=uploadPath%>/${obj.teacherBean.headpath}" 
									width="72" height="86" style="display: inline;"/>
								</a>
								<p>
									<a href="<%=pre%>/tea/info<%=suf %>?id=${obj.tea_id}" target="_blank">${obj.teacherBean.name }</a>
								</p>
							</div>
							<div class="s-r-list-detail">
								<div class="arw-left"></div>
								<div class="s-r-list-info">
									<div class="price">￥<span>${obj.price }</span></div>
									<h3>
										<a href="<%=pre %>/course/info<%=suf %>?id=${obj.id}" target="_blank" title="${obj.name }">${obj.name }</a>
									</h3>
									<p>
										<span>学科： ${obj.subject }</span><span>年级：${obj.grade }</span>
									</p>
									<p>
										<span>开课日期：${obj.startdate }至${obj.enddate }</span>
										<span>上课时间： ${obj.timeframe }${obj.starttime }-${obj.endtime }</span>
									</p>
									<p>上课地点：${obj.address }</p>
									<div class="ico-list">
										<span class="video">
											<b class="ico-video"></b>
											<a href="<%=pre %>/course/info<%=suf %>?id=${obj.id}" target="_blank">课程详情</a>
										</span>
										<span>
											<b class="ico-comment"></b>
											<a href="<%=pre%>/tea/info<%=suf %>?id=${obj.tea_id}" target="_blank">教师详情</a>
										</span>
										<span class="last">
											<b class="ico-catalog"></b>
											<a href="javascript:void(0);" onclick="javascript:showCatalog('${obj.id}')">课程目录</a>
										</span>
										
									</div>
								</div>
								<div class="sk-bm-box func-srlistbm s-r-list-bm">
									<p class="mtop30">
										<span class="sk-bm-btn sk-bm-btn01">
											<a href="javascript:;" onclick= "javascript:apply()">报名</a>
										</span>
									</p>
								</div>
							</div>
							<div class="clear"></div>
						</div>
					</c:forEach>
					
					<!-- 分页 -->
					<div id="divFenye"><!-- 分页开始 -->
						<div class="manu">
							<c:if test="${pager.totalPage >0}">
								<!-- prev -->
								<c:choose>
									<c:when test="${pager.curtPage == 1 }">
										<span class="disabled">&lt;上一页</span>
									</c:when>
									<c:otherwise>
										<a href="javascript:void();" onclick="changePage('${pager.curtPage - 1}')" >&lt; 上一页 </a>
									</c:otherwise>
								</c:choose>
								<c:if test="${pager.totalPage < 7 }">
									<c:forEach var = "item" begin="1" end = "${pager.totalPage }">
										<c:choose>
											<c:when test="${pager.curtPage == item }">
											  	<span class="current">${item }</span>
											</c:when>
											<c:otherwise>
												<a href="javascript:void();" onclick="changePage('${item}')" >${item }</a>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</c:if>
								<c:if test="${pager.totalPage >= 7 }">
									<c:if test="${pager.curtPage <= 4 }">
										<c:forEach var = "item" begin="1" end = "5">
											<c:choose>
												<c:when test="${pager.curtPage == item }">
												  	<span class="current">${item }</span>
												</c:when>
												<c:otherwise>
													<a href="javascript:void();" onclick="changePage('${item}')">${item }</a>
												</c:otherwise>
											</c:choose>
										</c:forEach>
										...
										<a href="javascript:void();" onclick="changePage('${pager.totalPage}')">${pager.totalPage}</a>
									</c:if>
									<c:if test="${pager.curtPage > 4 }">
										<a href="javascript:void();" onclick="changePage('1')">1</a>
										...
										<c:choose>
												<c:when test="${pager.curtPage < pager.totalPage - 3 }">
													<c:forEach var = "item" begin="${pager.curtPage - 2 }" end = "${pager.curtPage + 2 }">
														<c:choose>
															<c:when test="${pager.curtPage == item }">
															  	<span class="current">${item }</span>
															</c:when>
															<c:otherwise>
																<a href="javascript:void();" onclick="changePage('${item}')">${item }</a>
															</c:otherwise>
														</c:choose>
													</c:forEach>
													...
													<a href="javascript:void();" onclick="changePage('${pager.totalPage}')">${pager.totalPage}</a>
												</c:when>
												<c:otherwise>
													<c:forEach var = "item" begin="${pager.totalPage - 4 }" end = "${pager.totalPage }">
														<c:choose>
															<c:when test="${pager.curtPage == item }">
															  	<span class="current">${item }</span>
															</c:when>
															<c:otherwise>
																<a href="javascript:void();" onclick="changePage('${item}')">${item }</a>
															</c:otherwise>
														</c:choose>
													</c:forEach>
												</c:otherwise>
											</c:choose>
									</c:if>
								</c:if>
								<!--next-->
								<c:choose>
									<c:when test="${pager.curtPage == pager.totalPage }">
										<span class="disabled">下一页 &gt;</span>
									</c:when>
									<c:otherwise>
										<a href="javascript:void();" onclick="changePage('${pager.curtPage + 1}')">下一页  &gt; </a>
									</c:otherwise>
								</c:choose>	
							</c:if>
						</div>
					</div><!--分页结束-->
					
				</section>
				<section class="s-side-right"></section>
			</div>
			<div class="clear"></div>
		</div><!-- wrap -->
	</div><!-- content -->
	<!--底部-->
	<%@include file="/WEB-INF/jsp/front001/footer.jsp"%>
</body>
</html>