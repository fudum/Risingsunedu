<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/front001_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>旭日阳光教育-答疑视频播放</title>
<style>
.mod_player_title {
	clear: both;
	display: block;
	width: auto;
	height: 28px;
	line-height: 28px;
	font-weight: normal;
	font-family: hiragino sans gb,microsoft yahei;
	font-size: 18px;
	color: #2A2A2A;
	overflow: hidden;
	padding-top: 8px;
	white-space: nowrap;
	text-overflow: ellipsis;
}
.region{
	overflow: hidden;
	zoom:1;
	background-color: #fff;
	padding:30px 0px;
}

.mod-tabCon{
	margin-top: -15px;
	zoom:1;
	overflow: hidden;
}
.mod-tabCon .mhd {
	height: 47px;
	line-height: 47px;
	border-bottom: 1px solid #eeeeee;
	position: relative;
}

.mod-tabCon .mhd span {
	padding: 0 24px;
	float: left;
	height: 46px;
	line-height: 46px;
	font-size: 20px;
	color: #333;
	border-bottom: 2px solid #22b4b8;
}
		
</style>
<script type="text/javascript">
	$(document).ready(function(){
		$("#top_7").addClass("active");
	});
</script>
</head>
<body>
	<!-- 头部 -->
	<%@include file="/WEB-INF/jsp/front001/top.jsp"%>
	
	<div class="content">
		<div class="wrap">
			<div class="top-head">
				<ul>
					<li><a href="index.html">首页 / </a></li>
					<li><a href="#"><span>答疑视频</span>/</a></li>
					<li><a href="#"><span>答疑解析详情</span></a></li>
				</ul>
			</div>
			<div class="clear"></div>
			<div class="container">		
				<div class="mod_player_title">${bean.tea_name} ${bean.name}</div>
				<div style="margin:0 auto">
					<embed src="http://static.video.qq.com/TPout.swf?auto=1&vid=${bean.videopath }" quality="high" width="650" height="472" align="middle" allowScriptAccess="sameDomain"
					allowFullscreen="true" type="application/x-shockwave-flash" wmode="opaque"></embed>
				</div>		
				<div class="region">
					
					<div class="mod-tabCon">
						<div class="mhd">
							<span>题目详情</span>
						</div>
						<div class="mbd">
							${bean.info }
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--底部-->
	<%@include file="/WEB-INF/jsp/front001/footer.jsp"%>
</body>
</html>