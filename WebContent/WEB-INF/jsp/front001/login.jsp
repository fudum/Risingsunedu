<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/front001_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="renderer" content="webkit"/>
<title>旭日阳光教育-登入页面</title>
<link rel="stylesheet" type="text/css" href="<%=css %>/login.css" />
</head>
<body>
	<div class="div_login">
		<div class="div_tip">
			<span id="login-form-tip" style="color:red;font-size:12px;" class="login_tip"></span>
		</div>
		<input id="login-form-username" name="stuid" type="text" class="user_id" placeholder="用户ID" />
		<input id="login-form-password" name="password" type="password" class="password" placeholder="密码"/>
	</div>
</body>
</html>