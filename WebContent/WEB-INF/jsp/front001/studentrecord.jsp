<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/front001_include.jsp"%>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="renderer" content="webkit" />
	<title>旭日阳光教育-学生档案</title>
	  <script>
	    	// You can also use "$(window).load(function() {"
		    $(function () {
		      // Slideshow 1
		      $("#slider1").responsiveSlides({
		        maxwidth: 1600,
		        speed: 600
		      });
		});
	  </script>
	   <script type="text/javascript">
		jQuery(document).ready(function($) {
			$("#top_2").addClass("active");
			
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
			});
		});
	</script>
</head>
<body>
	<!-- 头部 -->
	<%@include file="/WEB-INF/jsp/front001/top.jsp"%>
	<div class="content">
		<div class="wrap">
			<div class="top-head">
				<ul>
					<li><a href="index.html">首页 / </a></li>
					<li><a href="#"><span>学生档案</span></a></li>
				</ul>
			</div>
			<div class="clear"></div>
			<div style="margin:20px;color:#666">
				暂未上线,开发中...
			</div>
		</div>
	</div>
	
	<!--底部-->
	<%@include file="/WEB-INF/jsp/front001/footer.jsp"%>
</body>
</html>