<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<%@include file="/WEB-INF/jsp/include/front001_include.jsp"%>
<html>
<head>
<meta name="renderer" content="webkit" />
<title>旭日阳光教育首页</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/index.css" />
<script>
	$(function () {
	    $("#slider1").responsiveSlides({
	      maxwidth: 1600,
	      speed: 600
	    });
	});
</script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
		});
	});
</script>
</head>
<body>
	<!---start-wrap---->
	<!---start-header---->
	<div class="header" id="top">
		<div class="wrap">
			<!---start-logo---->
			<div class="logo">
				<a href="#"><img src="<%=images%>/logo.png" title="logo" /></a>
			</div>
			<!---End-logo---->	
		</div>
	<!--End-header-->
	</div>
	<!---start-content-->
	<div class="content">
		
		<div class="content-top-grids">
			<div class="wrap">
			
				<div class="top-grid">
					<div class="top-grid-head">
						<h3><span>&nbsp;</span>教师信息</h3>
						<img src="<%=images%>/01.jpg" alt="教师信息" />
					</div>
					<div class="top-grid-info">
						<h4>内容介绍</h4>
						<p>DIPISICING ELIT SED DO EIUSMOD PORINCIDIDUNT UT LABORE ET DOLORE MAGNA ALIQUA.</p>
					</div>
					<a href="<%=pre%>/tea/index.html">进入</a>
				</div>
				
				<div class="top-grid">
					<div class="top-grid-head">
						<h3><span>&nbsp;</span>公开课</h3>
						<img src="<%=images%>/01.jpg" alt="" />
					</div>
					<div class="top-grid-info">
						<h4>内容介绍</h4>
						<p>DIPISICING ELIT SED DO EIUSMOD PORINCIDIDUNT UT LABORE ET DOLORE MAGNA ALIQUA.</p>
					</div>
					<a href="<%=pre%>/open/index.html">进入</a>
				</div>
				
				<div class="top-grid">
					<div class="top-grid-head">
						<h3><span>&nbsp;</span>我要报班</h3>
						<img src="<%=images%>/01.jpg" alt="我要报班" />
					</div>
					<div class="top-grid-info">
						<h4>内容介绍</h4>
						<p>DIPISICING ELIT SED DO EIUSMOD PORINCIDIDUNT UT LABORE ET DOLORE MAGNA ALIQUA.</p>
					</div>
					<a href="<%=pre%>/course/index.html">进入</a>
				</div>
				
				<div class="top-grid last-grid">
					<div class="top-grid-head">
						<h3><span>&nbsp;</span>培优课堂同步</h3>
						<img src="<%=images%>/01.jpg" alt="" />
					</div>
					<div class="top-grid-info">
						<h4>内容介绍</h4>
						<p>DIPISICING ELIT SED DO EIUSMOD PORINCIDIDUNT UT LABORE ET DOLORE MAGNA ALIQUA.</p>
					</div>
					<a href="<%=pre%>/peiyou/index.html">进入</a>
				</div>
				<div class="clear"> </div>
			</div>
		</div>
		<!-- 底部 -->
		<%@include file="/WEB-INF/jsp/front001/footer.jsp"%>
	</div>
	<!---End-content----->
</body>
</html>

