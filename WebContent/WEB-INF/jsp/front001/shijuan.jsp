<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/front001_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="renderer" content="webkit" />
<title>旭日阳光教育-试卷共享</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css">
<link rel="stylesheet" type="text/css" href="<%=css%>/kecheng.css">
<link rel="stylesheet" type="text/css" href="<%=css%>/fenye.css" />
<link rel="stylesheet" type="text/css" href="<%=css %>/shijuan.css" />
<script>
$(document).ready(function(){
	$("#top_9").addClass("active");
});
function changeGrade(border,grade){
	$("#o_grade").val(grade);
	$("#o_page").val(1);
	$("#ddgrade a").removeClass("current");
	$("#g"+border).addClass("current");
	myAjax();
}

function changeSubject(border,subject){
	$("#o_subject").val(subject);
	$("#o_page").val(1);
	$("#ddsubject a").removeClass("current");
	$("#s"+border).addClass("current");
	myAjax();
}

function changeStype(border,val){
	$("#o_stype").val(val);
	$("#o_page").val(1);
	$("#ddstype a").removeClass("current");
	$("#t"+border).addClass("current");
	myAjax();
}

function changeXueqi(border,val){
	$("#o_xueqi").val(val);
	$("#o_page").val(1);
	$("#ddxueqi a").removeClass("current");
	$("#x"+border).addClass("current");
	myAjax();
}

function changeYear(border,val){
	$("#o_year").val(val);
	$("#o_page").val(1);
	$("#ddyear a").removeClass("current");
	$("#y"+border).addClass("current");
	myAjax();
}

function changePage(page){
	$("#o_page").val(page);
	myAjax();
}

function myAjax(){
	$("#form").submit();
}
</script>
</head>
<body>
	<!-- 头部 -->
	<%@include file="/WEB-INF/jsp/front001/top.jsp"%>
	
	<!-- 内容 -->
	<div class="content">
		<div class="wrap">
			<div class="top-head">
				<ul>
					<li><a href="#">首页 / </a></li>
					<li><a href="#"><span>试卷</span></a></li>
				</ul>
			</div>
			<div class="clear"></div>
			<div class="container">
				<form id="form" action="<%=pre%>/paper/index<%=suf%>" method="get" >
					<input type="hidden" id="o_grade" name="grade" value="${bean.grade }"/>
					<input type="hidden" id="o_subject" name="subject" value="${bean.subject }" />
					<input type="hidden" id="o_stype" name="stype" value="${bean.stype }" />
					<input type="hidden" id="o_xueqi" name="xueqi" value="${bean.xueqi }" />
					<input type="hidden" id="o_year" name="year" value="${bean.year }" />
					<input type="hidden" id="o_page" name="curtPage" value="${pager.curtPage }" />
				</form>
				<div class="ui_filter">
					<dl class="filter_item border_bottom">
						<dt>年级</dt>
						<dd id="ddgrade">
							<a href="javascript:void(0)" id="g0" onclick="javascrpit:changeGrade('0','')" <c:if test="${bean.grade == '' || bean.grade == null }">class="current"</c:if> >全部</a>
							<c:forEach items="${listGrade }" var="obj">
								<a href="javascript:void(0)" id="g${obj.id }" onclick="javascript:changeGrade('${obj.id}','${obj.name }')" <c:if test="${bean.grade == obj.name }">class="current"</c:if>>${obj.name }</a>
							</c:forEach>
						</dd>
					</dl>
					<dl class="filter_item border_bottom">
						<dt>科目</dt>
						<dd id="ddsubject">
							<a href="javascript:void(0)" id="s0" onclick="javascript:changeSubject('0','')" <c:if test="${bean.subject == '' || bean.subject == null}">class="current"</c:if> >全部</a>
							<c:forEach items="${listSubject }" var="obj">
								<a href="javascript:void(0)" id="s${obj.id }" onclick="javascript:changeSubject('${obj.id}','${obj.name }')" <c:if test="${bean.subject == obj.name }">class="current"</c:if> >${obj.name }</a>
							</c:forEach>
						</dd>
					</dl>
					<dl class="filter_item border_bottom">
						<dt>试卷类型</dt>
						<dd id="ddstype">
							<a href="javascript:void(0)" id="t0" onclick="javascript:changeStype('0','')" <c:if test="${bean.stype == '' || bean.stype == null}">class="current"</c:if> >全部</a>
							<c:forEach items="${liststype }" var="obj">
								<a href="javascript:void(0)" id="t${obj.id }" onclick="javascript:changeStype('${obj.id}','${obj.name }')" <c:if test="${bean.stype == obj.name }">class="current"</c:if> >${obj.name }</a>
							</c:forEach>
						</dd>
					</dl>
					<dl class="filter_item border_bottom">
						<dt>年份</dt>
						<dd id="ddyear">
							<a href="javascript:void(0)" id="y0" onclick="javascript:changeYear('0','')" <c:if test="${bean.year == '' || bean.year == null}">class="current"</c:if> >全部</a>
							<c:forEach items="${listYear }" var="obj">
								<a href="javascript:void(0)" id="y${obj.id }" onclick="javascript:changeYear('${obj.id}','${obj.name }')" <c:if test="${bean.year == obj.name }">class="current"</c:if> >${obj.name }</a>
							</c:forEach>
						</dd>
					</dl>
					<dl class="filter_item">
						<dt>学期</dt>
						<dd id="ddxueqi">
							<a href="javascript:void(0)" id="x0" onclick="javascript:changeXueqi('0','')" <c:if test="${bean.xueqi == '' || bean.xueqi == null}">class="current"</c:if> >全部</a>
							<a href="javascript:void(0)" id="x1" onclick="javascript:changeXueqi('1','上学期')" <c:if test="${bean.xueqi == '上学期' }">class="current"</c:if> >上学期</a>
							<a href="javascript:void(0)" id="x2" onclick="javascript:changeXueqi('2','下学期')" <c:if test="${bean.xueqi == '下学期' }">class="current"</c:if> >下学期</a>
						</dd>
					</dl>
				</div>
			</div>
			<div class="clear"></div>
			<div class="div_sj">
				<table class="bordered" id="tbl_sj">
					<thead>
						<tr>
							<th style="width:10%">序号</th>
							<th>试卷名称</th>
							<th style="width:10%" >上传日期</th>
							<th style="width:10%">详情</th>
						</tr>
					</thead>
					
					<c:forEach items="${pager.pageList }" var="obj" varStatus="status">
						<tr>
							<td>${status.index +1}</td>
							<td>
								<a href="<%=pre %>/paper/info<%=suf%>?id=${obj.id }" target="_blank">${obj.name }</a>
							</td>		
							<td>${fn:substring(obj.inserttime,0,10) }</td>
							<td><a class="xq" href="<%=pre %>/paper/info<%=suf%>?id=${obj.id }" target="_blank">详情</a></td>
						</tr>
					</c:forEach>
				</table>
				
				<!-- 分页 -->
					<div id="divFenye"><!-- 分页开始 -->
						<div class="manu">
							<c:if test="${pager.totalPage >0}">
								<!-- prev -->
								<c:choose>
									<c:when test="${pager.curtPage == 1 }">
										<span class="disabled">&lt;上一页</span>
									</c:when>
									<c:otherwise>
										<a href="javascript:void();" onclick="changePage('${pager.curtPage - 1}')">&lt; 上一页 </a>
									</c:otherwise>
								</c:choose>
								<c:if test="${pager.totalPage < 7 }">
									<c:forEach var = "item" begin="1" end = "${pager.totalPage }">
										<c:choose>
											<c:when test="${pager.curtPage == item }">
											  	<span class="current">${item }</span>
											</c:when>
											<c:otherwise>
												<a href="javascript:void();" onclick="changePage('${item}')">${item }</a>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</c:if>
								<c:if test="${pager.totalPage >= 7 }">
									<c:if test="${pager.curtPage <= 4 }">
										<c:forEach var = "item" begin="1" end = "5">
											<c:choose>
												<c:when test="${pager.curtPage == item }">
												  	<span class="current">${item }</span>
												</c:when>
												<c:otherwise>
													<a href="javascript:void();" onclick="changePage('${item}')">${item }</a>
												</c:otherwise>
											</c:choose>
										</c:forEach>
										...
										<a href="javascript:void();" onclick="changePage('${pager.totalPage}')" >${pager.totalPage}</a>
									</c:if>
									<c:if test="${pager.curtPage > 4 }">
										<a href="javascript:void();" onclick="changePage('1')">1</a>
										...
										<c:choose>
												<c:when test="${pager.curtPage < pager.totalPage - 3 }">
													<c:forEach var = "item" begin="${pager.curtPage - 2 }" end = "${pager.curtPage + 2 }">
														<c:choose>
															<c:when test="${pager.curtPage == item }">
															  	<span class="current">${item }</span>
															</c:when>
															<c:otherwise>
																<a href="javascript:void();" onclick="changePage('${item}')">${item }</a>
															</c:otherwise>
														</c:choose>
													</c:forEach>
													...
													<a href="javascript:void();" onclick="changePage('${pager.totalPage}')">${pager.totalPage}</a>
												</c:when>
												<c:otherwise>
													<c:forEach var = "item" begin="${pager.totalPage - 4 }" end = "${pager.totalPage }">
														<c:choose>
															<c:when test="${pager.curtPage == item }">
															  	<span class="current">${item }</span>
															</c:when>
															<c:otherwise>
																<a href="javascript:void();" onclick="changePage('${item}')" >${item }</a>
															</c:otherwise>
														</c:choose>
													</c:forEach>
												</c:otherwise>
											</c:choose>
									</c:if>
								</c:if>
								<!--next-->
								<c:choose>
									<c:when test="${pager.curtPage == pager.totalPage }">
										<span class="disabled">下一页 &gt;</span>
									</c:when>
									<c:otherwise>
										<a href="javascript:void();" onclick="changePage('${pager.curtPage + 1}')" >下一页  &gt; </a>
									</c:otherwise>
								</c:choose>	
							</c:if>
						</div>
					</div><!--分页结束-->
				
			</div>
			
			
		</div><!-- wrap -->
	</div>
	<!--底部-->
	
	<%@include file="/WEB-INF/jsp/front001/footer.jsp"%>
</body>
</html>