<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/front001_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="renderer" content="webkit" />
<link rel="stylesheet" type="text/css" href="<%=css%>/shijuan.css">
<script type="text/javascript" src="<%=js%>/login.js"></script>
<title>旭日阳光教育-培优讲义详情</title>
<script type="text/javascript" src="<%=js%>/jquery.media.js"></script>
<script>
$(document).ready(function(){
	$("#top_2").addClass("active");
	$('a.media').media({width:800, height:600});
});

$(function () {
      $("#slider1").responsiveSlides({
        maxwidth: 1600,
        speed: 600
      });
});

//下载功能
function toDownload(storeName, realName) {
	realName = realName.replace(/,/g,'');
	realName = realName.replace(/，/g,'');
	realName = realName.replace(/、/g,'');
	$.ajax({
		type:'post',
		url:'<%=pre%>/home/check<%=suf%>',
		dataType:"json",
		success:function(data,status){
			if(data == 0){
				login('<%=pre%>/home/login<%=suf%>','<%=pre%>/home/loginin<%=suf%>');
			}
			else{
				document.getElementById('storeName').value = storeName;
				document.getElementById('realName').value = realName;
				document.getElementById('downForm').submit();
			}
			
		},
		error : function(xhr, textStatus, errorThrown) {
			return false;
		}
	});
}

jQuery(document).ready(function($) {
	$(".scroll").click(function(event){		
		event.preventDefault();
		$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
	});
});

</script>

</head>
<body>
<!-- 头部 -->
	<%@include file="/WEB-INF/jsp/front001/top.jsp"%>
	<!-- 内容 -->
	<div class="content">
		<div class="wrap">
			<div class="top-head">
				<ul>
					<li><a href="#">首页 / </a></li>
					<li><a href="#"><span>我的档案</span>/</a></li>
					<li>
						<a href="#">
							<c:if test="${type == 1 }"><span>培优作业详情</span></c:if>
							<c:if test="${type == 2 }"><span>完成作业详情</span></c:if>
						</a></li>
				</ul>
			</div>
			<div class="clear"></div>
			<div class="sjinfo">
				<div class="namepdf">
					<div class="sjname">${bean.name }</div>
					<div class="t_downpdf">
						<c:if test="${type == 1 }"> <a href="javascript:void();" class="downloadbtn" onclick="javascript:toDownload('${bean.originalpath}','${bean.name}');">下载</a></c:if>
						<c:if test="${type == 2 }"> <a href="javascript:void();" class="downloadbtn" onclick="javascript:toDownload('${bean.finishpath}','${bean.name}');">下载</a></c:if>
					</div>
					<div class="clear"></div>
				</div>
				<div class="sjpdf">
					<c:if test="${type == 1 }"><a class="media"  href="<%=uploadPath%>/${bean.originalpath}"></a> </c:if>
					<c:if test="${type == 2 }"><a class="media"  href="<%=uploadPath%>/${bean.finishpath}"></a> </c:if>
					
				</div>
				<div class="b_downpdf">
					<c:if test="${type == 1 }"> <a href="javascript:void();" class="downloadbtn" onclick="javascript:toDownload('${bean.originalpath}','${bean.name}');">下载</a></c:if>
					<c:if test="${type == 2 }"> <a href="javascript:void();" class="downloadbtn" onclick="javascript:toDownload('${bean.finishpath}','${bean.name}');">下载</a></c:if>
				</div>			
				<div>
					<form id="downForm" action="<%=pre%>/paper/download<%=suf%>" method="post">
						<input type="hidden" name="storeName" id="storeName" value="" /> 
						<input type="hidden" name="realName" id="realName" value="" />
					</form>
				</div>	
				<div class="clear"></div>
			</div>
		</div>
	</div>
	<!--底部-->
	<%@include file="/WEB-INF/jsp/front001/footer.jsp"%>
</body>
</html>