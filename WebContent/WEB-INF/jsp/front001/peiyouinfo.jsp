<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/front001_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="renderer" content="webkit" />
<title>旭日阳光教育-培优课堂同步班级视频播放</title>

<style>
.coures_content {background: #f6f6f6;height: auto;overflow: hidden;padding-bottom: 20px;width:100%;}
.coures_content h2 {
	height: 47px;
	line-height: 47px;
	margin: 0 auto;
	font-size: 16px;
	font-family: "Microsoft YaHei";
	font-weight: normal;
	color: #333333;
	padding-left: 10px;
	overflow: hidden;
}
.coures_content .content_title{
	width: 980px;
	margin: 0 auto;
	
}
.coures_content .content_player {
	width: 980px;
	height: 535px;
	background: #fff;
	margin: 0 auto;
	box-shadow: 3px 3px 5px #dcdcdc, -3px -3px 5px #dcdcdc;
	border-radius: 2px;
}		
.coures_content .content_player .player_list {
	float: right;
	width: 230px;
	height: 465px;
	border-bottom: 1px solid #efefef;
	border-left:1px solid #efefef;
}
.coures_content .content_player .player_list .list_title {
	height: 38px;
	line-height: 38px;
	background-position: 0 -239px;
	background-image: url("<%=images%>/btn.png");
	background-repeat: no-repeat;
}
.scrolls_con{
	width:230px;
	overflow-y:scroll;
	overflow-x:hidden;
	height:425px;
}
.ui_scrolls .scrolls_item .scrolls_con ul .scrolls_title {
	height: 41px;
	line-height:41px;
	overflow: hidden;
	border-bottom: 1px solid #e9e9e9;
	padding-left: 20px;
}
.ui_scrolls .scrolls_item .scrolls_con ul li {
	line-height: 43px;
	padding-left: 42px;
	font-size: 14px;
	color: #333333;
	position: relative;
	overflow: hidden;
}
.ui_scrolls .scrolls_item .scrolls_con ul {
	width: 230px;
}
.ui_scrolls .scrolls_item .scrolls_con .select_list li.current {
	background: url(<%=images%>/scroll_current.png) no-repeat;
}
.ui_scrolls .scrolls_item .scrolls_con .select_list li.current a {
	color:#fff;
}
.ui_scrolls .scrolls_item .scrolls_con .select_list li.current .icon_live_gary {
	background: url(<%=images%>/icon2_04.png) 1px 2px no-repeat;
}
.ui_scrolls .scrolls_item .scrolls_con ul li a {
	color: #666666;
}
.ui_scrolls .scrolls_item .scrolls_con ul li a:hover {
	text-decoration:underline;
}
.ui_scrolls .scrolls_item .scrolls_con .select_list li .icon_live_gary {
	float: left;
	width: 20px;
	height: 20px;
	text-align: center;
	vertical-align: top;
	text-indent: -9999em;
	margin-top: 12px;
	background: url(<%=images%>/icon2_03.png) 1px 2px no-repeat;
}
.coures_content .content_player .player_live {
	float: left;
	width: 745px;
	height: 465px;
}
.coures_content .content_player .player_info .listen_num {
	position: absolute;
	right: 45px;
	top: 0;
	z-index: 1;
	font-size: 18px;
	color: #666666;
	font-family: "Microsoft YaHei";
	padding-right: 20px;
}
.coures_content .content_player .player_info {
	overflow: hidden;
	width: 980px;
	height: 69px;
	line-height: 69px;
	float: left;
	position: relative;
}
.coures_content .content_player .player_info dl {
	float: left;
	display: inline;
	margin-left: 20px;
	font-size: 14px;
	color: #666666;
}
.coures_content .content_player .player_info dl dt {
	float:left;
}
.coures_content .content_player .player_info .listen_num strong {
	float: left;
	padding: 1px 5px 0 0;
	font-size: 28px;
	font-family: "Arial";
	color: #cc0000;
	font-weight: normal;
}
.coures_content .content_player .player_info .listen_num span {
	float: left;
	padding-right: 10px;
}
.coures_content .content_player .player_info dl dd {
	float: left;
	display: inline;
}
.ui_avatar ul.avatar_items li a {
	display: block;
	text-align: center;
	line-height: 180%;
	color: #666666;
}
.ui_avatar ul.avatar_items li img {
	width: 50px;
	height: 50px;
	margin-top:5px;
}
.region{
	overflow: hidden;
	zoom:1;
	background-color: #fff;
	padding:30px 0px;
}
.mod-tabCon{
	margin-top: -15px;
	zoom:1;
	overflow: hidden;
}
.mod-tabCon .mhd {
	height: 47px;
	line-height: 47px;
	border-bottom: 1px solid #eeeeee;
	position: relative;
}
.mod-tabCon .mhd span {
	padding: 0 24px;
	float: left;
	height: 46px;
	line-height: 46px;
	font-size: 20px;
	color: #333;
	border-bottom: 2px solid #22b4b8;
}
.mod-tabCon .mbd{
	padding:30px;
}
.jspContainer {
	overflow: hidden;
	position: relative;
}
.jspPane {
	position: absolute;
}
.jspVerticalBar {
	position: absolute;
	top: 0;
	right: 0;
	width: 16px;
	height: 100%;
	background: none;
}
.jspCap {
	display: none;
}
.jspTrack {
	position: relative;
	background: none;
}
.ui_scrolls {
	width: 230px;
	overflow: hidden;
}
</style>
<script type="text/javascript">
	$(document).ready(function(){
		$("#top_4").addClass("active");
	});
</script>
</head>
<body>
	<!-- 头部 -->
	<%@include file="/WEB-INF/jsp/front001/top.jsp"%>
	<!-- 中部内容 -->
	<div class="content">
		<div class="wrap">
			<div class="coures_content">
				<div class="content_title">
					<h2>${bean.grade }${bean.subject }${bean.banci }${bean.bjtype}(${bean.lessonday }${bean.starttime }-${bean.endtime })</h2>
				</div>
				<div class="content_player">
					<div class="player_list">
						<div class="list_title">
							<span style="padding-left:20px;">培优同步内容</span>
						</div>
						<div class="ui_scrolls scrolls_free">
							<div class="scrolls_item jspScrollable" >
								<div class="jspContainer" style="width: 230px; height: 465px;">
									<div class="jspPane">
										<div class="scrolls_con">
											<ul class="select_list">
												<c:forEach items ="${myList}" var = "objMap" varStatus="statusMap" >
													<li class="scrolls_title">
														<c:set var="str" value="${fn:split(objMap.mykey, ';')}" />
														第${str[0]}讲 ${str[1] }
													</li>
													<c:forEach items="${objMap.mylist}" var="obj" varStatus="status">
														<li id="${obj.id }" <c:if test="${status.index ==0 &&  statusMap.index == 0 }">class="current"</c:if>>
															<p>
																<em class="icon icon_live_gary"></em>
																<c:choose>
																	<c:when test="${statusMap.index < 2 }">
																		<a href="javascript:void(0)" onclick="javascript:changeVideo('${obj.videopath }','${obj.id}')">${obj.jieci_name }</a>
																	</c:when>
																	<c:otherwise>
																		<a href="javascript:void(0)" onclick="javascript:changeVideo('','${obj.id}')">${obj.jieci_name }</a>
																	</c:otherwise>
																</c:choose>
															</p>
														</li>
													</c:forEach>	
												</c:forEach>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="player_live">
						<div id="videoPlay">
							<embed id="embed1" wmode="opaque" src="http://static.video.qq.com/TPout.swf?auto=1&vid=${videoStr }" 
								quality="high" width="745" height="465" align="middle" allowScriptAccess="sameDomain" 
								allowFullscreen="true" type="application/x-shockwave-flash"></embed>
						</div>
					</div>
					<div class="player_info">
						<dl>
							<dt>授课老师：</dt>
							<dd>
								<div class="ui_avatar_con">
									<div class="ui_avatar">
										<ul class="avatar_items">
											<li>
												<a href="<%=pre %>/tea/info.html?id=${teacherBean.id}" class="userpic" target="_blank">
													<img src="<%=uploadPath %>/${teacherBean.headpath}" alt="${teacherBean.name }">
												</a>
											</li>
										</ul>
									</div>
								</div>
							</dd>
						</dl>
						<div class="listen_num">
							<strong>${bean.playnum}</strong>
							<span>播放</span>
						</div>
					</div>
				</div>
			</div>
			<div class="clear"></div>
			<div class="region">
				<div class="mod-tabCon">
					<div class="mhd">
						<span>讲义详情</span>
					</div>
					<div class="mbd">
						<%-- ${bean.info } --%>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--底部-->
	<%@include file="/WEB-INF/jsp/front001/footer.jsp"%>
</body>
<script>
	function changeVideo(videopath,id){
		$(".select_list li").each(function(){
			if($(this).attr("class") == 'current'){
				$(this).attr("class","");
			}
		});
		$("#" + id).attr("class",'current');
		if(videopath == ''){
			$.ajax({
				type:'post',
				url:'<%=pre%>/home/check<%=suf%>',
				dataType:"json",
				success:function(data,status){
					if(data == 0){
						login('<%=pre%>/home/login<%=suf%>','<%=pre%>/home/loginin<%=suf%>');
					}
					else{
						$.ajax({
							type:'post',
							url:'<%=pre%>/peiyou/vid<%=suf%>',
							data:{id:id},
							dataType:"json",
							success:function(data,status){
								if(data[0] == 1){
									videopath = data[1];
									var strHtml = "<embed src='http://static.video.qq.com/TPout.swf?auto=1&vid="+ videopath +"'";
									strHtml += 'quality="high" width="745" height="465" align="middle" allowScriptAccess="sameDomain"'; 
									strHtml += 'allowFullscreen="true" type="application/x-shockwave-flash" wmode="opaque"></embed>';
									$("#videoPlay").html(strHtml);
								}
								else{
									changeVideo(videopath,id);
								}
							},
							error : function(xhr, textStatus, errorThrown) {
								return false;
							}
							
						});
					}
					
				},
				error : function(xhr, textStatus, errorThrown) {
					return false;
				}
			});
		}
		else{
			var strHtml = "<embed src='http://static.video.qq.com/TPout.swf?auto=1&vid="+ videopath +"'";
			strHtml += 'quality="high" width="745" height="465" align="middle" allowScriptAccess="sameDomain"'; 
			strHtml += 'allowFullscreen="true" type="application/x-shockwave-flash" wmode="opaque"></embed>';
			$("#videoPlay").html(strHtml);
		}		
	}
</script>
</html>