<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script type="text/javascript" src="<%=js%>/login.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(".social-weixin a").mouseover(function(){
		$(".qr-code-wrap").css("display","block");
	});
	$(".social-weixin a").mouseleave(function(){
		$(".qr-code-wrap").css("display","none");
	});
	$("#pc-login").click(function(){
		login('<%=pre%>/home/login<%=suf%>','<%=pre%>/home/loginin<%=suf%>');
	});
	$("#dangan").click(function(){
		$.ajax({
			type:'post',
			url:'<%=pre%>/home/check<%=suf%>',
			dataType:"json",
			success:function(data,status){
				if(data == 0){
					loginStu('<%=pre%>/home/login<%=suf%>','<%=pre%>/home/loginin<%=suf%>','<%=pre%>/student/index<%=suf%>');
				}
				else{
					window.location.href='<%=pre%>/student/index<%=suf%>';
				}
			},
			error : function(xhr, textStatus, errorThrown) {
				return false;
			}
		});
	});
	$("#login_out").click(function(){
		var type = 1;
		if($("#top_2").hasClass('active')){
			type = 2;
		}
		loginout('<%=pre%>/home/loginout<%=suf%>',type);
	});
	
});
</script>
<!-- 顶条 -->
<div class="top_top">
   	<div class="top-fix">
  			<c:choose>
			<c:when test="${stu_login_session == null }">
				<ul class="header-logbar">
					<li id="pc-login">登录</li>
					</ul>
			</c:when>
			<c:otherwise>
				<div class="header-welcome">
					<span>用户：</span><a href="<%=pre%>/student/index<%=suf%>">${stu_login_session.name }</a><span>|</span><a href="javascript:void(0);" class="login_out" id="login_out">退出</a>
		  		</div>
  			</c:otherwise>
  			</c:choose>
		<div class="social-contact-wrap">
			<ul class="social-contact">
				<li id="show-QR_code" class="social social-weixin">
           			<a href="#"><span>微信</span></a>
              			<div class="qr-code-wrap" style="display: none;">
						<div class="qr-code">
							<div class="qr-code-title">
								<ul><li class="QR-title-R QR-title-on" style="width:100%;">订阅号</li></ul>
          						</div>
          						<div class="qr-code-img">
       	    					<ul>
				            	    <li class="qr-code-img-on">
				                	    <img src="<%=images%>/Order_QR_code.png" alt="订阅号">
				                    </li>
				                </ul>
                        		</div>
                   		</div>
               		</div>
              		</li>
               	<li class="social social-tel">
               		<a href="javascript:void(0);"><span>电话：027-87301979</span></a>
               	</li>
			</ul>
		</div>
   	</div>
   </div>

<!---start-header---->
<div class="header_f" id="top">
	<div class="wrap">
		<!---start-logo---->
		<div class="logo_f">
			<a href="<%=pre%>/home/index.html"><img src="<%=images%>/logo1.png" title="logo" /></a>
		</div>
		<!---End-logo---->	
		<!---start-top-nav---->
		<div class="top-nav_f">
			<ul>
				<li id="top_0"><a href="<%=pre%>/home/index.html">首页</a></li>
				<li id="top_1"><a href="<%=pre%>/excstu/index.html">优秀学员</a></li>
				<%-- <li id="top_2"><a href="<%=pre_top_top%>/sturecord/index.html">学生档案</a></li>
				<li id="top_3"><a href="<%=pre_top_top%>/exainfo/index.html">考试信息</a></li> --%>
				<li id="top_5"><a href="<%=pre%>/tea/index.html">教师信息</a></li>
				<li id="top_6"><a href="<%=pre%>/open/index.html">公开课</a></li>
				<li id="top_8"><a href="<%=pre%>/course/index.html">我要报班</a></li>
				<li id="top_4"><a href="<%=pre%>/peiyou/index.html">培优课堂同步</a></li>
				<li id="top_7"><a href="<%=pre%>/answer/index.html">答疑视频</a></li>
				<li id="top_9"><a href="<%=pre%>/paper/index.html">试卷</a></li>
				<li id="top_2"><a href="javascript:void(0);" id="dangan">学生档案</a></li>
			</ul>
		</div>
	</div><!---End-top-nav---->
</div><!--End-header-->