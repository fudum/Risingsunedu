<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/front001_include.jsp"%>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="renderer" content="webkit" />
<meta name="Author" content="www.fudum.com">
<meta name="Contact" content="QQ:974822058">
<meta name="description" content="专注于初中高中培训教育机构.">
<title>旭日阳光教育-优秀学员</title>
<link rel="stylesheet" type="text/css" href="<%=css%>/playbox.css" />
<script type="text/javascript" src="<%=js%>/playboxPercent.js"></script>
<style>
.mhd {height: 47px;line-height: 47px;border-bottom: 1px solid #eeeeee;position: relative;}
.mhd span {padding: 0 24px;float: left;height: 46px;line-height: 46px;font-size: 20px;color: #333;border-bottom: 2px solid #22b4b8;}
/**学霸们的成绩单内容列表**/
#gloryWidth {overflow: hidden;}
ul.glory_content {overflow: hidden;float: left;display: inline;width: 315px;margin-right: 17px;margin-left: 10px;padding-right: 10px;}
ul.glory_content li {position: relative;float: left;width: 313px;background: #fff;margin-bottom: 15px;border-radius: 4px 4px 4px 4px;overflow: hidden;box-shadow: 3px 3px 5px #dcdcdc, -3px -3px 5px #dcdcdc;border: 1px solid #ddd;}
.water_pic {width: 295px;margin: 10px auto;}
.water_pic img {display: block;width: 295px;}
ul.glory_content li .water_user {font-size:12px;width:300px;margin: 0px auto 10px;line-height: 150%;color: #666;}
ul.glory_content li .water_option {width: 100%;overflow: hidden;background: #f8f8f8;border-top: 1px solid #eeeeee;}
ul.glory_content li .water_option .userPic {float: left;display: inline;width: 50px;height: 50px;margin: 10px;overflow: hidden;text-align: center;}
ul.glory_content li .water_option .option_con {float: left;display: inline;width: 243px;color: #999;position: relative;overflow: hidden;}
ul.glory_content li .water_option .option_con .users {margin-top: 6px;}
ul.glory_content li .water_option .option_con p {font-size:12px;line-height: 20px;}
ul.glory_content li .water_option .option_con .grades {width: 220px;height: 20px;overflow: hidden;}
ul.glory_content li .water_option .option_con a {color: #333;}
a.show_course {font-size:12px;color: #dd6300;display: block;width: 100%;height: 25px;line-height: 25px;text-align: center;text-decoration: none;background: #fff6de repeat-x bottom;border: 1px solid #f6e4d5;border-bottom: none;}
ul.glory_content li .water_option .option_con .times {font-size:12px;width: 50px;height: 20px;position: absolute;right: 15px;top: 5px;z-index: 10px;text-align: right;}
.stuinfo ul{font-size:0.875em;color: rgba(85, 85, 85, 0.53);line-height: 1.5em;font-family: Arial, Helvetica, sans-serif;padding: 6px 0px 5px 0px;}
.jinrudangan{width:100%;border:1px solid #eee;margin:0 auto;padding:5px 0;text-align:center}
.jinrudangan:hover{background-color:#3E454D}
</style>
<script type="text/javascript">
$(document).ready(function(){
	$("#top_1").addClass("active"); 
	$(".scroll").click(function(event){		
		event.preventDefault();
		$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
	});
	$("#slider1").responsiveSlides({
	   maxwidth: 1600,
	   speed: 600
	});
	$("#filters li span").click(function(){
		$("#filters li span").removeClass("active");
		$(this).addClass("active");
	});
});

function inDangan(id){
	$.ajax({
		type:'post',
		url:'<%=pre%>/home/check<%=suf%>',
		dataType:"json",
		success:function(data,status){
			if(data == 0){
				loginStu('<%=pre%>/home/login<%=suf%>','<%=pre%>/home/loginin<%=suf%>','<%=pre%>/excstu/xuebadangan<%=suf%>?id=' +id);
			}
			else{
				window.location.href='<%=pre%>/excstu/xuebadangan<%=suf%>?id=' + id;
			}
		},
		error : function(xhr, textStatus, errorThrown) {
			return false;
		}
	});
}

function zishiying(){
	var divWidth = $('#playBoxPercent').width();
    $('.oUlplay img').width(divWidth);
    var divHeight = $(".oUlplay img").height();
    if(divHeight == 0){
    	divHeight = divWidth * 400 / 1600;
    }
    $('#playBoxPercent').height(divHeight);
}

function showCourse(stu_id){
	art.dialog.open("<%=pre%>/excstu/showCourse<%=suf%>?stu_id=" +stu_id,{
		title:'在学课程',
		height:'auto'
	});
}
</script>
</head>
<body>
<!-- 头部 -->
<%@include file="/WEB-INF/jsp/front001/top.jsp"%>
<div class="content">
	<div class="services">
		<div class="wrap">
			<div class="top-head">
				<ul>
					<li><a href="<%=pre%>/index.html">首页 / </a></li>
					<li><a href="#"><span>优秀学员</span></a></li>
				</ul>
			</div>
			<div class="clear"></div>
			<div class="mhd">
				<span>今日学霸</span>
			</div>
			<!-- 图片滑动 -->
		  	<div id="playBoxPercent">
		  		<div class="pre"></div>
		  		<div class="next"></div>
		  		<div class="smalltitle">
					<ul>
						<c:forEach items="${listXueba }" var="obj" varStatus = "status">
							<li <c:if test="${status.index == 0}"> class="thistitle"</c:if>></li>
						</c:forEach>
					</ul>
				</div>
				<ul class="oUlplay">
					<c:forEach items="${listXueba}" var="obj" varStatus = "status">
						<li><img src="<%=uploadPath%>/${obj.picpath}"></li>
					</c:forEach>
				</ul> 
			</div>
			<div class="clear"></div>
			
			<!-- 学霸们的成绩单 -->
			<div class="mhd" style="margin-bottom:20px;">
				<span>学霸们的成绩单</span>
			</div>
			
			<!-- 内容列表 -->
			<div id="gloryWidth">
				<ul class="glory_content">
					<c:forEach items="${pager.pageList}" var="obj" varStatus="status">
						<c:if test="${(status.index mod 3) == 0}">
							<li>
								<div class="water_pic">
									<a target="_blank" href="<%=uploadPath%>/${obj.picpath}">
										<img src="<%=uploadPath%>/${obj.picpath}">
									</a>
								</div>
								<div class="water_user">${obj.motto }</div>
								<div class="water_option">
									<span data-params="userId=693192" class="userPic ui_userinfo">
										<img height="50" weith="50" src="<%=uploadPath %>/${obj.studentBean.picture}" alt="">
									</span>
									<div class="option_con">
										<p class="users">${obj.name} 发表</p>
										<p class="grades">${obj.grade } ${obj.leixing }</p>
										<p class="teachers">感谢老师：<a href="<%=pre %>/tea/info<%=suf %>?id=${obj.teaid}" target="_blank">${obj.teaname }</a></p>
										<span class="times"></span>
									</div>
								</div>
								<a href="javascript:void(0);" class="show_course" onclick="showCourse('${obj.stuid}')">查看他在学的课程</a>
							</li>
						</c:if>
					</c:forEach>
				</ul>
				
				<ul class="glory_content">
					<c:forEach items="${pager.pageList}" var="obj" varStatus="status">
						<c:if test="${(status.index mod 3) == 1}">
							<li>
								<div class="water_pic">
									<a target="_blank" href="<%=uploadPath%>/${obj.picpath}">
										<img src="<%=uploadPath%>/${obj.picpath}">
									</a>
								</div>
								<div class="water_user">${obj.motto }</div>
								<div class="water_option">
									<span data-params="userId=693192" class="userPic ui_userinfo">
										<img height="50" weith="50" src="<%=uploadPath %>/${obj.studentBean.picture}" alt="">
									</span>
									<div class="option_con">
										<p class="users">${obj.name} 发表</p>
										<p class="grades">${obj.grade } ${obj.leixing }</p>
										<p class="teachers">感谢老师：<a href="<%=pre %>/tea/info<%=suf %>?id=${obj.teaid}" target="_blank">${obj.teaname }</a></p>
										<span class="times"></span>
									</div>
								</div>
								<a href="javascript:void(0);" class="show_course" onclick="showCourse('${obj.stuid}')">查看他在学的课程</a>
							</li>
						</c:if>
					</c:forEach>
				</ul>
				
				<ul class="glory_content">
					<c:forEach items="${pager.pageList}" var="obj" varStatus="status">
						<c:if test="${(status.index mod 3) == 2}">
							<li>
								<div class="water_pic">
									<a target="_blank" href="<%=uploadPath%>/${obj.picpath}">
										<img src="<%=uploadPath%>/${obj.picpath}">
									</a>
								</div>
								<div class="water_user">${obj.motto }</div>
								<div class="water_option">
									<span data-params="userId=693192" class="userPic ui_userinfo">
										<img height="50" weith="50" src="<%=uploadPath %>/${obj.studentBean.picture}" alt="">
									</span>
									<div class="option_con">
										<p class="users">${obj.name} 发表</p>
										<p class="grades">${obj.grade } ${obj.leixing }</p>
										<p class="teachers">感谢老师：<a href="<%=pre %>/tea/info<%=suf %>?id=${obj.teaid}" target="_blank">${obj.teaname }</a></p>
										<span class="times"></span>
									</div>
								</div>
								<a href="javascript:void(0);" class="show_course" onclick="showCourse('${obj.stuid}')">查看他在学的课程</a>
							</li>
						</c:if>
					</c:forEach>
				</ul>
			</div>
			<!-- 学霸档案 -->
			<div class="mhd">
				<span>学霸档案</span>
			</div>
			<div class="team">
				<div style="margin:20px;"></div>
					<c:forEach items="${pagerStudent.pageList}" var="obj" varStatus="status"> 
						<div class="team-member" style="margin-bottom:1em;">
							<a href="<%=pre%>/excstu/xuebadangan<%=suf%>?id=${obj.id}" class="flipLightBox">
								<img src="<%=uploadPath%>/${obj.picture}"  alt="" />
							</a>
							<h3 style="padding:0px">${obj.name }</h3>						
							<div class="stuinfo">
								<ul>
									<li>学校：${obj.school }</li>
								</ul>
							</div>			
							<div class="jinrudangan" >
								<a href="javascript:void(0);" onClick="javascript:inDangan('${obj.id}')" target="_blank">
									<img src="<%=images%>/jinrudangan.png" title="进入档案">
								</a>
							</div>
						</div>
					</c:forEach>
					<div class="clear"></div>
			</div>
		</div><!--wrap -->
		<div class="clear"> </div>
	</div>
</div>	
<%@include file="/WEB-INF/jsp/front001/footer.jsp"%>
</body>
</html>