<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/front001_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>在学课程</title>
<style>
*{margin:0;padding:0;}
.window_detail{margin-bottom:5px;font-size:13px;border-bottom:1px dashed #666}
.window_detail .learning {color: #666;}
.window_detail .teacher_name {width: 150px;}
a:hover{text-decoration:underline;color:#}
a.learning_course_name {height:16px;margin:5px 0;color: black;overflow: hidden;}
.one{height:16px; line-height:16px;margin-bottom:5px;}
.two{padding-left:20px;height:16px; line-height:16px;margin-bottom:5px;}
</style>
</head>
<body>
<div>
<c:forEach items="${list}" var="obj" varStatus="status">
	<div class="window_detail" <c:if test="${status.last }">style="border-bottom:none"</c:if>>
		<div class="one">
			<span class="learning">${status.index + 1}、正在学习：</span>
			<span class="teacher_name">	
				<a href="<%=pre %>/tea/info<%=suf%>?id=${obj.teaid}" target="_blank">${obj.teaname }</a> 
			</span>
			<span class="learning">老师的</span>
		</div>
		<div class="two">
			<a class="learning_course_name" href="<%=pre %>/peiyou/info<%=suf%>?id=${obj.banji_id}" target="_blank">${obj.banjiname }</a>
		</div>
	</div>
</c:forEach>
</div>
</body>
</html>