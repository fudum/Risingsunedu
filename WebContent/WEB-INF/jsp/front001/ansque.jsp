<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/front001_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>旭日阳光教育-答疑视频解析</title>
<style>
.ui_filter{
	margin:10px 0 20px;
	border-top: 1px solid #eeebe9;
	border-bottom:1px solid #eeebe9;
}
.filter_item {
	overflow: hidden;
	border-left: 1px solid #eeebe9;
	border-right: 1px solid #eeebe9;
	margin: 0;
	
}
.border_bottom {
	border-bottom: 1px dashed #e6e6e6;
}
.ui_filter .filter_item dt {
	float: left;
	display: inline;
	line-height:2em;
	padding: 5px 0;
	background:#f7f6f5;
	width: 77px;
	text-align: center;
	font-weight: bold;
	color: #333333;
	font-size:0.875em;
}

.ui_filter .filter_item dd {
	float: left;
	display: inline;
	line-height: 1.5em;
	padding: 5px 0;
	background: #ffffff;
}

.ui_filter .filter_item dd a {
	display: inline-block;
	margin: 0 0 0 5px;
	padding: 2px 6px;
	color: #666666;
	word-spacing: normal;
	font-size:0.875em;
}

.ui_filter .filter_item dd a:hover {
	text-decoration:underline;
}
.ui_filter .filter_item dd a.current {
	background:#22B4B8;
	color:#fff;
	text-decoration:none;
}

/******视频列表*******/
.mod_figure_list li{
	float:left;
	font-size:14px;
	margin-right:20px;
	font-family: tahoma,microsoft yahei;
	position:relative;
	width:175px;
	height:200px;
}

.mod_figure_list li a.a_pic:hover .icon_play{
	visibility:visible;
	top:32px;
}

.mod_figure_list li p {
	color:#787878;
	line-height:18px;
}

.mod_figure_list li h6 {
	color:#787878;
	line-height:20px;
}

.mod_figure_list li h6 a{
	color:#111;
}

.mod_figure_list li h6 a:hover {
	text-decoration:underline;
	color:#22B4B8;
}

.mod_sign .mod_sign{
	position: absolute;
	bottom: 0;
	left: 0;
	z-index: 10;
	width: 100%;
}

.mod_sign .icon_play {
	height: 36px;
	width: 36px;
	line-height: 0;
	background: url(<%=images%>/icon_play_1.png);
	position: absolute;
	left: 70px;
	top: 0px;
	visibility: hidden;
	_background: none;
}


/*****************************/
/*CSS manu style pagination*/

.manu {
	PADDING-RIGHT: 3px; PADDING-LEFT: 3px; PADDING-BOTTOM: 3px; MARGIN: 3px; PADDING-TOP: 3px; TEXT-ALIGN: center
}
.manu A {
	BORDER-RIGHT: #eee 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: #eee 1px solid; PADDING-LEFT: 5px; PADDING-BOTTOM: 2px; MARGIN: 2px; BORDER-LEFT: #eee 1px solid; COLOR: #22B4B8; PADDING-TOP: 2px; BORDER-BOTTOM: #eee 1px solid; TEXT-DECORATION: none
}
.manu A:hover {
	BORDER-RIGHT: #999 1px solid; BORDER-TOP: #999 1px solid; BORDER-LEFT: #999 1px solid; COLOR: #666; BORDER-BOTTOM: #999 1px solid
}
.manu A:active {
	BORDER-RIGHT: #999 1px solid; BORDER-TOP: #999 1px solid; BORDER-LEFT: #999 1px solid; COLOR: #666; BORDER-BOTTOM: #999 1px solid
}
.manu .current {
	BORDER-RIGHT: #22B4B8 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: #22B4B8 1px solid; PADDING-LEFT: 5px; FONT-WEIGHT: bold; PADDING-BOTTOM: 2px; MARGIN: 2px; BORDER-LEFT: #22B4B8 1px solid; COLOR: #fff; PADDING-TOP: 2px; BORDER-BOTTOM: #22B4B8 1px solid; BACKGROUND-COLOR: #22B4B8
}
.manu .disabled {
	BORDER-RIGHT: #eee 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: #eee 1px solid; PADDING-LEFT: 5px; PADDING-BOTTOM: 2px; MARGIN: 2px; BORDER-LEFT: #eee 1px solid; COLOR: #ddd; PADDING-TOP: 2px; BORDER-BOTTOM: #eee 1px solid
}

</style>
<script>
$(document).ready(function(){
	$("#top_7").addClass("active");
});

function changeGrade(border,grade){
	$("#o_grade").val(grade);
	$("#ddgrade a").removeClass("current");
	$("#g"+border).addClass("current");
	
	myChapter();
	myAjax();
}

function changeSubject(border,subject){
	$("#o_subject").val(subject);
	$("#ddsubject a").removeClass("current");
	$("#s"+border).addClass("current");
	$("#dl_nanyidu").addClass("border_bottom");
	myChapter();
	myAjax();
}

function changeNanyidu(border,nanyidu){
	$("#o_nanyidu").val(nanyidu);
	$("#ddnanyidu a").removeClass("current");
	$("#n" +border).addClass("current");
	myAjax();
}

function changeChapter(border,chapter){
	$("#o_chapter").val(chapter);
	$("#ddchapter a").removeClass("current");
	$("#c" +border).addClass("current");
	myAjax();
}

function changePage(page){
	$("#o_page").val(page);
	myAjax();
}

function myChapter(){
	var grade = $("#o_grade").val();
	var subject = $("#o_subject").val();
	if(grade == '' && subject == ''){
		var dl_chapter = $("#dl_chapter");
		$("#dl_nanyidu").removeClass("border_bottom");
		dl_chapter.html("");
	}
	else{
		$.ajax({
			type:"post",
			url:"<%=pre%>/answer/checkzhangjie<%=suf%>",
			data:{grade:grade,subject:subject},
			dataType:"json",
			success:function(data,status){
				if(data.ret == '1'){
					$("#dl_nanyidu").addClass("border_bottom");
					var dl_chapter = $("#dl_chapter");
					dl_chapter.text("");
					var strHtml ='<dt>章节</dt><dd id="ddchapter">';
					strHtml +='<a href="javascript:void(0)" id="c0" onclick="javascript:changeChapter(\'0\',\'\')" class="current">全部</a>';
					
					for(var i = 0; i < data.jos.length; i++){
						strHtml +='<a href="javascript:void(0)" id="c'+ data.jos[i].id+'" onclick="javascript:changeChapter(\''+data.jos[i].id+'\',\''+data.jos[i].name+'\')">';
						strHtml +=data.jos[i].name;
						if(grade == ''){
							strHtml +='('+data.jos[i].grade+')</a>';
						}
						else if(subject == ''){
							strHtml +='('+data.jos[i].subject+')</a>';
						}
						else{
							strHtml +='</a>';
						}
					}
					strHtml +='</dd>';
					
					dl_chapter.html(strHtml);
				}
			},
			error : function(xhr, textStatus, errorThrown) {
				art.dialog.alert('<span style="color:#f00">查找章节失败！</span>', 1);
				return false;
			}
		});
	}	
}
	
function myAjax(){
	var grade = $("#o_grade").val();
	var subject = $("#o_subject").val();
	var nanyidu = $("#o_nanyidu").val();
	var chapter = $("#o_chapter").val();
	var curtPage = $("#o_page").val();
	if(curtPage == ''){
		curtPage = 1;		
	}
	$.ajax({
		type:"post",
		url:"<%=pre%>/answer/search<%=suf%>",
		data:{grade:grade,subject:subject,curtPage:curtPage,nanyidu:nanyidu,chapter:chapter},
		dataType:"json",
		success:function(data,status){
			var ui_courses = $("#mod_figure_list");
			ui_courses.text("");
			var strHtml = "";
			for(var i = 0; i < data.jos.length; i++){
				strHtml += "<li>";
				strHtml +="<a href='<%=pre%>/answer/videoplay<%=suf%>?id="+ data.jos[i].id + "' class='a_pic' target='_blank'>";
				strHtml +="<img src='<%=uploadPath%>/" + data.jos[i].picpath + "'>";
				strHtml +="<div class='mod_sign'><span class='mod_info'><span class='icon_play'></span></span></div>";
				strHtml +="</a><h6><a href='<%=pre%>/answer/videoplay<%=suf%>?id=" + data.jos[i].id + "' target='_blank'>"+ data.jos[i].name + "</a></h6>";
				strHtml +="<p>教师："+ data.jos[i].tea_name+"</p><p>发布：" + data.jos[i].inserttime.substring(0,10) +"</p><p>播放："+data.jos[i].playnum+"</p>";
			}
			ui_courses.html(strHtml);
			
			//分页
			var strFenye = "";
			if(data.totalPage > 0) {
				strFenye += '<div class="manu">';
				
				if(data.curtPage  == 1) {
					strFenye +='<span class="disabled">&lt;上一页</span>';
				}
				else{
					var s = data.curtPage - 1;
					strFenye +='<a href="javascript:void(0);" onclick="javascrpit:changePage('+ s +')">&lt; 上一页 </a>';
				}
				
				if(data.totalPage < 7){
					for(var i = 1; i <= data.totalPage; i++ ){
						if(data.curtPage == i){
							strFenye +='<span class="current">'+ i +'</span>';
						}
						else{
							strFenye +='<a href="javascript:void(0);" onclick="javascript:changePage('+ i +')">'+i+'</a>';
						}
					}
				}
				if(data.totalPage >= 7){
					if(data.curtPage <= 4){
						for(var i = 1; i <= 5; i ++){
							if(data.curtPage == i){
								strFenye +='<span class="current">'+i+'</span>';
							}
							else{
								strFenye +='<a href="javascript:void(0);" onclick="javascript:changePage('+ i +')">'+i+'</a>';
							}
						}
						strFenye +='...';
						strFenye +='<a href="javascript:void(0);" onclick="javascript:changePage('+ data.curtPage +')">'+data.curtPage+'</a>';
					}
					if(data.curtPage > 4){
						strFenye +='<a href="javascript:void(0);" onclick="javascript:changePage(1)">1</a>';
						strFenye +='...';
						if(data.curtPage < data.totalPage-3){
							for(var i = data.curtPage - 2; i <= data.curtPage + 2; i++){
								if(data.curtPage  == i ){
									strFenye += '<span class="current">'+i+'</span>';
								}
								else{
									strFenye +='javascript:void(0);" onclick="javascript:changePage('+i+')">'+ i +'</a>';
								}
							}
							strFenye +='...';
							strFenye +='<a href="javascript:void(0);" onclick="javascript:changePage('+data.totalPage+')">'+data.totalPage+'</a>'
						}
						else{
							for(var i = data.totalPage - 4; i <= data.totalPage; i++){
								if(data.curtPage == i ){
									strFenye +='<span class="current">'+i+'</span>';
								}
								else{
									strFenye +='<a href="javascript:void(0);" onclick="javascript:changePage('+i+')">'+i+'</a>';
								}
							}
						}
					}
				}
				
				if(data.curtPage == data.totalPage){
					strFenye +='<span class="disabled">下一页 &gt;</span>';
				}
				else{
					var s = data.curtPage + 1;
					strFenye +='<a href="javascript:void(0);" onclick="javascript:changePage('+ s +')">下一页  &gt; </a>';
				}
				strFenye +='</div>';
			}
			var divFenye = $('#divFenye');
			divFenye.text = "";
			divFenye.html(strFenye);
			
		},
		error : function(xhr, textStatus, errorThrown) {
		}
	});
}
</script>
</head>
<body>
	<!-- 头部 -->
	<%@include file="/WEB-INF/jsp/front001/top.jsp"%>
	
	<div class="content">
		<div class="wrap">
			<div class="top-head">
				<ul>
					<li><a href="index.html">首页 / </a></li>
					<li><a href="#"><span>答疑视频</span></a></li>
				</ul>
			</div>
			<div class="clear"></div>
			<div class="container">
				<div>
					<input type="hidden" id = "o_grade"/>
					<input type="hidden" id="o_subject" />
					<input type="hidden" id="o_nanyidu" />
					<input type="hidden" id="o_chapter" />
					<input type="hidden" id="o_page" />
				</div>
				<div class="ui_filter">
					<dl class="filter_item border_bottom">
						<dt>年级</dt>
						<dd id="ddgrade">
							<a href="javascript:void(0)" id="g0" onclick="javascrpit:changeGrade('0','')" class="current">全部</a>
							<c:forEach items="${listbt }" var="obj">
								<c:if test="${obj.botype == '年级' }">
									<a href="javascript:void(0)" id="g${obj.border }" onclick="javascript:changeGrade('${obj.border}','${obj.name }')">${obj.name }</a>
								</c:if>
							</c:forEach>
						</dd>
					</dl>
					<dl class="filter_item border_bottom">
						<dt>学科</dt>
						<dd id="ddsubject">
							<a href="javascript:void(0)" id="s0" onclick="javascript:changeSubject('0','')" class="current">全部</a>
							<c:forEach items="${listbt }" var="obj">
								<c:if test="${obj.botype == '科目' }">
									<a href="javascript:void(0)" id="s${obj.border }" onclick="javascript:changeSubject('${obj.border}','${obj.name }')">${obj.name }</a>
								</c:if>
							</c:forEach>
						</dd>
					</dl>
					<dl class="filter_item" id="dl_nanyidu">
						<dt>难易度</dt>
						<dd id="ddnanyidu">
							<a href="javascript:void(0)" id="n0" onclick="javascript:changeNanyidu('0','')" class="current">全部</a>
							<c:forEach items="${listbt }" var="obj">
								<c:if test="${obj.botype == '难易度' }">
									<a href="javascript:void(0)" id="n${obj.border }" onclick="javascript:changeNanyidu('${obj.border}','${obj.name }')">${obj.name }</a>
								</c:if>
							</c:forEach>
						</dd>
					</dl>
					<dl class="filter_item" id="dl_chapter">
						
					</dl>
				</div><!-- ui_filter -->
				
				<div class="mod_cont">
					<ul class="mod_figure_list" id="mod_figure_list">
						<c:forEach items="${pager.pageList}" var="obj">
							<li>
								<a href="<%=pre%>/answer/videoplay<%=suf%>?id=${obj.id}" class="a_pic" target="_blank">
									
									<img src="<%=uploadPath%>/${obj.picpath}">
									<div class="mod_sign">
										<span class="mod_info">
											<span class="icon_play"></span>
										</span>
									</div>
								</a>
								<h6>
									<a href="<%=pre%>/answer/videoplay<%=suf%>?id=${obj.id}" target="_blank">${obj.name}</a>
								</h6>
								<p>教师：${obj.tea_name }</p>
								<p>发布：${fn:substring(obj.inserttime,0,10)}</p>
								<p>播放：${obj.playnum }</p>
							</li>
						</c:forEach>
					</ul>
				</div>			
				<div class="clear"></div>
				
				<div id="divFenye"><!-- 分页开始 -->
					<div class="manu">
						<c:if test="${pager.totalPage >0}">
							<!-- prev -->
							<c:choose>
								<c:when test="${pager.curtPage == 1 }">
									<span class="disabled">&lt;上一页</span>
								</c:when>
								<c:otherwise>
									<a href="<%=pre %>/answer/index<%=suf%>?curtPage=${pager.curtPage - 1}">&lt; 上一页 </a>
								</c:otherwise>
							</c:choose>
							<c:if test="${pager.totalPage < 7 }">
								<c:forEach var = "item" begin="1" end = "${pager.totalPage }">
									<c:choose>
										<c:when test="${pager.curtPage == item }">
										  	<span class="current">${item }</span>
										</c:when>
										<c:otherwise>
											<a href="<%=pre %>/answer/index<%=suf%>?curtPage=${item}">${item }</a>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</c:if>
							<c:if test="${pager.totalPage >= 7 }">
								<c:if test="${pager.curtPage <= 4 }">
									<c:forEach var = "item" begin="1" end = "5">
										<c:choose>
											<c:when test="${pager.curtPage == item }">
											  	<span class="current">${item }</span>
											</c:when>
											<c:otherwise>
												<a href="<%=pre %>/answer/index<%=suf%>?curtPage=${item}">${item }</a>
											</c:otherwise>
										</c:choose>
									</c:forEach>
									...
									<a href="<%=pre %>/answer/index<%=suf%>?curtPage=${pager.totalPage}">${pager.totalPage}</a>
								</c:if>
								<c:if test="${pager.curtPage > 4 }">
									<a href="<%=pre %>/answer/index<%=suf%>?curtPage=1">1</a>
									...
									<c:choose>
											<c:when test="${pager.curtPage < pager.totalPage - 3 }">
												<c:forEach var = "item" begin="${pager.curtPage - 2 }" end = "${pager.curtPage + 2 }">
													<c:choose>
														<c:when test="${pager.curtPage == item }">
														  	<span class="current">${item }</span>
														</c:when>
														<c:otherwise>
															<a href="<%=pre %>/answer/index<%=suf%>?curtPage=${item}">${item }</a>
														</c:otherwise>
													</c:choose>
												</c:forEach>
												...
												<a href="<%=pre %>/answer/index<%=suf%>?curtPage=${pager.totalPage}">${pager.totalPage}</a>
											</c:when>
											<c:otherwise>
												<c:forEach var = "item" begin="${pager.totalPage - 4 }" end = "${pager.totalPage }">
													<c:choose>
														<c:when test="${pager.curtPage == item }">
														  	<span class="current">${item }</span>
														</c:when>
														<c:otherwise>
															<a href="<%=pre %>/answer/index<%=suf%>?curtPage=${item}">${item }</a>
														</c:otherwise>
													</c:choose>
												</c:forEach>
											</c:otherwise>
										</c:choose>
								</c:if>
							</c:if>
							<!--next-->
							<c:choose>
								<c:when test="${pager.curtPage == pager.totalPage }">
									<span class="disabled">下一页 &gt;</span>
								</c:when>
								<c:otherwise>
									<a href="?curtPage=${pager.curtPage + 1}">下一页  &gt; </a>
								</c:otherwise>
							</c:choose>	
						</c:if>
					</div>
				</div><!--分页结束-->
			</div><!--container-->
		</div>
	</div>
	<!--底部-->
	<%@include file="/WEB-INF/jsp/front001/footer.jsp"%>
</body>
</html>