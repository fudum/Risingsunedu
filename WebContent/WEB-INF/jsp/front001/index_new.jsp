<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/front001_include_index.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="renderer" content="webkit" />
<meta name="Author" content="www.fudum.com">
<meta name="Contact" content="QQ:974822058">
<meta name="description" content="专注于初中高中培训教育机构.">
<title>旭日阳光教育-初高中数学、英语、物理、化学培训机构</title>
<link rel="stylesheet" type="text/css" href="<%=css %>/index_new.css" />
<link rel="stylesheet" type="text/css" href="<%=css %>/playbox.css" />
<script type="text/javascript" src="<%=js%>/login.js"></script>
<script type="text/javascript" src="<%=js%>/index_new.js"></script>

<style>
.div_adv{
	font-size:28px;
	text-align:center;
	font-family:"Microsoft Yahei","Hiragino Sans GB", Arial,Lucida,Verdana,SimSun,Helvetica,sans-serif;
	color:#22B4B8;
}
</style>
<script>
$(document).ready(function(){
	$(".social-weixin a").mouseover(function(){
		$(".qr-code-wrap").css("display","block");
	});
	$(".social-weixin a").mouseleave(function(){
		$(".qr-code-wrap").css("display","none");
	});
	$("#pc-login").click(function(){
		login('<%=pre%>/home/login<%=suf%>','<%=pre%>/home/loginin<%=suf%>');
	});
	$("#login_out").click(function(){
		loginout('<%=pre%>/home/loginout<%=suf%>',1);
	});
	
	$("#dangan").click(function(){
		$.ajax({
			type:'post',
			url:'<%=pre%>/home/check<%=suf%>',
			dataType:"json",
			success:function(data,status){
				if(data == 0){
					loginStu('<%=pre%>/home/login<%=suf%>','<%=pre%>/home/loginin<%=suf%>','<%=pre%>/student/index<%=suf%>');
				}
				else{
					window.location.href='<%=pre%>/student/index<%=suf%>';
				}
			},
			error : function(xhr, textStatus, errorThrown) {
				return false;
			}
		});
	});
	
});
</script>

</head>
<body>
  	<div class="header" role="heading">
  		<!-- 顶条 -->
  		<div class="top-fix-wrap">
        	<div class="top-fix">
       			<c:choose>
   					<c:when test="${stu_login_session == null }">
	   					<ul class="header-logbar">
	   						<li id="pc-login">登录</li>
	   						</ul>
	   					</c:when>
				  	<c:otherwise>
				  		<div class="header-welcome">
				  			<span>用户：</span><a href="<%=pre %>/student/index<%=suf%>">${stu_login_session.name }</a><span>|</span><a href="javascript:void();" class="login_out" id="login_out">退出</a>
				  		</div>
				  	</c:otherwise>
       			</c:choose>
                
				<div class="social-contact-wrap">
					<ul class="social-contact">
						<li id="show-QR_code" class="social social-weixin">
	               			<a href="#"><span>微信</span></a>
	                  		<div class="qr-code-wrap" style="display: none;">
								<div class="qr-code">
		  							<div class="qr-code-title">
		      							<ul><li class="QR-title-R QR-title-on" style="width:100%;">订阅号</li></ul>
		                         	</div>
		                         	<div class="qr-code-img">
			                     	    <ul>
			                         	    <li class="qr-code-img-on">
			                             	    <img src="<%=images %>/Order_QR_code.png" alt="订阅号">
			                                 </li>
			                             </ul>
		                           </div>
	                       		</div>
	                   		</div>
	                  	</li>
	                  	<li class="social social-tel">
	                  		<a href="javascript:void(0)"><span>电话：027-87301979</span></a>
	                  	</li>
					</ul>
				</div>
        	</div>
        </div>
        <!-- 导航 -->
        <div class="nav_wrap">
        	<nav class="nav">
                <!-- logo -->
                <a class="logo" href="#"><img src="<%=images %>/logo_new.png" width="100%" alt="jzs366 Logo"></a>
                <!-- logo end -->
            	<ul class="nav-list">
            		<li><a class="nav-item" href="<%=pre %>/excstu/index<%=suf%>">优秀学员</a></li>
                	<li><a class="nav-item" href="<%=pre %>/tea/index<%=suf%>">教师信息</a></li>
                    <li><a class="nav-item" href="<%=pre %>/open/index<%=suf%>">公开课<span></span></a></li>
                    <li><a class="nav-item" href="<%=pre %>/course/index<%=suf%>">我要报班</a></li>
                    <li><a class="nav-item" href="<%=pre %>/peiyou/index<%=suf%>">培优课堂同步<span></span></a></li>
                    <li><a class="nav-item" href="<%=pre %>/answer/index<%=suf%>">答疑视频</a></li>
                    <li><a class="nav-item" href="<%=pre %>/paper/index<%=suf%>">试卷</a></li>
                    <li><a class="nav-item" href="javascript:void(0);" id="dangan">学生档案</a></li>
                </ul>
                <span id="nav-show-all"><img src="" alt=""></span>
            </nav>
        </div>
  	</div>
  	
  	<!-- 图片滑动 -->
  	<div id="playBox">
  		<div class="pre"></div>
  		<div class="next"></div>
  		<div class="smalltitle">
			<ul>
				<c:forEach items="${list }" var="obj" varStatus = "status">
					<li <c:if test="${status.index == 0}"> class="thistitle"</c:if>></li>
				</c:forEach>
			</ul>
		</div>
		<ul class="oUlplay">
			<c:forEach items="${list }" var="obj" varStatus = "status">
				<li><a href="#" target="_blank"><img src="<%=uploadPath%>/${obj.picpath}"></a></li>
			</c:forEach>
		</ul> 
	</div>
	<div class="div_adv" >
		找老师、选课程，就上www.jzs366.com
	</div>
	<div class="clear"></div>
	<!-- footer -->
	<div class="footer" style="font-size:12px;">
		<div class="wrapper w1000 clearfix">
			<div class="footer-left" style=" text-align:center; margin:0px auto; margin-top:10px;">
				<a href="#" title="关于我们" style="margin-right:5px;" rel="external nofollow">关于我们</a> |
				<a href="#" title="意见反馈" style="margin-right:5px;" rel="external nofollow">意见反馈</a>
			</div>
			<div class="footer-right" style="margin:0px auto; padding:3px 0px 15px 0px; text-align:center;font-family:Arial" >
				Copyright © 2015 <a href="http://jzs366.com"> 旭日阳光教育</a> 鲁ICP备14013167号-2 
			</div>
		</div>
	</div>
</body>
</html>