<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/front001_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="renderer" content="webkit" />
<link rel="stylesheet" type="text/css" href="<%=css%>/shijuan.css">
<script type="text/javascript" src="<%=js%>/login.js"></script>
<title>旭日阳光教育-试卷详情</title>
<script>
$(document).ready(function(){
	$("#top_9").addClass("active");
});

$(function () {
      $("#slider1").responsiveSlides({
        maxwidth: 1600,
        speed: 600
      });
});

//下载功能
function toDownload(storeName, realName) {
	$.ajax({
		type:'post',
		url:'<%=pre%>/home/check<%=suf%>',
		dataType:"json",
		success:function(data,status){
			if(data == 0){
				login('<%=pre%>/home/login<%=suf%>','<%=pre%>/home/loginin<%=suf%>');
			}
			else{
				document.getElementById('storeName').value = storeName;
				document.getElementById('realName').value = realName;
				document.getElementById('downForm').submit();
			}
			
		},
		error : function(xhr, textStatus, errorThrown) {
			return false;
		}
	});
}
</script>
<script type="text/javascript">
jQuery(document).ready(function($) {
	$(".scroll").click(function(event){		
		event.preventDefault();
		$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
	});
});
</script>
<script type="text/javascript" src="<%=js%>/jquery.media.js"></script>
<script type="text/javascript">
$("document").ready(function(){
	$("#top_5").addClass("active");
	$('a.media').media({width:800, height:600});
});
</script>
</head>
<body>
	<!-- 头部 -->
	<%@include file="/WEB-INF/jsp/front001/top.jsp"%>
	<!-- 内容 -->
	<div class="content">
		<div class="wrap">
			<div class="top-head">
				<ul>
					<li><a href="#">首页 / </a></li>
					<li><a href="#"><span>试卷</span>/</a></li>
					<li><a href="#"><span>试卷详情</span></a></li>
				</ul>
			</div>
			<div class="clear"></div>
			<div class="sjinfo">
				<div class="namepdf">
					<div class="sjname">${bean.name }</div>
					<div class="t_downpdf">
						<c:choose>
							<c:when test="${bean.wordpath == '' }">
								<a href="javascript:void();" class="downloadbtn" onclick="javascript:toDownload('${bean.sjpath}','${bean.name}');">下载试卷</a>
							</c:when>
							<c:otherwise>
								<a href="javascript:void();" class="downloadbtn" onclick="javascript:toDownload('${bean.wordpath}','${bean.name}');">下载试卷</a>
    						</c:otherwise> 
						</c:choose>
					</div>
					<div class="clear"></div>
				</div>
				<div class="sjpdf">
					<a class="media" href="<%=uploadPath%>/${bean.sjpath}"></a> 
				</div>
				<div class="b_downpdf">
				
					<c:choose>
						<c:when test="${bean.wordpath == '' }">
							<a href="javascript:void();" class="downloadbtn" onclick="javascript:toDownload('${bean.sjpath}','${bean.name}');">下载试卷</a>
						</c:when>
						<c:otherwise>
							<a href="javascript:void();" class="downloadbtn" onclick="javascript:toDownload('${bean.wordpath}','${bean.name}');">下载试卷</a>
   						</c:otherwise> 
					</c:choose>
				</div>			
				<div>
					<form id="downForm" action="<%=pre%>/paper/download<%=suf%>" method="post">
						<input type="hidden" name="storeName" id="storeName" value="" /> 
						<input type="hidden" name="realName" id="realName" value="" />
					</form>
				</div>	
				<div class="clear"></div>
			</div>
		</div>
	</div>
	<!--底部-->
	<%@include file="/WEB-INF/jsp/front001/footer.jsp"%>
</body>
</html>