<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/front001_include.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="renderer" content="webkit" />
<title>旭日阳光教育-培优课堂同步</title>
<link rel="stylesheet" type="text/css" href="<%=css %>/fenye.css" />
<link rel="stylesheet" type="text/css" href="<%=css%>/common.css">
<style>

/***课程详情***/
.course_item_repeat{
display:block;

}
.course_item{
	float: left;
	display: inline;
	width: 370px;
	height: 100px;
	margin-right: 10px;
	margin-top: 5px;
	overflow: hidden;
	border: 1px solid #ededed;
	background: #fcfcfc;
	margin-bottom: 10px;
	padding-bottom: 10px;
}
.ui_avatar{
	float: left;
	display: inline;
	margin: 10px 10px 0 10px;
	width: 50px;
	position: relative;
	overflow: hidden;
}
.course_detail{
	float: left;
	display: inline;
	width: 290px;
	padding-right:10px;
}
.course_title{
	overflow: hidden;
	height: 46px;
	font-size: 16px;
	font-family: "Microsoft YaHei";
	color: #333333;
	font-weight: normal;
	padding: 7px 0;
}
.course_title em {
	float: left;
	display: inline;
	margin-top: 4px;
	width: 35px;
	font-family: "SimSun";
	line-height: 18px;
	background-image: url(<%=images%>/icon_subject.png);
	background-repeat: no-repeat;
	color: #ffffff;
	font-size: 12px;
	padding-left: 3px;
}
.course_title a {
	color: #333333;
}
.course_title a:hover,.course_info a:hover {
	text-decoration:underline;
	color:#21A9AD;
}
.course_info{
	line-height: 16px;
	color: #666666;
	font-size: 14px;
	padding-bottom:5px;
	font-family: "Microsoft YaHei";
}
.course_info a{
	margin: 0;
	color: #444;
}

</style>
<script type="text/javascript">
	$(document).ready(function(){
		$("#top_4").addClass("active");
	});
	
	function changeGrade(border,name){
		$("#o_grade").val(name);
		$("#o_page").val(1);
		$("#ddgrade a").removeClass("current");
		$("#g"+border).addClass("current");
		myAjax();
	}
	
	function changeSubject(border,name){
		$("#o_subject").val(name);
		$("#o_page").val(1);
		$("#ddsubject a").removeClass("current");
		$("#s"+border).addClass("current");
		myAjax();
	}
	
	function changeBanci(border,name){
		$("#o_banci").val(name);
		$("#o_page").val(1);
		$("#ddbanci a").removeClass("current");
		$("#bc"+border).addClass("current");
		myAjax();
	}
	
	function changeBjtype(border,name){
		$("#o_bjtype").val(name);
		$("#o_page").val(1);
		$("#ddbjtype a").removeClass("current");
		$("#bt"+border).addClass("current");
		myAjax();
	}
	
	function changeTimeframe(border,name){
		$("#o_timeframe").val(name);
		$("#o_page").val(1);
		$("#ddtimeframe a").removeClass("current");
		$("#timeframe"+ border).addClass("current");
		myAjax();
	}
	
	function changePage(page){
		$("#o_page").val(page);
		myAjax();
	}
	
	function myAjax(){
		$("#form").submit();
	}
</script>
</head>
<body>
	<!-- 头部 -->
	<%@include file="/WEB-INF/jsp/front001/top.jsp"%>
	<!-- 中部内容 -->
	<div class="content">
		<div class="wrap">
			<div class="top-head">
				<ul>
					<li><a href="index.html">首页 / </a></li>
					<li><a href="#"><span>培优课堂同步</span></a></li>
				</ul>
			</div>
			<div class="clear"></div>
			<div class="container">
				<form id="form" action="<%=pre%>/peiyou/index<%=suf%>" method="get" >
					<input type="hidden" id = "o_grade" name="grade" value="${bean.grade }"/>
					<input type="hidden" id="o_subject" name="subject" value="${bean.subject }" />
					<input type="hidden" id="o_banci" name="banci" value="${bean.banci }" />
					<input type="hidden" id="o_bjtype" name="bjtype" value="${bean.bjtype }" />
					<input type="hidden" id="o_timeframe" name="timeframe" value="${bean.timeframe }" />
					<input type="hidden" id="o_page" name="curtPage"/>
				</form>
				<div class="ui_filter">
					<dl class="filter_item border_bottom">
						<dt>年级</dt>
						<dd id="ddgrade">
							<a href="javascript:void(0)" id="g0" onclick="javascrpit:changeGrade('0','')" <c:if test="${bean.grade == '' || bean.grade == null }">class="current"</c:if>>全部</a>
							<c:forEach items="${listGrade }" var="obj">
								<a href="javascript:void(0)" id="g${obj.id }" onclick="javascript:changeGrade('${obj.id}','${obj.name }')" <c:if test="${bean.grade == obj.name }">class="current"</c:if> >${obj.name }</a>
							</c:forEach>
						</dd>
					</dl>
					<dl class="filter_item border_bottom">
						<dt>科目</dt>
						<dd id="ddsubject">
							<a href="javascript:void(0)" id="s0" onclick="javascript:changeSubject('0','')" <c:if test="${bean.subject == '' || bean.subject == null}">class="current"</c:if>>全部</a>
							<c:forEach items="${listSubject }" var="obj">
								<a href="javascript:void(0)" id="s${obj.id }" onclick="javascript:changeSubject('${obj.id}','${obj.name }')" <c:if test="${bean.subject == obj.name }">class="current"</c:if>>${obj.name }</a>
							</c:forEach>
						</dd>
					</dl>
					<dl class="filter_item border_bottom">
						<dt>课程类型</dt>
						<dd id="ddbanci">
							<a href="javascript:void(0)" id="bc0" onclick="javascript:changeBanci('0','')"  <c:if test="${bean.banci == '' || bean.banci == null}">class="current"</c:if>>全部</a>
							<c:forEach items="${listBanci }" var="obj">
								<a href="javascript:void(0)" id="bc${obj.id }" onclick="javascript:changeBanci('${obj.id}','${obj.name }')" <c:if test="${bean.banci == obj.name }">class="current"</c:if>>${obj.name }</a>
							</c:forEach>
						</dd>
					</dl>
					<dl class="filter_item border_bottom">
						<dt>上课时间</dt>
						<dd id="ddbjtype">
							<a href="javascript:void(0)" id="bt0" onclick="javascript:changeBjtype('0','')" <c:if test="${bean.bjtype == '' || bean.bjtype == null}">class="current"</c:if>>全部</a>
							<c:forEach items="${listBjtype }" var="obj">
								<a href="javascript:void(0)" id="bt${obj.id }" onclick="javascript:changeBjtype('${obj.id}','${obj.name }')" <c:if test="${bean.bjtype == obj.name }">class="current"</c:if>>${obj.name }</a>
							</c:forEach>
						</dd>
					</dl>
					<dl class="filter_item">
						<dt>上课时段</dt>
						<dd id="ddtimeframe">
							<a href="javascript:void(0)" id="timeframe0" onclick="javascript:changeTimeframe('0','')" <c:if test="${bean.timeframe == '' || bean.bjtype == null}">class="current"</c:if>>全部</a>
							<a href="javascript:void(0)" id="timeframe1" onclick="javascript:changeTimeframe('1','上午')" <c:if test="${bean.timeframe == '上午' }">class="current"</c:if>>上午</a>
							<a href="javascript:void(0)" id="timeframe2" onclick="javascript:changeTimeframe('2','下午')" <c:if test="${bean.timeframe == '下午' }">class="current"</c:if>>下午</a>
							<a href="javascript:void(0)" id="timeframe3" onclick="javascript:changeTimeframe('3','晚上')" <c:if test="${bean.timeframe == '晚上' }">class="current"</c:if>>晚上</a>
						</dd>
					</dl>
				</div>
			</div>
			<div class="clear"></div>
			<div class="course_item_repeat" id="course_item_repeat">
				<c:forEach items="${pager.pageList }" var = "obj">
					<div class="course_item">
						<div class="ui_avatar">
							<ul class="avatar_items">
								<li>
									<a href="<%=pre%>/tea/info<%=suf %>?id=${obj.tea_id}">
										<img src="<%=uploadPath %>/${obj.teacherBean.headpath}">
									</a>
								</li>
							</ul>
						</div>
						<div class="course_detail">
							<p class="course_title">
								<em>${obj.subject }</em>
								<a href="<%=pre%>/peiyou/info<%=suf%>?id=${obj.id}" target="_blank">${obj.year}年${obj.grade }${obj.subject }${obj.banci }${obj.bjtype}</a>
							</p>
							<p class="course_info">
								<span>授课老师：
									<a href="<%=pre%>/tea/info<%=suf %>?id=${obj.tea_id}" target="_blank">${obj.teacherBean.name}</a>
								</span>
								<span>(${obj.starttime }-${obj.endtime })</span>
							</p>
						</div>
					</div>
				</c:forEach>
			</div>
			<div class="clear"></div>
			
			<!-- 分页 -->
			<div id="divFenye"><!-- 分页开始 -->
				<div class="manu">
					<c:if test="${pager.totalPage >0}">
						<!-- prev -->
						<c:choose>
							<c:when test="${pager.curtPage == 1 }">
								<span class="disabled">&lt;上一页</span>
							</c:when>
							<c:otherwise>
								<a href="javascript:void();" onclick="changePage('${pager.curtPage - 1}')">&lt; 上一页 </a>
							</c:otherwise>
						</c:choose>
						<c:if test="${pager.totalPage < 7 }">
							<c:forEach var = "item" begin="1" end = "${pager.totalPage }">
								<c:choose>
									<c:when test="${pager.curtPage == item }">
									  	<span class="current">${item }</span>
									</c:when>
									<c:otherwise>
										<a href="javascript:void();" onclick="changePage('${item}')" >${item }</a>
									</c:otherwise>
								</c:choose>
							</c:forEach>
						</c:if>
						<c:if test="${pager.totalPage >= 7 }">
							<c:if test="${pager.curtPage <= 4 }">
								<c:forEach var = "item" begin="1" end = "5">
									<c:choose>
										<c:when test="${pager.curtPage == item }">
										  	<span class="current">${item }</span>
										</c:when>
										<c:otherwise>
											<a href="javascript:void();" onclick="changePage('${item}')" >${item }</a>
										</c:otherwise>
									</c:choose>
								</c:forEach>
								...
								<a href="javascript:void();" onclick="changePage('${pager.totalPage}')" >${pager.totalPage}</a>
							</c:if>
							<c:if test="${pager.curtPage > 4 }">
								<a href="javascript:void();" onclick="changePage('1')" >1</a>
								...
								<c:choose>
										<c:when test="${pager.curtPage < pager.totalPage - 3 }">
											<c:forEach var = "item" begin="${pager.curtPage - 2 }" end = "${pager.curtPage + 2 }">
												<c:choose>
													<c:when test="${pager.curtPage == item }">
													  	<span class="current">${item }</span>
													</c:when>
													<c:otherwise>
														<a href="javascript:void();" onclick="changePage('${item}')" >${item }</a>
													</c:otherwise>
												</c:choose>
											</c:forEach>
											...
											<a href="javascript:void();" onclick="changePage('${pager.totalPage}')" >${pager.totalPage}</a>
										</c:when>
										<c:otherwise>
											<c:forEach var = "item" begin="${pager.totalPage - 4 }" end = "${pager.totalPage }">
												<c:choose>
													<c:when test="${pager.curtPage == item }">
													  	<span class="current">${item }</span>
													</c:when>
													<c:otherwise>
														<a href="javascript:void();" onclick="changePage('${item}')" >${item }</a>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</c:otherwise>
									</c:choose>
							</c:if>
						</c:if>
						<!--next-->
						<c:choose>
							<c:when test="${pager.curtPage == pager.totalPage }">
								<span class="disabled">下一页 &gt;</span>
							</c:when>
							<c:otherwise>
								<a href="javascript:void();" onclick="changePage('${pager.curtPage + 1}')" >下一页  &gt; </a>
							</c:otherwise>
						</c:choose>	
					</c:if>
				</div>
			</div><!--分页结束-->
			
		</div>
	</div>
	<div class="clear"></div>
	<!--底部-->
	<%@include file="/WEB-INF/jsp/front001/footer.jsp"%>
</body>
</html>