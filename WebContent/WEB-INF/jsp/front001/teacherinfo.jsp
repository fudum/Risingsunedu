<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/jsp/include/front001_include.jsp"%>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="renderer" content="webkit"/>
<title>旭日阳光教育-教师信息</title>
<style>
.team-member .teainfo ul{font-size: 0.875em;color: rgba(85, 85, 85, 0.53);line-height: 1.5em;font-family: Arial, Helvetica, sans-serif;padding: 6px 0px 5px 0px;}
.tea-member .teainfo ul li {float:left;}
.pbd{width: 100%;margin: 0 auto;overflow: hidden;zoom:1;}
.mod-showDetail{overflow: hidden;zoom:1;margin-bottom:21px;}
.bigPic {width: 100%;height: 300px;}
img {border:none;vertical-align: middle;}
@media only screen and (min-width:1024px) {
	.picCon{width: 37%;height: 300px;padding:3px;border:1px solid #dddddd;float: left;margin-right: 28px;position: relative;}
	.teaContent{float: left;width: 18%;}
	.videoCon{width: 37%;height: 300px;padding:3px;border:1px solid #dddddd;float: left;margin-right: 28px;position: relative;}
}
@media only screen and (max-width: 1024px) and (min-width: 768px) {
	.picCon{width: 60%;height: 300px;padding:3px;border:1px solid #dddddd;float: left;margin-right: 28px;position: relative;}
	.teaContent{float: left;width: 30%;}
	.videoCon{margin-top:10px;width: 100%;height: 300px;padding:3px;border:1px solid #dddddd;float: left;margin-right: 28px;position: relative;}
}
@media only screen and (max-width: 768px) and (min-width: 640px) {
	.picCon{width: 100%;height: 300px;padding:3px;border:1px solid #dddddd;float: left;margin-right: 28px;position: relative;}
	.teaContent{float: left;width: 100%;}
	.videoCon{margin-top:10px;width: 100%;height:300px;padding:3px;border:1px solid #dddddd;float: left;margin-right: 28px;position: relative;}
}
.mod-showDetail .teaContent h2{font-size: 28px;color: #333;font-weight: normal;height: 45px;line-height: 45px;overflow: hidden;word-wrap:normal;white-space: nowrap;}
.posCon{color: #666;font-size: 15px;line-height:26px;height: 26px;overflow: hidden;}
.btnCon{text-align: center;color: #fff;background-color: #188eee;height: 49px;width: 203px;float: left;margin-right: 14px;font-size: 20px;}
.applyBtn{text-align: center;color: #fff;background-color: #188eee;height: 49px;line-height: 49px;width: 203px;float: left;margin-right: 14px;font-size: 20px;text-decoration: none;}
.partLeft{width: 100%;float: left;margin-right: 21px;border: 1px solid #dedede;}
.region{overflow: hidden;zoom:1;background-color: #fff;padding:30px;}
.mod-tabCon{margin-top: -15px;zoom:1;overflow: hidden;}
.mod-tabCon .mhd {height: 47px;line-height: 47px;border-bottom: 1px solid #eeeeee;position: relative;}
.mod-tabCon .mhd span {padding: 0 24px;float: left;height: 46px;line-height: 46px;font-size: 20px;color: #333;border-bottom: 2px solid #22b4b8;}
.teavideo{float:left;width:400px;height:300px;border:1px solid #red;background-color:#red;}

/***用户评价列表***/
.customerList_item{margin-top:20px;}
.customerPhoto{float:left;width:140px;margin:0 20px;height:140px;}
.customerDesWrap{margin-right:20px;}
.customerDesWrap .cdw_name{color:#333;font-size:18px;line-height: 1.5;}
.customerDesWrap .cdw_des {color:#333;font-size:14px;line-height:1.5;margin:10px 0px;}
.fenjie {margin:20px 20px 0;}
.fenjie hr{height:1px;border:none;border-top:1px dashed #22b4b8;}

/**讲义样式**/
.jytbl{border-collapse: collapse;margin:20px 20px;width:96%;}
.jytbl tr td {border:1px solid #22b4b8;text-align:center;color:#333;font-size:14px;font-family:"Microsoft YaHei";height:24px;}
.user_pj_a{display:inline-block;position:relative;width:140px;height:140px;}
.user_pj_span{position: absolute;width: 52px;height:52px;right:10px;bottom:10px;z-index: 20;background: url(<%=images%>/film_play_btn.png) no-repeat center center;}
#tbl_sj tr td{text-align:center;}
/******/
.div_sj{margin:10px 0px;}
#tbl_sj {*border-collapse: collapse;border-spacing: 0;width:100%;font-family: 'trebuchet MS', 'Lucida sans', Arial;}
.bordered {border: solid #ccc 1px;-moz-border-radius: 6px;-webkit-border-radius: 6px;border-radius: 6px;}
.bordered td a {color:#22B4B8;}
.bordered td a.xq {color:#22B4B8;}
.bordered td a:hover {text-decoration:underline;color:#22B4B8;}
.bordered tr:hover {background:  #ddf2f2;-o-transition: all 0.1s ease-in-out;-webkit-transition: all 0.1s ease-in-out;-moz-transition: all 0.1s ease-in-out;-ms-transition: all 0.1s ease-in-out;transition: all 0.1s ease-in-out;}    
.bordered td, .bordered th {border-left: 1px solid #ccc;border-top: 1px solid #ccc;padding: 10px;text-align: left;}
.bordered th {
    background-color: #f5f5f5;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#f5f5f5), to(#f5f5f5));
    background-image: -webkit-linear-gradient(top, #f5f5f5, #f5f5f5);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #f5f5f5);
    background-image:     -ms-linear-gradient(top, #f5f5f5, #f5f5f5);
    background-image:      -o-linear-gradient(top, #f5f5f5, #f5f5f5);
    background-image:         linear-gradient(top, #f5f5f5, #f5f5f5);
    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset;  
    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}
.bordered td:first-child, .bordered th:first-child {border-left: none;}
.bordered th:first-child {-moz-border-radius: 6px 0 0 0;-webkit-border-radius: 6px 0 0 0;border-radius: 6px 0 0 0;}
.bordered th:last-child {-moz-border-radius: 0 6px 0 0;-webkit-border-radius: 0 6px 0 0;border-radius: 0 6px 0 0;}
.bordered th:only-child{-moz-border-radius: 6px 6px 0 0;-webkit-border-radius: 6px 6px 0 0;border-radius: 6px 6px 0 0;}
.bordered tr:last-child td:first-child {-moz-border-radius: 0 0 0 6px;-webkit-border-radius: 0 0 0 6px;border-radius: 0 0 0 6px;}
.bordered tr:last-child td:last-child {-moz-border-radius: 0 0 6px 0;-webkit-border-radius: 0 0 6px 0;border-radius: 0 0 6px 0;}
</style>

<script>
   $(function () {
     $("#slider1").responsiveSlides({
       maxwidth: 1600,
       speed: 600
     });
});
</script>
<script type="text/javascript">
jQuery(document).ready(function($) {
	$("#top_5").addClass("active");
	$(".scroll").click(function(event){		
		event.preventDefault();
		$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
	});
});

function playVideo(videoPath){
	art.dialog.open("<%=pre%>/tea/vedioPreview<%=suf%>?vp=" + videoPath,{
		width:640,
	    height:360,
		title:"学生评价"
	});
}

function showCatalog(id){
	art.dialog.open("<%=pre%>/open/showcatalog<%=suf%>?id=" +id,{
		title:'目录详情',
		width:840,
		height:430
	})
}

function showKcCatalog(id){
	art.dialog.open("<%=pre%>/course/showcatalog<%=suf%>?id=" +id,{
		title:'课程目录',
		height:480
	})
}
</script>

</head>
<body>
	<!-- 头部 -->
	<%@include file="/WEB-INF/jsp/front001/top.jsp"%>
	<div class="content">
		<div class="wrap">
			<div class="top-head">
				<ul>
					<li><a href="index.html">首页 / </a></li>
					<li><a href="#"><span>教师信息</span></a> / </li>
					<li><a href="#"><span>教师详情</span></a></li>
				</ul>
			</div>
			
			<div>
			</div>
			
			<div class="mod-showDetail">
				<div class="picCon">
					<img src="<%=uploadPath%>/${bean.picpath}" class="bigPic">
				</div>
				<div class="teaContent">
					<h2>${bean.name }</h2>
					<div class="posCon">
						<span>教学级别：${bean.tealevel }</span>
					</div>
					<div class="posCon">
						<span>教龄：${bean.jiaoling }年  </span>
					</div>
					
					<div class="posCon">
						<span>科目：${bean.subject }</span>
					</div>
					<div class="posCon">
						<span>年级：${bean.grade }</span>
					</div>
				</div>
				<div class="videoCon">
					<embed src="http://static.video.qq.com/TPout.swf?auto=0&vid=${bean.videopath }" 
						quality="high" width="100%" height="300" align="middle" allowscriptaccess="sameDomain" 
					allowfullscreen="true" type="application/x-shockwave-flash">
				</div>
			</div>
			
			<div class="partLeft">
				<div class="region">
					<div class="mod-tabCon">
						<div class="mhd">
							<span>个人证书</span>
						</div>
						<div class="mbd">
							${taBean.teainfo }
						</div>
					</div>
				</div>
				<div class="clear"></div>
				<div class="region">
					<div class="mod-tabCon">
						<div class="mhd">
							<span>学员评估</span>
						</div>
						<div class="mbd">
							<c:forEach items="${list}" var="obj">
								<div class="customerList_item">
									<div class="customerPhoto">
										<a href="javascript:void(0)" class="user_pj_a" onclick="javascript:playVideo('${obj.videopath}')">
											<span title="播放" class="user_pj_span"></span>
											<img src="<%=uploadPath%>/${obj.picpath}" />
										</a>
									</div>
									<div class="customerDesWrap">
										<div class="cdw_name">
											${obj.school}&nbsp;${obj.stuname }
										</div>
										<div class="cdw_des">
											${obj.comment}
										</div>
									</div>
									<div class="clear"></div>
								</div>
								<div class="fenjie"><hr/></div>
							</c:forEach>
						</div>
					</div>
				</div>
				
				<div class="region">
					<div class="mod-tabCon">
						<div class="mhd">
							<span>开设课程</span>
						</div>
						<div class="mbd">
							<div class="div_sj">
								<table class="bordered" id="tbl_sj">
									<tr>
										<td>序号</td>
										<td>课程名称</td>
										<td>科目</td>
										<td>适应年级</td>
										<td>开课日期</td>
										<td>上课时间</td>
										<td>课程目录</td>
									</tr>
									<c:forEach items="${kechengPager.pageList}" var="obj" varStatus="status">
										<tr>
											<td>${status.index + 1 }</td>
											<td><a href="<%=pre %>/course/info<%=suf %>?id=${obj.id}" target="_blank" title="${obj.name }">${obj.name}</a></td>
											<td>${obj.subject}</td>
											<td>${obj.grade}</td>
											<td>${obj.startdate }至${obj.enddate }</td>
											<td>${obj.timeframe }${obj.starttime }-${obj.endtime }</td>
											<td><a href="javascript:void();" onclick="javascript:showKcCatalog('${obj.id}')">课程目录</a></td>
										</tr>
									</c:forEach>
								</table>
							</div>
							
						</div>
					</div>
				</div>
				
				<div class="region">
					<div class="mod-tabCon">
						<div class="mhd">
							<span>公开课</span>
						</div>
						<div class="mbd">
							<div class="div_sj">
								<table class="bordered" id="tbl_sj">
									<tr>
										<td>序号</td>
										<td>公开课名称</td>
										<td>科目</td>
										<td>适应年级</td>
										<td>课程类型</td>
										<td>播放次数</td>
										<td>课程目录</td>
									</tr>
									<c:forEach items="${openkPager.pageList}" var="obj" varStatus="status">
										<tr>
											<td>${status.index + 1 }</td>
											<td><a href="<%=pre%>/opensec/index<%=suf %>?id=${obj.id }" target="_blank">${obj.name}</a></td>
											<td>${obj.subject}</td>
											<td>${obj.grade}</td>
											<td>${obj.kctype}</td>
											<td>${obj.playnum }</td>
											<td><a href="javascript:void();" onclick="javascript:showCatalog('${obj.id}')">课程目录</a></td>
										</tr>
									</c:forEach>
								</table>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	<div class="clear"></div>
	<!--底部-->
	<%@include file="/WEB-INF/jsp/front001/footer.jsp"%>
	
	<script>
		$('audio,video').mediaelementplayer({
			success: function(player, node) {
				$('#' + node.id + '-mode').html('mode: ' + player.pluginType);
			}
		});
	</script>
	
</body>
</html>