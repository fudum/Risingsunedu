<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="clear"></div>
<div class="footer">
	<div class="wrap">
		<div class="footer-left">
			<a href="<%=pre%>/home/index<%=suf%>">
				<img src="<%=images%>/logo1.png"/>
			</a>
		</div>
		<div class="footer-right">
			<a href="#top" class="scroll"></a>
			<p>
				<a href="javascript:void();">关于我们</a> | <a href="javascript:void();">意见反馈</a>						
				<span>&copy;</span>2015 旭日阳光教育 鲁ICP备14013167号-2
			</p>
		</div>
		<div class="clear"></div>
	</div>
</div>