<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/include/r_common_include.jsp"%>
<!DOCTYPE HTML >
<html>
<head>
	<meta charset=UTF-8>
	<title>旭日阳光教育-中考信息</title>
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/kaoshi/kaoshi.css" />
	<script type="text/javascript">
		function loadnews(id){
			var str = "";
			switch(id) {
			case 1://2015年中考
				str = "../rkaoshi/zhongkao<%=sufStr%>";
				break;
			case 2://中考分数线
				str = "../rkaoshi/fenshuxian<%=sufStr%>";
				break;
			case 3://招生信息
				str = "../rkaoshi/zhaosheng<%=sufStr%>";
				break;
			case 4://试题库
				str = "../rkaoshi/shitiku<%=sufStr%>";
				break;
			case 5://模拟录取
				str = "../rkaoshi/moniluqu<%=sufStr%>";
				break;
			}
			$("#quote").load(str);
			$("#kaoshi_lei li").each(function(){
				$(this).removeClass('leinow');
			});
			$("#kaoshi_lei li").eq(id-1).addClass('leinow');
		}
	</script>
	<style>
		.leinow{
			background-color:#fff;
		}
	</style>
</head>
<body>
	<!-- 头部 -->
	<div>
		<%@include file="/WEB-INF/jsp/risingsunedu/top.jsp"%>
	</div>
	<!--导航-->
	<div class="nowlocation">
		<ul>
			<li><a href="/risingsunedu/client/index<%=sufStr%>">首页</a> </li>
			<li>&gt;</li>
			<li><a href="/risingsunedu/rkaoshi/kaoshimanage<%=sufStr%>">考试信息</a></li>
		</ul>
	</div>
	<!--clear-->
	<div class="clear"></div>
	
	<!-- 内容 -->
	<div class="content_1000">
		<!--分类-->
		<div class="classify">
			<ul id="kaoshi_lei">
				<li style="border-right:1px solid;border-color:#ddd;">
					<a href="#" onclick="loadnews(1)">2015年中考</a>
				</li>
				<li style="border-right:1px solid;border-color:#ddd;">
					<a href="#" onclick="loadnews(2)">中考分数线</a>
				</li>
				<li style="border-right:1px solid;border-color:#ddd;">
					<a href="#" onclick="loadnews(3)">招生信息</a>
				</li>
				<li style="border-right:1px solid;border-color:#ddd;">
					<a href="#" onclick="loadnews(4)">试题库</a>
				</li>
				<li>
					<a href="#" onclick="loadnews(5)">模拟录取</a>
				</li>
			</ul>
		</div>
		<div class="clear">
		</div>
		<!--5类信息-->
		<div id="quote">
			<div class="divinfo">
			<div class="divleft_zhongkao">
				<ul>
					<li style="border-top:1px solid;border-color:#ddd"><a href="#">最新信息</a></li>
					<li><a href="#">最热信息</a></li>
					<li><a href="#">名校动态</a></li>
					<li><a href="#">中考政策</a></li>
					<li><a href="#">中考解读</a></li>
				</ul>
			</div>
			<div class="divright"></div>
			</div>
		</div>
	</div>
	
	<!-- 底部 -->
	<div>
		<%@include file="/WEB-INF/jsp/risingsunedu/footer.jsp"%>
	</div>
</body>
</html>