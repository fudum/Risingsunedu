<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML >
<html>
<head>
	<meta charset=UTF-8>
	<title>试题库</title>
</head>
<body>
	<!--试题库-->
	<div class="divinfo">
		<div class="divleft">
			<ul>
				<li class="litop">
					<div>历年中考试卷</div>
					<div>
						<a href="#">数学</a>
						<a href="#">语文</a>
						<a href="#">英语</a>
						<a href="#">物化</a>
						<a href="#">政治</a>
					</div>
				</li>
				<li>
					<div>历年五调试卷</div>
					<div>
						<a href="#">数学</a>
						<a href="#">语文</a>
						<a href="#">英语</a>
						<a href="#">物化</a>
						<a href="#">政治</a>
					</div>
				</li>
				<li>
					<div>历年四调试卷</div>
					<div>
						<a href="#">数学</a>
						<a href="#">语文</a>
						<a href="#">英语</a>
						<a href="#">物化</a>
						<a href="#">政治</a>
					</div>
				</li>
				<li>
					<div>历年元调试卷</div>
					<div>
						<a href="#">数学</a>
						<a href="#">语文</a>
						<a href="#">英语</a>
						<a href="#">物化</a>
						<a href="#">政治</a>
					</div>
				</li>
				<li>
					<div>九年级试卷</div><div>
						<a href="#">上学期</a>
					</div>
					<!--  -->
					<div>
						<a href="#">数学</a>
						<a href="#">语文</a>
						<a href="#">英语</a>
						<a href="#">物化</a>
						<a href="#">政治</a>
					</div>
				</li>
				<li>
					<div>八年级试卷</div>						
					<div>
						<a href="#">上学期</a>
						<a href="#">下学期</a>
					</div>
					<div>
						<a href="#">期中</a>
						<a href="#">期末</a>
						<a href="#">月考</a>
					</div>
					<div>
						<a href="#">数学</a>
						<a href="#">语文</a>
						<a href="#">英语</a>
						<a href="#">物里</a>
						<a href="#">政治</a>
					</div>
				</li>
				<li>
					<div>七年级试卷</div>
					<div>
						<a href="#">上</a>
						<a href="#">下</a>
					</div>
					<div>
						<a href="#">数学</a>
						<a href="#">语文</a>
						<a href="#">英语</a>
						<a href="#">政治</a>
					</div>
				</li>
				
				
			</ul>
		</div>
		<!--右侧试题展示-->
		<div class="divright">
			<div class="divtitle">
				<span class="sp_1">历年中考试卷</span>
				<span class="sp_2">总共10套</span>
			</div>
			<table>
				<tr class="trfist">
					<td>序号</td>
					<td>试题</td>
					<td>下载</td>
				</tr>
				<tr>
					<td>1</td>
					<td>2014年中考真题</td>
					<td><a href="#">下载</a></td>
				</tr>
				<tr>
					<td>序号</td>
					<td>2015年中考真题</td>
					<td><a href="#">下载</a></td>
				</tr>

			</table>
		</div>
	</div>
</body>
</html>