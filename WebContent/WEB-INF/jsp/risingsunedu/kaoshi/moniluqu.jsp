<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/include/r_common_include.jsp"%>
<!DOCTYPE HTML >
<html>
<head>
	<meta charset=UTF-8>
	<title>模拟录取</title>
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/kaoshi/moniluqu.css" />
	
</head>
<body>
	<div class="mnlq_divleft">
		<div>请输入考试成绩：</div>
		<div class="clear">
			<ul>
				<li>科目</li>
				<li>科目总分</li>
				<li>考试成绩</li>
			</ul>
		</div>
		<div class="clear">
			<ul>
				<li>语文</li>
				<li><input type="text" /></li>
				<li><input type="text" /></li>
			</ul>
		</div>
		<div class="clear">
			<ul>
				<li>数学</li>
				<li><input type="text" /></li>
				<li><input type="text" /></li>
			</ul>
		</div>
		<div class="clear">
			<ul>
				<li>英语</li>
				<li><input type="text" /></li>
				<li><input type="text" /></li>
			</ul>
		</div>
		<div class="clear">
			<ul>
				<li>理综</li>
				<li><input type="text" /></li>
				<li><input type="text" /></li>
			</ul>
		</div>
		<div class="clear">
			<ul>
				<li>历史</li>
				<li><input type="text" /></li>
				<li><input type="text" /></li>
			</ul>
		</div>
		<div class="clear">
			<ul>
				<li>政治</li>
				<li><input type="text" /></li>
				<li><input type="text" /></li>
			</ul>
		</div>
		<div class="clear">
			<ul>
				<li>总分</li>
				<li><input type="text" /></li>
			</ul>
		</div>
		<div class="clear shuruwanbi">
			<input type="submit" value="输入完毕" />
		</div>
	</div>
	<div class="mnlq_divright">
		<!--标头-->
		<div class="right_gaozhong">
			<ul>
				<li><a href="#">我想上的高中</a></li>
				<li><a href="#">我能上的高中</a></li>
			</ul>
		</div>
		<!--内容-->
		<div class="div_dangci">
			<ul>
				<li>A1档:</li>
				<li><a href="#">华师一附中</a></li>
				<li><a href="#">二中</a></li>
				<li><a href="#">外校</a></li>
				<li><a href="#">省实验</a></li>
				<li>更多...</li>
			</ul>
			<ul>
				<li>A2档:</li>
				<li>武钢三中</li>
				<li>六中</li>
				<li>一中</li>
				<li>更多...</li>
			</ul>
			<ul>
				<li>A3档:</li>
				<li>四中</li>
				<li>华师附中</li>
				<li>十四中</li>
				<li>更多...</li>
			</ul>
			<ul>
				<li>A4档:</li>
				<li>十二中</li>
				<li>育才高中</li>
				<li>水高</li>
				<li>更多...</li>
			</ul>
			<ul>
				<li>B档:</li>
				<li>文华中学</li>
				<li>39中</li>
				<li>长虹中学</li>
				<li>更多...</li>
			</ul>
		</div>

		<!---->
		<div class="div_shujuduibi clear">
			<div class="shujuduibi">
				<label>数据对比</label>
			</div>
			<div class="shujuduibi_info">
				<table class="div_table">
					<tr>
						<td>科目</td>
						<td>考试成绩</td>
						<td>华师一学校成绩</td>
					</tr>
					<tr>
						<td>语文</td>
						<td>100</td>
						<td>90</td>
					</tr>
					<tr>
						<td>数学</td>
						<td>110</td>
						<td>90</td>
					</tr>
					<tr>
						<td>英语</td>
						<td>110</td>
						<td>90</td>
					</tr>
					<tr>
						<td>理综</td>
						<td>110</td>
						<td>90</td>
					</tr>
					<tr>
						<td>历史</td>
						<td>110</td>
						<td>90</td>
					</tr>
					<tr>
						<td>政治</td>
						<td>110</td>
						<td>90</td>
					</tr>
					<tr>
						<td>总分</td>
						<td>580</td>
						<td>500</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</body>
</html>