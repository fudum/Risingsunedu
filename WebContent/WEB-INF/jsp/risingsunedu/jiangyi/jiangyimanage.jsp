<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/include/r_common_include.jsp"%>
<!DOCTYPE HTML >
<html>
<head>
<meta charset=UTF-8>
<title>旭日阳光教育-最新讲义</title>
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/zuixinjiangyi.css">
	<script>
		function nianjiChange(id){
			var strid = id;
			$("#nianjidiv").children().removeClass('sel');
			$("#"+ strid).addClass('sel');
			var tiaojian = $('.sel').text();
			var teasel = $("#teasel").val();
			var tmp = tiaojian.split(";");
			$("#quote").load("../rjiangyi/jiangyisearch<%=sufStr%>?grade="+tmp[0] + "&subject="+tmp[1] + "&tea_id=" +teasel);
		}
		
		function subjectChange(id){
			var strid = id;
			$("#subjectdiv").children().removeClass('sel');
			$("#" + strid).addClass('sel');
			var tiaojian = $('.sel').text();
			var teasel = $("#teasel").val();
			var tmp = tiaojian.split(";");
			$("#quote").load("../rjiangyi/jiangyisearch.do?grade="+tmp[0] + "&subject="+tmp[1] + "&tea_id=" +teasel);
		}
		
		function teacherChange(id){
			var strid = id;
			$("#teasel").val(id);
			$("#teacherdiv").children().removeClass('sel');
			$("#" + strid).addClass('sel');
			var tiaojian = $('.sel').text();
			var tmp = tiaojian.split(";");
			$("#quote").load("../rjiangyi/jiangyisearch.do?grade="+tmp[0] + "&subject="+tmp[1] + "&tea_id=" +strid);
		}
		
	</script>
	<style>
	#quote{
		min-height:200px;
	}
	</style>
</head>
<body>
	<!-- 头部 -->
	<div>
		<%@include file="/WEB-INF/jsp/risingsunedu/top.jsp"%>
	</div>
	<!--位置-->
	<div class="nowlocation">
		<ul>
			<li><a href="/risingsunedu/client/index<%=sufStr%>">首页</a> </li>
			<li>&gt;</li>
			<li><a href="/risingsunedu/rjiangyi/jiangyishow<%=sufStr%>">最新讲义</a></li>
		</ul>
	</div>
	<!--clear-->
	<div class="clear">
	</div>

	<!--讲义基本信息-->
	<div class="jycontent">
		<!--选项菜单-->
		<div class="xuanxiang">
			<div class="one">
				<input type="hidden" value="all" id="teasel">
				<div class="divleft">年级</div>
				<div class="divright" id="nianjidiv"> 
					<a href = "#"  id="gall" class="sel" onclick="nianjiChange('gall')">全部;</a>
					<a href = "#" id="g7" onclick="nianjiChange('g7')">七年级;</a>
					<a href = "#" id="g8" onclick="nianjiChange('g8')">八年级;</a>
					<a href = "#" id="g9" onclick="nianjiChange('g9')">九年级;</a>
				</div>
			</div>
			<div class="one">
				<div class="divleft">学科</div>
				<div class="divright" id ="subjectdiv">
					<a href = "#" id="sall" class="sel" onclick="subjectChange('sall')">全部;</a>
					<a href = "#" id="s1" onclick="subjectChange('s1')">数学;</a>
					<a href = "#" id="s2" onclick="subjectChange('s2')">物理;</a>
					<a href = "#" id="s3" onclick="subjectChange('s3')">化学;</a>
					<a href = "#" id="s4" onclick="subjectChange('s4')">语文;</a>
					<a href = "#" id="s5" onclick="subjectChange('s5')">英语;</a>
				</div>
			</div>
			
			<div>
				<div class="divleft" >老师</div>
				<div class="divright" id="teacherdiv">
					<a href = "#" id="tall" class="sel" onclick="teacherChange('tall')">全部;</a>
					<c:forEach items="${teacherList }" var="obj">
						<a href = "#" id="${obj.id}" onclick="teacherChange('${obj.id}')">${obj.name };</a>
					</c:forEach>
				</div>
			</div>
		</div>
		<div id="quote">
			<table class="dt_1" >
				<tr>
					<td>序号</td>
					<td>讲义</td>
					<td>班级</td>
					<td>详情</td>
				</tr>
				<c:forEach items="${jyList}" var="obj" varStatus="stauts">
					<tr>
						<td>${stauts.index +1}</td>
						<td>${obj.name}</td>
						<td>${obj.banji_name}</td>
						<td>
							<a href="../rjiangyi/info<%=sufStr%>?id=${obj.id}" target="_blank">详情</a>
						</td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>
	<!-- 底部 -->
	<div>
		<%@include file="/WEB-INF/jsp/risingsunedu/footer.jsp"%>
	</div>
</body>
</html>