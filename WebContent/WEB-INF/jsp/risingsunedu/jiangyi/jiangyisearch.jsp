<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/include/r_common_include.jsp"%>
<!DOCTYPE HTML >
<html>
<head>
<meta charset=UTF-8>
<title></title>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/zuixinjiangyi.css">
</head>
<body>
	<table class="dt_1" >
		<tr>
			<td>序号</td>
			<td>讲义</td>
			<td>班级</td>
			<td>详情</td>
		</tr>
		<c:forEach items="${jyList}" var="obj" varStatus="stauts">
			<tr>
				<td>${stauts.index +1}</td>
				<td>${obj.name}</td>
				<td>${obj.banji_name}</td>
				<td>
					<a href="../rjiangyi/info.do?id=${obj.id}" target="_blank">详情</a>
				</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>