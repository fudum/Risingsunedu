<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String topsufStr =".html";
%>
<script>
</script>

<!--头部-->
<div id="head" >
	<div id="head_logo">
		<a href="/risingsunedu/client/index<%=topsufStr %>">旭日阳光教育</a>
	</div>
	<div id="head_user_in">
		<!-- jstl语法 -->
		<c:choose>
			<c:when test="${stuSession == null }">
				当前访客身份：游客  | <a href="/risingsunedu/client/login<%=topsufStr %>">登入</a>
			</c:when>
			<c:otherwise>
				你好，${stuSession.name } | <a href="/risingsunedu/client/quit<%=topsufStr %>">退出</a>
			</c:otherwise>
		</c:choose>
	</div>
</div>	
<!-- 导航条 -->
<div class="nav">
	<div class="nav01">
		<ul id="nav_ul">
			<li <c:if test="${navOpt == '1' }">class="current"</c:if>><a href="/risingsunedu/rdangan/danganmanage<%=topsufStr %>">学生档案</a></li>
			<li <c:if test="${navOpt == '2' }">class="current"</c:if>><a href="/risingsunedu/rjiangyi/jiangyishow<%=topsufStr %>">最新讲义</a></li>
			<li <c:if test="${navOpt == '3' }">class="current"</c:if>><a href="/risingsunedu/rkaoshi/kaoshimanage<%=topsufStr %>">考试信息</a></li>
			<li <c:if test="${navOpt == '4' }">class="current"</c:if>><a href="/risingsunedu/rteacher/teachermanage<%=topsufStr %>">教师信息</a></li>
			<li <c:if test="${navOpt == '5' }">class="current"</c:if>><a href="/risingsunedu/onlinecourse/manage<%=topsufStr %>">在线课程</a></li>
		</ul>
	</div>
</div>
