<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML >
<%@include file="/WEB-INF/jsp/include/r_common_include.jsp"%>
<html>
<head>
	<title>旭日阳光教育首页</title>
	<meta charset=UTF-8>
	<title>旭日阳光教育</title>
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/index.css">
	<style>
		.login_right a{
			text-decoration: none;
			color:#fff;
			font-size:12px;
		}
	</style>
</head>
<body>
	<!--头部-->
	<div class="index_head_out">
		<div class="index_head">
			<div class="index_head_logo">
				<a href="#">旭日阳光教育</a>
			</div>
			<div class="login_right">
				<a href="client/login.html?type=0">登入系统</a>
			</div>
			<div  class="head_nav" style="float:right;color:#fff">
				<a href="rjiangyi/jiangyishow<%=sufStr %>" style="text-decoration: none;color:#FFf">最新讲义</a> |
				<a href="rkaoshi/kaoshimanage<%=sufStr %>" style="text-decoration: none;color:#FFf">考试信息</a> |
				<a href="rteacher/teachermanage<%=sufStr %>" style="text-decoration: none;color:#FFf">教师信息</a> |
				<a href="onlinecourse/manage<%=sufStr %>" style="text-decoration: none;color:#FFf">在线课程</a>
			</div>
		</div>	
	</div>
	<!-- 内容 -->
	<div class= "index_content">
		<div class="content_nav">
			<ul>
				<li>
					<a href="rdangan/danganmanage<%=sufStr %>" >
						<img src="<%=imagePath %>/pic_xueshengdangan.png"/>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<!-- 底部 -->
	<div>
		<%@include file="/WEB-INF/jsp/risingsunedu/footer.jsp"%>
	</div>

</body>
</html>