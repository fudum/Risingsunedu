<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/include/r_common_include.jsp"%>
<!DOCTYPE HTML >
<html>
<head>
<meta charset=UTF-8>
<title>旭日阳光教育用户登入页面</title>
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/index.css">
<link rel="stylesheet" type="text/css" href="<%=cssPath%>/login.css">
<script>
	$("document").ready(function(){
		$("#btnGo").click(function(){
			var stuid = $("#stuid").val();
			var password = $("#password").val();
			var remember = true;
			
			//判断用户名是否为空
			if (stuid == ""){
				$("#stuidtip").html("*请输入用户名。");
				return;
			}
			
			//判断密码是否为空
			if (password == "") {
				$("#passwordtip").html("*请输入密码。");
				return;
			}
			
			//判断是否选中
			if($("#remember").attr("checked")=="checked"){//如果选中
				remember = true;
			}
			else {//如果没有选中
				remember = false;
			}
			
			$("#form1").submit();
		});
		
		$("#goBack").click(function(){
			location.href="/risingsunedu/client/index<%=sufStr%>";
		});
	});
</script>
<style>
	.login_1{
		mini-height:400px;
	}
</style>
</head>
<body>
	<div>
		<!--头部-->
		<div class="index_head_out">
			<div class="index_head">
				<div class="index_head_logo">
					<a href="/risingsunedu/client/index<%=sufStr%>">旭日阳光教育</a>
				</div>
			</div>
		</div>
		
		<!-- 内容 -->
		<div class="login_1">
			<div class="login_in">
				<form id="form1" action="/risingsunedu/client/loginin<%=sufStr %>" method="post">
					<input type="hidden" value="${strType}" id="strType" name="strType" />
					<table  style="margin:auto;" class="tb">
						<tr>
							<td style="text-align:center;font-size:28px;">
								<label style="color:#e9bc1b">用户登入页面</label>
							</td>
							<td style="width:100px;"></td>
						</tr>
						<tr>
							<td>
								<input type="text" id="stuid" name="stuid" placeholder="用户名" />
							</td>
							<td>
								<span id="stuidtip" style="color:red;font-size:12px;">*</span>
							</td>
						</tr>
						<tr>
							<td>
								<input type="password" id="password" name="password" placeholder="密码"  />
							</td>
							<td>
								<span id="passwordtip" style="color:red;font-size:12px;">*</span>
							</td>
						</tr>
						<tr>
							<td>
								<!-- <input type="checkbox" id="remember" name="remember" />记住密码 -->
							</td>
							<td>
								<span id="error">${error}</span>
							</td>
						</tr>
						<tr>
							<td style="text-align:center;">
								<input type="button" id="btnGo" name="btnGo" value="登入" />
								<input type="button" id="goBack" name="goBack" value="返回主页" />
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>

		<!-- 底部 -->
		<div>
			<%@include file="/WEB-INF/jsp/risingsunedu/footer.jsp"%>
		</div>
	</div>
</body>
</html>