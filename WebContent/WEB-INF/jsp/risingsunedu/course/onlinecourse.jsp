<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/include/r_common_include.jsp"%>
<!DOCTYPE HTML >
<html>
<head>
	<meta charset=UTF-8>
	<title>旭日阳光教育-在线课程</title>
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/course/onlinecourse.css"  />
</head>
<body>
	<!-- 头部 -->
	<div>
		<%@include file="/WEB-INF/jsp/risingsunedu/top.jsp"%>
	</div>
	<!--导航-->
	<div class="nowlocation">
		<ul>
			<li><a href="/risingsunedu/client/index.do">首页</a> </li>
			<li>&gt;</li>
			<li><a href="/risingsunedu/onlinecourse/manage.do">在线课程</a></li>
		</ul>
	</div>
	
	<!--clear-->
	<div class="clear">
	</div>
	<div class="content_1000">
		<div class="scroll">
			<a href="javascript:void(0)" class="arr_left" id="FS_arr_left_01">左移动</a>
			<a href="javascript:void(0)" class="arr_right" id="FS_arr_right_01">右移动</a>
			<div class="scroll_cont" style="overflow:hidden;">
				<div style="overflow:hidden;float:left;">
					<div class="box">
						<a href="#">
							<img src="<%=imagePath %>/onlinecourse_3.jpg" alt="TED:高科技技术">
						</a>
					</div>
				</div>
			</div>
			<div id="FS_numList_01" class="numList">
				<span class="selected" title="第一页">1</span>
				<span title="第二页">2</span>
				<span title="第三页">3</span>
				<span title="第四页">4</span>
				<span title="第五页">5</span>
			</div>
		</div>
		<div class="clear"></div>
		<div class="part4">
			<div class="tit02">
				<h2 class="fblue1" style="width:200px;">
					<span>数学</span>
				</h2>
				<div class="rt" style="width:700px">
					<span class="classNum lf">
						数学课
						<span class="fred">10</span>
						节
					</span>
				</div>
			</div>
			<div class="blk02 clear">
				<div class="zcbox2">
					<a href="#">
						<img src="<%=imagePath%>/U9906P352DT20140418171828.jpg" alt="">
						<span>特别策划：2014毕业最后一节</span>
					</a>
				</div>
				<div class="zcbox2">
					<a href="#">
						<img src="<%=imagePath%>/U9906P352DT20140418171828.jpg" alt="">
						<span>特别策划：2014毕业最后一节</span>
					</a>
				</div>
			</div>

			<div class="tit02">
				<h2 class="fblue1" style="width:200px;">
					<span>物理</span>
				</h2>
				<div class="rt" style="width:700px">
					<span class="classNum lf">
						物理课
						<span class="fred">10</span>
						节
					</span>
				</div>
			</div>
			<div class="blk02 clear">
				<div class="zcbox2">
					<a href="#">
						<img src="<%=imagePath%>/U9906P352DT20140418171828.jpg" alt="">
						<span>特别策划：2014毕业最后一节</span>
					</a>
				</div>
				<div class="zcbox2">
					<a href="#">
						<img src="<%=imagePath%>/U9906P352DT20140418171828.jpg" alt="">
						<span>特别策划：2014毕业最后一节</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<!-- 底部 -->
	<div>
		<%@include file="/WEB-INF/jsp/risingsunedu/footer.jsp"%>
	</div>
</body>
</html>