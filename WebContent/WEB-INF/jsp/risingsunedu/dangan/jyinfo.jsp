<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/include/r_common_include.jsp"%>

<!DOCTYPE HTML >
<html>
<head>
	<meta charset=UTF-8>
	<title>讲义详情</title>
	<script type="text/javascript" src="<%=jsPath%>/jquery.media.js"></script>
	<script type="text/javascript">
		$("document").ready(function(){
			$('a.media').media({width:800, height:1000});
		});
	</script>
	<style>
		.jyname{
			margin-top:20px;
			font-size:24px;
			font-family:'微软雅黑','黑体',arial;
			color:#333;
		}
		
		.jytag{
			margin-top:20px;
			font-family:"Microsoft YaHei""微软雅黑""黑体""宋体";
			color:#666;
			font:12px/1.333 arial,helvetica,clean;
			height:24px;
			lineheight:24px;
			float:left;
		}
		
		.download{
			margin:20px auto;
		}
		
		.download a{
			background:#e70;
			padding:10px;
			color:#fff;
			font-family:"Microsoft YaHei""微软雅黑""黑体""宋体";
		}
		
		.print{
			height:36px;
			line-height:36px;
			font-family:'微软雅黑','黑体',arial;
			font-size:24px;
			color:#e70;
		}
	</style>
</head>
<body>
	<!-- 头部 -->
	<div>
		<%@include file="/WEB-INF/jsp/risingsunedu/top.jsp"%>
	</div>
	<!--导航-->
	<div class="nowlocation">
		<ul>
			<li><a href="/risingsunedu/client/index<%=sufStr%>">首页</a> </li>
			<li>&gt;</li>
			<li><a href="/risingsunedu/rjiangyi/jiangyishow<%=sufStr%>">最新讲义</a></li>
			<li>&gt;</li>
			<li><a href="/risingsunedu/rjiangyi/info<%=sufStr%>?id=${bean.id }">讲义详情</a></li>
		</ul>
	</div>
	<!--clear-->
	<div class="clear">
	</div>
	
	<div class="content_1000">
		<div>
			<div class="jyname">${bean.name }</div>
			<div class="jytag">
				教师：${bean.tea_name} | 年级：${bean.grade } | 科目：${bean.subject }  | 班级：${bean.banji_name }
			</div>
			<div>
				<a class="media" href="<%=uploadPath%>/jiangyi/${bean.jiangyipath}"></a> 
			</div>
		</div>
		<div class="print">
			<label>如需打印，辅导班为你提供打印服务！</label>
		</div>
	</div>
	
	<div>
		<%@include file="/WEB-INF/jsp/risingsunedu/footer.jsp"%>
	</div>
</body>
</html>