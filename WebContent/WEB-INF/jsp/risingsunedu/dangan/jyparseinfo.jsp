<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/include/r_common_include.jsp"%>
<!DOCTYPE HTML >
<html>
<head>
	<meta charset=UTF-8>
	<title>讲义解析详情</title>
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/css.css" />
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/style.css" />
	<link rel="stylesheet" type="text/css" href="<%=cssPath %>/jyparseinfo.css"/>
	<script>
		function showParse(parse){
			art.dialog({
				title:'文字解析',
				content:parse
			});
		}
	
		function showVideo(videoPath){
			art.dialog.open("../jyparse/vedioPreview.do?vp=" + videoPath,{title:'视频解析',width:'660px',height:'380px'});
		}
	
		$("document").ready(function(){
			//添加题目点击触发事件
			$("#addBttn").click(function(){
				var jyid = $("#jyid").val();
				art.dialog.open(
					"../jyparse/jyparseadd.do?jy_id="+jyid,
					{
						 width:1000,
						 height: 500,
						 title: '添加讲义解析题目',
						 lock:false
					},
					false);
			});
		});
	</script>
	<style>
		.jyname{
			margin-top:20px;
			font-size:24px;
			font-family:'微软雅黑','黑体',arial;
			color:#333;
		}
		
		.jytag{
			margin-top:20px;
			font-family:"Microsoft YaHei""微软雅黑""黑体""宋体";
			color:#666;
			font:12px/1.333 arial,helvetica,clean;
			height:24px;
			lineheight:24px;
			float:left;
		}
	</style>
</head>
<body>
	<!-- 头部 -->
	<div>
		<%@include file="/WEB-INF/jsp/risingsunedu/top.jsp"%>
	</div>
	<!--导航-->
	<div class="nowlocation">
		<ul>
			<li><a href="/risingsunedu/client/index<%=sufStr%>">首页</a> </li>
			<li>&gt;</li>
			<li><a href="/risingsunedu/rdangan/danganmanage<%=sufStr%>">我的档案</a></li>
			<li>&gt;</li>
			<li><a href="/risingsunedu/rdangan/jyparseinfo<%=sufStr%>?id=${bean.id }">我的课堂讲义详情</a></li>
		</ul>
	</div>
	<!--clear-->
	<div class="clear">
	</div>
	
	<div class="content_1000">
		<div>
			<input type="hidden" value="${bean.id }" id="jyid"/>
		</div>
		<div class="jyname">
			${bean.name }
		</div>
		<div class="jytag">
			教师：${bean.tea_name} | 年级：${bean.grade } | 科目：${bean.subject }  
		</div>
		<div class="clear"></div>
		<div class="jiangyiinfo">
			<c:forEach items="${jyParseList}" var="obj">
				<div class="ti_all">
					<div class="divtimu">
						${obj.timu}
					</div>
					<div class="divjiexi">
						<a href="#" onclick="showParse('${obj.parse}')">显示解析</a>									
						<a href="#" onclick="showVideo('${obj.videopath}')">视频讲解</a>
					</div>
				</div>
			</c:forEach>
		</div>
	</div>
	<!-- 底部 -->
	<div>
		<%@include file="/WEB-INF/jsp/risingsunedu/footer.jsp"%>
	</div>
</body>
</html>