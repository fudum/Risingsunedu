<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/include/r_common_include.jsp"%>

<!DOCTYPE HTML >
<html>
<head>
	<meta charset=UTF-8>
	<title>旭日阳光教育-学生档案</title>
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/danganmanage.css">
	<style>
		/***最新讲义***/
		.zuixinjiangyi
		{
			margin-top:10px;
		}
		
		.zuixinjiangyi_table
		{
			text-align: center;
			width: 100%;
		}
		
		.zuixinjiangyi_table  
		{
			border-collapse: collapse;
		}
		
		.zuixinjiangyi_table tr td
		{
			line-height: 32px;
			font-size: 14px;
			color: #444;
			padding: 0px 5px;
		
			border-bottom: 1px solid;
			border-color: #ddd;
		}
		
		.divpage
		{
			height: 40px;
			text-align: center;
			margin: 10px;
			font-size: 14px;
		}
	</style>
</head>
<body>
	<!-- 头部 -->
	<div>
		<%@include file="/WEB-INF/jsp/risingsunedu/top.jsp"%>
	</div>
	<!--导航-->
	<div class="nowlocation">
		<ul>
			<li><a href="/risingsunedu/client/index<%=sufStr%>">首页</a> </li>
			<li>&gt;</li>
			<li><a href="/risingsunedu/rdangan/danganmanage<%=sufStr%>">我的档案</a></li>
		</ul>
	</div>
	<!--clear-->
	<div class="clear"></div>
	
	<!-- 中间内容 -->
	<div class="content_1000">
		<div class="dangan_top">
			<div class="dangan_top_img">
				<img width = "120" height="160" src="<%=imagePath %>/xueshengdangan_1.png" />
			</div>
			<div class="dangan_top_wenzi">
				<ul>
					<li>学生编号：${stuSession.stuid }</li>
					<li>姓名：${stuSession.name }</li>
					<li>学校：${stuSession.school }</li>
					<li>报名课程：</li>
					<li>电话：${stuSession.phone }</li>
				</ul>
			</div>
		</div>
		<div class="dangan_nav">
			<ul>
				<li>我的课程设计</li>
				<li>我的课堂讲义</li>
				<li>我的培优成绩</li>
				<li>我的学校成绩</li>
				<li>我的培优作业</li>
				<li>我的上课记录</li>
				<li>我的题库</li>
			</ul>
		</div>
		
		<div class="dangan_neirong">
			<div class="dangan_stu">
				<h3>我的课程设计</h3>
				<div>
					${stuAddnBean.kechengsheji }
				</div>
			</div>
			
			<div class="dangan_stu">
				<h3>我的课堂讲义</h3>
				<div class="zuixinjiangyi">
					<table class="zuixinjiangyi_table">
						<tr>
							<td>序号</td>
							<td>年级</td>
							<td>科目</td>
							<td>讲义</td>
							<td>详情</td>
						</tr>
						<c:forEach items="${jyList}" var="obj" varStatus="stauts">
							<tr>
								<td>${stauts.index +1}</td>
								<td>${obj.grade }</td>
								<td>${obj.subject }</td>
								<td>${obj.name }</td>
								<td>
									<a href="../rdangan/jyparseinfo<%=sufStr%>?id=${obj.id}" target="_blank">详情</a>
								</td>
							</tr>
						</c:forEach>
					</table>
					<div class="divpage">
						${banjiPage.htmlOutput}
					</div>
				</div>
			</div>
			
			<div class="dangan_stu">
				<h3>我的培优成绩</h3>
			</div>

			<div class="dangan_stu">
				<h3>我的学校成绩</h3>
			</div>

			<div class="dangan_stu">
				<h3>我的培优作业</h3>
			</div>
			<div class="dangan_stu">
				<h3>我的上课记录</h3>
				<div>
					${stuAddnBean.shangkejilu}
				</div>
			</div>
			<div class="dangan_stu">
				<h3>我的题库</h3>
				<div>
					${stuAddnBean.tiku}
				</div>
			</div>
		</div>
	</div>
	<!-- 底部 -->
	<div>
		<%@include file="/WEB-INF/jsp/risingsunedu/footer.jsp"%>
	</div>
</body>
</html>