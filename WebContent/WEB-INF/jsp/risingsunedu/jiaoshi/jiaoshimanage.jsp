<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/include/r_common_include.jsp"%>
<!DOCTYPE HTML >
<html>
<head>
	<meta charset=UTF-8>
	<title>旭日阳光教育-教师信息</title>
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/jiaoshimanage.css">
	<script>
	
		function nianjiChange(id){
			var strid = id;
			$("#nianjidiv").children().removeClass('sel');
			$("#"+ strid).addClass('sel');
			var tiaojian = $('.sel').text();
			var tmp = tiaojian.split(";");
			$("#quote").load("../rteacher/teachersearch<%=sufStr%>?grade="+tmp[0] + "&subject="+tmp[1]);
		}
		
		function subjectChange(id){
			var strid = id;
			$("#subjectdiv").children().removeClass('sel');
			$("#" + strid).addClass('sel');
			var tiaojian = $('.sel').text();
			var tmp = tiaojian.split(";");
			$("#quote").load("../rteacher/teachersearch<%=sufStr%>?grade="+tmp[0] + "&subject="+tmp[1]);
		}
				
	</script>
</head>
<body>
	<!-- 头部 -->
	<div>
		<%@include file="/WEB-INF/jsp/risingsunedu/top.jsp"%>
	</div>
	<!--导航-->
	<div class="nowlocation">
		<ul>
			<li><a href="/risingsunedu/client/index.do">首页</a> </li>
			<li>&gt;</li>
			<li><a href="/risingsunedu/rteacher/teachermanage.do">教师信息</a></li>
		</ul>
	</div>
	<!--clear-->
	<div class="clear">
	</div>
	
	<!-- 教师内容 -->
	<div class = "jscontent">
		<div class="xuanxiang">
			<div class="one">
				<input type="hidden" value="all" id="teasel">
				<div class="divleft">年级</div>
				<div class="divright" id="nianjidiv"> 
					<a href = "#"  id="gall" class="sel" onclick="nianjiChange('gall')">全部;</a>
					<a href = "#" id="g7" onclick="nianjiChange('g7')">七年级;</a>
					<a href = "#" id="g8" onclick="nianjiChange('g8')">八年级;</a>
					<a href = "#" id="g9" onclick="nianjiChange('g9')">九年级;</a>
				</div>
			</div>
			<div>
				<div class="divleft">学科</div>
				<div class="divright" id ="subjectdiv">
					<a href = "#" id="sall" class="sel" onclick="subjectChange('sall')">全部;</a>
					<a href = "#" id="s1" onclick="subjectChange('s1')">数学;</a>
					<a href = "#" id="s2" onclick="subjectChange('s2')">物理;</a>
					<a href = "#" id="s3" onclick="subjectChange('s3')">化学;</a>
					<a href = "#" id="s4" onclick="subjectChange('s4')">语文;</a>
					<a href = "#" id="s5" onclick="subjectChange('s5')">英语;</a>
				</div>
			</div>
		</div>
		
		<!--教师列表-->
		<div class="js_list" id="quote">
			<c:forEach items="${teaList }" var="obj">
				<div class="js_list_in">
					<div class="divleft">
						<img src="<%=uploadPath %>/${obj.picpath}" width="90px" height="120px">
					</div>
					<div class="divright">
						<a href="#">${obj.name }</a>
						<ul>
							<li><span>性别：</span>${obj.sex }</li>
							<li><span>教龄：</span>${obj.jiaoling }</li>
							<li><span>特长：</span>${obj.techang }</li>
						</ul>
					</div>
					<div class="divright_right">
						<a href="/risingsunedu/rteacher/teacherinfo<%=sufStr %>?id=${obj.id }" target="black_">查看详情</a>
					</div>
				</div>
				<div class="clear bottom_line"></div>
			</c:forEach>
		</div>
	</div>
	<!-- 底部 -->
	<div>
		<%@include file="/WEB-INF/jsp/risingsunedu/footer.jsp"%>
	</div>
</body>
</html>