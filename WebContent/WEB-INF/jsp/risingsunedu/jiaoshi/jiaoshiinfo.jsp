<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML >
<%@include file="/WEB-INF/jsp/include/r_common_include.jsp"%>
<html>
<head>
	<meta charset=UTF-8>
	<title>旭日阳光教育-教师详情</title>
	<script type="text/javascript" src="<%=jmPath %>/build/mediaelement-and-player.min.js" ></script>
	<link rel="stylesheet" type="text/css" href="<%=jmPath%>/build/mediaelementplayer.min.css" />
	<link rel="stylesheet" href="<%=jmPath%>/build/mejs-skins.css" />
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/jiaoshiinfo.css">

	<script type="text/javascript">
		$("document").ready(function(){
			//视频预览功能
			$("#btn1").click(
				function(){
					var videoPath = $("#videopath").val();
					art.dialog.open("../teacher/vedioPreview<%=sufStr%>?vp=" + videoPath,{title:'教师视频简介',width:'650px',height:'370px'});
				});
		});
		
	</script>
</head>
<body>
	<!-- 头部 -->
	<div>
		<%@include file="/WEB-INF/jsp/risingsunedu/top.jsp"%>
	</div>
	<!--导航-->
	<div class="nowlocation">
		<ul>
			<li><a href="/risingsunedu/client/index<%=sufStr%>">首页</a> </li>
			<li>&gt;</li>
			<li><a href="/risingsunedu/rteacher/teachermanage<%=sufStr%>">教师信息</a></li>
			<li>&gt;</li>
			<li><a href="/risingsunedu/rteacher/teacherinfo<%=sufStr%>?id=${bean.id}">教师详情</a></li>
		</ul>
	</div>
	<!--clear-->
	<div class="clear"></div>
	
	<!-- 内容 -->
	<div class="content_1000">
		<!-- content_top -->
		<div class="content_top">
			<input type="hidden" value="${bean.videopath }" id="videopath" />
			<div class="jsxq_div_left">
				<img src="<%=uploadPath %>/${bean.picpath}" width="150px" height="200px">
			</div>
			<div class="jsxq_div_center">
				<ul>
					<li>教师姓名：${bean.name }</li>
					<li>科目：${bean.subject }</li>
					<li>教龄：${bean.jiaoling }</li>
					<li>职位：${bean.techang }</li>
					<li>级别：${bean.tealevel }</li>
				</ul>
			</div>
			<div class="jsxq_div_right">
				<div class="shipinjianjie">
					<input type="button" id="btn1" class="btnbrief" value="视频简介"/>
				</div>
			</div>
		</div><!-- end of content top -->
		
		<div style="clear:both;"></div>
		
		<!-- 数据统计 -->
		<div class="div_tongji">
			<table class="div_table">
				<tr>
					<td class="tdshugang">学生人数（人）</td>
					<td class="tdshugang">累计课时（次）</td>
					<td class="tdshugang">累计讲义（个）</td>
					<td>教学评分</td>
				</tr>
				<tr>
					<td class="tdshugang">${bean.stunumber }</td>
					<td class="tdshugang">${bean.keshinumber }</td>
					<td class="tdshugang">${jynum }</td>
					<td>${bean.pingfen }</td>
				</tr>
			</table>
		</div>
		
		<!--教师信息-->
		<div>
			<!-- 二级信息 -->
			<div class="erjixinxi">
				<ul>
					<li><a href="#">教师信息</a></li>
					<li><a href="#">教学质量评估</a></li>
					<li><a href="#">最新讲义</a></li>
					<li><a href="#">在授课程</a></li>
					<li><a href="#">教学成果</a></li>
					<li><a href="#">违规记录</a></li>
					<li><a href="#">最新课表</a></li>
				</ul>
			</div>
			
			<!-- 教师具体信息 -->
			<div class="teacher_body">
				<div class="teacher_teach">
					<h3>教师信息</h3>
					<div>
					${teaaddition.teainfo}
					</div>
				</div>
			
				<!--教学质量评估-->
				<div class="teacher_pinggu">
					<h3>教学质量评估</h3>
					<div class="jxzlpg_nav">
						<ul>
							<li><a href="#">学员培优成绩</a></li>
							<li><a href="#">学员学校成绩</a></li>
							<li><a href="#">学员作业情况</a></li>
							<li><a href="#">学员课堂表现</a></li>
						</ul>
					</div>
				</div>
				
				<!--最新讲义-->
				<div class="teacher_jiangyi">
					<h3>最新讲义</h3>
					<div class="zuixinjiangyi">
						<table class="zuixinjiangyi_table">
							<tr>
								<td>序号</td>
								<td>年级</td>
								<td>科目</td>
								<td>讲义</td>
								<td>班级</td>
								<td>上传时间</td>
								<td>详情</td>
							</tr>
							<c:forEach items="${jyPage.pageList }" var="obj" varStatus="stauts">
								<tr>
									<td>${stauts.index +1}</td>
									<td>${obj.grade }</td>
									<td>${obj.subject }</td>
									<td>${obj.name }</td>
									<td>${obj.banji_name}</td>
									<td>${obj.inserttime }</td>
									<td>
										<a href="../rjiangyi/info<%=sufStr%>?id=${obj.id}" target="_blank">详情</a>
									</td>
								</tr>
							</c:forEach>
						</table>
						<div class="divpage">
							${banjiPage.htmlOutput}
						</div>
					</div>
				</div>
				
				<!--教学成果-->
				<div class="teacher_chengguo">
					<h3>教学成果</h3>
					<div>
					${teaaddition.teachengguo }
					</div>
				</div>

				<div class="teacher_jianyi">
					<h3>违规记录</h3>
					<div>
					${teaaddition.teaweigui }
					</div>
				</div>
				
				
			</div><!-- end of teachbody -->
		</div><!-- end of 教师信息 -->
	</div><!-- end content_1000  -->
	
	<!-- 底部 -->
	<div>
		<%@include file="/WEB-INF/jsp/risingsunedu/footer.jsp"%>
	</div>
</body>
</html>