<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/jsp/include/r_common_include.jsp"%>
<!DOCTYPE HTML >
<html>
<head>
	<meta charset=UTF-8>
	<title></title>
	<link rel="stylesheet" type="text/css" href="<%=cssPath%>/jiaoshimanage.css">
</head>
<body>
	<c:forEach items="${teaList }" var="obj">
		<div class="js_list_in">
			<div class="divleft">
				<img src="<%=uploadPath %>/${obj.picpath}" width="90px" height="120px">
			</div>
			<div class="divright">
				<a href="#">${obj.name }</a>
				<ul>
					<li><span>性别：</span>${obj.sex }</li>
					<li><span>教龄：</span>${obj.jiaoling }</li>
					<li><span>特长：</span>${obj.techang }</li>
				</ul>
			</div>
			<div class="divright_right">
				<a href="/risingsunedu/rteacher/teacherinfo<%=sufStr%>?id=${obj.id }" target="black_">查看详情</a>
			</div>
		</div>
		<div class="clear bottom_line"></div>
	</c:forEach>
</body>
</html>