package risingsunedu.Common.Impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

import risingsunedu.Common.ICommonFun;

public class CommonFunImpl implements ICommonFun {
	
	
	/*
	 * 组装数据库主键 prefix+年月日时分秒+三位随机数 (B20141030143001001)
	 */
	public static String uuid(String prefix) {
		String retVal = prefix;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		retVal += dateFormat.format(new Date());
		retVal += String.valueOf(new Random().nextInt(900) + 100);
		return retVal;
	}
	
	/*
	 * 组装32位的uuid，由系统生成
	 */
	public static String uuid32() {
		String retVal = UUID.randomUUID().toString();
		retVal = retVal.replaceAll("-", "");
		return retVal;
	}

	/*
	 * 获取系统时间 
	 * 1yyyy-MM-dd HH:mm:ss;
	 * 2yyyy-MM-dd
	 * 3HH:mm:ss
	 * 4yyyy
	 */
	public static String currentTime(int type) {
		String retVal;
		switch (type) {
		case 1:
			retVal = "yyyy-MM-dd HH:mm:ss";
			break;
		case 2:
			retVal = "yyyy-MM-dd";
			break;
		case 3:
			retVal = "HH:mm:ss";
			break;
		case 4:
			retVal = "yyyy";
			break;
		default:
			retVal = "yyyy-MM-dd HH:mm:ss";
			break;
		}
		retVal = new SimpleDateFormat(retVal).format(new Date());
		return retVal;
	}
}
