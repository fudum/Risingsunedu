package risingsunedu.Common.Impl;



public class CommonConstant {
	
	public static String preStr ="";
	
	/***********************linux版本路径***********************/
	
	public static String uploadPath = "/risingsunedu/uploadfile/upload";//上传总目录
	
	public static String attachedPath = "/risingsunedu/uploadfile/upload/attached";//附加文件路径
	
	public static String teacherPath ="/risingsunedu/uploadfile/upload/teacher";//教师路径
	
	public static String jyPath = "/risingsunedu/uploadfile/upload/jiangyi";//讲义路径
	
	public static String assessPath = "/risingsunedu/uploadfile/upload/teacher/assess";//学员评估
	
	public static String excstuPath = "/risingsunedu/uploadfile/upload/excstu";//优秀学员
	
	public static String openkPath = "/risingsunedu/uploadfile/upload/openk";//公开课
	
	public static String advertisementPath = "/risingsunedu/uploadfile/upload/advertisement";
	
	public static String studentPath = "/risingsunedu/uploadfile/upload/student";
	
	public static String openkImagesPath = openkPath +"/images";//公开课图片
	
	public static String openkVideoPath = openkPath +"/video";//公开课视频
	
	public static String dayiPicPath = uploadPath + "/dayi";//视频答疑路径
	
	public static String shijuanPath =  uploadPath + "/shijuan";//试卷路径
	
	public static String peiyouzuoyePath = studentPath + "/peiyouzuoye";//学生-培优作业
	
	public static String peiyouceshiPath = studentPath + "/peiyouceshi";//学生-培优测试
	
	public static String peiyoujiangyiPath = studentPath + "/peiyoujiangyi";//学生-培优讲义
	
	
	/*********************windows版本路径***********************/
	
	public static String w_uploadPath = "d:\\risingsunedu\\uploadfile\\upload";//上传总目录
	
	public static String w_teacherPath = "d:\\risingsunedu\\uploadfile\\upload\\teacher";//教师文件路径
	
	public static String w_attachedPath = "d:\\risingsunedu\\uploadfile\\upload\\attached";//附加文件路径
	
	public static String w_jyPath = "d:\\risingsunedu\\uploadfile\\upload\\jiangyi";//讲义路径
	
	public static String w_assessPath = "d:\\risingsunedu\\uploadfile\\upload\\teacher\\assess";//学员评估
	
	public static String w_excstuPath = "d:\\risingsunedu\\uploadfile\\upload\\excstu";//优秀学员
	
	public static String w_openkPath = "d:\\risingsunedu\\uploadfile\\upload\\openk";//公开课
	
	public static String w_advertisementPath ="d:\\risingsunedu\\uploadfile\\upload\\advertisement";//首页广告
	
	public static String w_studentPath = "d:\\risingsunedu\\uploadfile\\upload\\student";//学生路径
	
	public static String w_openkImagesPath = w_openkPath + "\\images";//公开课图片
	
	public static String w_openkVideoPath = w_openkPath + "\\video";//公开课视频
	
	public static String w_dayiPicPath = w_uploadPath + "\\dayi";//视频答疑路径
	
	public static String w_shijuanPath = w_uploadPath + "\\shijuan";//试卷路径
	
	public static String w_peiyouzuoyePath = w_studentPath + "\\peiyouzuoye";//学生-培优作业
	
	public static String w_peiyouceshiPath = w_studentPath + "\\peiyouceshi";//学生-培优测试
	
	public static String w_peiyoujiangyiPath = w_studentPath + "\\peiyoujiangyi";//学生-培优讲义
	
	/******************前后缀*************/
	public static String sufStr = ".html";
	
	
	
	//学生档案
	public static int dangan = 1;
	//最新讲义
	public static int jiangyi = 2;
	//中考信息
	public static int zhongkao = 3;
	//教师信息
	public static int jiaoshi = 4;
	//在线课程
	public static int onlinecourse = 5;
	//返回的值
	public static String navOpt = "navOpt";
}
