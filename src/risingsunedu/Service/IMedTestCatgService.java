package risingsunedu.Service;

import java.util.List;

import risingsunedu.Bean.MedTestCatgBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IMedTestCatgService extends IBaseService{

	public MedTestCatgBean selectOne(MedTestCatgBean bean);
	
	public List<MedTestCatgBean> selectList(MedTestCatgBean bean);
	
	public int update(MedTestCatgBean bean);
	
	public int insert(MedTestCatgBean bean);
	
	public int delete(MedTestCatgBean bean);
	
	public Pager<MedTestCatgBean> selectPage(MedTestCatgBean bean,Pager<MedTestCatgBean> page);

}
