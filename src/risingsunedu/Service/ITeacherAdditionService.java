package risingsunedu.Service;

import java.util.List;

import risingsunedu.Bean.TeacherAdditionBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface ITeacherAdditionService extends IBaseService{
	
	public TeacherAdditionBean selectOne(TeacherAdditionBean bean);
	
	public List<TeacherAdditionBean> selectList(TeacherAdditionBean bean);
	
	public int update(TeacherAdditionBean bean);
	
	public int insert(TeacherAdditionBean bean);
	
	public int delete(TeacherAdditionBean bean);
	
	public Pager<TeacherAdditionBean> selectPage(TeacherAdditionBean bean,Pager<TeacherAdditionBean> page);
	
}
