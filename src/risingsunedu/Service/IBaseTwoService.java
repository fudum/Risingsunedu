package risingsunedu.Service;

import java.util.List;

import risingsunedu.Bean.BaseTwoBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IBaseTwoService extends IBaseService{

	public BaseTwoBean selectOne(BaseTwoBean bean);
	
	public List<BaseTwoBean> selectList(BaseTwoBean bean);
	
	public int update(BaseTwoBean bean);
	
	public int insert(BaseTwoBean bean);
	
	public int delete(BaseTwoBean bean);
	
	public Pager<BaseTwoBean> selectPage(BaseTwoBean bean,Pager<BaseTwoBean> pager);
	
}
