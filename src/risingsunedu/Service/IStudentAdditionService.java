package risingsunedu.Service;

import java.util.List;

import risingsunedu.Bean.StudentAdditionBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IStudentAdditionService extends IBaseService{

	public StudentAdditionBean selectOne(StudentAdditionBean bean);
	
	public List<StudentAdditionBean> selectList(StudentAdditionBean bean);
	
	public int update(StudentAdditionBean bean);
	
	public int insert(StudentAdditionBean bean);
	
	public int delete(StudentAdditionBean bean);
	
	public Pager<StudentAdditionBean> selectPage(StudentAdditionBean bean,Pager<StudentAdditionBean> page);
	
}
