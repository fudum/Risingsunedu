package risingsunedu.Service;

import java.util.List;

import risingsunedu.Bean.XuebaBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IXuebaService extends IBaseService{

	public XuebaBean selectOne(XuebaBean bean);
	
	public List<XuebaBean> selectList(XuebaBean bean);
	
	public int update(XuebaBean bean);
	
	public int insert(XuebaBean bean);
	
	public int delete(XuebaBean bean);
	
	public Pager<XuebaBean> selectPage(XuebaBean bean,Pager<XuebaBean> pager);
	
}
