package risingsunedu.Service;

import java.util.List;
import java.util.Map;

import risingsunedu.Bean.OpenkBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IOpenkService extends IBaseService{

	public OpenkBean selectOne(OpenkBean bean);
	
	public List<OpenkBean> selectList(OpenkBean bean);
	
	public int update(OpenkBean bean);
	
	public int insert(OpenkBean bean);
	
	public int delete(OpenkBean bean);
	
	public Pager<OpenkBean> selectPage(OpenkBean bean,Pager<OpenkBean> page);
	
	public Pager<OpenkBean> selectOpenkTeacher(OpenkBean bean,Pager<OpenkBean> pager);
	
	/**
	 * 修改共公开课所属的课程类型
	 * @param map
	 * @return
	 */
	public int update(Map<String, String> map);
	
}
