package risingsunedu.Service;

import java.util.List;

import risingsunedu.Bean.BaseOneBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IBaseOneService extends IBaseService{

	public BaseOneBean selectOne(BaseOneBean bean);
	
	public List<BaseOneBean> selectList(BaseOneBean bean);
	
	public int update(BaseOneBean bean);
	
	public int insert(BaseOneBean bean);
	
	public int delete(BaseOneBean bean);
	
	public Pager<BaseOneBean> selectPage(BaseOneBean bean,Pager<BaseOneBean> pager);
	
}
