package risingsunedu.Service;

import java.util.List;

import risingsunedu.Bean.KechengBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IKechengService extends IBaseService{

	public KechengBean selectOne(KechengBean bean);
	
	public List<KechengBean> selectList(KechengBean bean);
	
	public int update(KechengBean bean);
	
	public int insert(KechengBean bean);
	
	public int delete(KechengBean bean);
	
	public Pager<KechengBean> selectPage(KechengBean bean,Pager<KechengBean> pager);
	
	public Pager<KechengBean> selectPagerWithTeacher(KechengBean bean,Pager<KechengBean> pager);
	
}
