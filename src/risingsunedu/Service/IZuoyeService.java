package risingsunedu.Service;

import java.util.List;

import risingsunedu.Bean.ZuoyeBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IZuoyeService extends IBaseService{

	public ZuoyeBean selectOne(ZuoyeBean bean);
	
	public List<ZuoyeBean> selectList(ZuoyeBean bean);
	
	public int update(ZuoyeBean bean);
	
	public int insert(ZuoyeBean bean);
	
	public int delete(ZuoyeBean bean);
	
	public Pager<ZuoyeBean> selectPage(ZuoyeBean bean,Pager<ZuoyeBean> pager);
	
}
