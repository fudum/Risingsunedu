package risingsunedu.Service;

import java.util.List;

import risingsunedu.Bean.ExcellentStudentBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IExcellentStudentService extends IBaseService{

	public ExcellentStudentBean selectOne(ExcellentStudentBean bean);
	
	public List<ExcellentStudentBean> selectList(ExcellentStudentBean bean);
	
	public int update(ExcellentStudentBean bean);
	
	public int insert(ExcellentStudentBean bean);
	
	public int delete(ExcellentStudentBean bean);
	
	public Pager<ExcellentStudentBean> selectPage(ExcellentStudentBean bean,Pager<ExcellentStudentBean> page);

	public Pager<ExcellentStudentBean> selectPageWithStudent(ExcellentStudentBean bean,Pager<ExcellentStudentBean> pager);
}
