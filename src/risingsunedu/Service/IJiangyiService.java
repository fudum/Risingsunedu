package risingsunedu.Service;

import java.util.List;

import risingsunedu.Bean.JiangyiBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IJiangyiService extends IBaseService{

	public JiangyiBean selectOne(JiangyiBean bean);
	
	public List<JiangyiBean> selectList(JiangyiBean bean);
	
	public int update(JiangyiBean bean);
	
	public int insert(JiangyiBean bean);
	
	public int delete(JiangyiBean bean);
	
	public Pager<JiangyiBean> selectPage(JiangyiBean bean,Pager<JiangyiBean> page);
	
	//搜索最新id讲义
	public List<JiangyiBean> selectNewest(JiangyiBean bean);
	
	//根据教师id搜寻数量
	public int selectCountByTeaId(String teaId);
	
	//根据学生编号搜寻讲义
	public List<JiangyiBean> selectByStuId(String stu_id);
	
	/**
	 * 根据教师主键查找讲义
	 * @param tea_id
	 * @return
	 */
	public List<JiangyiBean> selectByTeaId(String tea_id);
		
}
