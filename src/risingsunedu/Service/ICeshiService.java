package risingsunedu.Service;

import java.util.List;

import risingsunedu.Bean.CeshiBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface ICeshiService extends IBaseService{

	public CeshiBean selectOne(CeshiBean bean);
	
	public List<CeshiBean> selectList(CeshiBean bean);
	
	public int update(CeshiBean bean);
	
	public int insert(CeshiBean bean);
	
	public int delete(CeshiBean bean);
	
	public Pager<CeshiBean> selectPage(CeshiBean bean,Pager<CeshiBean> pager);
	
}
