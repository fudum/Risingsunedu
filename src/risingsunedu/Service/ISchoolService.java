package risingsunedu.Service;

import java.util.List;

import risingsunedu.Bean.SchoolBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface ISchoolService extends IBaseService{

	public SchoolBean selectOne(SchoolBean bean);
	
	public List<SchoolBean> selectList(SchoolBean bean);
	
	public int update(SchoolBean bean);
	
	public int insert(SchoolBean bean);
	
	public int delete(SchoolBean bean);
	
	public Pager<SchoolBean> selectPage(SchoolBean bean,Pager<SchoolBean> page);
	
}
