package risingsunedu.Service;

import java.util.List;

import risingsunedu.Bean.BookBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IBookService extends IBaseService{

	public BookBean selectOne(BookBean bean);
	
	public List<BookBean> selectList(BookBean bean);
	
	public int update(BookBean bean);
	
	public int insert(BookBean bean);
	
	public int delete(BookBean bean);
	
	public Pager<BookBean> selectPage(BookBean bean,Pager<BookBean> pager);
	
}
