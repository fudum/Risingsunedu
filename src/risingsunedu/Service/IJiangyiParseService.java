package risingsunedu.Service;

import java.util.List;

import risingsunedu.Bean.JiangyiParseBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IJiangyiParseService extends IBaseService{

	public JiangyiParseBean selectOne(JiangyiParseBean bean);
	
	public List<JiangyiParseBean> selectList(JiangyiParseBean bean);
	
	public int update(JiangyiParseBean bean);
	
	public int insert(JiangyiParseBean bean);
	
	public int delete(JiangyiParseBean bean);
	
	public Pager<JiangyiParseBean> selectPage(JiangyiParseBean bean,Pager<JiangyiParseBean> page);
}
