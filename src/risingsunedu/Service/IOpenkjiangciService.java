package risingsunedu.Service;

import java.util.List;

import risingsunedu.Bean.OpenkjiangciBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IOpenkjiangciService extends IBaseService{

	public OpenkjiangciBean selectOne(OpenkjiangciBean bean);
	
	public List<OpenkjiangciBean> selectList(OpenkjiangciBean bean);
	
	public int update(OpenkjiangciBean bean);
	
	public int insert(OpenkjiangciBean bean);
	
	public int delete(OpenkjiangciBean bean);
	
	public Pager<OpenkjiangciBean> selectPage(OpenkjiangciBean bean,Pager<OpenkjiangciBean> pager);
	
	public int updateJorder(OpenkjiangciBean bean);
}
