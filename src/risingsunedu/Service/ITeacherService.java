package risingsunedu.Service;

import java.util.List;

import risingsunedu.Bean.TeacherBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface ITeacherService extends IBaseService{

	public TeacherBean selectOne(TeacherBean bean);
	
	public List<TeacherBean> selectList(TeacherBean bean);
	
	public int update(TeacherBean bean);
	
	public int insert(TeacherBean bean);
	
	public int delete(TeacherBean bean);
	
	public Pager<TeacherBean> selectPage(TeacherBean bean,Pager<TeacherBean> page);
	
	public String getMaxTeaId();
	
	public Pager<TeacherBean> selectPageLike(TeacherBean bean,Pager<TeacherBean> page);
	
	
}
