package risingsunedu.Service;

import java.util.List;

import risingsunedu.Bean.ZhangjieBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IZhangjieService extends IBaseService{

	public ZhangjieBean selectOne(ZhangjieBean bean);
	
	public List<ZhangjieBean> selectList(ZhangjieBean bean);
	
	public int update(ZhangjieBean bean);
	
	public int insert(ZhangjieBean bean);
	
	public int delete(ZhangjieBean bean);
	
	public Pager<ZhangjieBean> selectPage(ZhangjieBean bean,Pager<ZhangjieBean> pager);
	
}
