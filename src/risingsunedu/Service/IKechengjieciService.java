package risingsunedu.Service;

import java.util.List;

import risingsunedu.Bean.KechengjieciBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IKechengjieciService extends IBaseService{

	public KechengjieciBean selectOne(KechengjieciBean bean);
	
	public List<KechengjieciBean> selectList(KechengjieciBean bean);
	
	public int update(KechengjieciBean bean);
	
	public int insert(KechengjieciBean bean);
	
	public int delete(KechengjieciBean bean);
	
	public Pager<KechengjieciBean> selectPage(KechengjieciBean bean,Pager<KechengjieciBean> page);
	
	/**
	 * 通过课程表的id来查询视频信息
	 * @param kechengid
	 * @return
	 */
	public List<KechengjieciBean> selectListByKechengId(String kechengId);
	
	public int updateByJiangciId(KechengjieciBean bean);
	
}
