package risingsunedu.Service;

import java.util.List;

import risingsunedu.Bean.BanjijiangciBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IBanjijiangciService extends IBaseService{

	public BanjijiangciBean selectOne(BanjijiangciBean bean);
	
	public List<BanjijiangciBean> selectList(BanjijiangciBean bean);
	
	public int update(BanjijiangciBean bean);
	
	public int insert(BanjijiangciBean bean);
	
	public int delete(BanjijiangciBean bean);
	
	public Pager<BanjijiangciBean> selectPage(BanjijiangciBean bean,Pager<BanjijiangciBean> pager);
		
}
