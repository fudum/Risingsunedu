package risingsunedu.Service;

import java.util.List;

import risingsunedu.Bean.StudentbanjiBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IStudentbanjiService extends IBaseService{

	public StudentbanjiBean selectOne(StudentbanjiBean bean);
	
	public List<StudentbanjiBean> selectList(StudentbanjiBean bean);
	
	public int update(StudentbanjiBean bean);
	
	public int insert(StudentbanjiBean bean);
	
	public int delete(StudentbanjiBean bean);
	
	public Pager<StudentbanjiBean> selectPage(StudentbanjiBean bean,Pager<StudentbanjiBean> page);
	
	public int deleteByStuid(StudentbanjiBean studentbanjiBean);	
}
