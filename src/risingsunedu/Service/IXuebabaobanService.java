package risingsunedu.Service;

import java.util.List;

import risingsunedu.Bean.XuebabaobanBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IXuebabaobanService extends IBaseService{

	public XuebabaobanBean selectOne(XuebabaobanBean bean);
	
	public List<XuebabaobanBean> selectList(XuebabaobanBean bean);
	
	public int update(XuebabaobanBean bean);
	
	public int insert(XuebabaobanBean bean);
	
	public int delete(XuebabaobanBean bean);
	
	public Pager<XuebabaobanBean> selectPage(XuebabaobanBean bean,Pager<XuebabaobanBean> pager);
	
}
