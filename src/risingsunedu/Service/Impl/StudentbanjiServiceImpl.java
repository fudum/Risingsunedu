package risingsunedu.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.StudentbanjiBean;
import risingsunedu.DAO.IStudentbanjiDAO;
import risingsunedu.Service.IStudentbanjiService;

@Service
public class StudentbanjiServiceImpl implements IStudentbanjiService{

	
	@Autowired
	IStudentbanjiDAO dao;
	
	@Override
	public StudentbanjiBean selectOne(StudentbanjiBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<StudentbanjiBean> selectList(StudentbanjiBean bean) {
		// TODO Auto-generated method stub
		return dao.selectList(bean);
	}

	@Override
	public int update(StudentbanjiBean bean) {
		// TODO Auto-generated method stub
		return dao.update(bean);
	}

	@Override
	public int insert(StudentbanjiBean bean) {
		// TODO Auto-generated method stub
		return dao.insert(bean);
	}

	@Override
	public int delete(StudentbanjiBean bean) {
		// TODO Auto-generated method stub
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<StudentbanjiBean> selectPage(StudentbanjiBean bean, Pager<StudentbanjiBean> paramPager) {
		// TODO Auto-generated method stub
		return dao.selectPage(bean, paramPager);
	}

	@Override
	public int deleteByStuid(StudentbanjiBean studentbanjiBean) {
		
		return dao.deleteByStuid(studentbanjiBean);
	}

	

	
}
