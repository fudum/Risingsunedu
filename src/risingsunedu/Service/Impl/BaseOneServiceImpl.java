package risingsunedu.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.BaseOneBean;
import risingsunedu.DAO.IBaseOneDAO;
import risingsunedu.Service.IBaseOneService;

@Service
public class BaseOneServiceImpl implements IBaseOneService{
	@Autowired
	IBaseOneDAO dao;
	
	@Override
	public BaseOneBean selectOne(BaseOneBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<BaseOneBean> selectList(BaseOneBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(BaseOneBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(BaseOneBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(BaseOneBean bean) {
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<BaseOneBean> selectPage(BaseOneBean bean, Pager<BaseOneBean> pager) {
		return dao.selectPage(bean, pager);
	}
	

}
