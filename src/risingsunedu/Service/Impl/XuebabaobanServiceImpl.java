package risingsunedu.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.XuebabaobanBean;
import risingsunedu.DAO.IXuebabaobanDAO;
import risingsunedu.Service.IXuebabaobanService;

@Service
public class XuebabaobanServiceImpl implements IXuebabaobanService{
	@Autowired
	IXuebabaobanDAO dao;
	
	@Override
	public XuebabaobanBean selectOne(XuebabaobanBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<XuebabaobanBean> selectList(XuebabaobanBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(XuebabaobanBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(XuebabaobanBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(XuebabaobanBean bean) {
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<XuebabaobanBean> selectPage(XuebabaobanBean bean, Pager<XuebabaobanBean> pager) {
		return dao.selectPage(bean, pager);
	}
	

}
