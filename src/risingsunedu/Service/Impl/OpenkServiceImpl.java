package risingsunedu.Service.Impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.OpenkBean;
import risingsunedu.DAO.IOpenkDAO;
import risingsunedu.Service.IOpenkService;

@Service
public class OpenkServiceImpl implements IOpenkService{

	
	@Autowired
	IOpenkDAO dao;
	
	@Override
	public OpenkBean selectOne(OpenkBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<OpenkBean> selectList(OpenkBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(OpenkBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(OpenkBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(OpenkBean bean) {
		return dao.delete(bean);
	}

	@Override
	public Pager<OpenkBean> selectPage(OpenkBean bean, Pager<OpenkBean> paramPager) {
		return dao.selectPage(bean, paramPager);
	}

	@Override
	public Pager<OpenkBean> selectOpenkTeacher(OpenkBean bean,
			Pager<OpenkBean> pager) {
		return dao.selectOpenkTeacher(bean, pager);
	}

	@Override
	public int update(Map<String, String> map) {
		return dao.update(map);
	}
}
