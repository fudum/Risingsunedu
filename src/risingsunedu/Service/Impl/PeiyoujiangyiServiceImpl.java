package risingsunedu.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.PeiyoujiangyiBean;
import risingsunedu.DAO.IPeiyoujiangyiDAO;
import risingsunedu.Service.IPeiyoujiangyiService;

@Service
public class PeiyoujiangyiServiceImpl implements IPeiyoujiangyiService{
	@Autowired
	IPeiyoujiangyiDAO dao;
	
	@Override
	public PeiyoujiangyiBean selectOne(PeiyoujiangyiBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<PeiyoujiangyiBean> selectList(PeiyoujiangyiBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(PeiyoujiangyiBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(PeiyoujiangyiBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(PeiyoujiangyiBean bean) {
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<PeiyoujiangyiBean> selectPage(PeiyoujiangyiBean bean, Pager<PeiyoujiangyiBean> pager) {
		return dao.selectPage(bean, pager);
	}
	
}
