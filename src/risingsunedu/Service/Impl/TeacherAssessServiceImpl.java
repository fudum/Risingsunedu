package risingsunedu.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.TeacherAssessBean;
import risingsunedu.DAO.ITeacherAssessDAO;
import risingsunedu.Service.ITeacherAssessService;

@Service
public class TeacherAssessServiceImpl implements ITeacherAssessService{
	
	@Autowired
	ITeacherAssessDAO dao;
	
	@Override
	public TeacherAssessBean selectOne(TeacherAssessBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<TeacherAssessBean> selectList(TeacherAssessBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(TeacherAssessBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(TeacherAssessBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(TeacherAssessBean bean) {
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<TeacherAssessBean> selectPage(TeacherAssessBean bean, Pager<TeacherAssessBean> paramPager) {
		return dao.selectPage(bean, paramPager);
	}

}
