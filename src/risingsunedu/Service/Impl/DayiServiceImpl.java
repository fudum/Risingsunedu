package risingsunedu.Service.Impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.DayiBean;
import risingsunedu.DAO.IDayiDAO;
import risingsunedu.Service.IDayiService;

@Service
public class DayiServiceImpl implements IDayiService{
	@Autowired
	IDayiDAO dao;
	
	@Override
	public DayiBean selectOne(DayiBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<DayiBean> selectList(DayiBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(DayiBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(DayiBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(DayiBean bean) {
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<DayiBean> selectPage(DayiBean bean, Pager<DayiBean> pager) {
		return dao.selectPage(bean, pager);
	}

	@Override
	public int update(Map<String, String> map) {
		return dao.update(map);
	}
	

}
