package risingsunedu.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.XuebaBean;
import risingsunedu.DAO.IXuebaDAO;
import risingsunedu.Service.IXuebaService;

@Service
public class XuebaServiceImpl implements IXuebaService{
	
	@Autowired
	IXuebaDAO dao;
	
	@Override
	public XuebaBean selectOne(XuebaBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<XuebaBean> selectList(XuebaBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(XuebaBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(XuebaBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(XuebaBean bean) {
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<XuebaBean> selectPage(XuebaBean bean, Pager<XuebaBean> pager) {
		return dao.selectPage(bean, pager);
	}
	

}
