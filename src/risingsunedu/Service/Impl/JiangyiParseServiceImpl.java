package risingsunedu.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.JiangyiParseBean;
import risingsunedu.DAO.IJiangyiParseDAO;
import risingsunedu.Service.IJiangyiParseService;

@Service
public class JiangyiParseServiceImpl implements IJiangyiParseService{
	
	@Autowired
	IJiangyiParseDAO dao;
	
	@Override
	public JiangyiParseBean selectOne(JiangyiParseBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<JiangyiParseBean> selectList(JiangyiParseBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(JiangyiParseBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(JiangyiParseBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(JiangyiParseBean bean) {
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<JiangyiParseBean> selectPage(JiangyiParseBean bean, Pager<JiangyiParseBean> paramPager) {
		return dao.selectPage(bean, paramPager);
	}
}
