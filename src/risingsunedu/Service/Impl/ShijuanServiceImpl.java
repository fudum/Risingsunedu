package risingsunedu.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.ShijuanBean;
import risingsunedu.DAO.IShijuanDAO;
import risingsunedu.Service.IShijuanService;

@Service
public class ShijuanServiceImpl implements IShijuanService{
	@Autowired
	IShijuanDAO dao;
	
	@Override
	public ShijuanBean selectOne(ShijuanBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<ShijuanBean> selectList(ShijuanBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(ShijuanBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(ShijuanBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(ShijuanBean bean) {
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<ShijuanBean> selectPage(ShijuanBean bean, Pager<ShijuanBean> pager) {
		return dao.selectPage(bean, pager);
	}
	

}
