package risingsunedu.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.OpenkjiangciBean;
import risingsunedu.DAO.IOpenkjiangciDAO;
import risingsunedu.Service.IOpenkjiangciService;

@Service
public class OpenkjiangciServiceImpl implements IOpenkjiangciService{

	
	@Autowired
	IOpenkjiangciDAO dao;
	
	@Override
	public OpenkjiangciBean selectOne(OpenkjiangciBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<OpenkjiangciBean> selectList(OpenkjiangciBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(OpenkjiangciBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(OpenkjiangciBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(OpenkjiangciBean bean) {
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<OpenkjiangciBean> selectPage(OpenkjiangciBean bean, Pager<OpenkjiangciBean> paramPager) {
		return dao.selectPage(bean, paramPager);
	}

	@Override
	public int updateJorder(OpenkjiangciBean bean) {
		return dao.updateJorder(bean);
	}
}
