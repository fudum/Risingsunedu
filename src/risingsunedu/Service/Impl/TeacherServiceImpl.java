package risingsunedu.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.TeacherBean;
import risingsunedu.DAO.ITeacherDAO;
import risingsunedu.Service.ITeacherService;

@Service
public class TeacherServiceImpl implements ITeacherService{

	
	@Autowired
	ITeacherDAO dao;
	
	@Override
	public TeacherBean selectOne(TeacherBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<TeacherBean> selectList(TeacherBean bean) {
		// TODO Auto-generated method stub
		return dao.selectList(bean);
	}

	@Override
	public int update(TeacherBean bean) {
		// TODO Auto-generated method stub
		return dao.update(bean);
	}

	@Override
	public int insert(TeacherBean bean) {
		// TODO Auto-generated method stub
		return dao.insert(bean);
	}

	@Override
	public int delete(TeacherBean bean) {
		// TODO Auto-generated method stub
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<TeacherBean> selectPage(TeacherBean bean, Pager<TeacherBean> paramPager) {
		// TODO Auto-generated method stub
		return dao.selectPage(bean, paramPager);
	}

	//获取教师最大编号
	@Override
	public String getMaxTeaId() {
		return dao.getMaxTeaId();
	}

	@Override
	public Pager<TeacherBean> selectPageLike(TeacherBean bean,
			Pager<TeacherBean> page) {
		return dao.selectPageLike(bean, page);
	}
	
	
	
	

}
