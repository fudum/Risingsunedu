package risingsunedu.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.ExcellentStudentBean;
import risingsunedu.DAO.IExcellentStudentDAO;
import risingsunedu.Service.IExcellentStudentService;

@Service
public class ExcellentStudentServiceImpl implements IExcellentStudentService{

	
	@Autowired
	IExcellentStudentDAO dao;
	
	@Override
	public ExcellentStudentBean selectOne(ExcellentStudentBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<ExcellentStudentBean> selectList(ExcellentStudentBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(ExcellentStudentBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(ExcellentStudentBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(ExcellentStudentBean bean) {
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<ExcellentStudentBean> selectPage(ExcellentStudentBean bean, Pager<ExcellentStudentBean> paramPager) {
		return dao.selectPage(bean, paramPager);
	}

	@Override
	public Pager<ExcellentStudentBean> selectPageWithStudent(
			ExcellentStudentBean bean, Pager<ExcellentStudentBean> pager) {
		return dao.selectPageWithStudent(bean, pager);
	}
}
