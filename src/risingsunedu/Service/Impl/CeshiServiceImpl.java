package risingsunedu.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.CeshiBean;
import risingsunedu.DAO.ICeshiDAO;
import risingsunedu.Service.ICeshiService;

@Service
public class CeshiServiceImpl implements ICeshiService{
	@Autowired
	ICeshiDAO dao;
	
	@Override
	public CeshiBean selectOne(CeshiBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<CeshiBean> selectList(CeshiBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(CeshiBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(CeshiBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(CeshiBean bean) {
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<CeshiBean> selectPage(CeshiBean bean, Pager<CeshiBean> pager) {
		return dao.selectPage(bean, pager);
	}
	
}
