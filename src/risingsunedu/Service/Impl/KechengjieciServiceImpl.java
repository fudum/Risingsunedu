package risingsunedu.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.KechengjieciBean;
import risingsunedu.DAO.IKechengjieciDAO;
import risingsunedu.Service.IKechengjieciService;

@Service
public class KechengjieciServiceImpl implements IKechengjieciService{

	@Autowired
	IKechengjieciDAO dao;
	
	@Override
	public KechengjieciBean selectOne(KechengjieciBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<KechengjieciBean> selectList(KechengjieciBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(KechengjieciBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(KechengjieciBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(KechengjieciBean bean) {
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<KechengjieciBean> selectPage(KechengjieciBean bean, Pager<KechengjieciBean> paramPager) {
		return dao.selectPage(bean, paramPager);
	}

	@Override
	public List<KechengjieciBean> selectListByKechengId(String kechengId){
		return dao.selectListKechengId(kechengId);
	}
	
	@Override
	public int updateByJiangciId(KechengjieciBean bean){
		return dao.updateByJiangciId(bean);
	}
}
