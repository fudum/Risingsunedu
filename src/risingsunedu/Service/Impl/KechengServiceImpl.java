package risingsunedu.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.KechengBean;
import risingsunedu.DAO.IKechengDAO;
import risingsunedu.Service.IKechengService;

@Service
public class KechengServiceImpl implements IKechengService{

	
	@Autowired
	IKechengDAO dao;
	
	@Override
	public KechengBean selectOne(KechengBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<KechengBean> selectList(KechengBean bean) {
		// TODO Auto-generated method stub
		return dao.selectList(bean);
	}

	@Override
	public int update(KechengBean bean) {
		// TODO Auto-generated method stub
		return dao.update(bean);
	}

	@Override
	public int insert(KechengBean bean) {
		// TODO Auto-generated method stub
		return dao.insert(bean);
	}

	@Override
	public int delete(KechengBean bean) {
		// TODO Auto-generated method stub
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<KechengBean> selectPage(KechengBean bean, Pager<KechengBean> paramPager) {
		// TODO Auto-generated method stub
		return dao.selectPage(bean, paramPager);
	}

	@Override
	public Pager<KechengBean> selectPagerWithTeacher(KechengBean bean,
			Pager<KechengBean> pager) {
		// TODO Auto-generated method stub
		return dao.selectPagerWithTeacher(bean, pager);
	}
	
}
