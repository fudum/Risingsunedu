package risingsunedu.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.ZhangjieBean;
import risingsunedu.DAO.IZhangjieDAO;
import risingsunedu.Service.IZhangjieService;

@Service
public class ZhangjieServiceImpl implements IZhangjieService{
	@Autowired
	IZhangjieDAO dao;
	
	@Override
	public ZhangjieBean selectOne(ZhangjieBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<ZhangjieBean> selectList(ZhangjieBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(ZhangjieBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(ZhangjieBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(ZhangjieBean bean) {
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<ZhangjieBean> selectPage(ZhangjieBean bean, Pager<ZhangjieBean> pager) {
		return dao.selectPage(bean, pager);
	}
	

}
