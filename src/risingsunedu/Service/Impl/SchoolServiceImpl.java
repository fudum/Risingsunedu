package risingsunedu.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.SchoolBean;
import risingsunedu.DAO.ISchoolDAO;
import risingsunedu.Service.ISchoolService;

@Service
public class SchoolServiceImpl implements ISchoolService{

	
	@Autowired
	ISchoolDAO dao;
	
	@Override
	public SchoolBean selectOne(SchoolBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<SchoolBean> selectList(SchoolBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(SchoolBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(SchoolBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(SchoolBean bean) {
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<SchoolBean> selectPage(SchoolBean bean, Pager<SchoolBean> paramPager) {
		return dao.selectPage(bean, paramPager);
	}

}
