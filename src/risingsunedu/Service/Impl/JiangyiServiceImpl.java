package risingsunedu.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.JiangyiBean;
import risingsunedu.DAO.IJiangyiDAO;
import risingsunedu.Service.IJiangyiService;

@Service
public class JiangyiServiceImpl implements IJiangyiService{

	
	@Autowired
	IJiangyiDAO dao;
	
	@Override
	public JiangyiBean selectOne(JiangyiBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<JiangyiBean> selectList(JiangyiBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(JiangyiBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(JiangyiBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(JiangyiBean bean) {
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<JiangyiBean> selectPage(JiangyiBean bean, Pager<JiangyiBean> paramPager) {
		return dao.selectPage(bean, paramPager);
	}

	//搜寻最新的讲义
	@Override
	public List<JiangyiBean> selectNewest(JiangyiBean bean) {
		return dao.selectNewest(bean);
	}

	//搜寻数量
	public int selectCountByTeaId(String teaId) {
		return dao.selectCountByTeaId(teaId);
	}

	@Override
	public List<JiangyiBean> selectByStuId(String stu_id) {
		return dao.selectByStuId(stu_id);
	}

	@Override
	public List<JiangyiBean> selectByTeaId(String tea_id) {
		return dao.selectByTeaId(tea_id);
	}

}
