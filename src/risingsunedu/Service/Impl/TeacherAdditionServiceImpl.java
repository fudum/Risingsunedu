package risingsunedu.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.TeacherAdditionBean;
import risingsunedu.DAO.ITeacherAdditionDAO;
import risingsunedu.Service.ITeacherAdditionService;

@Service
public class TeacherAdditionServiceImpl implements ITeacherAdditionService{

	
	@Autowired
	ITeacherAdditionDAO dao;
	
	@Override
	public TeacherAdditionBean selectOne(TeacherAdditionBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<TeacherAdditionBean> selectList(TeacherAdditionBean bean) {
		// TODO Auto-generated method stub
		return dao.selectList(bean);
	}

	@Override
	public int update(TeacherAdditionBean bean) {
		// TODO Auto-generated method stub
		return dao.update(bean);
	}

	@Override
	public int insert(TeacherAdditionBean bean) {
		// TODO Auto-generated method stub
		return dao.insert(bean);
	}

	@Override
	public int delete(TeacherAdditionBean bean) {
		// TODO Auto-generated method stub
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<TeacherAdditionBean> selectPage(TeacherAdditionBean bean, Pager<TeacherAdditionBean> paramPager) {
		// TODO Auto-generated method stub
		return dao.selectPage(bean, paramPager);
	}


}
