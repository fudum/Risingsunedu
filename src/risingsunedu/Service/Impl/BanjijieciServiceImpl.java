package risingsunedu.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.BanjijieciBean;
import risingsunedu.DAO.IBanjijieciDAO;
import risingsunedu.Service.IBanjijieciService;

@Service
public class BanjijieciServiceImpl implements IBanjijieciService{

	@Autowired
	IBanjijieciDAO dao;
	
	@Override
	public BanjijieciBean selectOne(BanjijieciBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<BanjijieciBean> selectList(BanjijieciBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(BanjijieciBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(BanjijieciBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(BanjijieciBean bean) {
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<BanjijieciBean> selectPage(BanjijieciBean bean, Pager<BanjijieciBean> paramPager) {
		return dao.selectPage(bean, paramPager);
	}

	@Override
	public List<BanjijieciBean> selectListByBanjiId(String banjiid){
		return dao.selectListByBanjiId(banjiid);
	}
	
	@Override
	public int updateByJiangciId(BanjijieciBean bean){
		return dao.updateByJiangciId(bean);
	}
}
