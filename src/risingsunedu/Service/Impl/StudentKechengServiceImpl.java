package risingsunedu.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.StudentKechengBean;
import risingsunedu.DAO.IStudentKechengDAO;
import risingsunedu.Service.IStudentKechengService;

@Service
public class StudentKechengServiceImpl implements IStudentKechengService{
	@Autowired
	IStudentKechengDAO dao;
	
	@Override
	public StudentKechengBean selectOne(StudentKechengBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<StudentKechengBean> selectList(StudentKechengBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(StudentKechengBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(StudentKechengBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(StudentKechengBean bean) {
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<StudentKechengBean> selectPage(StudentKechengBean bean, Pager<StudentKechengBean> pager) {
		return dao.selectPage(bean, pager);
	}
	

}
