package risingsunedu.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.XinwenBean;
import risingsunedu.DAO.IXinwenDAO;
import risingsunedu.Service.IXinwenService;

@Service
public class XinwenServiceImpl implements IXinwenService{

	
	@Autowired
	IXinwenDAO dao;
	
	@Override
	public XinwenBean selectOne(XinwenBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<XinwenBean> selectList(XinwenBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(XinwenBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(XinwenBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(XinwenBean bean) {
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<XinwenBean> selectPage(XinwenBean bean, Pager<XinwenBean> paramPager) {
		return dao.selectPage(bean, paramPager);
	}
}
