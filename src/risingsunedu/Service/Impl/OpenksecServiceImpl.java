package risingsunedu.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.OpenksecBean;
import risingsunedu.DAO.IOpenksecDAO;
import risingsunedu.Service.IOpenksecService;

@Service
public class OpenksecServiceImpl implements IOpenksecService{

	
	@Autowired
	IOpenksecDAO dao;
	
	@Override
	public OpenksecBean selectOne(OpenksecBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<OpenksecBean> selectList(OpenksecBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(OpenksecBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(OpenksecBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(OpenksecBean bean) {
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<OpenksecBean> selectPage(OpenksecBean bean, Pager<OpenksecBean> paramPager) {
		return dao.selectPage(bean, paramPager);
	}

	@Override
	public List<OpenksecBean> selectListByOpenkId(String openkId) {
		return dao.selectListByOpenkId(openkId);
	}
}
