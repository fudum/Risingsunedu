package risingsunedu.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.BookBean;
import risingsunedu.DAO.IBookDAO;
import risingsunedu.Service.IBookService;

@Service
public class BookServiceImpl implements IBookService{
	@Autowired
	IBookDAO dao;
	
	@Override
	public BookBean selectOne(BookBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<BookBean> selectList(BookBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(BookBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(BookBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(BookBean bean) {
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<BookBean> selectPage(BookBean bean, Pager<BookBean> pager) {
		return dao.selectPage(bean, pager);
	}
	

}
