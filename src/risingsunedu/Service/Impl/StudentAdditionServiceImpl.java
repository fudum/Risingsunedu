package risingsunedu.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.StudentAdditionBean;
import risingsunedu.DAO.IStudentAdditionDAO;
import risingsunedu.Service.IStudentAdditionService;

@Service
public class StudentAdditionServiceImpl implements IStudentAdditionService{

	
	@Autowired
	IStudentAdditionDAO dao;
	
	@Override
	public StudentAdditionBean selectOne(StudentAdditionBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<StudentAdditionBean> selectList(StudentAdditionBean bean) {
		// TODO Auto-generated method stub
		return dao.selectList(bean);
	}

	@Override
	public int update(StudentAdditionBean bean) {
		// TODO Auto-generated method stub
		return dao.update(bean);
	}

	@Override
	public int insert(StudentAdditionBean bean) {
		// TODO Auto-generated method stub
		return dao.insert(bean);
	}

	@Override
	public int delete(StudentAdditionBean bean) {
		// TODO Auto-generated method stub
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<StudentAdditionBean> selectPage(StudentAdditionBean bean, Pager<StudentAdditionBean> paramPager) {
		// TODO Auto-generated method stub
		return dao.selectPage(bean, paramPager);
	}
}
