package risingsunedu.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.StudentBean;
import risingsunedu.DAO.IStudentDAO;
import risingsunedu.Service.IStudentService;

@Service
public class StudentServiceImpl implements IStudentService{

	
	@Autowired
	IStudentDAO dao;
	
	@Override
	public StudentBean selectOne(StudentBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<StudentBean> selectList(StudentBean bean) {
		// TODO Auto-generated method stub
		return dao.selectList(bean);
	}

	@Override
	public int update(StudentBean bean) {
		// TODO Auto-generated method stub
		return dao.update(bean);
	}

	@Override
	public int insert(StudentBean bean) {
		// TODO Auto-generated method stub
		return dao.insert(bean);
	}

	@Override
	public int delete(StudentBean bean) {
		// TODO Auto-generated method stub
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<StudentBean> selectPage(StudentBean bean, Pager<StudentBean> paramPager) {
		// TODO Auto-generated method stub
		return dao.selectPage(bean, paramPager);
	}

	//获取学生最大id
	@Override
	public String getMaxStuId() {
		return dao.getMaxStuId();
	}
	
}
