package risingsunedu.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.KechengjiangciBean;
import risingsunedu.DAO.IKechengjiangciDAO;
import risingsunedu.Service.IKechengjiangciService;

@Service
public class KechengjiangciServiceImpl implements IKechengjiangciService{

	@Autowired
	IKechengjiangciDAO dao;
	
	@Override
	public KechengjiangciBean selectOne(KechengjiangciBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<KechengjiangciBean> selectList(KechengjiangciBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(KechengjiangciBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(KechengjiangciBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(KechengjiangciBean bean) {
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<KechengjiangciBean> selectPage(KechengjiangciBean bean, Pager<KechengjiangciBean> paramPager) {
		return dao.selectPage(bean, paramPager);
	}
}
