package risingsunedu.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.UserBean;
import risingsunedu.DAO.IUserDAO;
import risingsunedu.Service.IUserService;

@Service
public class UserServiceImpl implements IUserService{

	
	@Autowired
	IUserDAO userDAO;
	
	@Override
	public UserBean selectOne(UserBean bean) {
		return userDAO.selectOne(bean);
	}

	@Override
	public List<UserBean> selectList(UserBean bean) {
		// TODO Auto-generated method stub
		return userDAO.selectList(bean);
	}

	@Override
	public int update(UserBean bean) {
		// TODO Auto-generated method stub
		return userDAO.update(bean);
	}

	@Override
	public int insert(UserBean bean) {
		// TODO Auto-generated method stub
		return userDAO.insert(bean);
	}

	@Override
	public int delete(UserBean bean) {
		// TODO Auto-generated method stub
		return userDAO.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<UserBean> selectPage(UserBean bean, Pager<UserBean> paramPager) {
		// TODO Auto-generated method stub
		return userDAO.selectPage(bean, paramPager);
	}
	
	

}
