package risingsunedu.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.BaseTwoBean;
import risingsunedu.DAO.IBaseTwoDAO;
import risingsunedu.Service.IBaseTwoService;

@Service
public class BaseTwoServiceImpl implements IBaseTwoService{
	@Autowired
	IBaseTwoDAO dao;
	
	@Override
	public BaseTwoBean selectOne(BaseTwoBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<BaseTwoBean> selectList(BaseTwoBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(BaseTwoBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(BaseTwoBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(BaseTwoBean bean) {
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<BaseTwoBean> selectPage(BaseTwoBean bean, Pager<BaseTwoBean> pager) {
		return dao.selectPage(bean, pager);
	}
	

}
