package risingsunedu.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.MedTestCatgBean;
import risingsunedu.DAO.IMedTestCatgDAO;
import risingsunedu.Service.IMedTestCatgService;

@Service
public class MedTestCatgServiceImpl implements IMedTestCatgService{

	
	@Autowired
	IMedTestCatgDAO dao;
	
	@Override
	public MedTestCatgBean selectOne(MedTestCatgBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<MedTestCatgBean> selectList(MedTestCatgBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(MedTestCatgBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(MedTestCatgBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(MedTestCatgBean bean) {
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<MedTestCatgBean> selectPage(MedTestCatgBean bean, Pager<MedTestCatgBean> paramPager) {
		return dao.selectPage(bean, paramPager);
	}
}
