package risingsunedu.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.BanjijiangciBean;
import risingsunedu.DAO.IBanjijiangciDAO;
import risingsunedu.Service.IBanjijiangciService;

@Service
public class BanjijiangciServiceImpl implements IBanjijiangciService{

	@Autowired
	IBanjijiangciDAO dao;
	
	@Override
	public BanjijiangciBean selectOne(BanjijiangciBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<BanjijiangciBean> selectList(BanjijiangciBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(BanjijiangciBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(BanjijiangciBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(BanjijiangciBean bean) {
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<BanjijiangciBean> selectPage(BanjijiangciBean bean, Pager<BanjijiangciBean> paramPager) {
		return dao.selectPage(bean, paramPager);
	}
}
