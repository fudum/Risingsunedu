package risingsunedu.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.SchoolLevelBean;
import risingsunedu.DAO.ISchoolLevelDAO;
import risingsunedu.Service.ISchoolLevelService;

@Service
public class SchoolLevelServiceImpl implements ISchoolLevelService{

	
	@Autowired
	ISchoolLevelDAO dao;
	
	@Override
	public SchoolLevelBean selectOne(SchoolLevelBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<SchoolLevelBean> selectList(SchoolLevelBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(SchoolLevelBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(SchoolLevelBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(SchoolLevelBean bean) {
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<SchoolLevelBean> selectPage(SchoolLevelBean bean, Pager<SchoolLevelBean> paramPager) {
		return dao.selectPage(bean, paramPager);
	}

}
