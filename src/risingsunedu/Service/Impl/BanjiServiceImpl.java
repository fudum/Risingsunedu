package risingsunedu.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.BanjiBean;
import risingsunedu.DAO.IBanjiDAO;
import risingsunedu.Service.IBanjiService;

@Service
public class BanjiServiceImpl implements IBanjiService{

	
	@Autowired
	IBanjiDAO dao;
	
	@Override
	public BanjiBean selectOne(BanjiBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<BanjiBean> selectList(BanjiBean bean) {
		// TODO Auto-generated method stub
		return dao.selectList(bean);
	}

	@Override
	public int update(BanjiBean bean) {
		// TODO Auto-generated method stub
		return dao.update(bean);
	}

	@Override
	public int insert(BanjiBean bean) {
		// TODO Auto-generated method stub
		return dao.insert(bean);
	}

	@Override
	public int delete(BanjiBean bean) {
		// TODO Auto-generated method stub
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<BanjiBean> selectPage(BanjiBean bean, Pager<BanjiBean> paramPager) {
		// TODO Auto-generated method stub
		return dao.selectPage(bean, paramPager);
	}
	
	
	//搜寻最大id
	@Override
	public String selectMaxId(BanjiBean bean) {
		return dao.selectMaxId(bean);
	}

	@Override
	public Pager<BanjiBean> selectPagerWithTeacher(BanjiBean bean,
			Pager<BanjiBean> pager) {
		return dao.selectPagerWithTeacher(bean,pager);
	}
	
	

}
