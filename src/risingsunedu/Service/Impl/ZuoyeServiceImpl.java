package risingsunedu.Service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import framework.base.common.Pager;
import risingsunedu.Bean.ZuoyeBean;
import risingsunedu.DAO.IZuoyeDAO;
import risingsunedu.Service.IZuoyeService;

@Service
public class ZuoyeServiceImpl implements IZuoyeService{
	@Autowired
	IZuoyeDAO dao;
	
	@Override
	public ZuoyeBean selectOne(ZuoyeBean bean) {
		return dao.selectOne(bean);
	}

	@Override
	public List<ZuoyeBean> selectList(ZuoyeBean bean) {
		return dao.selectList(bean);
	}

	@Override
	public int update(ZuoyeBean bean) {
		return dao.update(bean);
	}

	@Override
	public int insert(ZuoyeBean bean) {
		return dao.insert(bean);
	}

	@Override
	public int delete(ZuoyeBean bean) {
		return dao.delete(bean);
	}

	//分页查询功能
	@Override
	public Pager<ZuoyeBean> selectPage(ZuoyeBean bean, Pager<ZuoyeBean> pager) {
		return dao.selectPage(bean, pager);
	}
	
}
