package risingsunedu.Service;

import java.util.List;

import risingsunedu.Bean.StudentBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IStudentService extends IBaseService{

	public StudentBean selectOne(StudentBean bean);
	
	public List<StudentBean> selectList(StudentBean bean);
	
	public int update(StudentBean bean);
	
	public int insert(StudentBean bean);
	
	public int delete(StudentBean bean);
	
	public Pager<StudentBean> selectPage(StudentBean bean,Pager<StudentBean> page);
	
	public String getMaxStuId();
	
	
}
