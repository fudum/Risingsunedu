package risingsunedu.Service;

import java.util.List;

import risingsunedu.Bean.PeiyoujiangyiBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IPeiyoujiangyiService extends IBaseService{

	public PeiyoujiangyiBean selectOne(PeiyoujiangyiBean bean);
	
	public List<PeiyoujiangyiBean> selectList(PeiyoujiangyiBean bean);
	
	public int update(PeiyoujiangyiBean bean);
	
	public int insert(PeiyoujiangyiBean bean);
	
	public int delete(PeiyoujiangyiBean bean);
	
	public Pager<PeiyoujiangyiBean> selectPage(PeiyoujiangyiBean bean,Pager<PeiyoujiangyiBean> pager);
	
}
