package risingsunedu.Service;

import java.util.List;

import risingsunedu.Bean.UserBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IUserService extends IBaseService{

	public UserBean selectOne(UserBean bean);
	
	public List<UserBean> selectList(UserBean bean);
	
	public int update(UserBean bean);
	
	public int insert(UserBean bean);
	
	public int delete(UserBean bean);
	
	public Pager<UserBean> selectPage(UserBean bean,Pager<UserBean> page);
	
}
