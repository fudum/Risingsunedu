package risingsunedu.Service;

import java.util.List;

import risingsunedu.Bean.TeacherAssessBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface ITeacherAssessService extends IBaseService{

	public TeacherAssessBean selectOne(TeacherAssessBean bean);
	
	public List<TeacherAssessBean> selectList(TeacherAssessBean bean);
	
	public int update(TeacherAssessBean bean);
	
	public int insert(TeacherAssessBean bean);
	
	public int delete(TeacherAssessBean bean);
	
	public Pager<TeacherAssessBean> selectPage(TeacherAssessBean bean,Pager<TeacherAssessBean> page);
	
}
