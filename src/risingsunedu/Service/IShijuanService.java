package risingsunedu.Service;

import java.util.List;

import risingsunedu.Bean.ShijuanBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IShijuanService extends IBaseService{

	public ShijuanBean selectOne(ShijuanBean bean);
	
	public List<ShijuanBean> selectList(ShijuanBean bean);
	
	public int update(ShijuanBean bean);
	
	public int insert(ShijuanBean bean);
	
	public int delete(ShijuanBean bean);
	
	public Pager<ShijuanBean> selectPage(ShijuanBean bean,Pager<ShijuanBean> pager);
	
}
