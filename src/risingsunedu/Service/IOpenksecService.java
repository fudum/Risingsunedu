package risingsunedu.Service;

import java.util.List;

import risingsunedu.Bean.OpenksecBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IOpenksecService extends IBaseService{

	public OpenksecBean selectOne(OpenksecBean bean);
	
	public List<OpenksecBean> selectList(OpenksecBean bean);
	
	public int update(OpenksecBean bean);
	
	public int insert(OpenksecBean bean);
	
	public int delete(OpenksecBean bean);
	
	public Pager<OpenksecBean> selectPage(OpenksecBean bean,Pager<OpenksecBean> page);
	
	/**
	 * 通过openk表的id来查询视频信息
	 * @param openkId
	 * @return
	 */
	public List<OpenksecBean> selectListByOpenkId(String openkId);
	
}
