package risingsunedu.Service;

import java.util.List;
import java.util.Map;

import risingsunedu.Bean.DayiBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IDayiService extends IBaseService{

	public DayiBean selectOne(DayiBean bean);
	
	public List<DayiBean> selectList(DayiBean bean);
	
	public int update(DayiBean bean);
	
	public int insert(DayiBean bean);
	
	public int delete(DayiBean bean);
	
	public Pager<DayiBean> selectPage(DayiBean bean,Pager<DayiBean> pager);
	
	/**
	 * 通过章节更新答疑
	 * @param map
	 * @return
	 */
	public int update(Map<String,String> map);
	
}
