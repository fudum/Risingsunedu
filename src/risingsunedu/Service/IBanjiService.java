package risingsunedu.Service;

import java.util.List;

import risingsunedu.Bean.BanjiBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IBanjiService extends IBaseService{

	public BanjiBean selectOne(BanjiBean bean);
	
	public List<BanjiBean> selectList(BanjiBean bean);
	
	public int update(BanjiBean bean);
	
	public int insert(BanjiBean bean);
	
	public int delete(BanjiBean bean);
	
	public Pager<BanjiBean> selectPage(BanjiBean bean,Pager<BanjiBean> pager);
	
	public Pager<BanjiBean> selectPagerWithTeacher(BanjiBean bean,Pager<BanjiBean> pager);
	
	//获取最大编号
	public String selectMaxId(BanjiBean bean);
	
}
