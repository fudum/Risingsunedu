package risingsunedu.Service;

import java.util.List;

import risingsunedu.Bean.StudentKechengBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IStudentKechengService extends IBaseService{

	public StudentKechengBean selectOne(StudentKechengBean bean);
	
	public List<StudentKechengBean> selectList(StudentKechengBean bean);
	
	public int update(StudentKechengBean bean);
	
	public int insert(StudentKechengBean bean);
	
	public int delete(StudentKechengBean bean);
	
	public Pager<StudentKechengBean> selectPage(StudentKechengBean bean,Pager<StudentKechengBean> pager);
	
}
