package risingsunedu.Service;

import java.util.List;

import risingsunedu.Bean.AdvertisementBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IAdvertisementService extends IBaseService{

	public AdvertisementBean selectOne(AdvertisementBean bean);
	
	public List<AdvertisementBean> selectList(AdvertisementBean bean);
	
	public int update(AdvertisementBean bean);
	
	public int insert(AdvertisementBean bean);
	
	public int delete(AdvertisementBean bean);
	
	public Pager<AdvertisementBean> selectPage(AdvertisementBean bean,Pager<AdvertisementBean> pager);
	
}
