package risingsunedu.Service;

import java.util.List;

import risingsunedu.Bean.SchoolLevelBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface ISchoolLevelService extends IBaseService{

	public SchoolLevelBean selectOne(SchoolLevelBean bean);
	
	public List<SchoolLevelBean> selectList(SchoolLevelBean bean);
	
	public int update(SchoolLevelBean bean);
	
	public int insert(SchoolLevelBean bean);
	
	public int delete(SchoolLevelBean bean);
	
	public Pager<SchoolLevelBean> selectPage(SchoolLevelBean bean,Pager<SchoolLevelBean> page);
	
}
