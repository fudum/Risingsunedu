package risingsunedu.Service;

import java.util.List;

import risingsunedu.Bean.KechengjiangciBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IKechengjiangciService extends IBaseService{

	public KechengjiangciBean selectOne(KechengjiangciBean bean);
	
	public List<KechengjiangciBean> selectList(KechengjiangciBean bean);
	
	public int update(KechengjiangciBean bean);
	
	public int insert(KechengjiangciBean bean);
	
	public int delete(KechengjiangciBean bean);
	
	public Pager<KechengjiangciBean> selectPage(KechengjiangciBean bean,Pager<KechengjiangciBean> pager);
		
}
