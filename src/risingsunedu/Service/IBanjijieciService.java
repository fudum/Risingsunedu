package risingsunedu.Service;

import java.util.List;

import risingsunedu.Bean.BanjijieciBean;
import framework.base.common.Pager;
import framework.base.service.IBaseService;

public interface IBanjijieciService extends IBaseService{

	public BanjijieciBean selectOne(BanjijieciBean bean);
	
	public List<BanjijieciBean> selectList(BanjijieciBean bean);
	
	public int update(BanjijieciBean bean);
	
	public int insert(BanjijieciBean bean);
	
	public int delete(BanjijieciBean bean);
	
	public Pager<BanjijieciBean> selectPage(BanjijieciBean bean,Pager<BanjijieciBean> page);
	
	/**
	 * 通过banji表的id来查询视频信息
	 * @param banjiid
	 * @return
	 */
	public List<BanjijieciBean> selectListByBanjiId(String banjiid);
	
	public int updateByJiangciId(BanjijieciBean bean);
	
}
