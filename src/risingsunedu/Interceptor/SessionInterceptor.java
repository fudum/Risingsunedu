package risingsunedu.Interceptor;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.UserBean;
import risingsunedu.Common.Impl.CommonConstant;

public class SessionInterceptor implements HandlerInterceptor {

	@Override
	public void afterCompletion(HttpServletRequest arg0,
			HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1,
			Object arg2, ModelAndView arg3) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
			Object o) throws Exception {
		UserBean user = (UserBean) request.getSession().getAttribute("usersession");
		if(user == null){
			response.sendRedirect(CommonConstant.preStr + "/login/index" + CommonConstant.sufStr);			
			return false;
		}
		else {
			return true;
		}
		
	}

}
