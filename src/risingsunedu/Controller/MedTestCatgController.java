package risingsunedu.Controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.MedTestCatgBean;
import risingsunedu.Common.Impl.CommonFunImpl;
import risingsunedu.Service.IMedTestCatgService;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/medtestcatg")
public class MedTestCatgController extends BaseController {

	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	IMedTestCatgService medTestCatgService;
	
	@Override
	public void setLogger() {
		logger = LoggerFactory.getLogger(getClass());
	}
	
	@RequestMapping("/manage")
	public ModelAndView manage() {
		ModelAndView modelAndView = new ModelAndView("background/zhongkao/categorymanage");
		
		//中考信息类别
		List<MedTestCatgBean> list = medTestCatgService.selectList(null);
		modelAndView.addObject("list", list);
		
		return modelAndView;
	}
	
	@RequestMapping("/add")
	public ModelAndView add() {
		ModelAndView modelAndView = new ModelAndView("background/zhongkao/categoryadd");
		return modelAndView;
	}
	
	@RequestMapping("/insert")
	public ModelAndView insert(MedTestCatgBean medTestCatgBean) {
		ModelAndView modelAndView = new ModelAndView("background/zhongkao/categoryedit");
		medTestCatgBean.setId(CommonFunImpl.uuid("MT"));
		medTestCatgBean.setInserttime(CommonFunImpl.currentTime(1));
		String retVal;
		if (medTestCatgService.insert(medTestCatgBean) > 0) {
			retVal = "添加成功！";
		}
		else {
			retVal = "添加失败！";
		}
		modelAndView.addObject("retVal", retVal);
		modelAndView.addObject("bean", medTestCatgBean);
		return modelAndView;
	}
	
	@RequestMapping("/update")
	public ModelAndView update(MedTestCatgBean medTestCatgBean) {
		ModelAndView modelAndView = new ModelAndView("background/zhongkao/categoryedit");
		String retVal;
		if (medTestCatgService.insert(medTestCatgBean) > 0) {
			retVal = "编辑成功！";
		}
		else {
			retVal = "编辑失败！";
		}
		modelAndView.addObject("retVal", retVal);
		modelAndView.addObject("bean", medTestCatgBean);
		return modelAndView;
	}
	
	//删除
	@RequestMapping("/delete")
	public ModelAndView delete(MedTestCatgBean medTestCatgBean,HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		List<String> list = new ArrayList<String>();
		int ret = 1;
		ret = medTestCatgService.delete(medTestCatgBean);
		if (ret > 0) {
			list.add("1");
			list.add("删除成功!");
		} else {
			list.add("0");
			list.add("删除失败!");
		}
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			String s = JSONArray.fromObject(list).toString();
			out.print(s);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	//编辑页面
	@RequestMapping("/edit")
	public ModelAndView edit(MedTestCatgBean medTestCatgBean) {
		ModelAndView modelAndView = new ModelAndView("background/zhongkao/categoryedit");
		medTestCatgBean = medTestCatgService.selectOne(medTestCatgBean);
		modelAndView.addObject("bean", medTestCatgBean);
		return modelAndView;
	}

}
