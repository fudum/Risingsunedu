package risingsunedu.Controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.BanjiBean;
import risingsunedu.Bean.JiangyiBean;
import risingsunedu.Bean.JiangyiParseBean;
import risingsunedu.Bean.TeacherBean;
import risingsunedu.Common.Impl.CommonFunImpl;
import risingsunedu.Service.IBanjiService;
import risingsunedu.Service.IJiangyiParseService;
import risingsunedu.Service.IJiangyiService;
import risingsunedu.Service.ITeacherService;
import framework.base.common.Pager;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/jyparse")
public class JiangyiParseController extends BaseController {

	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IJiangyiParseService jiangyiParseService;

	@Autowired
	IJiangyiService jiangyiService;

	@Autowired
	ITeacherService teacherService;
	
	@Autowired
	IBanjiService banjiService;
	
	@Override
	public void setLogger() {
		logger = LoggerFactory.getLogger(getClass());
	}

	// 讲义管理页面 //1
	@RequestMapping("/jyparsemng")
	public ModelAndView userManage() {
		ModelAndView modelAndView = new ModelAndView();

		Pager<JiangyiBean> pager = new Pager<JiangyiBean>();
		Pager<JiangyiBean> jyPage = jiangyiService.selectPage(null, pager);

		//讲义信息
		modelAndView.addObject("jyPage", jyPage);
		modelAndView.setViewName("background/jiangyi/jyparsemng");
		return modelAndView;
	}

	// 添加讲义页面 //2
	@RequestMapping("/jyparseadd")
	public ModelAndView jiangyiAdd(JiangyiParseBean jyparseBean) {
		ModelAndView modelAndView = new ModelAndView("background/jiangyi/jyparseadd");
		
		//把bean添加到页面		
		modelAndView.addObject("bean", jyparseBean);
		
		return modelAndView;
	}

	// 班级插入 //3
	@RequestMapping("/jyparseinsert")
	public ModelAndView jyparseInsert(JiangyiParseBean bean,MultipartFile videoFile,HttpServletRequest request) {
		
		ModelAndView modelAndView = new ModelAndView("background/jiangyi/jyparseadd");
		
		bean.setId(CommonFunImpl.uuid("JP"));//主键
		bean.setInserttime(CommonFunImpl.currentTime(1));//插入时间		
		
		//讲义不为空时候上传讲义
		if (!videoFile.isEmpty()) {
			
			String path = request.getSession().getServletContext().getRealPath("upload\\jyparse"); 
			path += "\\";
			
			logger.debug("-------" + path);
			
			String tmp = videoFile.getOriginalFilename();
			tmp = tmp.substring(tmp.lastIndexOf('.'));
			String fileName = bean.getId() + tmp;
			
			File targetFile = new File(path, fileName);
			if(!targetFile.exists()){  
	            targetFile.mkdirs();  
	        }  
			
			//保存  
	        try {  
	        	videoFile.transferTo(targetFile);  
	        } catch (Exception e) {  
	            e.printStackTrace();  
	        }  
	        
	        //设置bean中的内容
	        bean.setVideopath(fileName);
	        logger.debug("-------" +fileName);
		}
		
		
		String tipString;
		//如果插入成功
		if (jiangyiParseService.insert(bean) > 0) {
			tipString = "ok";
		}
		else {
			tipString = "error";
		}
		modelAndView.addObject("bean", bean);
		modelAndView.addObject("tip", tipString);
		return modelAndView;
	}

	// 删除班级 4
	@RequestMapping("/delete")
	public ModelAndView Delete(JiangyiParseBean bean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		List<String> list = new ArrayList<String>();
		int ret = 1;
		ret = jiangyiParseService.delete(bean);
		if (ret > 0) {
			list.add("1");
			list.add("删除成功!");
		} else {
			list.add("0");
			list.add("删除失败!");
		}
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			String s = JSONArray.fromObject(list).toString();
			out.print(s);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	// 上一页 5
	@RequestMapping("/frontpage")
	public ModelAndView frontPage(int pageNum, int totalPage) {
		ModelAndView modelAndView = new ModelAndView();

		Pager<JiangyiBean> pager = new Pager<JiangyiBean>();

		if (pageNum > 1) {// 如果小于总页数
			pageNum--;
		} else {// 返回首页
			pageNum = totalPage;
		}
		pager.setCurtPage(pageNum);

		Pager<JiangyiBean> pageT = jiangyiService.selectPage(null, pager);

		// 数据list
		modelAndView.addObject("list", pageT.getPageList());
		// 总页数
		modelAndView.addObject("totalPage", pageT.getTotalPage());
		// 当前页
		modelAndView.addObject("countPerPage", pageT.getCurtPage());

		modelAndView.setViewName("background/banji/banjimanage");
		return modelAndView;
	}

	// 下一页 6
	@RequestMapping("/nextpage")
	public ModelAndView nextPage(int pageNum, int totalPage) {
		ModelAndView modelAndView = new ModelAndView();

		Pager<JiangyiBean> pager = new Pager<JiangyiBean>();

		if (pageNum < totalPage) {// 如果小于总页数
			pageNum++;
		} else {// 返回首页
			pageNum = 1;
		}
		pager.setCurtPage(pageNum);

		Pager<JiangyiBean> userPage = jiangyiService.selectPage(null, pager);

		// 数据list
		modelAndView.addObject("list", userPage.getPageList());
		// 总页数
		modelAndView.addObject("totalPage", userPage.getTotalPage());
		// 当前页
		modelAndView.addObject("countPerPage", userPage.getCurtPage());

		modelAndView.setViewName("background/banji/banjimanage");
		return modelAndView;
	}

	// 最后一页 7
	@RequestMapping("/lastpage")
	public ModelAndView lastPage(int totalPage) {
		ModelAndView modelAndView = new ModelAndView();

		Pager<JiangyiBean> pager = new Pager<JiangyiBean>();
		pager.setCurtPage(totalPage);

		Pager<JiangyiBean> userPage = jiangyiService.selectPage(null, pager);

		// 数据list
		modelAndView.addObject("list", userPage.getPageList());
		// 总页数
		modelAndView.addObject("totalPage", userPage.getTotalPage());
		// 当前页
		modelAndView.addObject("countPerPage", userPage.getCurtPage());

		modelAndView.setViewName("background/banji/banjimanage");
		return modelAndView;
	}
	
	
	
	//讲义附加信息编辑页面 
	@RequestMapping("/jyparseedit")
	public ModelAndView userEdit(JiangyiBean bean) {
		ModelAndView modelAndView = new ModelAndView("background/jiangyi/jyparseedit");
		
		//讲义解析信息
		JiangyiParseBean jyParseBean = new JiangyiParseBean();
		jyParseBean.setJy_id(bean.getId());
		List<JiangyiParseBean> jyParseList = jiangyiParseService.selectList(jyParseBean);
		modelAndView.addObject("jyParseList", jyParseList);
		
		//讲义信息添加到页面上
		bean = jiangyiService.selectOne(bean);
		modelAndView.addObject("jyBean", bean);
		
		return modelAndView;
	}
	
	// 班级插入 //3
	@RequestMapping("/jiangyiupdate")
	public ModelAndView jiangyiUpdate(JiangyiBean bean, MultipartFile jiangyiFile,HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("background/jiangyi/jiangyiedit");
		
		//设置bean属性
		bean.setUpdatetime(CommonFunImpl.currentTime(1));//设置更新时间
		
		
		//讲义不为空时候上传讲义
		if (!jiangyiFile.isEmpty()) {
			
			String path = request.getSession().getServletContext().getRealPath("upload\\jiangyi\\"); 
			//path += bean.getTea_id();
			
			String tmp = jiangyiFile.getOriginalFilename();
			tmp = tmp.substring(tmp.lastIndexOf('.'));
			String fileName = bean.getId() + tmp;
			
			File targetFile = new File(path, fileName);
			if(!targetFile.exists()){  
	            targetFile.mkdirs();  
	        }  
			
			//保存  
	        try {  
	        	jiangyiFile.transferTo(targetFile);  
	        } catch (Exception e) {  
	            e.printStackTrace();  
	        }  
	        
	        //设置bean中的内容
	        //bean.setJiangyipath(bean.getTea_id()+"/"+fileName);
		}
		
		String tipString;
		//如果插入成功
		if (jiangyiService.update(bean) > 0) {
			tipString = "ok";
		}
		else {
			tipString = "error";
		}
		
		List<TeacherBean> teaList = teacherService.selectList(null);
		List<BanjiBean> banjiList = banjiService.selectList(null);
		modelAndView.addObject("tealist", teaList);
		modelAndView.addObject("banjilist", banjiList);
		modelAndView.addObject("bean", bean);
		modelAndView.addObject("tip", tipString);
		return modelAndView;
	}

	@RequestMapping("/vedioPreview")
	public ModelAndView vedioPreview(String vp) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("background/jiangyi/vediopreview");
		modelAndView.addObject("vp", vp);
		return modelAndView;
	}
	
}
