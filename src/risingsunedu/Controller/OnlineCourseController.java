package risingsunedu.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Common.Impl.CommonConstant;

@Controller
@RequestMapping("/onlinecourse")
public class OnlineCourseController {

	//在线课程主页
	@RequestMapping("/manage")
	public ModelAndView manage() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject(CommonConstant.navOpt, CommonConstant.onlinecourse);
		modelAndView.setViewName("risingsunedu/course/onlinecourse");
		return modelAndView;
	}

}
