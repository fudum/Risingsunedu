package risingsunedu.Controller.Front;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.CeshiBean;
import risingsunedu.Bean.PeiyoujiangyiBean;
import risingsunedu.Bean.StudentBean;
import risingsunedu.Bean.StudentKechengBean;
import risingsunedu.Bean.ZuoyeBean;
import risingsunedu.Service.ICeshiService;
import risingsunedu.Service.IPeiyoujiangyiService;
import risingsunedu.Service.IStudentKechengService;
import risingsunedu.Service.IZuoyeService;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/student")
public class FStudentController extends BaseController{

	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;
	
	public class StrList{
		String strkey;
		List<Object> list;
		
		public String getStrkey() {
			return strkey;
		}
		public void setStrkey(String strkey) {
			this.strkey = strkey;
		}
		public List<Object> getList() {
			return list;
		}
		public void setList(List<Object> list) {
			this.list = list;
		}
	}
	
	@Autowired
	IPeiyoujiangyiService peiyoujiangyiService;
	
	@Autowired
	IZuoyeService zuoyeService;
	
	@Autowired
	ICeshiService ceshiService;
	
	@Autowired
	IStudentKechengService studentKechengService;

	@Override
	public void setLogger() {
		
	}
	
	@RequestMapping("/index")
	public ModelAndView index(HttpSession session) {
		ModelAndView modelAndView = new ModelAndView("front001/student");
		StudentBean bean = (StudentBean)session.getAttribute("stu_login_session");
		
		StudentKechengBean studentKechengBean = new StudentKechengBean();
		studentKechengBean.setStuid(bean.getId());
		List<StudentKechengBean> listKc = studentKechengService.selectList(studentKechengBean);
		modelAndView.addObject("listKc", listKc);
		
		//培优讲义
		PeiyoujiangyiBean peiyoujiangyiBean = new PeiyoujiangyiBean();
		peiyoujiangyiBean.setStuid(bean.getId());
		List<PeiyoujiangyiBean> listPyjy = peiyoujiangyiService.selectList(peiyoujiangyiBean);
		List<StrList> strListPyjy = new ArrayList<StrList>();
		for (int i = 0; i < listPyjy.size(); i++) {
			boolean flag = false;
			for (int j = 0; j < strListPyjy.size(); j++) {
				if (listPyjy.get(i).getSubject().equals(strListPyjy.get(j).getStrkey())) {
					strListPyjy.get(j).getList().add(listPyjy.get(i));
					flag = true;
					break;
				}
			}
			if (flag == false) {
				StrList strList = new StrList();
				strList.setStrkey(listPyjy.get(i).getSubject());
				strList.setList(new ArrayList<Object>());
				strList.getList().add(listPyjy.get(i));
				strListPyjy.add(strList);
			}
		}
		modelAndView.addObject("strListPyjy", strListPyjy);
		
		//培优作业
		ZuoyeBean zuoyeBean = new ZuoyeBean();
		zuoyeBean.setStuid(bean.getId());
		List<ZuoyeBean> listZuoye = zuoyeService.selectList(zuoyeBean);
		List<StrList> strListZuoye = new ArrayList<StrList>();
		for (int i = 0; i < listZuoye.size(); i++) {
			boolean flag = false;
			for (int j = 0; j < strListZuoye.size(); j++) {
				if (listZuoye.get(i).getSubject().equals(strListZuoye.get(j).getStrkey())) {
					strListZuoye.get(j).getList().add(listZuoye.get(i));
					flag = true;
					break;
				}
			}
			if (flag == false) {
				StrList strList = new StrList();
				strList.setStrkey(listZuoye.get(i).getSubject());
				strList.setList(new ArrayList<Object>());
				strList.getList().add(listZuoye.get(i));
				strListZuoye.add(strList);
			}
		}
		modelAndView.addObject("strListZuoye", strListZuoye);
		
		
		//培优测试
		CeshiBean ceshiBean = new CeshiBean();
		ceshiBean.setStuid(bean.getId());
		List<CeshiBean> listCeshi = ceshiService.selectList(ceshiBean);
		List<StrList> strListCeshi = new ArrayList<StrList>();
		for (int i = 0; i < listCeshi.size(); i++) {
			boolean flag = false;
			for (int j = 0; j < strListCeshi.size(); j++) {
				if (listCeshi.get(i).getSubject().equals(strListCeshi.get(j).getStrkey())) {
					strListCeshi.get(j).getList().add(listCeshi.get(i));
					flag = true;
					break;
				}
			}
			if (flag == false) {
				StrList strList = new StrList();
				strList.setStrkey(listCeshi.get(i).getSubject());
				strList.setList(new ArrayList<Object>());
				strList.getList().add(listCeshi.get(i));
				strListCeshi.add(strList);
			}
		}
		modelAndView.addObject("strListCeshi", strListCeshi);
		
		return modelAndView;
	}
	
	@RequestMapping("/showcatalog")
	public ModelAndView showCatalog(StudentKechengBean bean){
		ModelAndView modelAndView = new ModelAndView("front001/studentcatalog");
		bean = studentKechengService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		return modelAndView;
	}

	
}
