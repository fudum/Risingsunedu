package risingsunedu.Controller.Front;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.AdvertisementBean;
import risingsunedu.Bean.StudentBean;
import risingsunedu.Service.IAdvertisementService;
import risingsunedu.Service.IStudentService;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/home")
public class FHomeController extends BaseController{

	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IAdvertisementService advertisementService;
	
	@Autowired
	IStudentService studentService;

	@Override
	public void setLogger() {		
	}
	
	@RequestMapping("/index")
	public ModelAndView index() {
		ModelAndView modelAndView = new ModelAndView("front001/index_new");
		
		List<AdvertisementBean> list = advertisementService.selectList(null);
		modelAndView.addObject("list", list);
		return modelAndView;
	}
	
	@RequestMapping("/login")
	public ModelAndView login() {
		ModelAndView modelAndView = new ModelAndView("front001/login");
		return modelAndView;
	}
	
	@RequestMapping("/loginin")
	public void loginin(StudentBean bean,HttpSession session,HttpServletResponse response){
		String retVal = "0";
		if(check(bean.getStuid()) && check(bean.getPassword())){
			bean = studentService.selectOne(bean);
			if (bean != null) {
				bean.setPassword(null);
				session.setAttribute("stu_login_session", bean);
				retVal = "1";
			}
		}
		// 返回值
		try {
			PrintWriter out = response.getWriter();
			System.out.print(retVal);
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}
	}
	
	@RequestMapping("/loginout")
	public void loginout(HttpSession session,HttpServletResponse response){
		String retVal = "1";
		session.removeAttribute("stu_login_session");
		// 返回值
		try {
			PrintWriter out = response.getWriter();
			System.out.print(retVal);
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}
		
	}
	
	/**
	 * 检查数据是否合法
	 * @param str
	 * @return
	 */
	private boolean check(String str){
		if(str == null){
			return false;
		}
		else if(str.isEmpty()){
			return false;
		}
		else{
			return true;
		}
	}
	
	@RequestMapping("/check")
	public void sessionCheck(HttpSession session,HttpServletResponse response){
		//判断session
		String retVal = session.getAttribute("stu_login_session") == null? "0": "1";		
		try {
			PrintWriter out = response.getWriter();
			System.out.print(retVal);
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}
	}

}
