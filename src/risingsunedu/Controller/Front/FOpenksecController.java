package risingsunedu.Controller.Front;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.OpenkBean;
import risingsunedu.Bean.OpenkjiangciBean;
import risingsunedu.Bean.OpenksecBean;
import risingsunedu.Bean.TeacherBean;
import risingsunedu.Service.IOpenkService;
import risingsunedu.Service.IOpenkjiangciService;
import risingsunedu.Service.IOpenksecService;
import risingsunedu.Service.ITeacherService;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/opensec")
public class FOpenksecController extends  BaseController {

	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 模仿map
	 * @author Administrator
	 *
	 */
	public class SecLC{
		String mykey;
		List<OpenksecBean> mylist;
		public String getMykey() {
			return mykey;
		}
		public void setMykey(String mykey) {
			this.mykey = mykey;
		}
		public List<OpenksecBean> getMylist() {
			return mylist;
		}
		public void setMylist(List<OpenksecBean> mylist) {
			this.mylist = mylist;
		}		
	}
	
	@Autowired
	IOpenkService oService;
	
	@Autowired
	IOpenksecService openksecService;
	
	@Autowired
	IOpenkjiangciService openkjiangciService;
	
	@Autowired
	ITeacherService teacherService;
	
	@Override
	public void setLogger() {
	}
	
	@RequestMapping("/index")
	public ModelAndView index(OpenkBean bean) {
		ModelAndView modelAndView = new ModelAndView("front001/openksec");
		bean = oService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		
		//teacherBean
		TeacherBean teacherBean = new TeacherBean();
		teacherBean.setId(bean.getTea_id());
		teacherBean = teacherService.selectOne(teacherBean);
		modelAndView.addObject("teacherBean", teacherBean);
		
		//opensecBean
		OpenkjiangciBean openkjiangciBean = new OpenkjiangciBean();
		openkjiangciBean.setOpenk_id(bean.getId());
		List<OpenkjiangciBean > openkjcList = openkjiangciService.selectList(openkjiangciBean);
		List<SecLC> secLCs = new ArrayList<FOpenksecController.SecLC>();
		
		String videoStr  = "";
		for (int i = 0; i < openkjcList.size(); i++) {
			OpenksecBean bean2 = new OpenksecBean();
			bean2.setOpenk_id(openkjcList.get(i).getId());
			List<OpenksecBean> list = openksecService.selectList(bean2);
			if (list.size()> 0) {
				if(i == 0) {
					videoStr = list.get(0).getVideopath();
				}
				SecLC secLC = new SecLC();
				secLC.setMykey(openkjcList.get(i).getJorder() + ";" +openkjcList.get(i).getName());
				secLC.setMylist(list);
				secLCs.add(secLC);
			}
		}
		modelAndView.addObject("myMap", secLCs);
		modelAndView.addObject("videoStr", videoStr);
		
		//更新playnum
		OpenkBean bean2 = new OpenkBean();
		bean2.setId(bean.getId());
		bean2.setPlaynum(bean.getPlaynum() + 1);
		oService.update(bean2);
		
		return modelAndView;
	}
	
	@RequestMapping("/vid")
	public void getVid(OpenksecBean bean,HttpSession session,HttpServletResponse response){
		response.setCharacterEncoding("UTF-8");
		int ret = session.getAttribute("stu_login_session") == null? 0: 1;
		List<String> list = new ArrayList<String>();
		list.add(Integer.toString(ret));
		if (ret == 1) {
			bean = openksecService.selectOne(bean);
			list.add(bean.getVideopath());
		}
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			String s = JSONArray.fromObject(list).toString();
			out.print(s);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
