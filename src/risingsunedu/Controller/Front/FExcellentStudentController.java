package risingsunedu.Controller.Front;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.BaseTwoBean;
import risingsunedu.Bean.CeshiBean;
import risingsunedu.Bean.ExcellentStudentBean;
import risingsunedu.Bean.PeiyoujiangyiBean;
import risingsunedu.Bean.StudentBean;
import risingsunedu.Bean.StudentKechengBean;
import risingsunedu.Bean.StudentbanjiBean;
import risingsunedu.Bean.XuebaBean;
import risingsunedu.Bean.XuebabaobanBean;
import risingsunedu.Bean.ZuoyeBean;
import risingsunedu.Service.IBaseTwoService;
import risingsunedu.Service.ICeshiService;
import risingsunedu.Service.IExcellentStudentService;
import risingsunedu.Service.IPeiyoujiangyiService;
import risingsunedu.Service.IStudentKechengService;
import risingsunedu.Service.IStudentService;
import risingsunedu.Service.IStudentbanjiService;
import risingsunedu.Service.IXuebaService;
import risingsunedu.Service.IXuebabaobanService;
import risingsunedu.Service.IZuoyeService;
import framework.base.common.Pager;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/excstu")
public class FExcellentStudentController extends BaseController {
	
	public class StrList{
		String strkey;
		List<Object> list;
		
		public String getStrkey() {
			return strkey;
		}
		public void setStrkey(String strkey) {
			this.strkey = strkey;
		}
		public List<Object> getList() {
			return list;
		}
		public void setList(List<Object> list) {
			this.list = list;
		}
	}

	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IExcellentStudentService esService;
	
	@Autowired
	IXuebaService xuebaService;
	
	@Autowired
	IBaseTwoService baseTwoService;
	
	@Autowired
	IStudentbanjiService studentbanjiService;
	
	@Autowired
	IStudentService studentService;
	
	@Autowired
	IStudentKechengService studentKechengService;

	@Autowired
	IPeiyoujiangyiService peiyoujiangyiService;
	
	@Autowired
	IZuoyeService zuoyeService;
	
	@Autowired
	ICeshiService ceshiService;
	
	@Autowired
	IXuebabaobanService xuebabaobanService;
	
	@Override
	public void setLogger() {
	}
	
	@RequestMapping("/index")
	public ModelAndView index(Pager<ExcellentStudentBean> pager) {
		ModelAndView modelAndView = new ModelAndView("front001/excellentstudent/excellentstudent");
		pager = esService.selectPageWithStudent(null, pager);
		modelAndView.addObject("pager", pager);
		
		List<XuebaBean> listXueba = xuebaService.selectList(null);
		modelAndView.addObject("listXueba", listXueba);
		
		BaseTwoBean baseTwoBean = new BaseTwoBean();
		baseTwoBean.setBotype("年级");
		List<BaseTwoBean> listGrade = baseTwoService.selectList(baseTwoBean);
		modelAndView.addObject("listGrade", listGrade);
		
		StudentBean studentBean = new StudentBean();
		studentBean.setKaiqidangan("1");
		Pager<StudentBean> pagerStudent = new Pager<StudentBean>();
		pagerStudent = studentService.selectPage(studentBean, pagerStudent);
		for (int i = 0; i < pagerStudent.getPageList().size(); i++) {
			pagerStudent.getPageList().get(i).setPassword(null);
		}
		modelAndView.addObject("pagerStudent", pagerStudent);
				
		return modelAndView;
	}
	
	@RequestMapping("/showCourse")
	public ModelAndView showCourse(StudentbanjiBean bean){
		ModelAndView modelAndView = new ModelAndView("front001/excellentstudent/showCourse");
		List<StudentbanjiBean> list = studentbanjiService.selectList(bean);
		modelAndView.addObject("list", list);
		return modelAndView;
	}
	
	@RequestMapping("/xuebadangan")
	public ModelAndView xuebadangan(StudentBean bean){
		ModelAndView modelAndView = new ModelAndView("front001/excellentstudent/xuebadangan");
		bean = studentService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		
		//培优课程
		StudentKechengBean studentKechengBean = new StudentKechengBean();
		studentKechengBean.setStuid(bean.getId());
		List<StudentKechengBean> listKc = studentKechengService.selectList(studentKechengBean);
		modelAndView.addObject("listKc", listKc);
		
		//培优讲义
		PeiyoujiangyiBean peiyoujiangyiBean = new PeiyoujiangyiBean();
		peiyoujiangyiBean.setStuid(bean.getId());
		List<PeiyoujiangyiBean> listPyjy = peiyoujiangyiService.selectList(peiyoujiangyiBean);
		List<StrList> strListPyjy = new ArrayList<StrList>();
		for (int i = 0; i < listPyjy.size(); i++) {
			boolean flag = false;
			for (int j = 0; j < strListPyjy.size(); j++) {
				if (listPyjy.get(i).getSubject().equals(strListPyjy.get(j).getStrkey())) {
					strListPyjy.get(j).getList().add(listPyjy.get(i));
					flag = true;
					break;
				}
			}
			if (flag == false) {
				StrList strList = new StrList();
				strList.setStrkey(listPyjy.get(i).getSubject());
				strList.setList(new ArrayList<Object>());
				strList.getList().add(listPyjy.get(i));
				strListPyjy.add(strList);
			}
		}
		modelAndView.addObject("strListPyjy", strListPyjy);
		
		//培优作业
		ZuoyeBean zuoyeBean = new ZuoyeBean();
		zuoyeBean.setStuid(bean.getId());
		List<ZuoyeBean> listZuoye = zuoyeService.selectList(zuoyeBean);
		List<StrList> strListZuoye = new ArrayList<StrList>();
		for (int i = 0; i < listZuoye.size(); i++) {
			boolean flag = false;
			for (int j = 0; j < strListZuoye.size(); j++) {
				if (listZuoye.get(i).getSubject().equals(strListZuoye.get(j).getStrkey())) {
					strListZuoye.get(j).getList().add(listZuoye.get(i));
					flag = true;
					break;
				}
			}
			if (flag == false) {
				StrList strList = new StrList();
				strList.setStrkey(listZuoye.get(i).getSubject());
				strList.setList(new ArrayList<Object>());
				strList.getList().add(listZuoye.get(i));
				strListZuoye.add(strList);
			}
		}
		modelAndView.addObject("strListZuoye", strListZuoye);
		
		
		//培优测试
		CeshiBean ceshiBean = new CeshiBean();
		ceshiBean.setStuid(bean.getId());
		List<CeshiBean> listCeshi = ceshiService.selectList(ceshiBean);
		List<StrList> strListCeshi = new ArrayList<StrList>();
		for (int i = 0; i < listCeshi.size(); i++) {
			boolean flag = false;
			for (int j = 0; j < strListCeshi.size(); j++) {
				if (listCeshi.get(i).getSubject().equals(strListCeshi.get(j).getStrkey())) {
					strListCeshi.get(j).getList().add(listCeshi.get(i));
					flag = true;
					break;
				}
			}
			if (flag == false) {
				StrList strList = new StrList();
				strList.setStrkey(listCeshi.get(i).getSubject());
				strList.setList(new ArrayList<Object>());
				strList.getList().add(listCeshi.get(i));
				strListCeshi.add(strList);
			}
		}
		modelAndView.addObject("strListCeshi", strListCeshi);
		
		
		//学霸报班
		XuebabaobanBean xuebabaobanBean = new XuebabaobanBean();
		xuebabaobanBean.setStuid(bean.getId());
		List<XuebabaobanBean> listXBBB = xuebabaobanService.selectList(xuebabaobanBean);
		modelAndView.addObject("listXBBB", listXBBB);
		
		return modelAndView;
	}
	
	
}
