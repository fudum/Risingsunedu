package risingsunedu.Controller.Front;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.BaseTwoBean;
import risingsunedu.Bean.KechengBean;
import risingsunedu.Bean.OpenkBean;
import risingsunedu.Bean.TeacherAdditionBean;
import risingsunedu.Bean.TeacherAssessBean;
import risingsunedu.Bean.TeacherBean;
import risingsunedu.Service.IBaseTwoService;
import risingsunedu.Service.IJiangyiService;
import risingsunedu.Service.IKechengService;
import risingsunedu.Service.IOpenkService;
import risingsunedu.Service.ITeacherAdditionService;
import risingsunedu.Service.ITeacherAssessService;
import risingsunedu.Service.ITeacherService;
import framework.base.common.Pager;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/tea")
public class FTeacherController extends BaseController{
	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	ITeacherService tService;
	
	@Autowired
	ITeacherAdditionService taService;
	
	@Autowired
	ITeacherAssessService tAssessService;
	
	@Autowired
	IJiangyiService jiangyiService;
	
	@Autowired
	IBaseTwoService basetwoService;
	
	@Autowired
	IOpenkService openkService;
	
	@Autowired
	IKechengService kechengService;

	@Override
	public void setLogger() {
	}
	
	@RequestMapping("/index")
	public ModelAndView index(TeacherBean bean, Pager<TeacherBean> pager) {
		ModelAndView modelAndView = new ModelAndView("/front001/teacher");
		pager.setCountPerPage(10);
		modelAndView.addObject("bean", bean);
		if (!(bean.getJiaoling() == null  || "".equals(bean.getJiaoling()))) {
			String tmp = bean.getJiaoling();
			String [] tmpArr = tmp.split("-");
			bean.setStunumber(tmpArr[0]);
			bean.setKeshinumber(tmpArr[1]);
		}
		pager = tService.selectPageLike(bean, pager);
		modelAndView.addObject("pager", pager);
		
		//基本信息
		List<BaseTwoBean> listbt = basetwoService.selectList(null);
		List<BaseTwoBean> listGrade = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listSubject = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listBanci = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listJiaoling = new ArrayList<BaseTwoBean>();
		for (BaseTwoBean baseTwoBean : listbt) {
			switch (baseTwoBean.getBotype()) {
			case "年级":
				listGrade.add(baseTwoBean);
				break;
			case "科目":
				listSubject.add(baseTwoBean);
				break;
			case "教师带班类型":
				listBanci.add(baseTwoBean);
				break;
			case "教龄":
				listJiaoling.add(baseTwoBean);
				break;
			}
		}
		modelAndView.addObject("listGrade", listGrade);
		modelAndView.addObject("listSubject", listSubject);
		modelAndView.addObject("listBanci", listBanci);
		modelAndView.addObject("listJiaoling", listJiaoling);
		
 		return modelAndView;
	}
	
	@RequestMapping("/info")
	public ModelAndView info(TeacherBean bean){
		ModelAndView modelAndView = new ModelAndView("/front001/teacherinfo");
		//教师基本信息
		bean = tService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		//教师附加信息
		TeacherAdditionBean taBean = new TeacherAdditionBean();
		taBean.setTea_id(bean.getId());
		taBean = taService.selectOne(taBean);
		modelAndView.addObject("taBean", taBean);
		
		//学员评估
		TeacherAssessBean tAssessBean = new TeacherAssessBean();
		tAssessBean.setTea_id(bean.getId());
		List<TeacherAssessBean> list = tAssessService.selectList(tAssessBean);
		modelAndView.addObject("list", list);
			
		//公开课
		Pager<OpenkBean> openkPager = new Pager<OpenkBean>();
		openkPager.setCountPerPage(1000);
		OpenkBean openkBean = new OpenkBean();
		openkBean.setTea_id(bean.getId());
		openkPager = openkService.selectPage(openkBean,openkPager);
		modelAndView.addObject("openkPager", openkPager);
		
		//课程信息
		Pager<KechengBean> kechengPager = new Pager<KechengBean>();
		KechengBean kechengBean = new KechengBean();
		kechengBean.setTea_id(bean.getId());
		kechengPager = kechengService.selectPage(kechengBean, kechengPager);
		modelAndView.addObject("kechengPager", kechengPager);
		kechengPager.setCountPerPage(1000);
		return modelAndView;
	}
	
	@RequestMapping("/vedioPreview")
	public ModelAndView vedioPreview(String vp) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("front001/teachervideoplay");
		modelAndView.addObject("vp", vp);
		return modelAndView;
	}

}
