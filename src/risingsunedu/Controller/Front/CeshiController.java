package risingsunedu.Controller.Front;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.CeshiBean;
import risingsunedu.Service.ICeshiService;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/ceshi")
public class CeshiController extends BaseController{

	/**
	 * 培优测试
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	ICeshiService ceshiService;

	@Override
	public void setLogger() {
	}
	
	@RequestMapping("/info")
	public ModelAndView info(CeshiBean bean){
		ModelAndView modelAndView = new ModelAndView("front001/ceshiinfo");
		bean = ceshiService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		return modelAndView;
	}

}
