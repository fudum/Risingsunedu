package risingsunedu.Controller.Front;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.BanjiBean;
import risingsunedu.Bean.JiangyiBean;
import risingsunedu.Common.Impl.FileOperateUtilImpl;
import risingsunedu.Service.IBanjiService;
import risingsunedu.Service.IJiangyiService;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/jy")
public class FJiangyiController extends BaseController {

	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IJiangyiService jiangyiService;
	
	@Autowired
	IBanjiService bjService;

	@Override
	public void setLogger() {		
	}
	
	//讲义详情
	@RequestMapping("/info")
	public ModelAndView info(JiangyiBean bean) {
		ModelAndView modelAndView = new ModelAndView("front001/handoutsinfo");
		//讲义bean
		bean = jiangyiService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		//班级bean
		BanjiBean bjBean = new BanjiBean();
		bjBean.setId(bean.getBj_id());
		bjBean = bjService.selectOne(bjBean);
		modelAndView.addObject("bjBean", bjBean);
		return modelAndView;
	}
	
	@RequestMapping("/download")
	public ModelAndView download(String storeName,String realName,HttpServletRequest request,HttpServletResponse response) throws Exception  {
		String contentType = "application/octet-stream";
		realName += ".pdf";
		FileOperateUtilImpl.download(request, response, storeName, contentType,
				realName);
		return null;
	}
	
	

}
