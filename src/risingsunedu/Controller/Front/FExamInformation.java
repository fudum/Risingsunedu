package risingsunedu.Controller.Front;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import framework.base.controller.BaseController;

@Controller
@RequestMapping("exainfo")
public class FExamInformation extends BaseController{

	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void setLogger() {		
	}
	
	@RequestMapping("/index")
	public ModelAndView index() {
		ModelAndView modelAndView = new ModelAndView("/front001/examinformation");
		return modelAndView;
	}
	

}
