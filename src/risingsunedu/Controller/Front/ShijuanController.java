package risingsunedu.Controller.Front;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.BaseTwoBean;
import risingsunedu.Bean.ShijuanBean;
import risingsunedu.Common.Impl.FileOperateUtilImpl;
import risingsunedu.Service.IBaseTwoService;
import risingsunedu.Service.IShijuanService;
import framework.base.common.Pager;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/paper")
public class ShijuanController extends BaseController{

	/**
	 * 试卷管理
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IBaseTwoService basetwoService;
	
	@Autowired
	IShijuanService shijuanService;

	@Override
	public void setLogger() {		
	}

	@RequestMapping("/index")
	public ModelAndView index(ShijuanBean bean, Pager<ShijuanBean> pager) {
		ModelAndView modelAndView = new ModelAndView("front001/shijuan");
		modelAndView.addObject("bean", bean);
		pager = shijuanService.selectPage(bean, pager);
		modelAndView.addObject("pager", pager);
		//基本信息
		List<BaseTwoBean> listbt = basetwoService.selectList(null);
		List<BaseTwoBean> listGrade = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listSubject = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listYear = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> liststype = new ArrayList<BaseTwoBean>();
		for (BaseTwoBean baseTwoBean : listbt) {
			switch (baseTwoBean.getBotype()) {
			case "年级":
				listGrade.add(baseTwoBean);
				break;
			case "试卷科目":
				listSubject.add(baseTwoBean);
				break;
			case "试卷年份":
				listYear.add(baseTwoBean);
				break;
			case "试卷类型":
				liststype.add(baseTwoBean);
				break;
			}
		}
		modelAndView.addObject("listGrade", listGrade);
		modelAndView.addObject("listSubject", listSubject);
		modelAndView.addObject("listYear", listYear);
		modelAndView.addObject("liststype", liststype);
		
		return modelAndView;
	}
	
	@RequestMapping("/info")
	public ModelAndView info(ShijuanBean bean){
		ModelAndView modelAndView = new ModelAndView("front001/shijuaninfo");
		bean = shijuanService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		return modelAndView;
	}
	
	@RequestMapping("/download")
	public void download(String storeName,String realName,HttpSession session,HttpServletRequest request,HttpServletResponse response) throws Exception  {
		//判断session
		if (session.getAttribute("stu_login_session") == null) {
			String retVal = "0";
			// 返回值
			try {
				PrintWriter out = response.getWriter();
				out.print(retVal);
				out.close();
			} catch (IOException e) {
				e.printStackTrace();// 打印异常
			}
		}
		else {
			String contentType = "application/octet-stream";
			String tmp = storeName.substring(storeName.lastIndexOf("."));
			realName += tmp;
			FileOperateUtilImpl.download(request, response, storeName, contentType,realName);
		}
		
	}
	
}
