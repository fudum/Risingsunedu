package risingsunedu.Controller.Front;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.BanjiBean;
import risingsunedu.Bean.BanjijiangciBean;
import risingsunedu.Bean.BanjijieciBean;
import risingsunedu.Bean.BaseTwoBean;
import risingsunedu.Bean.TeacherBean;
import risingsunedu.Service.IBanjiService;
import risingsunedu.Service.IBanjijiangciService;
import risingsunedu.Service.IBanjijieciService;
import risingsunedu.Service.IBaseTwoService;
import risingsunedu.Service.ITeacherService;
import framework.base.common.Pager;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/peiyou")
public class FPeiyouController extends BaseController{
	
	/**
	 * 班级节次List 模拟map
	 * @author Administrator
	 *
	 */
	public class JieciCL{
		private String mykey;
		private List<BanjijieciBean> mylist;
		public String getMykey() {
			return mykey;
		}
		public void setMykey(String mykey) {
			this.mykey = mykey;
		}
		public List<BanjijieciBean> getMylist() {
			return mylist;
		}
		public void setMylist(List<BanjijieciBean> mylist) {
			this.mylist = mylist;
		}
		
		
	}

	/**
	 * 培优同步课堂
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IBaseTwoService basetwoService;
	
	@Autowired
	IBanjiService banjiService;
	
	@Autowired
	ITeacherService teacherService;
	
	@Autowired
	IBanjijiangciService banjijiangciService;
	
	@Autowired
	IBanjijieciService banjijieciService;

	@Override
	public void setLogger() {
	}
	
	@RequestMapping("/index")
	public ModelAndView index(BanjiBean bean,Pager<BanjiBean> pager){
		ModelAndView modelAndView = new ModelAndView("front001/peiyou");
		modelAndView.addObject("bean", bean);
		pager.setCountPerPage(12);
		pager = banjiService.selectPagerWithTeacher(bean, pager);
		modelAndView.addObject("pager", pager);
		//基本信息
		List<BaseTwoBean> listbt = basetwoService.selectList(null);
		List<BaseTwoBean> listGrade = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listSubject = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listBanci = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listBjtype = new ArrayList<BaseTwoBean>();
		for (BaseTwoBean baseTwoBean : listbt) {
			switch (baseTwoBean.getBotype()) {
			case "年级":
				listGrade.add(baseTwoBean);
				break;
			case "科目":
				listSubject.add(baseTwoBean);
				break;
			case "班次":
				listBanci.add(baseTwoBean);
				break;
			case "班级类型":
				listBjtype.add(baseTwoBean);
				break;
			}
		}
		modelAndView.addObject("listGrade", listGrade);
		modelAndView.addObject("listSubject", listSubject);
		modelAndView.addObject("listBanci", listBanci);
		modelAndView.addObject("listBjtype", listBjtype);
		return modelAndView;
	}
	
	@RequestMapping("/info")
	public ModelAndView info(BanjiBean bean) {
		ModelAndView modelAndView = new ModelAndView("front001/peiyouinfo");
		bean = banjiService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		
		//teacherBean
		TeacherBean teacherBean = new TeacherBean();
		teacherBean.setId(bean.getTea_id());
		teacherBean = teacherService.selectOne(teacherBean);
		modelAndView.addObject("teacherBean", teacherBean);
		
		//banjijieciBean
		BanjijiangciBean banjijiangciBean = new BanjijiangciBean();
		banjijiangciBean.setBanji_id(bean.getId());
		List<BanjijiangciBean> jiangciList = banjijiangciService.selectList(banjijiangciBean);
		List<JieciCL> jieciCLs = new ArrayList<FPeiyouController.JieciCL>();
		
		String videoStr = "";
		for (int i = 0; i < jiangciList.size(); i++) {
			BanjijieciBean banjijieciBean = new BanjijieciBean();
			banjijieciBean.setJiangci_id(jiangciList.get(i).getId());
			List<BanjijieciBean> list = banjijieciService.selectList(banjijieciBean);
			if (list.size()> 0) {
				if(i == 0) {
					videoStr = list.get(0).getVideopath();
				}
				JieciCL jieciCL = new JieciCL();
				jieciCL.setMykey(jiangciList.get(i).getJorder() + ";" +jiangciList.get(i).getName());
				jieciCL.setMylist(list);
				jieciCLs.add(jieciCL);
			}
		}
		
		//更新playnum
		BanjiBean bean2 = new BanjiBean();
		bean2.setId(bean.getId());
		bean2.setPlaynum(bean.getPlaynum() + 1);
		banjiService.update(bean2);
		
		modelAndView.addObject("myList", jieciCLs);
		modelAndView.addObject("videoStr", videoStr);
		
		return modelAndView;
	}
	
	@RequestMapping("/search")
	public void search(BanjiBean bean,Pager<BanjiBean> pager,HttpServletResponse response) {
		response.setHeader("Content-type", "text/html;charset=UTF-8"); 
		pager.setCountPerPage(12);
 		pager = banjiService.selectPagerWithTeacher(bean, pager);
 		JSONObject json = new JSONObject();
 		JSONArray jos = new JSONArray();
 		for(BanjiBean oB : pager.getPageList()){
 			JSONObject jo = new JSONObject();
 			jo.put("id", oB.getId());
 			jo.put("name", oB.getYear() + "年" + oB.getGrade() + oB.getSubject() + oB.getBanci() + oB.getBjtype());
 			jo.put("subject", oB.getSubject());
 			jo.put("tea_id", oB.getTea_id());
 			jo.put("tea_name", oB.getTeacherBean().getName());
 			jo.put("headpath", oB.getTeacherBean().getHeadpath());
 			jo.put("time", oB.getLessonday() + oB.getStarttime() + "-" + oB.getEndtime());
 			jos.add(jo);
 		}
 		
 		json.put("jos",jos);
 		json.put("totalPage", pager.getTotalPage());
 		json.put("curtPage", pager.getCurtPage());
 		
 		String ret = "";
 		ret = json.toString();
 		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/vid")
	public void getVid(BanjijieciBean bean,HttpSession session,HttpServletResponse response){
		response.setCharacterEncoding("UTF-8");
		int ret = session.getAttribute("stu_login_session") == null? 0: 1;
		List<String> list = new ArrayList<String>();
		list.add(Integer.toString(ret));
		if (ret == 1) {
			bean = banjijieciService.selectOne(bean);
			list.add(bean.getVideopath());
		}
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			String s = JSONArray.fromObject(list).toString();
			out.print(s);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
