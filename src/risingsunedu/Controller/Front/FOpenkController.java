package risingsunedu.Controller.Front;

import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.BaseTwoBean;
import risingsunedu.Bean.OpenkBean;
import risingsunedu.Common.Impl.FileOperateUtilImpl;
import risingsunedu.Service.IBaseTwoService;
import risingsunedu.Service.IOpenkService;
import framework.base.common.Pager;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/open")
public class FOpenkController extends BaseController{

	/**
	 * @author fudum
	 * @qq 974822058
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IOpenkService oService;
	
	@Autowired
	IBaseTwoService baseTwoService;

	@Override
	public void setLogger() {		
	}
	
	@RequestMapping("/index")
	public ModelAndView index(OpenkBean bean,Pager<OpenkBean> pager) {
		ModelAndView modelAndView = new ModelAndView("front001/openk");
		modelAndView.addObject("bean", bean);
		pager.setCountPerPage(10);
		pager = oService.selectOpenkTeacher(bean, pager);
		modelAndView.addObject("pager", pager);
		
		//班级 科目 课程类型
		List<BaseTwoBean> listBT = baseTwoService.selectList(null);
		modelAndView.addObject("listbt", listBT);
		
		return modelAndView;
	}
	
	@RequestMapping("/search")
	public void search(OpenkBean bean,Pager<OpenkBean> pager,HttpServletResponse response) {
		response.setHeader("Content-type", "text/html;charset=UTF-8"); 
		pager.setCountPerPage(10);
 		pager = oService.selectOpenkTeacher(bean, pager);
 		JSONObject json = new JSONObject();
 		JSONArray jos = new JSONArray();
 		for(OpenkBean oB : pager.getPageList()){
 			JSONObject jo = new JSONObject();
 			jo.put("id", oB.getId());
 			jo.put("name", oB.getName());
 			jo.put("grade", oB.getGrade());
 			jo.put("subject", oB.getSubject());
 			jo.put("kctype", oB.getKctype());
 			jo.put("playnum", oB.getPlaynum());
 			jo.put("tea_id", oB.getTea_id());
 			jo.put("tea_name", oB.getTeacherBean().getName());
 			jo.put("headpath", oB.getTeacherBean().getHeadpath());
 			jos.add(jo);
 		}
 		
 		json.put("jos",jos);
 		json.put("totalPage", pager.getTotalPage());
 		json.put("curtPage", pager.getCurtPage());
 		
 		String ret = "";
 		ret = json.toString();
 		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@RequestMapping("/showcatalog")
	public ModelAndView showCatalog(OpenkBean bean){
		ModelAndView modelAndView = new ModelAndView("front001/opencatalog");
		bean = oService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		return modelAndView;
	}
	
	@RequestMapping("/download")
	public void download(String storeName,String realName,HttpServletRequest request,HttpServletResponse response) throws Exception  {
		String contentType = "application/octet-stream";
		realName += ".pdf";
		FileOperateUtilImpl.download(request, response, storeName, contentType,realName);
	}

}
