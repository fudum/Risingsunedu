package risingsunedu.Controller.Front;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import risingsunedu.Service.IStudentKechengService;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/stukc")
public class StudentKechengController extends BaseController {

	/**
	 * 学生课程控制层
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IStudentKechengService studentKechengService;

	@Override
	public void setLogger() {
	}
	

}
