package risingsunedu.Controller.Front;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.BaseTwoBean;
import risingsunedu.Bean.KechengBean;
import risingsunedu.Bean.KechengjiangciBean;
import risingsunedu.Bean.KechengjieciBean;
import risingsunedu.Bean.TeacherBean;
import risingsunedu.Service.IBaseTwoService;
import risingsunedu.Service.IKechengService;
import risingsunedu.Service.IKechengjiangciService;
import risingsunedu.Service.IKechengjieciService;
import risingsunedu.Service.ITeacherService;
import framework.base.common.Pager;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/course")
public class FKechengController extends BaseController{

	/**
	 * 课程
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IBaseTwoService basetwoService;
	
	@Autowired
	IKechengService kechengService;
	
	@Autowired
	ITeacherService teacherService;
	
	@Autowired
	IKechengjieciService kechengjieciService;
	
	@Autowired
	IKechengjiangciService kechengjiangciService;
	
	public class SecLC{
		String mykey;
		List<KechengjieciBean> mylist;
		public String getMykey() {
			return mykey;
		}
		public void setMykey(String mykey) {
			this.mykey = mykey;
		}
		public List<KechengjieciBean> getMylist() {
			return mylist;
		}
		public void setMylist(List<KechengjieciBean> mylist) {
			this.mylist = mylist;
		}		
	}
	
	

	@Override
	public void setLogger() {
	}
	
	@RequestMapping("/index")
	public ModelAndView index(KechengBean bean,Pager<KechengBean> pager) {
		ModelAndView modelAndView = new ModelAndView("front001/kecheng");
		modelAndView.addObject("bean", bean);
		pager.setCountPerPage(10);
		
		//课程基本信息
		pager = kechengService.selectPagerWithTeacher(bean, pager);
		modelAndView.addObject("pager", pager);
		//基本信息
		List<BaseTwoBean> listbt = basetwoService.selectList(null);
		List<BaseTwoBean> listGrade = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listSubject = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listBanci = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listBjtype = new ArrayList<BaseTwoBean>();
		for (BaseTwoBean baseTwoBean : listbt) {
			switch (baseTwoBean.getBotype()) {
			case "年级":
				listGrade.add(baseTwoBean);
				break;
			case "科目":
				listSubject.add(baseTwoBean);
				break;
			case "班次":
				listBanci.add(baseTwoBean);
				break;
			case "班级类型":
				listBjtype.add(baseTwoBean);
				break;
			}
		}
		modelAndView.addObject("listGrade", listGrade);
		modelAndView.addObject("listSubject", listSubject);
		modelAndView.addObject("listBanci", listBanci);
		modelAndView.addObject("listBjtype", listBjtype);
		
		return modelAndView;
	}
	
	@RequestMapping("/showcatalog")
	public ModelAndView showCatalog(KechengBean bean){
		ModelAndView modelAndView = new ModelAndView("front001/kechengcatalog");
		bean = kechengService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		return modelAndView;
	}
	
	@RequestMapping("/info")
	public ModelAndView info(KechengBean bean){
		ModelAndView modelAndView = new ModelAndView("front001/kechenginfo");
		bean = kechengService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		
		//教师bean
		TeacherBean teacherBean = new TeacherBean();
		teacherBean.setId(bean.getTea_id());
		teacherBean = teacherService.selectOne(teacherBean);
		modelAndView.addObject("teacherBean", teacherBean);
		
		//opensecBean
		KechengjiangciBean kechengjiangciBean = new KechengjiangciBean();
		kechengjiangciBean.setKecheng_id(bean.getId());
		List<KechengjiangciBean> kechengjcList = kechengjiangciService.selectList(kechengjiangciBean);
		List<SecLC> secLCs = new ArrayList<FKechengController.SecLC>();
		
		String videoStr  = "";
		for (int i = 0; i < kechengjcList.size(); i++) {
			KechengjieciBean bean2 = new KechengjieciBean();
			bean2.setJiangci_id(kechengjcList.get(i).getId());
			List<KechengjieciBean> list = kechengjieciService.selectList(bean2);
			if (list.size()> 0) {
				if(i == 0) {
					videoStr = list.get(0).getVideopath();
				}
				SecLC secLC = new SecLC();
				secLC.setMykey(kechengjcList.get(i).getJorder() + ";" +kechengjcList.get(i).getName());
				secLC.setMylist(list);
				secLCs.add(secLC);
			}
		}
		
		modelAndView.addObject("myMap", secLCs);
		modelAndView.addObject("videoStr", videoStr);
		return modelAndView;
	}
	
	@RequestMapping("/vid")
	public void getVid(KechengjieciBean bean,HttpSession session,HttpServletResponse response){
		response.setCharacterEncoding("UTF-8");
		int ret = session.getAttribute("stu_login_session") == null? 0: 1;
		List<String> list = new ArrayList<String>();
		list.add(Integer.toString(ret));
		if (ret == 1) {
			bean = kechengjieciService.selectOne(bean);
			list.add(bean.getVideopath());
		}
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			String s = JSONArray.fromObject(list).toString();
			out.print(s);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
