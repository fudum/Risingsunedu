package risingsunedu.Controller.Front;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.ZuoyeBean;
import risingsunedu.Service.IZuoyeService;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/zuoye")
public class ZuoyeController extends BaseController {

	/**
	 * 培优作业
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IZuoyeService zuoyeService;

	@Override
	public void setLogger() {
		
	}
	
	@RequestMapping("/info")
	public ModelAndView info(ZuoyeBean bean, int type){
		ModelAndView modelAndView = new ModelAndView("front001/zuoyeinfo");
		bean = zuoyeService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		modelAndView.addObject("type", type);
		return modelAndView;
	}

}
