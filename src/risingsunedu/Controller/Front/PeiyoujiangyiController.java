package risingsunedu.Controller.Front;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.PeiyoujiangyiBean;
import risingsunedu.Service.IPeiyoujiangyiService;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/peiyoujy")
public class PeiyoujiangyiController extends BaseController{

	/**
	 * 培优讲义
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IPeiyoujiangyiService peiyoujiangyiService;

	@Override
	public void setLogger() {
		
	}
	
	@RequestMapping("/info")
	public ModelAndView info(PeiyoujiangyiBean bean){
		ModelAndView modelAndView = new ModelAndView("front001/peiyoujyinfo");
		bean = peiyoujiangyiService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		return modelAndView;
	}

}
