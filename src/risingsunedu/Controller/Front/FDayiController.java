package risingsunedu.Controller.Front;

import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.BaseTwoBean;
import risingsunedu.Bean.DayiBean;
import risingsunedu.Bean.ZhangjieBean;
import risingsunedu.Service.IBaseTwoService;
import risingsunedu.Service.IDayiService;
import risingsunedu.Service.IZhangjieService;
import framework.base.common.Pager;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/answer")
public class FDayiController extends BaseController{

	/**
	 * 前台答疑视频解析
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IDayiService dayiService;
	
	@Autowired
	IBaseTwoService baseTwoService;
	
	@Autowired
	IZhangjieService zhangjieService;

	@Override
	public void setLogger() {		
	}
	
	@RequestMapping("/index")
	public ModelAndView index(Pager<DayiBean> pager){
		ModelAndView modelAndView = new ModelAndView("front001/ansque");
		pager.setCountPerPage(24);
		pager = dayiService.selectPage(null, pager);
		modelAndView.addObject("pager", pager);
		//班级 科目 课程类型
		List<BaseTwoBean> listBT = baseTwoService.selectList(null);
		modelAndView.addObject("listbt", listBT);
		
		
		return modelAndView;
	}
	
	
	@RequestMapping("/videoplay")
	public ModelAndView videoPlay(DayiBean bean) {
		ModelAndView modelAndView = new ModelAndView("front001/ansquevideo");
		bean = dayiService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		//增加播放次数
		DayiBean dybBean = new DayiBean();
		dybBean.setId(bean.getId());
		dybBean.setPlaynum(bean.getPlaynum() + 1);
		dayiService.update(dybBean);
		return modelAndView;
	}
	
	@RequestMapping("/search")
	public void search(DayiBean bean,Pager<DayiBean> pager,HttpServletResponse response){
		response.setHeader("Content-type", "text/html;charset=UTF-8");
		pager.setCountPerPage(24);
		
 		pager = dayiService.selectPage(bean, pager);
 		JSONObject json = new JSONObject();
 		
 		JSONArray jos = new JSONArray();
 		for(DayiBean oB : pager.getPageList()){
 			JSONObject jo = new JSONObject();
 			jo.put("id", oB.getId());
 			jo.put("name", oB.getName());
 			jo.put("grade", oB.getGrade());
 			jo.put("subject", oB.getSubject());
 			jo.put("playnum", oB.getPlaynum());
 			jo.put("tea_id", oB.getTea_id());
 			jo.put("tea_name", oB.getTea_name());
 			jo.put("picpath", oB.getPicpath());
 			jo.put("inserttime", oB.getInserttime());
 			jo.put("videopath", oB.getVideopath());
 			jos.add(jo);
 		}
 		
 		json.put("jos",jos);
 		json.put("totalPage", pager.getTotalPage());
 		json.put("curtPage", pager.getCurtPage());
 		
 		String ret = "";
 		ret = json.toString();
 		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/checkzhangjie")
	public void checkZhangjie(ZhangjieBean bean,HttpServletResponse response){
		response.setHeader("Content-type", "text/html;charset=UTF-8");
		List<ZhangjieBean> list = zhangjieService.selectList(bean);
		JSONObject json = new JSONObject();
		JSONArray jos = new JSONArray();
	    for (int i = 0; i < list.size(); i++) {
	    	JSONObject jo = new JSONObject();
	    	jo.put("name", list.get(i).getName());
	    	jo.put("subject", list.get(i).getSubject());
	    	jo.put("grade", list.get(i).getGrade());
	    	jo.put("id", list.get(i).getId());
	    	jos.add(jo);
		}
	    json.put("jos", jos);
	    json.put("ret", "1");
	    String ret = "";
 		ret = json.toString();
 		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
}
