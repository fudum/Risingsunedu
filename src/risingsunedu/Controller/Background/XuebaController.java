package risingsunedu.Controller.Background;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.XuebaBean;
import risingsunedu.Common.Impl.CommonFunImpl;
import risingsunedu.Service.IXuebaService;
import framework.base.common.Pager;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/b_xueba")
public class XuebaController extends BaseController {

	/**
	 * 学霸控制层
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IXuebaService xuebaService;

	@Override
	public void setLogger() {
	}
	
	@RequestMapping("/index")
	public ModelAndView index(Pager<XuebaBean> pager){
		ModelAndView modelAndView = new ModelAndView("background/excstu/xueba/index");
		pager = xuebaService.selectPage(null, pager);
		modelAndView.addObject("pager", pager);
		return modelAndView;
	}
	
	@RequestMapping("/add")
	public ModelAndView add(){
		ModelAndView modelAndView = new ModelAndView("background/excstu/xueba/add");
		return modelAndView;
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(XuebaBean bean){
		ModelAndView modelAndView = new ModelAndView("background/excstu/xueba/edit");
		bean = xuebaService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		return modelAndView;
	}
	
	@RequestMapping("/insert")
	public void insert(XuebaBean bean,HttpServletResponse response){
		bean.setId(CommonFunImpl.uuid("A"));
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String retVal = xuebaService.insert(bean) > 0 ? "1" : "0";
		// 返回值
		try {
			PrintWriter out = response.getWriter();
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}
	}
	
	@RequestMapping("/update")
	public void update(XuebaBean bean,HttpServletResponse response){
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String retVal = xuebaService.update(bean) > 0 ? "1":"0";
		
		// 返回值
		try {
			PrintWriter out = response.getWriter();
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}
	}
	
	@RequestMapping("/delete")
	public ModelAndView Delete(XuebaBean bean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		List<String> list = new ArrayList<String>();
		int ret = 1;
		ret = xuebaService.delete(bean);
		if (ret > 0) {
			list.add("1");
			list.add("删除成功!");
		} else {
			list.add("0");
			list.add("删除失败!");
		}
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			String s = JSONArray.fromObject(list).toString();
			out.print(s);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
