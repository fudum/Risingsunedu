package risingsunedu.Controller.Background;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import framework.base.common.Pager;
import risingsunedu.Bean.TeacherAdditionBean;
import risingsunedu.Bean.TeacherBean;
import risingsunedu.Common.Impl.CommonConstant;
import risingsunedu.Common.Impl.CommonFunImpl;
import risingsunedu.Service.ITeacherAdditionService;
import risingsunedu.Service.ITeacherService;

@Controller
@RequestMapping("/teacheraddition")
public class TeacherAdditionController {

	@Autowired
	ITeacherAdditionService teacherAdditionService;
	
	@Autowired
	ITeacherService tService;
	
	
	/**
	 * 教师附加信息首页
	 * @return
	 */
	@RequestMapping("/index")
	public ModelAndView index(Pager<TeacherBean> pager) {
		ModelAndView modelAndView = new ModelAndView("background/teacher/addition/teacheraddition");
		pager = tService.selectPage(null, pager);
		modelAndView.addObject("pager", pager);
		return modelAndView;
	}
	
	/**
	 * 教师附加信息编辑
	 * @return
	 */
	@RequestMapping("/teacheradditionedit")
	public ModelAndView teacherAddtionEdit(@RequestParam("eS") String eS,TeacherAdditionBean bean) {
		ModelAndView modelAndView = new ModelAndView("background/teacher/addition/teacheradditionedit");
		TeacherAdditionBean beanOne = teacherAdditionService.selectOne(bean);
		if (beanOne == null) {
			modelAndView.addObject("bean", bean);
		}
		else {
			modelAndView.addObject("bean", beanOne);
		}
		switch (eS) {
		case "0":
			eS="";
			break;
		case "1":
			eS="编辑成功！";
			break;
		case "2":
			eS="编辑失败！";
			break;
		}
		modelAndView.addObject("eS", eS);
		return modelAndView;
	}
	
	
	/**
	 * 编辑附加信息
	 * @param bean
	 * @return
	 */
	@RequestMapping("/teacheradditionupdate")
	public ModelAndView teacherAdditionUpdate(TeacherAdditionBean bean) {
		ModelAndView modelAndView =new ModelAndView();
		String eS = "";
		if (bean.getId().equals(null) || "".equals(bean.getId())) {
			bean.setId(CommonFunImpl.uuid("TA"));
			bean.setInserttime(CommonFunImpl.currentTime(1));
			if (teacherAdditionService.insert(bean) > 0) {
				eS = "1";
			}
			else {
				eS = "2";
			}
		}
		else {
			if (teacherAdditionService.update(bean) > 0) {
				eS = "1";
			}
			else {
				eS = "2";
			}
		}
		modelAndView.setViewName("redirect:/teacheraddition/teacheradditionedit"+CommonConstant.sufStr+"?eS="+eS+"&tea_id=" +bean.getTea_id());
		return modelAndView;
		
	}
}
