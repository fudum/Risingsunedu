package risingsunedu.Controller.Background;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.BanjiBean;
import risingsunedu.Bean.StudentbanjiBean;
import risingsunedu.Common.Impl.CommonFunImpl;
import risingsunedu.Service.IBanjiService;
import risingsunedu.Service.IStudentbanjiService;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/b_studentbanji")
public class StudentBanjiController extends BaseController {

	/**
	 * 学生选班级控制层
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IBanjiService banjiService;
	
	@Autowired
	IStudentbanjiService studentbanjiService;

	@Override
	public void setLogger() {
		// TODO Auto-generated method stub
		
	}
	
	@RequestMapping("/add")
	public ModelAndView add(String stuid){
		ModelAndView modelAndView = new ModelAndView("background/user/student/studentbanji/add");
		modelAndView.addObject("stuid", stuid);
		//培优课堂同步
		List<BanjiBean> list = banjiService.selectList(null);
		modelAndView.addObject("list", list);
		return modelAndView;
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(StudentbanjiBean bean){
		ModelAndView modelAndView = new ModelAndView("background/user/student/studentbanji/edit");
		bean = studentbanjiService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		//培优课堂同步
		List<BanjiBean> list = banjiService.selectList(null);
		modelAndView.addObject("list", list);
		return modelAndView;
	}
	
	
	
	@RequestMapping("/insert")
	public void insert(StudentbanjiBean bean,HttpServletResponse response){
		bean.setId(CommonFunImpl.uuid("SB"));
		bean.setInserttime(CommonFunImpl.currentTime(1));
		int retVal;
		if(bean.getBanjiname() == null || "".equals(bean.getBanjiname())){
			retVal = 0;
		}else{
			String arr[] = bean.getBanjiname().split(";");
			bean.setTeaid(arr[0]);
			bean.setTeaname(arr[1]);
			bean.setBanji_id(arr[2]);
			bean.setBanjiname(arr[3]);
			retVal = studentbanjiService.insert(bean) > 0 ? 1 : 0;
		}
		try {
			PrintWriter out = response.getWriter();
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}
	}
	
	@RequestMapping("/update")
	public void update(StudentbanjiBean bean,HttpServletResponse response){
		bean.setInserttime(CommonFunImpl.currentTime(1));
		int retVal;
		if(bean.getBanjiname() == null || "".equals(bean.getBanjiname())){
			retVal = 0;
		}else{
			String arr[] = bean.getBanjiname().split(";");
			bean.setTeaid(arr[0]);
			bean.setTeaname(arr[1]);
			bean.setBanji_id(arr[2]);
			bean.setBanjiname(arr[3]);
			retVal = studentbanjiService.update(bean) > 0 ? 1 : 0;
		}
		try {
			PrintWriter out = response.getWriter();
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/delete")
	public void Delete(StudentbanjiBean bean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		List<String> list = new ArrayList<String>();
		int ret = 1;
		ret = studentbanjiService.delete(bean);
		if (ret > 0) {
			list.add("1");
			list.add("删除成功!");
		} else {
			list.add("0");
			list.add("删除失败!");
		}
		try {
			PrintWriter out = response.getWriter();
			String s = JSONArray.fromObject(list).toString();
			out.print(s);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
