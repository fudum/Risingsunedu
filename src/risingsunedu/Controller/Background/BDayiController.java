package risingsunedu.Controller.Background;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import risingsunedu.Bean.BaseTwoBean;
import risingsunedu.Bean.DayiBean;
import risingsunedu.Bean.TeacherBean;
import risingsunedu.Bean.ZhangjieBean;
import risingsunedu.Common.Impl.CommonConstant;
import risingsunedu.Common.Impl.CommonFunImpl;
import risingsunedu.Common.Impl.OSinfo;
import risingsunedu.Service.IBaseTwoService;
import risingsunedu.Service.IDayiService;
import risingsunedu.Service.ITeacherService;
import risingsunedu.Service.IZhangjieService;
import framework.base.common.Pager;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/bdayi")
public class BDayiController extends BaseController{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IDayiService dayiService;
	
	@Autowired
	ITeacherService teacherService;
	
	@Autowired
	IBaseTwoService baseTwoService;
	
	@Autowired
	IZhangjieService zhangjieService;

	@Override
	public void setLogger() {
	}
	
	@RequestMapping("/index")
	public ModelAndView index(Pager<DayiBean> pager) {
		ModelAndView modelAndView = new ModelAndView("/background/dayi/index");
		pager = dayiService.selectPage(null, pager);
		modelAndView.addObject("pager", pager);
		return modelAndView;
	}
	
	@RequestMapping("/add")
	public ModelAndView add(){
		ModelAndView modelAndView = new ModelAndView("/background/dayi/add");
		List<TeacherBean> list = teacherService.selectList(null); 
		modelAndView.addObject("list", list);
		
		//班级 科目 课程类型
		List<BaseTwoBean> listBT = baseTwoService.selectList(null);
		modelAndView.addObject("listbt", listBT);
		return modelAndView;
		
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(DayiBean bean,String tip) {
		ModelAndView modelAndView = new ModelAndView("/background/dayi/edit");
		bean = dayiService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		modelAndView.addObject("tip", tip);
		List<TeacherBean> list = teacherService.selectList(null); 
		modelAndView.addObject("list", list);
		
		//班级 科目 课程类型
		List<BaseTwoBean> listBT = baseTwoService.selectList(null);
		modelAndView.addObject("listbt", listBT);
		//章节
		ZhangjieBean zjBean = new ZhangjieBean();
		zjBean.setSubject(bean.getSubject());
		zjBean.setGrade(bean.getGrade());
		List<ZhangjieBean> listzj = zhangjieService.selectList(zjBean);
		modelAndView.addObject("listzj", listzj);
		return modelAndView;
	}
	
	@RequestMapping("/picupload")
	public void picUpload(MultipartFile picFile,HttpServletResponse response){
		response.setContentType("text/html");
		String fileName = "";
		boolean flag = false;
		if (!picFile.isEmpty()) {
			String path = OSinfo.isWindows() ? CommonConstant.w_dayiPicPath :CommonConstant.dayiPicPath;
			String tmp = picFile.getOriginalFilename();
			tmp = tmp.substring(tmp.lastIndexOf('.'));
			fileName = CommonFunImpl.uuid32() + tmp;
			
			File targetFile = new File(path, fileName);
			if(!targetFile.exists()){  
	            targetFile.mkdirs();  
	        }  
			
			//保存  
	        try {  
	        	picFile.transferTo(targetFile);  
	        	flag = true;
	        } catch (Exception e) {  
	            e.printStackTrace();  
	        } 
		}
		String ret = (flag == true) ? "1":"0";
		JSONObject jb = new JSONObject();
		jb.put("ret", ret);
		jb.put("picpath", "dayi/" + fileName);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/insert")
	public ModelAndView insert(DayiBean bean) {
		bean.setId(CommonFunImpl.uuid("O"));
		String arr[] = bean.getTea_id().split(";");
		bean.setTea_id(arr[0]);
		bean.setTea_name(arr[1]);
		if(bean.getPlaynum() == null){
			bean.setPlaynum(0);
		}
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String tip = dayiService.insert(bean) > 0 ? "1" : "0";
		ModelAndView modelAndView = new ModelAndView(new RedirectView(CommonConstant.preStr + "/bdayi/edit" + CommonConstant.sufStr),"tip",tip);
		modelAndView.addObject("id", bean.getId());
		return modelAndView;
	}
	
	@RequestMapping("/update")
	public ModelAndView update(DayiBean bean) {
		String arr[] = bean.getTea_id().split(";");
		bean.setTea_id(arr[0]);
		bean.setTea_name(arr[1]);
		bean.setInserttime(CommonFunImpl.currentTime(1));
		if(bean.getPlaynum() == null){
			bean.setPlaynum(0);
		}
		String tip = dayiService.update(bean) > 0 ? "3" : "2";
		ModelAndView modelAndView = new ModelAndView(new RedirectView(CommonConstant.preStr + "/bdayi/edit" + CommonConstant.sufStr),"tip",tip);
		modelAndView.addObject("id", bean.getId());
		return modelAndView;
	}
	
	@RequestMapping("/delete")
	public ModelAndView delete(DayiBean bean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		List<String> list = new ArrayList<String>();
		int ret = 1;
		ret = dayiService.delete(bean);
		if (ret > 0) {
			list.add("1");
			list.add("删除成功!");
		} else {
			list.add("0");
			list.add("删除失败!");
		}
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			String s = JSONArray.fromObject(list).toString();
			out.print(s);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping("/checkzhangjie")
	public void checkZhangjie(ZhangjieBean bean,HttpServletResponse response){
		response.setHeader("Content-type", "text/html;charset=UTF-8");
		List<ZhangjieBean> list = zhangjieService.selectList(bean);
		JSONObject json = new JSONObject();
		JSONArray jos = new JSONArray();
	    for (int i = 0; i < list.size(); i++) {
	    	JSONObject jo = new JSONObject();
	    	jo.put("name", list.get(i).getName());
	    	jo.put("zorder", list.get(i).getZorder());
	    	jos.add(jo);
		}
	    json.put("jos", jos);
	    json.put("ret", "1");
	    String ret = "";
 		ret = json.toString();
 		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
