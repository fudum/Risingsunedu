package risingsunedu.Controller.Background;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;






import risingsunedu.Bean.BaseOneBean;
import risingsunedu.Bean.BaseTwoBean;
import risingsunedu.Common.Impl.CommonFunImpl;
import risingsunedu.Service.IBaseOneService;
import risingsunedu.Service.IBaseTwoService;
import risingsunedu.Service.IOpenkService;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/basetwo")
public class BaseTwoController extends BaseController {

	/**
	 * 基本信息二级
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IBaseOneService baseOneService;
	
	@Autowired
	IBaseTwoService baseTwoService;
	
	@Autowired
	IOpenkService openkService;

	@Override
	public void setLogger() {
	}
	
	@RequestMapping("/add")
	public ModelAndView add() {
		ModelAndView modelAndView = new ModelAndView("background/base/twoadd");
		List<BaseOneBean> listone = baseOneService.selectList(null);
		modelAndView.addObject("listone", listone);
		return modelAndView;
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(BaseTwoBean bean){
		ModelAndView modelAndView = new ModelAndView("background/base/twoedit");
		bean = baseTwoService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		List<BaseOneBean> listone = baseOneService.selectList(null);
		modelAndView.addObject("listone", listone);
		return modelAndView;
	}
	
	
	@RequestMapping("/insert")
	public void insert(BaseTwoBean bean, HttpServletResponse response) {
		response.setContentType("text/html");
		bean.setId(CommonFunImpl.uuid("BT"));
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String ret ="";
		JSONObject jb = new JSONObject();
		if (baseTwoService.insert(bean) > 0) {
			ret = "1";
		}
		else{
			ret = "0";
		}
		jb.put("ret", ret);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/update")
	public void update(BaseTwoBean bean,String old_name, HttpServletResponse response) {
		response.setContentType("text/html");
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String ret ="";
		JSONObject jb = new JSONObject();
		if (baseTwoService.update(bean) > 0) {
			ret = "1";
		}
		else{
			ret = "0";
		}
		jb.put("ret", ret);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//更新公开课
		if(bean.getBotype().equals("课程类型") && (!(old_name == null || old_name == ""))){
			Map<String,String> map = new HashMap<String,String>();
			map.put("name", bean.getName());
			map.put("old_name", old_name);
			openkService.update(map);
		}
	}
	
	@RequestMapping("/delete")
	public void delete(BaseTwoBean bean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		List<String> list = new ArrayList<String>();
		int ret = 1;
		ret = baseTwoService.delete(bean);
		if (ret > 0) {
			list.add("1");
			list.add("删除成功!");
		} else {
			list.add("0");
			list.add("删除失败!");
		}
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			String s = JSONArray.fromObject(list).toString();
			out.print(s);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
