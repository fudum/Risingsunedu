package risingsunedu.Controller.Background;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.UserBean;
import risingsunedu.Service.IUserService;
import framework.base.common.Pager;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/user")
public class UserController extends BaseController {

	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	IUserService userService;

	@Override
	public void setLogger() {
		logger = LoggerFactory.getLogger(getClass());
	}
	
	// 首页头部信息
	@RequestMapping("/top")
	public ModelAndView top() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("background/top");
		return modelAndView;
	}

	// 首页左侧信息
	@RequestMapping("/left")
	public ModelAndView left() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("background/left");
		return modelAndView;
	}

	// 首页中间信息
	@RequestMapping("/mainfra")
	public ModelAndView mainfra() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("background/mainfra");
		return modelAndView;
	}

	// 用户管理页面
	@RequestMapping("/usermanage")
	public ModelAndView userManage() {
		ModelAndView modelAndView = new ModelAndView();

		Pager<UserBean> pager = new Pager<UserBean>();

		Pager<UserBean> userPage = userService.selectPage(null, pager);

		// 数据list
		modelAndView.addObject("userlist", userPage.getPageList());
		// 总页数
		modelAndView.addObject("totalPage", userPage.getTotalPage());
		// 当前页
		modelAndView.addObject("countPerPage", userPage.getCurtPage());

		modelAndView.setViewName("background/user/usermanage");
		return modelAndView;
	}

	// 添加用户页面
	@RequestMapping("/useradd")
	public ModelAndView userAdd() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("background/user/useradd");
		return modelAndView;
	}

	// 编辑用户页面
	@RequestMapping("/useredit")
	public ModelAndView userEdit(UserBean bean) {
		ModelAndView modelAndView = new ModelAndView();
		bean = userService.selectOne(bean);
		modelAndView.addObject("user", bean);
		modelAndView.setViewName("background/user/useredit");
		return modelAndView;
	}

	// 下一页
	@RequestMapping("/nextpage")
	public ModelAndView nextPage(int pageNum, int totalPage) {
		ModelAndView modelAndView = new ModelAndView();

		Pager<UserBean> pager = new Pager<UserBean>();

		if (pageNum < totalPage) {// 如果小于总页数
			pageNum++;
		} else {// 返回首页
			pageNum = 1;
		}
		pager.setCurtPage(pageNum);

		Pager<UserBean> userPage = userService.selectPage(null, pager);

		// 数据list
		modelAndView.addObject("userlist", userPage.getPageList());
		// 总页数
		modelAndView.addObject("totalPage", userPage.getTotalPage());
		// 当前页
		modelAndView.addObject("countPerPage", userPage.getCurtPage());

		modelAndView.setViewName("background/user/usermanage");
		return modelAndView;
	}

	// 上一页
	@RequestMapping("/frontpage")
	public ModelAndView frontPage(int pageNum, int totalPage) {
		ModelAndView modelAndView = new ModelAndView();

		Pager<UserBean> pager = new Pager<UserBean>();

		if (pageNum > 1) {// 如果小于总页数
			pageNum--;
		} else {// 返回首页
			pageNum = totalPage;
		}
		pager.setCurtPage(pageNum);

		Pager<UserBean> userPage = userService.selectPage(null, pager);

		// 数据list
		modelAndView.addObject("userlist", userPage.getPageList());
		// 总页数
		modelAndView.addObject("totalPage", userPage.getTotalPage());
		// 当前页
		modelAndView.addObject("countPerPage", userPage.getCurtPage());

		modelAndView.setViewName("background/user/usermanage");
		return modelAndView;
	}

	// 最后一页
	@RequestMapping("/lastpage")
	public ModelAndView lastPage(int totalPage) {
		ModelAndView modelAndView = new ModelAndView();

		Pager<UserBean> pager = new Pager<UserBean>();
		pager.setCurtPage(totalPage);

		Pager<UserBean> userPage = userService.selectPage(null, pager);

		// 数据list
		modelAndView.addObject("userlist", userPage.getPageList());
		// 总页数
		modelAndView.addObject("totalPage", userPage.getTotalPage());
		// 当前页
		modelAndView.addObject("countPerPage", userPage.getCurtPage());

		modelAndView.setViewName("background/user/usermanage");
		return modelAndView;
	}

	// 用户插入
	@RequestMapping("/userinsert")
	public ModelAndView userInsert(UserBean bean, HttpServletResponse response) {
		String uuid = UUID.randomUUID().toString().replace("-", "");
		bean.setId(uuid);
		String retVal;
		// 如果插入成功
		if (userService.insert(bean) > 0) {
			retVal = "1";
		} else {
			retVal = "0";
		}

		// 返回值
		try {
			PrintWriter out = response.getWriter();
			System.out.print(retVal);
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}

		return null;
	}

	// 删除单选题
	@RequestMapping("/delete")
	public ModelAndView Delete(UserBean userBean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		List<String> list = new ArrayList<String>();
		int ret = 1;
		ret = userService.delete(userBean);
		if (ret > 0) {
			list.add("1");
			list.add("删除成功!");
		} else {
			list.add("0");
			list.add("删除失败!");
		}
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			String s = JSONArray.fromObject(list).toString();
			out.print(s);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping("/userupdate")
	public ModelAndView userUpdate(UserBean bean, HttpServletResponse response) {
		// 返回值
		String retVal;
		// 如果插入成功
		if (userService.update(bean) > 0) {
			retVal = "1";
		} else {
			retVal = "0";
		}

		// 把数据异步的方式输入到页面
		try {
			PrintWriter out = response.getWriter();
			System.out.print(retVal);
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}
		return null;
	}
	
	//退出
	@RequestMapping("/loginout")
	public  ModelAndView loginOut(HttpSession session) {
		ModelAndView modelAndView = new ModelAndView("background/login");
		session.removeAttribute("usersession");
		return modelAndView;
	}

}
