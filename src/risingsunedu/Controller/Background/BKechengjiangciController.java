package risingsunedu.Controller.Background;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.KechengjiangciBean;
import risingsunedu.Common.Impl.CommonFunImpl;
import risingsunedu.Service.IKechengjiangciService;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/b_kechengjiangci")
public class BKechengjiangciController extends BaseController {

	/**
	 * 后台课程controller
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IKechengjiangciService kechengjiangciService;

	@Override
	public void setLogger() {		
	}
	
	@RequestMapping("/add")
	public ModelAndView add(String kecheng_id){
		ModelAndView modelAndView = new ModelAndView("background/kecheng/jiangci/add");
		modelAndView.addObject("kecheng_id",kecheng_id);
		return modelAndView;
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(KechengjiangciBean bean) {
		ModelAndView modelAndView = new ModelAndView("background/kecheng/jiangci/edit");
		bean = kechengjiangciService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		return modelAndView;
	}
	
	@RequestMapping("/insert")
	public void insert(KechengjiangciBean bean, HttpServletResponse response){
		response.setContentType("text/html");
		bean.setId(CommonFunImpl.uuid("Kjc"));
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String ret ="";
		JSONObject jb = new JSONObject();
		if (kechengjiangciService.insert(bean) > 0) {
			ret = "1";
		}
		else{
			ret = "0";
		}
		jb.put("ret", ret);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/update")
	public void update(KechengjiangciBean bean, HttpServletResponse response){
		response.setContentType("text/html");
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String ret ="";
		JSONObject jb = new JSONObject();
		if (kechengjiangciService.update(bean) > 0) {
			
			/*//更新对应的节次
			BanjijieciBean banjijieciBean = new BanjijieciBean();
			banjijieciBean.setJiangci_name("第" + bean.getJorder() + "讲");
			banjijieciBean.setJiangci_id(bean.getId());
			KechengjiangciBean.updateByJiangciId(banjijieciBean);*/
			ret = "1";
		}
		else{
			ret = "0";
		}
		jb.put("ret", ret);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/delete")
	public void delete(KechengjiangciBean bean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		List<String> list = new ArrayList<String>();
		int ret = 1;
		ret = kechengjiangciService.delete(bean);
		if (ret > 0) {
			list.add("1");
			list.add("删除成功!");
		} else {
			list.add("0");
			list.add("删除失败!");
		}
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			String s = JSONArray.fromObject(list).toString();
			out.print(s);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
