package risingsunedu.Controller.Background;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.BaseTwoBean;
import risingsunedu.Bean.BookBean;
import risingsunedu.Common.Impl.CommonFunImpl;
import risingsunedu.Service.IBaseTwoService;
import risingsunedu.Service.IBookService;
import framework.base.common.Pager;

@Controller
@RequestMapping("/b_book")
public class BookController {
	
	@Autowired
	IBookService bookService;
	
	@Autowired
	IBaseTwoService basetwoService;
	
	@RequestMapping("/index")
	public ModelAndView index(BookBean bean, Pager<BookBean> pager) {
		ModelAndView modelAndView = new ModelAndView("background/book/index");
		pager = bookService.selectPage(bean, pager);
		return modelAndView;
	}
	
	@RequestMapping("/add")
	public ModelAndView add(){
		ModelAndView modelAndView = new ModelAndView("background/book/add");
		
		//基本信息
		List<BaseTwoBean> listbt = basetwoService.selectList(null);
		List<BaseTwoBean> listGrade = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listSubject = new ArrayList<BaseTwoBean>();
		for (BaseTwoBean baseTwoBean : listbt) {
			switch (baseTwoBean.getBotype()) {
			case "年级":
				listGrade.add(baseTwoBean);
				break;
			case "科目":
				listSubject.add(baseTwoBean);
				break;
			}
		}
		modelAndView.addObject("listGrade", listGrade);
		modelAndView.addObject("listSubject", listSubject);
		
		return modelAndView;
	}
	
	@RequestMapping("/insert")
	public void insert(BookBean bean,HttpServletResponse response){
		//设置试卷属性
		bean.setId(CommonFunImpl.uuid("b"));
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String retVal;
		// 如果插入成功
		if (bookService.insert(bean) > 0) {
			retVal = "1";
		} else {
			retVal = "0";
		}
		// 返回值
		try {
			PrintWriter out = response.getWriter();
			System.out.print(retVal);
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}
	}
}
