package risingsunedu.Controller.Background;

import java.io.File;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.stereotype.Controller;

import risingsunedu.Common.Impl.CommonConstant;
import risingsunedu.Common.Impl.CommonFunImpl;
import risingsunedu.Common.Impl.OSinfo;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/b_com")
public class CommonController extends BaseController {

	/**
	 * 通用控制层
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void setLogger() {
		
	}
	
	/**
	 * 上传文件 图片、pdf
	 * @param picFile
	 * @param wjjname
	 * @param response
	 */
	@RequestMapping("/fileupload")
	public void fileUpload(MultipartFile uploadFile,String wjjname,HttpServletResponse response){
		response.setContentType("text/html");
		String fileName = "";
		boolean flag = false;
		if(uploadFile != null){
			if (!uploadFile.isEmpty()) {
				String path = getWjjName(wjjname);
				String tmp = uploadFile.getOriginalFilename();
				tmp = tmp.substring(tmp.lastIndexOf('.'));
				fileName = CommonFunImpl.uuid32() + tmp;
				File targetFile = new File(path, fileName);
				if(!targetFile.exists()){  
		            targetFile.mkdirs();  
		        }  
				
				//保存  
		        try {  
		        	uploadFile.transferTo(targetFile);  
		        	flag = true;
		        } catch (Exception e) {  
		            e.printStackTrace();  
		        } 
			}
		}
		String ret = (flag == true) ? "1":"0";
		JSONObject jb = new JSONObject();
		jb.put("ret", ret);
		jb.put("path", wjjname + "/" + fileName);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	/**
	 * 获取文件的路径
	 * @param wjjName
	 * @return
	 */
	private String getWjjName(String wjjName) {
		String retVal = "";
		switch (wjjName) {
		case "shijuan"://试卷路径
			retVal = OSinfo.isWindows() ? CommonConstant.w_shijuanPath :CommonConstant.shijuanPath;
			break;
		case "dayi"://答疑路径
			retVal = OSinfo.isWindows() ? CommonConstant.w_dayiPicPath :CommonConstant.dayiPicPath;
			break;
		case "openk"://公开课目录
			retVal = OSinfo.isWindows() ? CommonConstant.w_openkPath :CommonConstant.openkPath;
			break;
		case "advertisement"://首页图片
			retVal = OSinfo.isWindows() ? CommonConstant.w_advertisementPath :CommonConstant.advertisementPath;
			break;
		case "student"://学生路径
			retVal = OSinfo.isWindows() ? CommonConstant.w_studentPath :CommonConstant.studentPath;
			break;
		case "excstu"://优秀学生
			retVal = OSinfo.isWindows() ? CommonConstant.w_excstuPath :CommonConstant.excstuPath;
			break;
		}
		return retVal;
	}
	

}
