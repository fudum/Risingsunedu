package risingsunedu.Controller.Background;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import risingsunedu.Bean.BaseTwoBean;
import risingsunedu.Bean.OpenkBean;
import risingsunedu.Bean.OpenkjiangciBean;
import risingsunedu.Bean.OpenksecBean;
import risingsunedu.Bean.TeacherBean;
import risingsunedu.Common.Impl.CommonConstant;
import risingsunedu.Common.Impl.CommonFunImpl;
import risingsunedu.Service.IBaseTwoService;
import risingsunedu.Service.IOpenkService;
import risingsunedu.Service.IOpenkjiangciService;
import risingsunedu.Service.IOpenksecService;
import risingsunedu.Service.ITeacherService;
import framework.base.common.Pager;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/openk")
public class OpenkController extends BaseController {

	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	ITeacherService tService;
	
	@Autowired
	IOpenkService oService;
	
	@Autowired
	IOpenkjiangciService ojcService;
	
	@Autowired
	IOpenksecService openksecService;
	
	@Autowired
	IBaseTwoService baseTwoService;

	@Override
	public void setLogger() {
	}
	
	@RequestMapping("/index")
	public ModelAndView index(Pager<OpenkBean> pager) {
		ModelAndView modelAndView = new ModelAndView("background/openk/index");
		pager = oService.selectPage(null, pager);
		modelAndView.addObject("pager", pager);
		return modelAndView;
	}
	
	@RequestMapping("/add")
	public ModelAndView add() {
		ModelAndView modelAndView = new ModelAndView("background/openk/add");
		List<TeacherBean> list = tService.selectList(null); 
		modelAndView.addObject("list", list);
		
		//班级 科目 课程类型
		List<BaseTwoBean> listbt = baseTwoService.selectList(null);
		List<BaseTwoBean> listGrade = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listSubject = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listKctype = new ArrayList<BaseTwoBean>();
		for (BaseTwoBean baseTwoBean : listbt) {
			switch (baseTwoBean.getBotype()) {
			case "年级":
				listGrade.add(baseTwoBean);
				break;
			case "科目":
				listSubject.add(baseTwoBean);
				break;
			case "课程类型":
				listKctype.add(baseTwoBean);
				break;
			}
		}
		modelAndView.addObject("listGrade", listGrade);
		modelAndView.addObject("listSubject", listSubject);
		modelAndView.addObject("listKctype", listKctype);
		return modelAndView;
	}
	
	@RequestMapping("/insert")
	public ModelAndView insert(OpenkBean bean) {
		ModelAndView modelAndView = new ModelAndView(new RedirectView(CommonConstant.preStr+ "/openk/index" + CommonConstant.sufStr));
		bean.setId(CommonFunImpl.uuid("O"));
		String arr[] = bean.getTea_id().split(";");
		bean.setTea_id(arr[0]);
		bean.setTea_name(arr[1]);
		bean.setInserttime(CommonFunImpl.currentTime(1));
		if (bean.getPlaynum() == null) {
			bean.setPlaynum(0);
		}
		String tip = oService.insert(bean) > 0 ? "1" : "0";
		modelAndView.addObject("tip", tip);		
		return modelAndView;
	}
	
	@RequestMapping("/update")
	public ModelAndView update(OpenkBean bean) {
		String arr[] = bean.getTea_id().split(";");
		bean.setTea_id(arr[0]);
		bean.setTea_name(arr[1]);
		String tip;
		//如果插入成功
		if (oService.update(bean) > 0) {
			tip = "u1";
		}
		else {
			tip = "u0";
		}
		ModelAndView modelAndView = new ModelAndView(new RedirectView(CommonConstant.preStr +"/openk/edit" + CommonConstant.sufStr + "?id=" +bean.getId() + "&tip=" +tip));
		return modelAndView;
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(OpenkBean bean,String tip) {
		ModelAndView modelAndView = new ModelAndView("background/openk/edit");
		//公开课
		bean = oService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		//教师
		List<TeacherBean> list = tService.selectList(null);
		modelAndView.addObject("list",list);
		modelAndView.addObject("tip", tip);
		//讲次
		OpenkjiangciBean openkjiangciBean = new OpenkjiangciBean();
		openkjiangciBean.setOpenk_id(bean.getId());
		List<OpenkjiangciBean> listjc = ojcService.selectList(openkjiangciBean);
		modelAndView.addObject("listjc", listjc);
		
		//视频
		List<OpenksecBean> listvideo = openksecService.selectListByOpenkId(bean.getId());
		modelAndView.addObject("listvideo", listvideo);
		
		//班级 科目 课程类型
		List<BaseTwoBean> listbt = baseTwoService.selectList(null);
		List<BaseTwoBean> listGrade = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listSubject = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listKctype = new ArrayList<BaseTwoBean>();
		for (BaseTwoBean baseTwoBean : listbt) {
			switch (baseTwoBean.getBotype()) {
			case "年级":
				listGrade.add(baseTwoBean);
				break;
			case "科目":
				listSubject.add(baseTwoBean);
				break;
			case "课程类型":
				listKctype.add(baseTwoBean);
				break;
			}
		}
		modelAndView.addObject("listGrade", listGrade);
		modelAndView.addObject("listSubject", listSubject);
		modelAndView.addObject("listKctype", listKctype);
		return modelAndView;
	}
	
	@RequestMapping("/delete")
	public ModelAndView delete(OpenkBean bean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		List<String> list = new ArrayList<String>();
		int ret = 1;
		ret = oService.delete(bean);
		if (ret > 0) {
			list.add("1");
			list.add("删除成功!");
		} else {
			list.add("0");
			list.add("删除失败!");
		}
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			String s = JSONArray.fromObject(list).toString();
			out.print(s);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping("/vedioPreview")
	public ModelAndView vedioPreview(String vp) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("background/openk/video/vediopreview");
		modelAndView.addObject("vp", vp);
		return modelAndView;
	}

}
