package risingsunedu.Controller.Background;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.BaseTwoBean;
import risingsunedu.Bean.ExcellentStudentBean;
import risingsunedu.Bean.StudentBean;
import risingsunedu.Bean.TeacherBean;
import risingsunedu.Common.Impl.CommonFunImpl;
import risingsunedu.Service.IBaseTwoService;
import risingsunedu.Service.IExcellentStudentService;
import risingsunedu.Service.IStudentService;
import risingsunedu.Service.ITeacherService;
import framework.base.common.Pager;
import framework.base.controller.BaseController;

/**
 * 优秀学员管理
 * @author fudum
 * @date 2014年12月1日
 */
@Controller
@RequestMapping("/b_excstu")
public class BExcellentStudentController extends BaseController {

	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IExcellentStudentService esService;
	
	@Autowired
	IStudentService studentService;
	
	@Autowired
	ITeacherService teacherService;
	
	@Autowired
	IBaseTwoService baseTwoService;

	@Override
	public void setLogger() {		
	}
	
	
	@RequestMapping("/index")
	public ModelAndView index(Pager<ExcellentStudentBean> pager) {
		ModelAndView modelAndView = new ModelAndView("background/excstu/cjd/index");
		pager= esService.selectPage(null, pager);
		modelAndView.addObject("pager", pager);
		return modelAndView;
	}
	
	@RequestMapping("/xuebadangan")
	public ModelAndView xuebadangan(StudentBean bean,Pager<StudentBean> pager){
		ModelAndView modelAndView = new ModelAndView("background/excstu/xuebadangan/index");
		bean.setKaiqidangan("1");
		pager = studentService.selectPage(bean, pager);
		return modelAndView;
	}
	
	//添加优秀学员
	@RequestMapping("/add")
	public ModelAndView add(){
		ModelAndView modelAndView = new ModelAndView("background/excstu/cjd/add");
		List<StudentBean> listStudent = studentService.selectList(null);
		modelAndView.addObject("listStudent", listStudent);
		
		List<TeacherBean> listTeacher = teacherService.selectList(null);
		modelAndView.addObject("listTeacher", listTeacher);
		
		//学霸成绩单年级
		BaseTwoBean baseTwoBean = new BaseTwoBean();
		baseTwoBean.setBotype("年级");
		List<BaseTwoBean> listGrade = baseTwoService.selectList(baseTwoBean);
		modelAndView.addObject("listGrade", listGrade);
		return modelAndView;
	}
	
	//编辑优秀学员
	@RequestMapping("/insert")
	public void insert(ExcellentStudentBean bean,HttpServletResponse response) {
		bean.setId(CommonFunImpl.uuid("ES"));
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String arr[] = bean.getName().split(";");
		bean.setStuid(arr[0]);
		bean.setName(arr[1]);
		arr = null;
		arr = bean.getTeaname().split(";");
		bean.setTeaid(arr[0]);
		bean.setTeaname(arr[1]);
		String retVal = esService.insert(bean) > 0 ? "1" : "0";
		// 返回值
		try {
			PrintWriter out = response.getWriter();
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}
	}
	
	/*
	 * 更新页面信息
	 */
	@RequestMapping("/update")
	public void update(ExcellentStudentBean bean,HttpServletResponse response) {
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String arr[] = bean.getName().split(";");
		bean.setStuid(arr[0]);
		bean.setName(arr[1]);
		arr = null;
		arr = bean.getTeaname().split(";");
		bean.setTeaid(arr[0]);
		bean.setTeaname(arr[1]);
		String retVal = esService.update(bean) > 0 ? "1" : "0";
		// 返回值
		try {
			PrintWriter out = response.getWriter();
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(ExcellentStudentBean bean) {
		ModelAndView modelAndView = new ModelAndView("background/excstu/cjd/edit");
		bean = esService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		
		List<StudentBean> listStudent = studentService.selectList(null);
		modelAndView.addObject("listStudent", listStudent);
		
		List<TeacherBean> listTeacher = teacherService.selectList(null);
		modelAndView.addObject("listTeacher", listTeacher);
		
		//学霸成绩单类型
		BaseTwoBean baseTwoBean = new BaseTwoBean();
		baseTwoBean.setBotype("学霸成绩单类型");
		List<BaseTwoBean> listLeixing = baseTwoService.selectList(baseTwoBean);
		modelAndView.addObject("listLeixing", listLeixing);
		
		//学霸成绩单年级
		baseTwoBean.setBotype("年级");
		List<BaseTwoBean> listGrade = baseTwoService.selectList(baseTwoBean);
		modelAndView.addObject("listGrade", listGrade);
		
		return modelAndView;
	}
	
	// 删除班级 4
	@RequestMapping("/delete")
	public void Delete(ExcellentStudentBean bean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		List<String> list = new ArrayList<String>();
		int ret = 1;
		ret = esService.delete(bean);
		if (ret > 0) {
			list.add("1");
			list.add("删除成功!");
		} else {
			list.add("0");
			list.add("删除失败!");
		}
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			String s = JSONArray.fromObject(list).toString();
			out.print(s);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
