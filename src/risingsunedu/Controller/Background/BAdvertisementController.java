package risingsunedu.Controller.Background;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.AdvertisementBean;
import risingsunedu.Common.Impl.CommonFunImpl;
import risingsunedu.Service.IAdvertisementService;
import framework.base.common.Pager;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/b_adv")
public class BAdvertisementController extends BaseController {

	/**
	 * 首页广告控制层
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IAdvertisementService advertisementService;

	@Override
	public void setLogger() {
	}
	
	@RequestMapping("/index")
	public ModelAndView index(Pager<AdvertisementBean> pager){
		ModelAndView modelAndView = new ModelAndView("background/advertisement/index");
		pager = advertisementService.selectPage(null, pager);
		modelAndView.addObject("pager", pager);
		return modelAndView;
	}
	
	@RequestMapping("/add")
	public ModelAndView add(){
		ModelAndView modelAndView = new ModelAndView("background/advertisement/add");
		return modelAndView;
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(AdvertisementBean bean){
		ModelAndView modelAndView = new ModelAndView("background/advertisement/edit");
		bean = advertisementService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		return modelAndView;
	}
	
	@RequestMapping("/insert")
	public void insert(AdvertisementBean bean,HttpServletResponse response){
		bean.setId(CommonFunImpl.uuid("A"));
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String retVal;
		// 如果插入成功
		if (advertisementService.insert(bean) > 0) {
			retVal = "1";
		} else {
			retVal = "0";
		}

		// 返回值
		try {
			PrintWriter out = response.getWriter();
			System.out.print(retVal);
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}
	}
	
	@RequestMapping("/update")
	public void update(AdvertisementBean bean,HttpServletResponse response){
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String retVal;
		// 如果插入成功
		if (advertisementService.update(bean) > 0) {
			retVal = "1";
		} else {
			retVal = "0";
		}

		// 返回值
		try {
			PrintWriter out = response.getWriter();
			System.out.print(retVal);
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}
	}
	
	@RequestMapping("/delete")
	public ModelAndView Delete(AdvertisementBean bean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		List<String> list = new ArrayList<String>();
		int ret = 1;
		ret = advertisementService.delete(bean);
		if (ret > 0) {
			list.add("1");
			list.add("删除成功!");
		} else {
			list.add("0");
			list.add("删除失败!");
		}
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			String s = JSONArray.fromObject(list).toString();
			out.print(s);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	

}
