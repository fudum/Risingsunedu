package risingsunedu.Controller.Background;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.BaseTwoBean;
import risingsunedu.Bean.CeshiBean;
import risingsunedu.Bean.StudentBean;
import risingsunedu.Common.Impl.CommonConstant;
import risingsunedu.Common.Impl.CommonFunImpl;
import risingsunedu.Common.Impl.OSinfo;
import risingsunedu.Service.IBaseTwoService;
import risingsunedu.Service.ICeshiService;
import risingsunedu.Service.IStudentService;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/b_ceshi")
public class BCeshiController extends BaseController {

	/**
	 * 后台作业控制层
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	ICeshiService ceshiService;
	
	@Autowired
	IStudentService studentService;
	
	@Autowired
	IBaseTwoService basetwoService;

	@Override
	public void setLogger() {		
	}
	
	@RequestMapping("/index")
	public ModelAndView index(CeshiBean bean){
		ModelAndView modelAndView = new ModelAndView("background/user/student/ceshi/index");
		List<CeshiBean> list = ceshiService.selectList(bean);
		StudentBean studentBean = new StudentBean();
		studentBean.setId(bean.getStuid());
		studentBean = studentService.selectOne(studentBean);
		modelAndView.addObject("studentBean", studentBean);
		modelAndView.addObject("list", list);
		return modelAndView;
	}
	
	@RequestMapping("/add")
	public ModelAndView add(String stuid) {
		ModelAndView modelAndView = new ModelAndView("background/user/student/ceshi/add");
		modelAndView.addObject("stuid", stuid);
		
		//基本信息
		List<BaseTwoBean> listbt = basetwoService.selectList(null);
		List<BaseTwoBean> listSubject = new ArrayList<BaseTwoBean>();
		for (BaseTwoBean baseTwoBean : listbt) {
			switch (baseTwoBean.getBotype()) {
			case "科目":
				listSubject.add(baseTwoBean);
				break;
			}
		}
		modelAndView.addObject("listSubject", listSubject);
		return modelAndView;
	}
	

	@RequestMapping("/edit")
	public ModelAndView edit(CeshiBean	bean) {
		ModelAndView modelAndView = new ModelAndView("background/user/student/ceshi/edit");
		bean = ceshiService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		
		//基本信息
		List<BaseTwoBean> listbt = basetwoService.selectList(null);
		List<BaseTwoBean> listSubject = new ArrayList<BaseTwoBean>();
		for (BaseTwoBean baseTwoBean : listbt) {
			switch (baseTwoBean.getBotype()) {
			case "科目":
				listSubject.add(baseTwoBean);
				break;
			}
		}
		modelAndView.addObject("listSubject", listSubject);
		return modelAndView;
	}
	
	@RequestMapping("/insert")
	public void insert(CeshiBean bean,HttpServletResponse response){
		response.setContentType("text/html");
		bean.setId(CommonFunImpl.uuid("Z"));
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String ret ="";
		JSONObject jb = new JSONObject();
		if (ceshiService.insert(bean) > 0) {
			ret = "1";
		}
		else{
			ret = "0";
		}
		jb.put("ret", ret);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/update")
	public void update(CeshiBean bean,HttpServletResponse response){
		response.setContentType("text/html");
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String ret ="";
		JSONObject jb = new JSONObject();
		if (ceshiService.update(bean) > 0) {
			ret = "1";
		}
		else{
			ret = "0";
		}
		jb.put("ret", ret);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping("/uploadOriginal")
	public void uploadOriginal(MultipartFile originalFile,HttpServletResponse response){
		response.setContentType("text/html");
		String fileName = "";
		boolean flag = false;
		if(originalFile != null){
			if (!originalFile.isEmpty()) {
				String path = OSinfo.isWindows() ? CommonConstant.w_peiyouceshiPath : CommonConstant.peiyouceshiPath;
				String tmp = originalFile.getOriginalFilename();
				tmp = tmp.substring(tmp.lastIndexOf('.'));
				fileName = CommonFunImpl.uuid32() + tmp;
				File targetFile = new File(path, fileName);
				if(!targetFile.exists()){  
		            targetFile.mkdirs();  
		        }  
				
				//保存  
		        try {  
		        	originalFile.transferTo(targetFile);  
		        	flag = true;
		        } catch (Exception e) {  
		            e.printStackTrace();  
		        } 
			}
		}
		String ret = (flag == true) ? "1":"0";
		JSONObject jb = new JSONObject();
		jb.put("ret", ret);
		jb.put("path",  fileName);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/uploadFinish")
	public void uploadFinish(MultipartFile finishFile,HttpServletResponse response){
		response.setContentType("text/html");
		String fileName = "";
		boolean flag = false;
		if(finishFile != null){
			if (!finishFile.isEmpty()) {
				String path = OSinfo.isWindows() ? CommonConstant.w_peiyouceshiPath : CommonConstant.peiyouceshiPath;
				String tmp = finishFile.getOriginalFilename();
				tmp = tmp.substring(tmp.lastIndexOf('.'));
				fileName = CommonFunImpl.uuid32() + tmp;
				File targetFile = new File(path, fileName);
				if(!targetFile.exists()){  
		            targetFile.mkdirs();  
		        }  
				
				//保存  
		        try {  
		        	finishFile.transferTo(targetFile);  
		        	flag = true;
		        } catch (Exception e) {  
		            e.printStackTrace();  
		        } 
			}
		}
		String ret = (flag == true) ? "1":"0";
		JSONObject jb = new JSONObject();
		jb.put("ret", ret);
		jb.put("path",  fileName);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// 删除班级 4
	@RequestMapping("/delete")
	public void Delete(CeshiBean bean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		List<String> list = new ArrayList<String>();
		int ret = 1;
		ret = ceshiService.delete(bean);
		if (ret > 0) {
			list.add("1");
			list.add("删除成功!");
		} else {
			list.add("0");
			list.add("删除失败!");
		}
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			String s = JSONArray.fromObject(list).toString();
			out.print(s);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/showZuoye")
	public ModelAndView showZuoye(String zuoyePath){
		ModelAndView modelAndView = new ModelAndView("background/user/student/zuoye/show");
		modelAndView.addObject("zuoyePath", zuoyePath);
		return modelAndView;
	}
	
}
