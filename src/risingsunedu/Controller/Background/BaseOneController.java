package risingsunedu.Controller.Background;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;





import risingsunedu.Bean.BaseOneBean;
import risingsunedu.Bean.BaseTwoBean;
import risingsunedu.Common.Impl.CommonFunImpl;
import risingsunedu.Service.IBaseOneService;
import risingsunedu.Service.IBaseTwoService;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/baseone")
public class BaseOneController extends BaseController {

	/**
	 * fudum yfj
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IBaseOneService baseOneService;
	
	@Autowired
	IBaseTwoService baseTwoService;

	@Override
	public void setLogger() {
		
	}
	
	@RequestMapping("/index")
	public ModelAndView index() {
		ModelAndView modelAndView = new ModelAndView("background/base/index");
		List<BaseOneBean> listOne = baseOneService.selectList(null);
		modelAndView.addObject("listone", listOne);
		
		List<BaseTwoBean> listTwo = baseTwoService.selectList(null);
		modelAndView.addObject("listtwo", listTwo);
		
		return modelAndView;
	}

	@RequestMapping("/add")
	public ModelAndView add() {
		ModelAndView modelAndView = new ModelAndView("background/base/add");
		return modelAndView;
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(BaseOneBean bean) {
		ModelAndView modelAndView = new ModelAndView("background/base/edit");
		bean = baseOneService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		return modelAndView;
	}
	
	@RequestMapping("/insert")
	public void insert(BaseOneBean bean, HttpServletResponse response) {
		response.setContentType("text/html");
		bean.setId(CommonFunImpl.uuid("BO"));
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String ret ="";
		JSONObject jb = new JSONObject();
		if (baseOneService.insert(bean) > 0) {
			ret = "1";
		}
		else{
			ret = "0";
		}
		jb.put("ret", ret);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/update")
	public void update(BaseOneBean bean, HttpServletResponse response) {
		response.setContentType("text/html");
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String ret ="";
		JSONObject jb = new JSONObject();
		if (baseOneService.update(bean) > 0) {
			ret = "1";
		}
		else{
			ret = "0";
		}
		jb.put("ret", ret);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/delete")
	public void delete(BaseOneBean bean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		List<String> list = new ArrayList<String>();
		int ret = 1;
		ret = baseOneService.delete(bean);
		if (ret > 0) {
			list.add("1");
			list.add("删除成功!");
		} else {
			list.add("0");
			list.add("删除失败!");
		}
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			String s = JSONArray.fromObject(list).toString();
			out.print(s);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
