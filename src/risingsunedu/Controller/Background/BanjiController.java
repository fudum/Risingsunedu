package risingsunedu.Controller.Background;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.BanjiBean;
import risingsunedu.Bean.BanjijiangciBean;
import risingsunedu.Bean.BanjijieciBean;
import risingsunedu.Bean.BaseTwoBean;
import risingsunedu.Bean.OpenkBean;
import risingsunedu.Bean.TeacherBean;
import risingsunedu.Common.Impl.CommonFunImpl;
import risingsunedu.Service.IBanjiService;
import risingsunedu.Service.IBanjijiangciService;
import risingsunedu.Service.IBanjijieciService;
import risingsunedu.Service.IBaseTwoService;
import risingsunedu.Service.ITeacherService;
import framework.base.common.Pager;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/banji")
public class BanjiController extends BaseController {

	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IBanjiService banjiService;
	
	@Autowired
	ITeacherService tService;
	
	@Autowired
	IBaseTwoService basetwoService;
	
	@Autowired
	IBanjijiangciService banjijiangciService;
	
	@Autowired
	IBanjijieciService banjijieciService;

	@Override
	public void setLogger() {
		logger = LoggerFactory.getLogger(getClass());
	}

	// 班级管理页面 //1
	@RequestMapping("/index")
	public ModelAndView index(Pager<BanjiBean> pager) {
		ModelAndView modelAndView = new ModelAndView("background/banji/banjimanage");
		pager = banjiService.selectPage(null, pager);
		modelAndView.addObject("pager", pager);
		return modelAndView;
	}

	// 添加班级页面 //2
	@RequestMapping("/banjiadd")
	public ModelAndView userAdd() {
		ModelAndView modelAndView = new ModelAndView("background/banji/banjiadd");
		//教师信息
		List<TeacherBean> list = tService.selectList(null);
		modelAndView.addObject("list", list);
		
		//基本信息
		List<BaseTwoBean> listbt = basetwoService.selectList(null);
		List<BaseTwoBean> listGrade = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listSubject = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listBanci = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listBjtype = new ArrayList<BaseTwoBean>();
		for (BaseTwoBean baseTwoBean : listbt) {
			switch (baseTwoBean.getBotype()) {
			case "年级":
				listGrade.add(baseTwoBean);
				break;
			case "科目":
				listSubject.add(baseTwoBean);
				break;
			case "班次":
				listBanci.add(baseTwoBean);
				break;
			case "班级类型":
				listBjtype.add(baseTwoBean);
				break;
			}
		}
		modelAndView.addObject("listGrade", listGrade);
		modelAndView.addObject("listSubject", listSubject);
		modelAndView.addObject("listBanci", listBanci);
		modelAndView.addObject("listBjtype", listBjtype);
		return modelAndView;
	}

	// 班级插入 //3
	@RequestMapping("/banjiinsert")
	public ModelAndView userInsert(BanjiBean bean, HttpServletResponse response) {
		
		//设置班级属性
		bean.setId(CommonFunImpl.uuid("B"));
		String arr[] = bean.getTea_id().split(";");
		bean.setTea_name(arr[1]);
		bean.setTea_id(arr[0]);
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String retVal;
		// 如果插入成功
		if (banjiService.insert(bean) > 0) {
			retVal = "1";
		} else {
			retVal = "0";
		}

		// 返回值
		try {
			PrintWriter out = response.getWriter();
			System.out.print(retVal);
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}

		return null;
	}

	// 删除班级 4
	@RequestMapping("/delete")
	public ModelAndView Delete(BanjiBean BanjiBean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		List<String> list = new ArrayList<String>();
		int ret = 1;
		ret = banjiService.delete(BanjiBean);
		if (ret > 0) {
			list.add("1");
			list.add("删除成功!");
		} else {
			list.add("0");
			list.add("删除失败!");
		}
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			String s = JSONArray.fromObject(list).toString();
			out.print(s);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	// 上一页 5
	@RequestMapping("/frontpage")
	public ModelAndView frontPage(int pageNum, int totalPage) {
		ModelAndView modelAndView = new ModelAndView();

		Pager<BanjiBean> pager = new Pager<BanjiBean>();

		if (pageNum > 1) {// 如果小于总页数
			pageNum--;
		} else {// 返回首页
			pageNum = totalPage;
		}
		pager.setCurtPage(pageNum);

		Pager<BanjiBean> pageT = banjiService.selectPage(null, pager);

		// 数据list
		modelAndView.addObject("list", pageT.getPageList());
		// 总页数
		modelAndView.addObject("totalPage", pageT.getTotalPage());
		// 当前页
		modelAndView.addObject("countPerPage", pageT.getCurtPage());

		modelAndView.setViewName("background/banji/banjimanage");
		return modelAndView;
	}

	// 下一页 6
	@RequestMapping("/nextpage")
	public ModelAndView nextPage(int pageNum, int totalPage) {
		ModelAndView modelAndView = new ModelAndView();

		Pager<BanjiBean> pager = new Pager<BanjiBean>();

		if (pageNum < totalPage) {// 如果小于总页数
			pageNum++;
		} else {// 返回首页
			pageNum = 1;
		}
		pager.setCurtPage(pageNum);

		Pager<BanjiBean> userPage = banjiService.selectPage(null, pager);

		// 数据list
		modelAndView.addObject("list", userPage.getPageList());
		// 总页数
		modelAndView.addObject("totalPage", userPage.getTotalPage());
		// 当前页
		modelAndView.addObject("countPerPage", userPage.getCurtPage());

		modelAndView.setViewName("background/banji/banjimanage");
		return modelAndView;
	}

	// 最后一页 7
	@RequestMapping("/lastpage")
	public ModelAndView lastPage(int totalPage) {
		ModelAndView modelAndView = new ModelAndView();

		Pager<BanjiBean> pager = new Pager<BanjiBean>();
		pager.setCurtPage(totalPage);

		Pager<BanjiBean> userPage = banjiService.selectPage(null, pager);

		// 数据list
		modelAndView.addObject("list", userPage.getPageList());
		// 总页数
		modelAndView.addObject("totalPage", userPage.getTotalPage());
		// 当前页
		modelAndView.addObject("countPerPage", userPage.getCurtPage());

		modelAndView.setViewName("background/banji/banjimanage");
		return modelAndView;
	}
	
	/*
	 * 组装编辑编号
	 */
	public String getClassId(BanjiBean bean) {
		String retVal ="C";
		//科目
		switch (bean.getSubject()) {
		case "数学":
			retVal +="M";
			break;
		case "物理":
			retVal +="P";
			break;
		case "化学":
			retVal +="C";
			break;
		case "英语":
			retVal +="E";
			break;
		case "语文":
			retVal +="L";
			break;
		}
		//年级
		switch (bean.getGrade()) {
		case "九年级":
			retVal +="9";
			break;
		case "八年级":
			retVal +="8";
			break;
		case "七年级":
			retVal +="7";
			break;
		}
		//年份
		retVal += CommonFunImpl.currentTime(4);
		
		//最大编号
		String str = banjiService.selectMaxId(bean);
		if (str == null || str == "") {
			str = "0001";
		}
		else {
			str = str.substring(str.length() - 4);
			int i = Integer.parseInt(str) + 1;	
			str = String.valueOf(i);
			for (int j = 4; j > str.length();) {
				str = "0" + str;
			}
		}
		
		retVal += str;
		return retVal;
	}
	
	// 编辑班级页面 //6
	@RequestMapping("/banjiedit")
	public ModelAndView userEdit(BanjiBean bean) {
		ModelAndView modelAndView = new ModelAndView("background/banji/banjiedit");
		//班级信息
		bean = banjiService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		//教师信息
		List<TeacherBean> list = tService.selectList(null);
		modelAndView.addObject("list", list);
		
		//基本信息
		List<BaseTwoBean> listbt = basetwoService.selectList(null);
		List<BaseTwoBean> listGrade = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listSubject = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listBanci = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listBjtype = new ArrayList<BaseTwoBean>();
		for (BaseTwoBean baseTwoBean : listbt) {
			switch (baseTwoBean.getBotype()) {
			case "年级":
				listGrade.add(baseTwoBean);
				break;
			case "科目":
				listSubject.add(baseTwoBean);
				break;
			case "班次":
				listBanci.add(baseTwoBean);
				break;
			case "班级类型":
				listBjtype.add(baseTwoBean);
				break;
			}
		}
		modelAndView.addObject("listGrade", listGrade);
		modelAndView.addObject("listSubject", listSubject);
		modelAndView.addObject("listBanci", listBanci);
		modelAndView.addObject("listBjtype", listBjtype);
		
		//班级讲次
		BanjijiangciBean banjijiangciBean = new BanjijiangciBean();
		banjijiangciBean.setBanji_id(bean.getId());
		List<BanjijiangciBean> listjiangci = banjijiangciService.selectList(banjijiangciBean);
		modelAndView.addObject("listjiangci", listjiangci);
		
		//班级节次
		List<BanjijieciBean> listjieci = banjijieciService.selectListByBanjiId(bean.getId());
		modelAndView.addObject("listjieci", listjieci);
		
		return modelAndView;
	}
	
	// 班级插入 //3
	@RequestMapping("/banjiupdate")
	public ModelAndView userUpdate(BanjiBean bean, HttpServletResponse response) {
		String retVal;
		String arr[] = bean.getTea_id().split(";");
		bean.setTea_name(arr[1]);
		bean.setTea_id(arr[0]);
		// 如果插入成功
		if (banjiService.update(bean) > 0) {
			retVal = "1";
		} else {
			retVal = "0";
		}
		// 返回值
		try {
			PrintWriter out = response.getWriter();
			System.out.print(retVal);
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}
		return null;
	}
	
	@RequestMapping("/search")
	public void search(BanjiBean bean,Pager<OpenkBean> pager,HttpServletResponse response){
		
	}

}
