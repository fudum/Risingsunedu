package risingsunedu.Controller.Background;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.BaseTwoBean;
import risingsunedu.Bean.StudentKechengBean;
import risingsunedu.Common.Impl.CommonFunImpl;
import risingsunedu.Service.IBaseTwoService;
import risingsunedu.Service.IStudentKechengService;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/b_student_kc")
public class BStudentKechengController extends BaseController{

	/**
	 * 学生课程控制层
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IBaseTwoService basetwoService;
	
	@Autowired
	IStudentKechengService studentKechengService;

	@Override
	public void setLogger() {		
	}
	
	@RequestMapping("/add")
	public ModelAndView add(String stuid){
		ModelAndView modelAndView = new ModelAndView("background/user/student/kc/add");
		modelAndView.addObject("stuid", stuid);
		//基本信息
		List<BaseTwoBean> listbt = basetwoService.selectList(null);
		List<BaseTwoBean> listGrade = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listSubject = new ArrayList<BaseTwoBean>();
		for (BaseTwoBean baseTwoBean : listbt) {
			switch (baseTwoBean.getBotype()) {
			case "年级":
				listGrade.add(baseTwoBean);
				break;
			case "科目":
				listSubject.add(baseTwoBean);
				break;
			}
		}
		modelAndView.addObject("listGrade", listGrade);
		modelAndView.addObject("listSubject", listSubject);
		
		return modelAndView;
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(StudentKechengBean bean){
		ModelAndView modelAndView = new ModelAndView("background/user/student/kc/edit");
		bean = studentKechengService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		
		//基本信息
		List<BaseTwoBean> listbt = basetwoService.selectList(null);
		List<BaseTwoBean> listGrade = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listSubject = new ArrayList<BaseTwoBean>();
		for (BaseTwoBean baseTwoBean : listbt) {
			switch (baseTwoBean.getBotype()) {
			case "年级":
				listGrade.add(baseTwoBean);
				break;
			case "科目":
				listSubject.add(baseTwoBean);
				break;
			}
		}
		modelAndView.addObject("listGrade", listGrade);
		modelAndView.addObject("listSubject", listSubject);
		
		return modelAndView;
	}
	
	@RequestMapping("/insert")
	public void insert(StudentKechengBean bean,HttpServletResponse response){
		response.setContentType("text/html");
		bean.setId(CommonFunImpl.uuid("SK"));
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String ret ="";
		JSONObject jb = new JSONObject();
		if (studentKechengService.insert(bean) > 0) {
			ret = "1";
		}
		else{
			ret = "0";
		}
		jb.put("ret", ret);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/update")
	public void update(StudentKechengBean bean,HttpServletResponse response){
		response.setContentType("text/html");
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String ret ="";
		JSONObject jb = new JSONObject();
		if (studentKechengService.update(bean) > 0) {
			ret = "1";
		}
		else{
			ret = "0";
		}
		jb.put("ret", ret);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// 删除班级 4
	@RequestMapping("/delete")
	public void Delete(StudentKechengBean bean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		List<String> list = new ArrayList<String>();
		int ret = 1;
		ret = studentKechengService.delete(bean);
		if (ret > 0) {
			list.add("1");
			list.add("删除成功!");
		} else {
			list.add("0");
			list.add("删除失败!");
		}
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			String s = JSONArray.fromObject(list).toString();
			out.print(s);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


}
