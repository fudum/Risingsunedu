package risingsunedu.Controller.Background;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.BaseTwoBean;
import risingsunedu.Bean.TeacherBean;
import risingsunedu.Common.Impl.CommonConstant;
import risingsunedu.Common.Impl.CommonFunImpl;
import risingsunedu.Common.Impl.OSinfo;
import risingsunedu.Service.IBaseTwoService;
import risingsunedu.Service.ITeacherService;
import framework.base.common.Pager;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/teacher")
public class TeacherController extends BaseController {

	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	ITeacherService teacherService;
	
	@Autowired
	IBaseTwoService baseTwoService;

	@Override
	public void setLogger() {
		logger = LoggerFactory.getLogger(getClass());
	}

	@RequestMapping("/index")
	public ModelAndView index(Pager<TeacherBean> pager) {
		ModelAndView modelAndView = new ModelAndView("background/teacher/base/teachermanage");
		pager = teacherService.selectPage(null, pager);
		modelAndView.addObject("pager", pager);
		return modelAndView;
	}

	@RequestMapping("/add")
	public ModelAndView add() {
		ModelAndView modelAndView = new ModelAndView("background/teacher/base/teacheradd");
		BaseTwoBean baseTwoBean = new BaseTwoBean();
		//科目
		baseTwoBean.setBotype("科目");
		List<BaseTwoBean> listSubject = baseTwoService.selectList(baseTwoBean);
		modelAndView.addObject("listSubject", listSubject);
		//年级
		baseTwoBean.setBotype("年级");
		List<BaseTwoBean> listGrade = baseTwoService.selectList(baseTwoBean);
		modelAndView.addObject("listGrade", listGrade);
		//带班类型
		baseTwoBean.setBotype("教师带班类型");
		List<BaseTwoBean> listTechang = baseTwoService.selectList(baseTwoBean);
		modelAndView.addObject("listTechang", listTechang);
		
		return modelAndView;
	}

	@RequestMapping("/insert")
	public ModelAndView teacherInsert(TeacherBean bean,MultipartFile headFile, MultipartFile picFile, HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("background/teacher/base/teacheredit");
		bean.setId(CommonFunImpl.uuid("T"));//设置主键
		bean.setInserttime(CommonFunImpl.currentTime(1));//设置插入时间
		
		//头像不为空时候上传图片
		if (!headFile.isEmpty()) {
			String path = "";
			if(OSinfo.isWindows()){//判断是windows服务器
				path = CommonConstant.w_teacherPath;
			}else {
				path = CommonConstant.teacherPath;
			}
		
			String tmp = headFile.getOriginalFilename();
			tmp = tmp.substring(tmp.lastIndexOf('.'));
			String fileName = CommonFunImpl.uuid32() + tmp;
			
			File targetFile = new File(path, fileName);
			if(!targetFile.exists()){  
	            targetFile.mkdirs();  
	        }  
			
			//保存  
	        try {  
	        	headFile.transferTo(targetFile);  
	        } catch (Exception e) {  
	            e.printStackTrace();  
	        }  
	        
	        //设置bean中的内容
	        bean.setHeadpath("teacher/" + fileName);
		}
		
		//图片不为空时候上传图片
		if (!picFile.isEmpty()) {
			String path = "";
			if(OSinfo.isWindows()){//判断是windows服务器
				path = CommonConstant.w_teacherPath;
			}else {
				path = CommonConstant.teacherPath;
			}
			String tmp = picFile.getOriginalFilename();
			tmp = tmp.substring(tmp.lastIndexOf('.'));
			String fileName = CommonFunImpl.uuid32() + tmp;
			
			File targetFile = new File(path, fileName);
			if(!targetFile.exists()){  
	            targetFile.mkdirs();  
	        }  
			
			//保存  
	        try {  
	        	picFile.transferTo(targetFile);  
	        } catch (Exception e) {  
	            e.printStackTrace();  
	        }  
	        //设置bean中的内容
	        bean.setPicpath("teacher/" + fileName);
		}
		
		String tipString;
		//如果插入成功
		if (teacherService.insert(bean) > 0) {
			tipString = "ok";
		}
		else {
			tipString = "error";
		}
		modelAndView.addObject("tea", bean);
		modelAndView.addObject("tip", tipString);
		BaseTwoBean baseTwoBean = new BaseTwoBean();
		//科目
		baseTwoBean.setBotype("科目");
		List<BaseTwoBean> listSubject = baseTwoService.selectList(baseTwoBean);
		modelAndView.addObject("listSubject", listSubject);
		//年级
		baseTwoBean.setBotype("年级");
		List<BaseTwoBean> listGrade = baseTwoService.selectList(baseTwoBean);
		modelAndView.addObject("listGrade", listGrade);
		//带班类型
		baseTwoBean.setBotype("教师带班类型");
		List<BaseTwoBean> listTechang = baseTwoService.selectList(baseTwoBean);
		modelAndView.addObject("listTechang", listTechang);
		return modelAndView;
	}

	@RequestMapping("/delete")
	public ModelAndView Delete(TeacherBean TeacherBean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		List<String> list = new ArrayList<String>();
		int ret = 1;
		ret = teacherService.delete(TeacherBean);
		if (ret > 0) {
			list.add("1");
			list.add("删除成功!");
		} else {
			list.add("0");
			list.add("删除失败!");
		}
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			String s = JSONArray.fromObject(list).toString();
			out.print(s);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	// 编辑班级页面 //6
	@RequestMapping("/edit")
	public ModelAndView edit(TeacherBean bean) {
		ModelAndView modelAndView = new ModelAndView("background/teacher/base/teacheredit");
		bean = teacherService.selectOne(bean);
		modelAndView.addObject("tea", bean);
		
		BaseTwoBean baseTwoBean = new BaseTwoBean();
		//科目
		baseTwoBean.setBotype("科目");
		List<BaseTwoBean> listSubject = baseTwoService.selectList(baseTwoBean);
		modelAndView.addObject("listSubject", listSubject);
		//年级
		baseTwoBean.setBotype("年级");
		List<BaseTwoBean> listGrade = baseTwoService.selectList(baseTwoBean);
		modelAndView.addObject("listGrade", listGrade);
		//带班类型
		baseTwoBean.setBotype("教师带班类型");
		List<BaseTwoBean> listTechang = baseTwoService.selectList(baseTwoBean);
		modelAndView.addObject("listTechang", listTechang);
		
		return modelAndView;
	}
	
	// 班级插入 //3
	@RequestMapping("/update")
	public ModelAndView update(TeacherBean bean,MultipartFile headFile, MultipartFile picFile, HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("background/teacher/base/teacheredit");
		//图片不为空时候上传图片
		if (!headFile.isEmpty()) {
			String path = OSinfo.isWindows()? CommonConstant.w_teacherPath: CommonConstant.teacherPath;
			String tmp = headFile.getOriginalFilename();
			tmp = tmp.substring(tmp.lastIndexOf('.'));
			String fileName = CommonFunImpl.uuid32() + tmp;
			File targetFile = new File(path, fileName);
			if(!targetFile.exists()){  
	            targetFile.mkdirs();  
	        }  
			//保存  
	        try {  
	        	headFile.transferTo(targetFile);  
	        } catch (Exception e) {  
	            e.printStackTrace();  
	        }  
	        //设置bean中的内容
	        bean.setHeadpath("teacher/" + fileName);
		}
		
		//图片不为空时候上传图片
		if (!picFile.isEmpty()) {
			String path = OSinfo.isWindows()? CommonConstant.w_teacherPath: CommonConstant.teacherPath;
			String tmp = picFile.getOriginalFilename();
			tmp = tmp.substring(tmp.lastIndexOf('.'));
			String fileName = CommonFunImpl.uuid32() + tmp;
			File targetFile = new File(path, fileName);
			if(!targetFile.exists()){  
	            targetFile.mkdirs();  
	        }  
			//保存  
	        try {  
	        	picFile.transferTo(targetFile);  
	        } catch (Exception e) {  
	            e.printStackTrace();  
	        }  
	        //设置bean中的内容
	        bean.setPicpath("teacher/" + fileName);
		}
		
		String tipString;
		//如果编辑成功
		if (teacherService.update(bean) > 0) {
			tipString = "ok";
		}
		else {
			tipString = "error";
		}
		//从数据库中读取数据
		TeacherBean bean2 = new TeacherBean();
		bean2.setId(bean.getId());
		bean = teacherService.selectOne(bean2);
		modelAndView.addObject("tea", bean);
		modelAndView.addObject("tip",tipString);
		
		BaseTwoBean baseTwoBean = new BaseTwoBean();
		//科目
		baseTwoBean.setBotype("科目");
		List<BaseTwoBean> listSubject = baseTwoService.selectList(baseTwoBean);
		modelAndView.addObject("listSubject", listSubject);
		//年级
		baseTwoBean.setBotype("年级");
		List<BaseTwoBean> listGrade = baseTwoService.selectList(baseTwoBean);
		modelAndView.addObject("listGrade", listGrade);
		//带班类型
		baseTwoBean.setBotype("教师带班类型");
		List<BaseTwoBean> listTechang = baseTwoService.selectList(baseTwoBean);
		modelAndView.addObject("listTechang", listTechang);
		
		return modelAndView;
	}
	
	@RequestMapping("/vedioPreview")
	public ModelAndView vedioPreview(String vp) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("background/teacher/vediopreview");
		modelAndView.addObject("vp", vp);
		return modelAndView;
	}
	
	
	/**
	 * 教师附加信息
	 * @return
	 */
	@RequestMapping("/teacheraddition")
	public ModelAndView teacherAddition() {
		ModelAndView modelAndView = new ModelAndView("background/teacher/addition/teacheraddition");
		Pager<TeacherBean> pager = new Pager<TeacherBean>();
		pager = teacherService.selectPage(null, pager);
		modelAndView.addObject("pager", pager);
		return modelAndView;
	}
	
}
