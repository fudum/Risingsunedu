package risingsunedu.Controller.Background;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.OpenkBean;
import risingsunedu.Bean.OpenkjiangciBean;
import risingsunedu.Common.Impl.CommonFunImpl;
import risingsunedu.Service.IOpenkService;
import risingsunedu.Service.IOpenkjiangciService;
import framework.base.common.Pager;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/openkjc")
public class OpenkjiangciController extends BaseController {

	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IOpenkService oService;
	
	@Autowired
	IOpenkjiangciService openkjiangciService;

	@Override
	public void setLogger() {
	}
	
	@RequestMapping("/index")
	public ModelAndView index(Pager<OpenkBean> pager) {
		ModelAndView modelAndView = new ModelAndView("background/openk/jiangci/index");
		pager = oService.selectPage(null, pager);
		modelAndView.addObject("pager", pager);
		return modelAndView;
	}
	
	@RequestMapping("/manage")
	public ModelAndView manage(OpenkjiangciBean bean) {
		ModelAndView modelAndView = new ModelAndView("background/openk/jiangci/manage");
		List<OpenkjiangciBean> list =  openkjiangciService.selectList(bean);
		modelAndView.addObject("list", list);
		modelAndView.addObject("bean", bean);
		return modelAndView;
	}
	
	@RequestMapping("/add")
	public ModelAndView add(OpenkjiangciBean bean) {
		ModelAndView modelAndView = new ModelAndView("background/openk/jiangci/add");
		modelAndView.addObject("openk_id", bean.getOpenk_id());
		return modelAndView;
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(OpenkjiangciBean bean) {
		ModelAndView modelAndView = new ModelAndView("background/openk/jiangci/edit");
		bean = openkjiangciService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		return modelAndView;
	}
	
	@RequestMapping("/insert")
	public void insert(OpenkjiangciBean bean, HttpServletResponse response){
		response.setContentType("text/html");
		bean.setId(CommonFunImpl.uuid("Ojc"));
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String ret ="";
		JSONObject jb = new JSONObject();
		
		//更新大于它的jorder
		OpenkjiangciBean bean3 = new OpenkjiangciBean();
		bean3.setOpenk_id(bean.getOpenk_id());
		bean3.setJorder(bean.getJorder());
		if (openkjiangciService.selectOne(bean3) != null) {
			OpenkjiangciBean bean2 = new OpenkjiangciBean();
			bean2.setOpenk_id(bean.getOpenk_id());
			bean2.setJorder(bean.getJorder());
			openkjiangciService.updateJorder(bean2);
		}
		
		if (openkjiangciService.insert(bean) > 0) {
			ret = "1";
		}
		else{
			ret = "0";
		}
		jb.put("ret", ret);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/update")
	public void update(OpenkjiangciBean bean, HttpServletResponse response){
		response.setContentType("text/html");
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String ret ="";
		JSONObject jb = new JSONObject();
		
		//更新大于它的jorder
		OpenkjiangciBean bean3 = new OpenkjiangciBean();
		bean3.setOpenk_id(bean.getOpenk_id());
		bean3.setJorder(bean.getJorder());
		if (openkjiangciService.selectOne(bean3) != null) {
			OpenkjiangciBean bean2 = new OpenkjiangciBean();
			bean2.setOpenk_id(bean.getOpenk_id());
			bean2.setJorder(bean.getJorder());
			openkjiangciService.updateJorder(bean2);
		}
		
		if (openkjiangciService.update(bean) > 0) {
			ret = "1";
		}
		else{
			ret = "0";
		}
		jb.put("ret", ret);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/delete")
	public void delete(OpenkjiangciBean bean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		List<String> list = new ArrayList<String>();
		int ret = 1;
		ret = openkjiangciService.delete(bean);
		if (ret > 0) {
			list.add("1");
			list.add("删除成功!");
		} else {
			list.add("0");
			list.add("删除失败!");
		}
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			String s = JSONArray.fromObject(list).toString();
			out.print(s);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
