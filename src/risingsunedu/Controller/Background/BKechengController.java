package risingsunedu.Controller.Background;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;



import risingsunedu.Bean.BaseTwoBean;
import risingsunedu.Bean.KechengBean;
import risingsunedu.Bean.KechengjiangciBean;
import risingsunedu.Bean.KechengjieciBean;
import risingsunedu.Bean.TeacherBean;
import risingsunedu.Common.Impl.CommonFunImpl;
import risingsunedu.Service.IBaseTwoService;
import risingsunedu.Service.IKechengService;
import risingsunedu.Service.IKechengjiangciService;
import risingsunedu.Service.IKechengjieciService;
import risingsunedu.Service.ITeacherService;
import framework.base.common.Pager;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/b_kecheng")
public class BKechengController extends BaseController {

	/**
	 * 后台课程controller
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IKechengService kechengService;
	
	@Autowired
	ITeacherService teacherService;
	
	@Autowired
	IBaseTwoService baseTwoService;
	
	@Autowired
	IKechengjiangciService kechengjiangciService;
	
	@Autowired
	IKechengjieciService kechengjieciService;
	
	@Override
	public void setLogger() {		
	}
	
	@RequestMapping("/index")
	public ModelAndView index(Pager<KechengBean> pager){
		pager.setCountPerPage(100);
		pager = kechengService.selectPage(null, pager);
		ModelAndView modelAndView = new ModelAndView("background/kecheng/index");
		modelAndView.addObject("pager", pager);
		return modelAndView;
	}
	
	@RequestMapping("/add")
	public ModelAndView add(){
		ModelAndView modelAndView = new ModelAndView("background/kecheng/add");
		//教师信息
		List<TeacherBean> list = teacherService.selectList(null);
		modelAndView.addObject("list", list);
		//基本信息
		List<BaseTwoBean> listbt = baseTwoService.selectList(null);
		List<BaseTwoBean> listGrade = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listSubject = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listBanci = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listBjtype = new ArrayList<BaseTwoBean>();
		for (BaseTwoBean baseTwoBean : listbt) {
			switch (baseTwoBean.getBotype()) {
			case "年级":
				listGrade.add(baseTwoBean);
				break;
			case "科目":
				listSubject.add(baseTwoBean);
				break;
			case "班次":
				listBanci.add(baseTwoBean);
				break;
			case "班级类型":
				listBjtype.add(baseTwoBean);
				break;
			}
		}
		modelAndView.addObject("listGrade", listGrade);
		modelAndView.addObject("listSubject", listSubject);
		modelAndView.addObject("listBanci", listBanci);
		modelAndView.addObject("listBjtype", listBjtype);
		return modelAndView;
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(KechengBean bean){
		ModelAndView modelAndView = new ModelAndView("background/kecheng/edit");
		//课程信息
		bean = kechengService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		
		//课程讲次
		KechengjiangciBean kechengjiangciBean = new KechengjiangciBean();
		kechengjiangciBean.setKecheng_id(bean.getId());
		List<KechengjiangciBean> listjiangci = kechengjiangciService.selectList(kechengjiangciBean);
		modelAndView.addObject("listjiangci", listjiangci);
		
		//班级节次
		List<KechengjieciBean> listjieci = kechengjieciService.selectListByKechengId(bean.getId());
		modelAndView.addObject("listjieci", listjieci);
		
		//教师信息
		List<TeacherBean> list = teacherService.selectList(null);
		modelAndView.addObject("list", list);
		//基本信息
		List<BaseTwoBean> listbt = baseTwoService.selectList(null);
		List<BaseTwoBean> listGrade = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listSubject = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listBanci = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listBjtype = new ArrayList<BaseTwoBean>();
		for (BaseTwoBean baseTwoBean : listbt) {
			switch (baseTwoBean.getBotype()) {
			case "年级":
				listGrade.add(baseTwoBean);
				break;
			case "科目":
				listSubject.add(baseTwoBean);
				break;
			case "班次":
				listBanci.add(baseTwoBean);
				break;
			case "班级类型":
				listBjtype.add(baseTwoBean);
				break;
			}
		}
		modelAndView.addObject("listGrade", listGrade);
		modelAndView.addObject("listSubject", listSubject);
		modelAndView.addObject("listBanci", listBanci);
		modelAndView.addObject("listBjtype", listBjtype);
		return modelAndView;
	}
	
	@RequestMapping("/insert")
	public void insert(KechengBean bean, HttpServletResponse response){
		//设置班级属性
		bean.setId(CommonFunImpl.uuid("B"));
		String arr[] = bean.getTea_id().split(";");
		bean.setTea_name(arr[1]);
		bean.setTea_id(arr[0]);
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String retVal;
		// 如果插入成功
		if (kechengService.insert(bean) > 0) {
			retVal = "1";
		} else {
			retVal = "0";
		}

		// 返回值
		try {
			PrintWriter out = response.getWriter();
			System.out.print(retVal);
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}
	}
	
	@RequestMapping("/update")
	public void update(KechengBean bean,HttpServletResponse response){
		String retVal;
		String arr[] = bean.getTea_id().split(";");
		bean.setTea_name(arr[1]);
		bean.setTea_id(arr[0]);
		bean.setInserttime(CommonFunImpl.currentTime(1));
		// 如果插入成功
		if (kechengService.update(bean) > 0) {
			retVal = "1";
		} else {
			retVal = "0";
		}
		// 返回值
		try {
			PrintWriter out = response.getWriter();
			System.out.print(retVal);
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();//输出异常
		}
	}
	
	@RequestMapping("/updateCatalog")
	public void updateCatalog(KechengBean bean,HttpServletResponse response){
		String retVal;
		// 如果插入成功
		if (kechengService.update(bean) > 0) {
			retVal = "1";
		} else {
			retVal = "0";
		}
		// 返回值
		try {
			PrintWriter out = response.getWriter();
			System.out.print(retVal);
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();//输出异常
		}
	}
	
	// 删除班级 4
	@RequestMapping("/delete")
	public ModelAndView Delete(KechengBean bean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		List<String> list = new ArrayList<String>();
		int ret = 1;
		ret = kechengService.delete(bean);
		if (ret > 0) {
			list.add("1");
			list.add("删除成功!");
		} else {
			list.add("0");
			list.add("删除失败!");
		}
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			String s = JSONArray.fromObject(list).toString();
			out.print(s);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
