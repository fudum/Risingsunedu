package risingsunedu.Controller.Background;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.BanjijiangciBean;
import risingsunedu.Bean.BanjijieciBean;
import risingsunedu.Common.Impl.CommonFunImpl;
import risingsunedu.Service.IBanjijiangciService;
import risingsunedu.Service.IBanjijieciService;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/banjijieci")
public class BanjijieciController  extends BaseController{

	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IBanjijieciService banjijieciService;
	
	@Autowired
	IBanjijiangciService banjijiangciService;

	@Override
	public void setLogger() {
	}
	
	@RequestMapping("/add")
	public ModelAndView add(BanjijiangciBean banjijiangciBean) {
		ModelAndView modelAndView = new ModelAndView("background/banji/jieci/add");
		List<BanjijiangciBean> listjiangci = banjijiangciService.selectList(banjijiangciBean);
		modelAndView.addObject("listjiangci", listjiangci);
		return modelAndView;
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(BanjijieciBean bean,String banji_id) {
		ModelAndView modelAndView = new ModelAndView("background/banji/jieci/edit");
		bean = banjijieciService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		
		BanjijiangciBean banjijiangciBean = new BanjijiangciBean();
		banjijiangciBean.setBanji_id(banji_id);
		List<BanjijiangciBean> listjiangci = banjijiangciService.selectList(banjijiangciBean);
		modelAndView.addObject("listjiangci", listjiangci);
		return modelAndView;
	}
	
	
	@RequestMapping("/insert")
	public void insert(BanjijieciBean bean,HttpServletResponse response) {
		response.setContentType("text/html");
		bean.setId(CommonFunImpl.uuid("jiec"));
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String arr[] = bean.getJiangci_name().split(";");
		bean.setJiangci_id(arr[0]);
		bean.setJiangci_name(arr[1]);
		
		String ret ="";
		JSONObject jb = new JSONObject();
		if (banjijieciService.insert(bean) > 0) {
			ret = "1";
		}
		else {
			ret = "0";
		}
		jb.put("ret", ret);
		jb.put("videopath", bean.getVideopath());
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/delete")
	public ModelAndView delete(BanjijieciBean bean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		List<String> list = new ArrayList<String>();
		int ret = 1;
		ret = banjijieciService.delete(bean);
		if (ret > 0) {
			list.add("1");
			list.add("删除成功!");
		} else {
			list.add("0");
			list.add("删除失败!");
		}
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			String s = JSONArray.fromObject(list).toString();
			out.print(s);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping("/update")
	public void update(BanjijieciBean bean,HttpServletResponse response) {
		response.setContentType("text/html");
		String arr[] = bean.getJiangci_name().split(";");
		bean.setJiangci_id(arr[0]);
		bean.setJiangci_name(arr[1]);
		bean.setInserttime(CommonFunImpl.currentTime(1));
		
		String ret ="";
		JSONObject jb = new JSONObject();
		if (banjijieciService.update(bean) > 0) {
			ret = "1";
		}
		else {
			ret = "0";
		}
		jb.put("ret", ret);
		jb.put("videopath", bean.getVideopath());
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	

}
