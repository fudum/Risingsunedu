package risingsunedu.Controller.Background;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.BanjiBean;
import risingsunedu.Bean.JiangyiBean;
import risingsunedu.Common.Impl.CommonConstant;
import risingsunedu.Common.Impl.CommonFunImpl;
import risingsunedu.Common.Impl.OSinfo;
import risingsunedu.Service.IBanjiService;
import risingsunedu.Service.IJiangyiService;
import risingsunedu.Service.ITeacherService;
import framework.base.common.Pager;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/jiangyi")
public class JiangyiController extends BaseController {

	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	IJiangyiService jiangyiService;

	@Autowired
	ITeacherService teacherService;
	
	@Autowired
	IBanjiService banjiService;
	
	@Override
	public void setLogger() {
		logger = LoggerFactory.getLogger(getClass());
	}

	// 讲义管理页面 //1
	@RequestMapping("/jiangyimanage")
	public ModelAndView jiangyiManage() {
		ModelAndView modelAndView = new ModelAndView("background/jiangyi/base/jiangyimanage");

		Pager<JiangyiBean> pager = new Pager<JiangyiBean>();
		pager = jiangyiService.selectPage(null, pager);
		// 数据list
		modelAndView.addObject("pager",pager);

		return modelAndView;
	}

	// 添加讲义页面 //2
	@RequestMapping("/jiangyiadd")
	public ModelAndView jiangyiAdd() {
		ModelAndView modelAndView = new ModelAndView("background/jiangyi/base/jiangyiadd");
		//班级bean
		List<BanjiBean> blist = banjiService.selectList(null);
		modelAndView.addObject("blist", blist);
		return modelAndView;
	}

	/**
	 * 插入讲义
	 * @param bean
	 * @param jiangyiFile
	 * @param request
	 * @return
	 */
	@RequestMapping("/jiangyiinsert")
	public ModelAndView jiangyiInsert(JiangyiBean bean,MultipartFile jiangyiFile,HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("background/jiangyi/base/jiangyiedit");
		//设置班级属性
		bean.setId(CommonFunImpl.uuid("J"));
		bean.setInserttime(CommonFunImpl.currentTime(1));//设置插入时间
		bean.setUpdatetime(CommonFunImpl.currentTime(1));//设置更新时间
		String arr[] = bean.getBj_id().split(";");
		bean.setBj_id(arr[0]);
		bean.setBj_name(arr[1]);
		
		//讲义不为空时候上传讲义
		if (!jiangyiFile.isEmpty()) {
			String path = "";
			if(OSinfo.isWindows()){
				path = CommonConstant.w_jyPath;
			}
			else {
				path = CommonConstant.jyPath;
			}
			String tmp = jiangyiFile.getOriginalFilename();
			tmp = tmp.substring(tmp.lastIndexOf('.'));
			String fileName = bean.getId() + tmp;
			File targetFile = new File(path, fileName);
			if(!targetFile.exists()){  
	            targetFile.mkdirs();  
	        }  
			//保存  
	        try {  
	        	jiangyiFile.transferTo(targetFile);  
	        } catch (Exception e) {  
	            e.printStackTrace();  
	        }
	        bean.setJiangyipath("jiangyi/" + fileName);
		}
		
		
		String tipString;
		//如果插入成功
		if (jiangyiService.insert(bean) > 0) {
			tipString = "ok";
		}
		else {
			tipString = "error";
		}
		
		List<BanjiBean> blist = banjiService.selectList(null);
		modelAndView.addObject("blist", blist);
		modelAndView.addObject("bean", bean);
		modelAndView.addObject("tip", tipString);
		return modelAndView;
	}

	// 删除班级 4
	@RequestMapping("/delete")
	public ModelAndView Delete(JiangyiBean JiangyiBean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		List<String> list = new ArrayList<String>();
		int ret = 1;
		ret = jiangyiService.delete(JiangyiBean);
		if (ret > 0) {
			list.add("1");
			list.add("删除成功!");
		} else {
			list.add("0");
			list.add("删除失败!");
		}
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			String s = JSONArray.fromObject(list).toString();
			out.print(s);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	// 上一页 5
	@RequestMapping("/frontpage")
	public ModelAndView frontPage(int pageNum, int totalPage) {
		ModelAndView modelAndView = new ModelAndView();

		Pager<JiangyiBean> pager = new Pager<JiangyiBean>();

		if (pageNum > 1) {// 如果小于总页数
			pageNum--;
		} else {// 返回首页
			pageNum = totalPage;
		}
		pager.setCurtPage(pageNum);

		Pager<JiangyiBean> pageT = jiangyiService.selectPage(null, pager);

		// 数据list
		modelAndView.addObject("list", pageT.getPageList());
		// 总页数
		modelAndView.addObject("totalPage", pageT.getTotalPage());
		// 当前页
		modelAndView.addObject("countPerPage", pageT.getCurtPage());

		modelAndView.setViewName("background/banji/banjimanage");
		return modelAndView;
	}

	// 下一页 6
	@RequestMapping("/nextpage")
	public ModelAndView nextPage(int pageNum, int totalPage) {
		ModelAndView modelAndView = new ModelAndView();

		Pager<JiangyiBean> pager = new Pager<JiangyiBean>();

		if (pageNum < totalPage) {// 如果小于总页数
			pageNum++;
		} else {// 返回首页
			pageNum = 1;
		}
		pager.setCurtPage(pageNum);

		Pager<JiangyiBean> userPage = jiangyiService.selectPage(null, pager);

		// 数据list
		modelAndView.addObject("list", userPage.getPageList());
		// 总页数
		modelAndView.addObject("totalPage", userPage.getTotalPage());
		// 当前页
		modelAndView.addObject("countPerPage", userPage.getCurtPage());

		modelAndView.setViewName("background/banji/banjimanage");
		return modelAndView;
	}

	// 最后一页 7
	@RequestMapping("/lastpage")
	public ModelAndView lastPage(int totalPage) {
		ModelAndView modelAndView = new ModelAndView();

		Pager<JiangyiBean> pager = new Pager<JiangyiBean>();
		pager.setCurtPage(totalPage);

		Pager<JiangyiBean> userPage = jiangyiService.selectPage(null, pager);

		// 数据list
		modelAndView.addObject("list", userPage.getPageList());
		// 总页数
		modelAndView.addObject("totalPage", userPage.getTotalPage());
		// 当前页
		modelAndView.addObject("countPerPage", userPage.getCurtPage());

		modelAndView.setViewName("background/banji/banjimanage");
		return modelAndView;
	}	
	
	// 编辑班级页面 //6
	@RequestMapping("/jiangyiedit")
	public ModelAndView userEdit(JiangyiBean bean) {
		ModelAndView modelAndView = new ModelAndView("background/jiangyi/base/jiangyiedit");
		bean = jiangyiService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		List<BanjiBean> blist = banjiService.selectList(null);
		modelAndView.addObject("blist", blist);
		return modelAndView;
	}
	
	// 班级插入 //3
	@RequestMapping("/jiangyiupdate")
	public ModelAndView jiangyiUpdate(JiangyiBean bean, MultipartFile jiangyiFile,HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("background/jiangyi/base/jiangyiedit");
		//设置班级属性
		bean.setUpdatetime(CommonFunImpl.currentTime(1));//设置更新时间
		String arr[] = bean.getBj_id().split(";");
		bean.setBj_id(arr[0]);
		bean.setBj_name(arr[1]);
		//讲义不为空时候上传讲义
		if (!jiangyiFile.isEmpty()) {
			String path = "";
			if(OSinfo.isWindows()){
				path = CommonConstant.w_jyPath;
			}else {
				path = CommonConstant.jyPath;
			}
			String tmp = jiangyiFile.getOriginalFilename();
			tmp = tmp.substring(tmp.lastIndexOf('.'));
			String fileName = bean.getId() + tmp;
			File targetFile = new File(path, fileName);
			if(!targetFile.exists()){  
	            targetFile.mkdirs();  
	        }  
			//保存  
	        try {  
	        	jiangyiFile.transferTo(targetFile);  
	        } catch (Exception e) {  
	            e.printStackTrace();  
	        }  
	        bean.setJiangyipath("jiangyi/" + fileName);
		}
		
		String tip;
		if (jiangyiService.update(bean) > 0) {//编辑成功
			tip = "编辑成功！";
		}
		else {
			tip = "编辑失败！";
		}
		modelAndView.addObject("bean", bean);
		List<BanjiBean> blist = banjiService.selectList(null);
		modelAndView.addObject("blist", blist);
		modelAndView.addObject("tip", tip);
		return modelAndView;
	}

}
