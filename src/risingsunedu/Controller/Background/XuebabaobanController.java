package risingsunedu.Controller.Background;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import framework.base.controller.BaseController;
import risingsunedu.Bean.KechengBean;
import risingsunedu.Bean.StudentBean;
import risingsunedu.Bean.XuebabaobanBean;
import risingsunedu.Common.Impl.CommonFunImpl;
import risingsunedu.Service.IKechengService;
import risingsunedu.Service.IStudentService;
import risingsunedu.Service.IXuebabaobanService;

@Controller
@RequestMapping("b_xuebabaoban")
public class XuebabaobanController extends BaseController{

	/**
	 * 学霸档案报班控制层
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IXuebabaobanService xuebabaobanService;
	
	@Autowired
	IStudentService studentService;
	
	@Autowired
	IKechengService kechengService;
	
	
	@RequestMapping("/index")
	public ModelAndView index(StudentBean bean){
		ModelAndView modelAndView = new ModelAndView("background/excstu/xuebadangan/indexxuebabaoban");
		bean = studentService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		
		XuebabaobanBean xuebabaobanBean = new XuebabaobanBean();
		xuebabaobanBean.setStuid(bean.getId());
		List<XuebabaobanBean> list = xuebabaobanService.selectList(xuebabaobanBean);
		modelAndView.addObject("list", list);
		
		return modelAndView;
	}
	
	@RequestMapping("/add")
	public ModelAndView add(XuebabaobanBean bean){
		ModelAndView modelAndView = new ModelAndView("background/excstu/xuebadangan/add");
		modelAndView.addObject("bean", bean);
		
		List<KechengBean> listKc = kechengService.selectList(null);
		modelAndView.addObject("listKc", listKc);
		
		return modelAndView;
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(XuebabaobanBean bean){
		ModelAndView modelAndView = new ModelAndView("background/excstu/xuebadangan/edit");
		bean = xuebabaobanService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		List<KechengBean> listKc = kechengService.selectList(null);
		modelAndView.addObject("listKc", listKc);
		return modelAndView;
	}
	
	@RequestMapping("/insert")
	public void insert(XuebabaobanBean bean,HttpServletResponse response){
		int retVal;
		if (bean.getKechengname() == null || "".equals(bean.getKechengname())) {
			retVal = 0;
		}
		else {
			String arr[] = bean.getKechengname().split(";");
			bean.setTeaid(arr[0]);
			bean.setTeaname(arr[1]);
			bean.setKechengid(arr[2]);
			bean.setKechengname(arr[3]);
			bean.setId(CommonFunImpl.uuid("XBBB"));
			bean.setInserttime(CommonFunImpl.currentTime(1));
			retVal = xuebabaobanService.insert(bean) > 0 ? 1 : 0;
		}
		try {
			PrintWriter	 out = response.getWriter();
			out.print(retVal);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@RequestMapping("/update")
	public void update(XuebabaobanBean bean,HttpServletResponse response){
		int retVal;
		if (bean.getKechengname() == null || "".equals(bean.getKechengname())) {
			retVal = 0;
		}
		else {
			String arr[] = bean.getKechengname().split(";");
			bean.setTeaid(arr[0]);
			bean.setTeaname(arr[1]);
			bean.setKechengid(arr[2]);
			bean.setKechengname(arr[3]);
			bean.setInserttime(CommonFunImpl.currentTime(1));
			retVal = xuebabaobanService.update(bean) > 0 ? 1 : 0;
		}
		try {
			PrintWriter	 out = response.getWriter();
			out.print(retVal);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@RequestMapping("/delete")
	public void delete(XuebabaobanBean bean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		List<String> list = new ArrayList<String>();
		int ret = 1;
		ret = xuebabaobanService.delete(bean);
		if (ret > 0) {
			list.add("1");
			list.add("删除成功!");
		} else {
			list.add("0");
			list.add("删除失败!");
		}
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			String s = JSONArray.fromObject(list).toString();
			out.print(s);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	
	@Override
	public void setLogger() {
		
	}

}
