package risingsunedu.Controller.Background;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.BanjiBean;
import risingsunedu.Bean.StudentBean;
import risingsunedu.Bean.StudentKechengBean;
import risingsunedu.Bean.StudentbanjiBean;
import risingsunedu.Common.Impl.CommonFunImpl;
import risingsunedu.Service.IBanjiService;
import risingsunedu.Service.IStudentKechengService;
import risingsunedu.Service.IStudentService;
import risingsunedu.Service.IStudentbanjiService;
import framework.base.common.Pager;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/b_student")
public class StudentController extends BaseController {

	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	IStudentService studentService;
	
	@Autowired
	IBanjiService banjiService;
	
	@Autowired
	IStudentbanjiService studentbanjiService;

	@Autowired
	IStudentKechengService studentKechengService;
	
	
	@Override
	public void setLogger() {
		logger = LoggerFactory.getLogger(getClass());
	}

	// 班级管理页面 //1
	@RequestMapping("/index")
	public ModelAndView index(Pager<StudentBean> pager) {
		ModelAndView modelAndView = new ModelAndView("background/user/student/index");
		pager = studentService.selectPage(null, pager);
		//数据list
		modelAndView.addObject("pager", pager);
		return modelAndView;
	}

	//添加班级页面 //2
	@RequestMapping("/add")
	public ModelAndView add() {
		ModelAndView modelAndView = new ModelAndView("background/user/student/add");
		List<BanjiBean> list = banjiService.selectList(null);
		modelAndView.addObject("list", list);
		return modelAndView;
	}

	@RequestMapping("/insert")
	public void insert(StudentBean bean,HttpServletResponse response) {
		bean.setId(CommonFunImpl.uuid("S"));
		bean.setInserttime(CommonFunImpl.currentTime(1));
		//如果插入成功
		String retVal;
		if (studentService.insert(bean) > 0) {
			retVal = "1";
		}
		else {
			retVal = "0";
		}
		// 返回值
		try {
			PrintWriter out = response.getWriter();
			System.out.print(retVal);
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}
		
	}

	// 删除班级 4
	@RequestMapping("/delete")
	public ModelAndView Delete(StudentBean bean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		List<String> list = new ArrayList<String>();
		int ret = 1;
		ret = studentService.delete(bean);
		if (ret > 0) {
			list.add("1");
			list.add("删除成功!");
		} else {
			list.add("0");
			list.add("删除失败!");
		}
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			String s = JSONArray.fromObject(list).toString();
			out.print(s);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	// 编辑学生页面 //6
	@RequestMapping("/edit")
	public ModelAndView studentEdit(StudentBean bean) {
		ModelAndView modelAndView = new ModelAndView("background/user/student/edit");
		bean = studentService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		
		StudentKechengBean studentKechengBean = new StudentKechengBean();
		studentKechengBean.setStuid(bean.getId());
		List<StudentKechengBean> listKc = studentKechengService.selectList(studentKechengBean);
		modelAndView.addObject("listKc", listKc);
		
		StudentbanjiBean studentbanjiBean = new StudentbanjiBean();
		studentbanjiBean.setStu_id(bean.getId());
		List<StudentbanjiBean> listSB = studentbanjiService.selectList(studentbanjiBean);
		modelAndView.addObject("listSB", listSB);
		
		return modelAndView;
	}
	
	// 班级插入 //3
	@RequestMapping("/update")
	public void studentUpdate(StudentBean bean,String banji, HttpServletResponse response) {
		bean.setInserttime(CommonFunImpl.currentTime(1));
		//如果插入成功
		String retVal;
		if (studentService.update(bean) > 0) {
			retVal = "1";
		}
		else {
			retVal = "0";
		}
		// 返回值
		try {
			PrintWriter out = response.getWriter();
			System.out.print(retVal);
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}
	}
	
	/*
	 * 创建学生编号
	 */
	/*private String getMaxStuId(){
		//
		String retVal;
		String maxid = studentService.getMaxStuId();
		if (maxid == null) {
			retVal = "S000001";
		}
		else {
			retVal = maxid.substring(1);
			int itmp = Integer.valueOf(retVal) + 1;
			retVal = String.valueOf(itmp);
			for (int i = 6; i > retVal.length();) {
				retVal = "0" + retVal;
			}
			retVal ="S" + retVal;
		}
		
		return retVal; 
	}*/
	
}
