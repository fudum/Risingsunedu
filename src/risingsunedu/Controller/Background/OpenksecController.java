package risingsunedu.Controller.Background;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.OpenkBean;
import risingsunedu.Bean.OpenkjiangciBean;
import risingsunedu.Bean.OpenksecBean;
import risingsunedu.Common.Impl.CommonConstant;
import risingsunedu.Common.Impl.CommonFunImpl;
import risingsunedu.Common.Impl.OSinfo;
import risingsunedu.Service.IOpenkService;
import risingsunedu.Service.IOpenkjiangciService;
import risingsunedu.Service.IOpenksecService;
import framework.base.common.Pager;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/openksec")
public class OpenksecController  extends BaseController{

	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	IOpenkService oService;
	
	@Autowired
	IOpenksecService openksecService;
	
	@Autowired
	IOpenkjiangciService openkjiangciService;
	
	@Override
	public void setLogger() {
	}
	
	/**
	 * 公开课视频管理首页
	 * @return
	 */
	@RequestMapping("/index")
	public ModelAndView index(Pager<OpenkBean> pager ) {
		ModelAndView modelAndView = new ModelAndView("background/openk/video/index");
		pager = oService.selectPage(null, pager);
		modelAndView.addObject("pager", pager);
		return modelAndView;
	}
	
	@RequestMapping("/manage")
	public ModelAndView manage(OpenkBean bean) {
		ModelAndView modelAndView = new ModelAndView("background/openk/video/manage");
		//公开课信息
		bean = oService.selectOne(bean);
		modelAndView.addObject("bean",bean);
		//公开课视频信息
		List<OpenksecBean> list = openksecService.selectListByOpenkId(bean.getId());
		modelAndView.addObject("list", list);
		
		return modelAndView;
	}
	
	@RequestMapping("/add")
	public ModelAndView add(String openk_id) {
		ModelAndView modelAndView = new ModelAndView("background/openk/video/newAdd");
		OpenkjiangciBean openkjiangciBean = new OpenkjiangciBean();
		openkjiangciBean.setOpenk_id(openk_id);
		List<OpenkjiangciBean> listsec = openkjiangciService.selectList(openkjiangciBean);
		
		List<String> thirdList = new ArrayList<String>();
		for (int i = 1; i <= 20; i++) {
			thirdList.add("第" + i + "节");
		}
		modelAndView.addObject("listsec", listsec);
		modelAndView.addObject("thirdList", thirdList);
		modelAndView.addObject("openk_id", openk_id);
		return modelAndView;
	}
	
	@RequestMapping("/insert")
	public void insert(OpenksecBean bean,MultipartFile videoFile,MultipartFile picFile,HttpServletResponse response) {
		response.setContentType("text/html");
		bean.setId(CommonFunImpl.uuid("os"));
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String arr[] = bean.getSecname().split(";");
		bean.setOpenk_id(arr[0]);
		bean.setSecname(arr[1]);
		String fileName = "";
		if (!videoFile.isEmpty()) {
			String path = OSinfo.isWindows() ? CommonConstant.w_openkVideoPath : CommonConstant.openkVideoPath; 
			String tmp = videoFile.getOriginalFilename();
			tmp = tmp.substring(tmp.lastIndexOf('.'));
			fileName = CommonFunImpl.uuid32() + tmp;
			
			File targetFile = new File(path, fileName);
			if(!targetFile.exists()){  
	            targetFile.mkdirs();  
	        }  
			
			//保存  
	        try {  
	        	videoFile.transferTo(targetFile);  
	        } catch (Exception e) {  
	            e.printStackTrace();  
	        }  
	        
	        //设置bean中的内容
	        bean.setVideopath("openk/video/" + fileName);
		}
		String ret ="";
		JSONObject jb = new JSONObject();
		if (openksecService.insert(bean) > 0) {
			ret = "1";
		}
		else {
			ret = "0";
		}
		jb.put("ret", ret);
		jb.put("videopath", fileName);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//图片上传功能
	@RequestMapping("/picupload")
	public void picupload(String openk_id,MultipartFile picFile,HttpServletResponse response) {
		response.setContentType("text/html");
		String fileName = "";
		boolean flag = false;
		if (!picFile.isEmpty()) {
			String path = OSinfo.isWindows() ? CommonConstant.w_openkImagesPath :CommonConstant.openkImagesPath;
			String tmp = picFile.getOriginalFilename();
			tmp = tmp.substring(tmp.lastIndexOf('.'));
			fileName = CommonFunImpl.uuid32() + tmp;
			
			File targetFile = new File(path, fileName);
			if(!targetFile.exists()){  
	            targetFile.mkdirs();  
	        }  
			
			//保存  
	        try {  
	        	picFile.transferTo(targetFile);  
	        	flag = true;
	        } catch (Exception e) {  
	            e.printStackTrace();  
	        } 
		}
		String ret = (flag == true) ? "1":"0";
		JSONObject jb = new JSONObject();
		jb.put("ret", ret);
		jb.put("picpath", "openk/images/" + fileName);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/insertnew")
	public void newinsert(OpenksecBean bean,HttpServletResponse response) {
		response.setContentType("text/html");
		bean.setId(CommonFunImpl.uuid("os"));
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String arr[] = bean.getSecname().split(";");
		bean.setOpenk_id(arr[0]);
		bean.setSecname(arr[1]);
		
		String ret ="";
		JSONObject jb = new JSONObject();
		if (openksecService.insert(bean) > 0) {
			ret = "1";
		}
		else {
			ret = "0";
		}
		jb.put("ret", ret);
		jb.put("videopath", bean.getVideopath());
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(OpenksecBean bean,String openkId) {
		ModelAndView modelAndView = new ModelAndView("background/openk/video/edit");
		bean = openksecService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		
		OpenkjiangciBean openkjiangciBean = new OpenkjiangciBean();
		openkjiangciBean.setOpenk_id(openkId);
		List<OpenkjiangciBean> listsec = openkjiangciService.selectList(openkjiangciBean);
		
		List<String> thirdList = new ArrayList<String>();
		for (int i = 1; i <= 20; i++) {
			thirdList.add("第" + i + "节");
		}
		modelAndView.addObject("listsec", listsec);
		modelAndView.addObject("thirdList", thirdList);
		return modelAndView;
	}
	
	@RequestMapping("/update")
	public void update(OpenksecBean bean,HttpServletResponse response) {
		response.setContentType("text/html");
		String arr[] = bean.getSecname().split(";");
		bean.setOpenk_id(arr[0]);
		bean.setSecname(arr[1]);
		
		String ret ="";
		JSONObject jb = new JSONObject();
		if (openksecService.update(bean) > 0) {
			ret = "1";
		}
		else {
			ret = "0";
		}
		jb.put("ret", ret);
		jb.put("videopath", bean.getVideopath());
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/delete")
	public ModelAndView delete(OpenksecBean bean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		List<String> list = new ArrayList<String>();
		int ret = 1;
		ret = openksecService.delete(bean);
		if (ret > 0) {
			list.add("1");
			list.add("删除成功!");
		} else {
			list.add("0");
			list.add("删除失败!");
		}
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			String s = JSONArray.fromObject(list).toString();
			out.print(s);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
