package risingsunedu.Controller.Background;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;




import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.TeacherAssessBean;
import risingsunedu.Bean.TeacherBean;
import risingsunedu.Common.Impl.CommonConstant;
import risingsunedu.Common.Impl.CommonFunImpl;
import risingsunedu.Common.Impl.OSinfo;
import risingsunedu.Service.ITeacherAssessService;
import risingsunedu.Service.ITeacherService;
import framework.base.common.Pager;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/b_ta")
public class TeacherAssessController extends BaseController {

	
	private static final long serialVersionUID = 1L;
	
	@Autowired
	ITeacherAssessService taService;
	
	@Autowired
	ITeacherService tService;

	@Override
	public void setLogger() {		
	}
	
	@RequestMapping("/index")
	public ModelAndView index(Pager<TeacherBean> pager) {
		ModelAndView modelAndView = new ModelAndView("background/teacher/assess/index");
		pager = tService.selectPage(null, pager);
		modelAndView.addObject("pager", pager);
		
		return modelAndView;
	}
	
	
	@RequestMapping("/edit")
	public ModelAndView edit(TeacherBean tBean) {
		ModelAndView modelAndView = new ModelAndView("background/teacher/assess/edit");
		tBean = tService.selectOne(tBean); 
		modelAndView.addObject("tBean", tBean);
		
		TeacherAssessBean taBean = new TeacherAssessBean();
		taBean.setTea_id(tBean.getId());
		List<TeacherAssessBean> list = taService.selectList(taBean);
		modelAndView.addObject("list", list);
		return modelAndView;
	}
	
	@RequestMapping("/add")
	public ModelAndView add(String tea_id) {
		ModelAndView modelAndView = new ModelAndView("background/teacher/assess/add");
		modelAndView.addObject("tea_id", tea_id);
		return modelAndView;
	}
	
	/**
	 * 编辑学员评估
	 * @return
	 */
	@RequestMapping("/editassess")
	public ModelAndView editAssess(TeacherAssessBean bean) {
		ModelAndView modelAndView = new ModelAndView("background/teacher/assess/editassess");
		bean = taService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		return modelAndView;
	}
	
	@RequestMapping("/insert")
	public void insert(TeacherAssessBean taBean,HttpServletResponse response) {
		taBean.setId(CommonFunImpl.uuid("ta"));
		taBean.setInserttime(CommonFunImpl.currentTime(1));
		String ret ="";
		JSONObject jb = new JSONObject();
		if (taService.insert(taBean) > 0) {
			ret = "1";
		}
		else{
			ret = "0";
		}
		jb.put("ret", ret);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@RequestMapping("/update")
	public void update(TeacherAssessBean taBean,HttpServletResponse response) {
		taBean.setInserttime(CommonFunImpl.currentTime(1));
		String ret ="";
		JSONObject jb = new JSONObject();
		if (taService.update(taBean) > 0) {
			ret = "1";
		}
		else{
			ret = "0";
		}
		jb.put("ret", ret);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// 删除班级 4
	@RequestMapping("/delete")
	public ModelAndView Delete(TeacherAssessBean bean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		List<String> list = new ArrayList<String>();
		int ret = 1;
		ret = taService.delete(bean);
		if (ret > 0) {
			list.add("1");
			list.add("删除成功!");
		} else {
			list.add("0");
			list.add("删除失败!");
		}
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			String s = JSONArray.fromObject(list).toString();
			out.print(s);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping("/picupload")
	public void picUpload(MultipartFile picFile,HttpServletResponse response){
		response.setContentType("text/html");
		String fileName = "";
		boolean flag = false;
		if (!picFile.isEmpty()) {
			String path = OSinfo.isWindows() ? CommonConstant.w_assessPath :CommonConstant.assessPath;
			String tmp = picFile.getOriginalFilename();
			tmp = tmp.substring(tmp.lastIndexOf('.'));
			fileName = CommonFunImpl.uuid32() + tmp;
			
			File targetFile = new File(path, fileName);
			if(!targetFile.exists()){  
	            targetFile.mkdirs();  
	        }  
			
			//保存  
	        try {  
	        	picFile.transferTo(targetFile);  
	        	flag = true;
	        } catch (Exception e) {  
	            e.printStackTrace();  
	        } 
		}
		String ret = (flag == true) ? "1":"0";
		JSONObject jb = new JSONObject();
		jb.put("ret", ret);
		jb.put("picpath", "teacher/assess/" + fileName);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
