package risingsunedu.Controller.Background;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.BaseTwoBean;
import risingsunedu.Bean.ShijuanBean;
import risingsunedu.Common.Impl.CommonConstant;
import risingsunedu.Common.Impl.CommonFunImpl;
import risingsunedu.Common.Impl.OSinfo;
import risingsunedu.Service.IBaseTwoService;
import risingsunedu.Service.IShijuanService;
import framework.base.common.Pager;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/b_shijuan")
public class BShijuanController extends BaseController {

	/**
	 * 后台试卷控制
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IShijuanService shijuanService;
	
	@Autowired
	IBaseTwoService basetwoService;

	@Override
	public void setLogger() {
		// TODO Auto-generated method stub
		
	}
	
	@RequestMapping("/index")
	public ModelAndView index(Pager<ShijuanBean> pager) {
		ModelAndView modelAndView = new ModelAndView("background/shijuan/index");
		pager = shijuanService.selectPage(null, pager);
		modelAndView.addObject("pager", pager);
		return modelAndView;
	}
	
	@RequestMapping("/add")
	public ModelAndView add(){
		ModelAndView modelAndView = new ModelAndView("background/shijuan/add");
		
		//基本信息
		List<BaseTwoBean> listbt = basetwoService.selectList(null);
		List<BaseTwoBean> listGrade = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listSubject = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listNianFen = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listLeixing = new ArrayList<BaseTwoBean>();
		for (BaseTwoBean baseTwoBean : listbt) {
			switch (baseTwoBean.getBotype()) {
			case "年级":
				listGrade.add(baseTwoBean);
				break;
			case "试卷科目":
				listSubject.add(baseTwoBean);
				break;
			case "试卷年份":
				listNianFen.add(baseTwoBean);
				break;
			case "试卷类型":
				listLeixing.add(baseTwoBean);
				break;
			}
		}
		modelAndView.addObject("listGrade", listGrade);
		modelAndView.addObject("listSubject", listSubject);
		modelAndView.addObject("listNianFen", listNianFen);
		modelAndView.addObject("listLeixing", listLeixing);
		return modelAndView;
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(ShijuanBean bean){
		ModelAndView modelAndView = new ModelAndView("background/shijuan/edit");
		bean = shijuanService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		
		//基本信息
		List<BaseTwoBean> listbt = basetwoService.selectList(null);
		List<BaseTwoBean> listGrade = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listSubject = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listNianFen = new ArrayList<BaseTwoBean>();
		List<BaseTwoBean> listLeixing = new ArrayList<BaseTwoBean>();
		for (BaseTwoBean baseTwoBean : listbt) {
			switch (baseTwoBean.getBotype()) {
			case "年级":
				listGrade.add(baseTwoBean);
				break;
			case "试卷科目":
				listSubject.add(baseTwoBean);
				break;
			case "试卷年份":
				listNianFen.add(baseTwoBean);
				break;
			case "试卷类型":
				listLeixing.add(baseTwoBean);
				break;
			}
		}
		modelAndView.addObject("listGrade", listGrade);
		modelAndView.addObject("listSubject", listSubject);
		modelAndView.addObject("listNianFen", listNianFen);
		modelAndView.addObject("listLeixing", listLeixing);
		
		return modelAndView;
	}
	
	@RequestMapping("/insert")
	public void insert(ShijuanBean bean,HttpServletResponse response){
		//设置试卷属性
		bean.setId(CommonFunImpl.uuid("sj"));
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String retVal;
		// 如果插入成功
		if (shijuanService.insert(bean) > 0) {
			retVal = "1";
		} else {
			retVal = "0";
		}
		// 返回值
		try {
			PrintWriter out = response.getWriter();
			System.out.print(retVal);
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}
	}
	
	@RequestMapping("/update")
	public void update(ShijuanBean bean,HttpServletResponse response){
		//设置试卷属性
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String retVal;
		// 如果插入成功
		if (shijuanService.update(bean) > 0) {
			retVal = "1";
		} else {
			retVal = "0";
		}
		// 返回值
		try {
			PrintWriter out = response.getWriter();
			System.out.print(retVal);
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}
	}

	@RequestMapping("/delete")
	public ModelAndView Delete(ShijuanBean bean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		List<String> list = new ArrayList<String>();
		int ret = 1;
		ret = shijuanService.delete(bean);
		if (ret > 0) {
			list.add("1");
			list.add("删除成功!");
		} else {
			list.add("0");
			list.add("删除失败!");
		}
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			String s = JSONArray.fromObject(list).toString();
			out.print(s);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping("/wordupload")
	public void wordUpload(MultipartFile wordFile,String wjjname,HttpServletResponse response){
		response.setContentType("text/html");
		String fileName = "";
		boolean flag = false;
		if(wordFile != null){
			if (!wordFile.isEmpty()) {
				String path = OSinfo.isWindows() ? CommonConstant.w_shijuanPath :CommonConstant.shijuanPath;
				String tmp = wordFile.getOriginalFilename();
				tmp = tmp.substring(tmp.lastIndexOf('.'));
				fileName = CommonFunImpl.uuid32() + tmp;
				File targetFile = new File(path, fileName);
				if(!targetFile.exists()){  
		            targetFile.mkdirs();  
		        }  
				
				//保存  
		        try {  
		        	wordFile.transferTo(targetFile);  
		        	flag = true;
		        } catch (Exception e) {  
		            e.printStackTrace();  
		        } 
			}
		}
		String ret = (flag == true) ? "1":"0";
		JSONObject jb = new JSONObject();
		jb.put("ret", ret);
		jb.put("path", wjjname + "/" + fileName);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
}
