package risingsunedu.Controller.Background;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.UserBean;
import risingsunedu.Service.IUserService;
import framework.base.controller.BaseController;

@RequestMapping("/login")
@Controller
public class BLoginController extends BaseController{

	/**
	 * 登入控制
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IUserService userService;

	@Override
	public void setLogger() {
		// TODO Auto-generated method stub
	}
	
	@RequestMapping("/index")
	public ModelAndView index(HttpSession session){
		if (session.getAttribute("usersession") == null) {
			return new ModelAndView("background/login");
		}
		else{
			return new ModelAndView("background/index");
		}
	}
	
	@RequestMapping("/login")
	public ModelAndView login(UserBean bean, HttpSession session,HttpServletResponse response) {
		String retVal = "0";
		if (bean.getUserid() == null || bean.getPassword() == null) {
		}
		else if(bean.getPassword().trim() == "" || bean.getUserid().trim() =="" ){
		}
		else{
			bean = userService.selectOne(bean);
			if (bean != null) {// 验证成功
				// 把基本信息存入session中
				bean.setPassword(null);
				session.setAttribute("usersession", bean);//把用户存入session中
				retVal = "1";
			} else {
				// 验证失败
			}
		}
		
		// 返回值
		try {
			PrintWriter out = response.getWriter();
			System.out.print(retVal);
			out.print(retVal);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();// 打印异常
		}
		return null;
	}

}
