package risingsunedu.Controller.Background;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;






import risingsunedu.Bean.BaseTwoBean;
import risingsunedu.Bean.ZhangjieBean;
import risingsunedu.Common.Impl.CommonFunImpl;
import risingsunedu.Service.IBaseTwoService;
import risingsunedu.Service.IDayiService;
import risingsunedu.Service.IZhangjieService;
import framework.base.common.Pager;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/zhangjie")
public class ZhangjieController extends BaseController{

	/**
	 * 章节控制层
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IZhangjieService zhangjieService;
	
	@Autowired
	IBaseTwoService baseTwoService;
	
	@Autowired
	IDayiService dayiService;

	@Override
	public void setLogger() {		
	}
	
	@RequestMapping("/index")
	public ModelAndView index(Pager<ZhangjieBean> pager){
		ModelAndView modelAndView = new ModelAndView("background/dayi/zhangjie/index");
		pager = zhangjieService.selectPage(null, pager);
		return modelAndView;
	}
	
	@RequestMapping("/add")
	public ModelAndView add(){
		ModelAndView modelAndView = new ModelAndView("background/dayi/zhangjie/add");
		//班级 科目 课程类型
		List<BaseTwoBean> listBT = baseTwoService.selectList(null);
		modelAndView.addObject("listbt", listBT);
		return modelAndView;
	}
	
	@RequestMapping("/edit")
	public ModelAndView edit(ZhangjieBean bean){
		ModelAndView modelAndView = new ModelAndView("background/dayi/zhangjie/edit");
		bean = zhangjieService.selectOne(bean);
		modelAndView.addObject("bean",bean);
		//班级 科目 课程类型
		List<BaseTwoBean> listBT = baseTwoService.selectList(null);
		modelAndView.addObject("listbt", listBT);
		return modelAndView;
	}
	
	
	@RequestMapping("/insert")
	public void insert(ZhangjieBean bean,HttpServletResponse response) {
		response.setContentType("text/html");
		bean.setId(CommonFunImpl.uuid("ZJ"));
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String ret ="";
		JSONObject jb = new JSONObject();
		if (zhangjieService.insert(bean) > 0) {
			ret = "1";
		}
		else {
			ret = "0";
		}
		jb.put("ret", ret);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/update")
	public void update(ZhangjieBean bean,String old_chapter,HttpServletResponse response) {
		response.setContentType("text/html");
		bean.setInserttime(CommonFunImpl.currentTime(1));
		String ret ="";
		JSONObject jb = new JSONObject();
		if (zhangjieService.update(bean) > 0) {
			ret = "1";
		}
		else {
			ret = "0";
		}
		jb.put("ret", ret);
		ret = jb.toString();
		try {
			PrintWriter out = response.getWriter();
			out.print(ret);
			out.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//更新答疑所属的章节
		if(!(bean.getName() == null || old_chapter == null)){
			if (!bean.getName().equals(old_chapter)) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("chapter", bean.getName());
				map.put("old_chapter", old_chapter);
				dayiService.update(map);
			}
		}
	}
	
	@RequestMapping("/delete")
	public ModelAndView delete(ZhangjieBean bean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		List<String> list = new ArrayList<String>();
		int ret = 1;
		ret = zhangjieService.delete(bean);
		if (ret > 0) {
			list.add("1");
			list.add("删除成功!");
		} else {
			list.add("0");
			list.add("删除失败!");
		}
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			String s = JSONArray.fromObject(list).toString();
			out.print(s);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	

}
