package risingsunedu.Controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.BanjiBean;
import risingsunedu.Bean.StudentAdditionBean;
import risingsunedu.Bean.StudentBean;
import risingsunedu.Bean.StudentbanjiBean;
import risingsunedu.Common.Impl.CommonFunImpl;
import risingsunedu.Service.IStudentAdditionService;
import risingsunedu.Service.IStudentService;
import risingsunedu.Service.IStudentbanjiService;
import framework.base.common.Pager;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/stuaddn")
public class StudentAdditionController extends BaseController {

	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	IStudentService studentService;
	
	@Autowired
	IStudentAdditionService studentAdditionService;
	
	@Autowired
	IStudentbanjiService studentbanjiService;

	@Override
	public void setLogger() {
		logger = LoggerFactory.getLogger(getClass());
	}

	// 班级管理页面 //1
	@RequestMapping("/stuaddnmng")
	public ModelAndView StudentAdditionManage() {
		ModelAndView modelAndView = new ModelAndView();

		Pager<StudentBean> pager = new Pager<StudentBean>();
		Pager<StudentBean> stuPage = studentService.selectPage(null, pager);
		//分页后的数据
		modelAndView.addObject("stuPage", stuPage);
		
		modelAndView.setViewName("background/dangan/studentadditionmanage");
		return modelAndView;
	}


	// 学生插入 //3
	@RequestMapping("/studentinsert")
	public ModelAndView studentInsert(StudentBean bean,String banji, HttpServletResponse response) {
		
		ModelAndView modelAndView = new ModelAndView("background/user/studentedit");
		//设置班级属性
		bean.setId(CommonFunImpl.uuid("S"));
		
		bean.setInserttime(CommonFunImpl.currentTime(1));
		
		//如果有班级信息的话
		if (!(banji == null || banji == "")) {
			String[] strArr = banji.split(",");
			for (int i = 0; i < strArr.length; i++) {
				//把班级信息插入到班级-学生表中
				StudentbanjiBean  studentbanjiBean = new StudentbanjiBean();
				studentbanjiBean.setId(CommonFunImpl.uuid("sc"));
				studentbanjiBean.setBanji_id(strArr[i]);
				studentbanjiBean.setStu_id(bean.getId());
				studentbanjiBean.setInserttime(CommonFunImpl.currentTime(1));
				studentbanjiService.insert(studentbanjiBean);
			}
		}
		
		//如果插入成功
		String tip;
		if (studentService.insert(bean) > 0) {
			tip = "1";
		}
		else {
			tip = "0";
		}
		
		StudentbanjiBean sbbean = new StudentbanjiBean();
		sbbean.setStu_id(bean.getId());
		List<StudentbanjiBean> sbList = studentbanjiService.selectList(sbbean);
		modelAndView.addObject("sblist",sbList);
		modelAndView.addObject("bean", bean);
		modelAndView.addObject("tip", tip);
		
		String sbString = "";		
		for (int i = 0; i < sbList.size(); i++) {
			BanjiBean bj = new BanjiBean();
			bj.setId(sbList.get(i).getBanji_id());
			sbString += bj.getGrade() + bj.getName() + bj.getSubject() + bj.getName();
			sbString += ";";
		}
		modelAndView.addObject("sbString", sbString);
		
		return modelAndView;
	}

	// 删除班级 4
	@RequestMapping("/delete")
	public ModelAndView Delete(StudentBean StudentBean, HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		List<String> list = new ArrayList<String>();
		int ret = 1;
		ret = studentService.delete(StudentBean);
		if (ret > 0) {
			list.add("1");
			list.add("删除成功!");
		} else {
			list.add("0");
			list.add("删除失败!");
		}
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			String s = JSONArray.fromObject(list).toString();
			out.print(s);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	// 上一页 5
	@RequestMapping("/frontpage")
	public ModelAndView frontPage(int pageNum, int totalPage) {
		ModelAndView modelAndView = new ModelAndView();

		Pager<StudentBean> pager = new Pager<StudentBean>();

		if (pageNum > 1) {// 如果小于总页数
			pageNum--;
		} else {// 返回首页
			pageNum = totalPage;
		}
		pager.setCurtPage(pageNum);

		Pager<StudentBean> pageT = studentService.selectPage(null, pager);

		// 数据list
		modelAndView.addObject("list", pageT.getPageList());
		// 总页数
		modelAndView.addObject("totalPage", pageT.getTotalPage());
		// 当前页
		modelAndView.addObject("countPerPage", pageT.getCurtPage());

		modelAndView.setViewName("background/user/studentmanage");
		return modelAndView;
	}

	// 下一页 6
	@RequestMapping("/nextpage")
	public ModelAndView nextPage(int pageNum, int totalPage) {
		ModelAndView modelAndView = new ModelAndView();

		Pager<StudentBean> pager = new Pager<StudentBean>();

		if (pageNum < totalPage) {// 如果小于总页数
			pageNum++;
		} else {// 返回首页
			pageNum = 1;
		}
		pager.setCurtPage(pageNum);

		Pager<StudentBean> userPage = studentService.selectPage(null, pager);

		// 数据list
		modelAndView.addObject("list", userPage.getPageList());
		// 总页数
		modelAndView.addObject("totalPage", userPage.getTotalPage());
		// 当前页
		modelAndView.addObject("countPerPage", userPage.getCurtPage());

		modelAndView.setViewName("background/user/studentmanage");
		return modelAndView;
	}

	// 最后一页 7
	@RequestMapping("/lastpage")
	public ModelAndView lastPage(int totalPage) {
		ModelAndView modelAndView = new ModelAndView();

		Pager<StudentBean> pager = new Pager<StudentBean>();
		pager.setCurtPage(totalPage);

		Pager<StudentBean> userPage = studentService.selectPage(null, pager);

		// 数据list
		modelAndView.addObject("list", userPage.getPageList());
		// 总页数
		modelAndView.addObject("totalPage", userPage.getTotalPage());
		// 当前页
		modelAndView.addObject("countPerPage", userPage.getCurtPage());

		modelAndView.setViewName("background/user/studentmanage");
		return modelAndView;
	}
	
	
	// 编辑学生页面 //6
	@RequestMapping("/stuaddnedit")
	public ModelAndView studentEdit(StudentAdditionBean bean,String type) {
		ModelAndView modelAndView = new ModelAndView("background/dangan/studentadditionedit");
		
		//学生课程设计、课程记录、我的题库
		StudentAdditionBean stuAddnBean = studentAdditionService.selectOne(bean);
		if (stuAddnBean == null) {
			modelAndView.addObject("bean", bean);
		}
		else {
			modelAndView.addObject("bean", stuAddnBean);
		}
		modelAndView.addObject("type", type);
		return modelAndView;
	}
	
	// 班级插入 //3
	@RequestMapping("/stuaddnupdate")
	public ModelAndView studentAddnUpdate(StudentAdditionBean bean) {
		ModelAndView modelAndView =new ModelAndView();
		String eS = "";
		if (bean.getId().equals(null) || "".equals(bean.getId())) {
			bean.setId(CommonFunImpl.uuid("TA"));
			bean.setInserttime(CommonFunImpl.currentTime(1));
			if (studentAdditionService.insert(bean) > 0) {
				eS = "保存成功！";
			}
			else {
				eS = "保存失败！";
			}
		}
		else {
			if (studentAdditionService.update(bean) > 0) {
				eS = "保存成功！";
			}
			else {
				eS = "保存失败！";
			}
			
		}
		
		modelAndView.addObject("es", eS);
		modelAndView.setViewName("redirect:/stuaddn/stuaddnedit.do?stu_id=" +bean.getStu_id() +"&type=" + eS);
		
		return modelAndView;
	}
	
	
}
