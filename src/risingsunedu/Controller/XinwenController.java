package risingsunedu.Controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.MedTestCatgBean;
import risingsunedu.Bean.SchoolBean;
import risingsunedu.Bean.XinwenBean;
import risingsunedu.Common.Impl.CommonFunImpl;
import risingsunedu.Service.IMedTestCatgService;
import risingsunedu.Service.ISchoolService;
import risingsunedu.Service.IXinwenService;
import framework.base.common.Pager;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/xinwen")
public class XinwenController extends BaseController {

	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	IMedTestCatgService medTestCatgService;
	
	@Autowired
	IXinwenService xinwenService;
	
	@Autowired
	ISchoolService schoolService;
	
	@Override
	public void setLogger() {
		logger = LoggerFactory.getLogger(getClass());
	}
	
	@RequestMapping("/manage")
	public ModelAndView manage() {
		ModelAndView modelAndView = new ModelAndView("background/zhongkao/xinwenmanage");
		
		//新闻
		Pager<XinwenBean> pager = new Pager<XinwenBean>();
		Pager<XinwenBean> list = xinwenService.selectPage(null, pager);
		modelAndView.addObject("list", list);
		
		return modelAndView;
	}
	
	//添加新闻
	@RequestMapping("/add")
	public ModelAndView add() {
		ModelAndView modelAndView = new ModelAndView("background/zhongkao/xinwenadd");
		
		//新闻类别列表
		List<MedTestCatgBean> xinwenTypeList = medTestCatgService.selectList(null);
		modelAndView.addObject("xinwenTypeList", xinwenTypeList);
		
		//学校信息列表
		List<SchoolBean> schoolList = schoolService.selectList(null);
		modelAndView.addObject("schoolList", schoolList);
		
		return modelAndView;
	}
	
	@RequestMapping("/insert")
	public ModelAndView insert(XinwenBean xinwenBean) {
		ModelAndView modelAndView = new ModelAndView("background/zhongkao/xinwenedit");
		xinwenBean.setId(CommonFunImpl.uuid("MT"));
		xinwenBean.setRiqi(CommonFunImpl.currentTime(2));
		xinwenBean.setId(CommonFunImpl.currentTime(1));
		String retVal;
		if (xinwenService.insert(xinwenBean) > 0) {
			retVal = "添加成功！";
		}
		else {
			retVal = "添加失败！";
		}
		modelAndView.addObject("retVal", retVal);
		modelAndView.addObject("bean", xinwenBean);
		
		//新闻类别列表
		List<MedTestCatgBean> xinwenTypeList = medTestCatgService.selectList(null);
		modelAndView.addObject("xinwenTypeList", xinwenTypeList);
		
		//学校信息列表
		List<SchoolBean> schoolList = schoolService.selectList(null);
		modelAndView.addObject("schoolList", schoolList);
		
		return modelAndView;
	}
	
	@RequestMapping("/update")
	public ModelAndView update(XinwenBean xinwenBean) {
		ModelAndView modelAndView = new ModelAndView("background/zhongkao/xinwenedit");
		String retVal;
		if (xinwenService.insert(xinwenBean) > 0) {
			retVal = "编辑成功！";
		}
		else {
			retVal = "编辑失败！";
		}
		modelAndView.addObject("retVal", retVal);
		modelAndView.addObject("bean", xinwenBean);
		return modelAndView;
	}
	
	//删除
	@RequestMapping("/delete")
	public ModelAndView delete(MedTestCatgBean medTestCatgBean,HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		List<String> list = new ArrayList<String>();
		int ret = 1;
		ret = medTestCatgService.delete(medTestCatgBean);
		if (ret > 0) {
			list.add("1");
			list.add("删除成功!");
		} else {
			list.add("0");
			list.add("删除失败!");
		}
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			String s = JSONArray.fromObject(list).toString();
			out.print(s);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	//编辑页面
	@RequestMapping("/edit")
	public ModelAndView edit(XinwenBean xinwenBean) {
		ModelAndView modelAndView = new ModelAndView("background/zhongkao/xinwenedit");
		xinwenBean = xinwenService.selectOne(xinwenBean);
		modelAndView.addObject("bean", xinwenBean);
		
		//新闻类别列表
		List<MedTestCatgBean> xinwenTypeList = medTestCatgService.selectList(null);
		modelAndView.addObject("xinwenTypeList", xinwenTypeList);
		
		//学校信息列表
		List<SchoolBean> schoolList = schoolService.selectList(null);
		modelAndView.addObject("schoolList", schoolList);
		return modelAndView;
	}

}
