package risingsunedu.Controller;

import javax.servlet.http.HttpSession;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.StudentBean;
import risingsunedu.Common.Impl.CommonConstant;
import risingsunedu.Service.IStudentService;
import framework.base.controller.BaseController;


/**
 * 用户登入页面的controller
 * @author fudum
 * @date 2014年11月2日
 */
@Controller
@RequestMapping("/client")
public class ClientController extends BaseController{

	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IStudentService studentService;

	@Override
	public void setLogger() {
		logger = LoggerFactory.getLogger(getClass());
	}
	
	//用户登入页面
	@RequestMapping("/login")
	public ModelAndView login(String type) {
		ModelAndView modelAndView = new ModelAndView("risingsunedu/login");
		modelAndView.addObject("strType", type);
		return modelAndView;
	}
	
	//显示首页
	@RequestMapping("/index")
	public ModelAndView index() {
		ModelAndView modelAndView = new ModelAndView("index");
		return modelAndView;
	}
	
	//用户登入进页面
	@RequestMapping("/loginin")
	public ModelAndView loginin(StudentBean bean,Boolean remember,HttpSession session,String strType) {
		ModelAndView modelAndView = new ModelAndView();
		bean = studentService.selectOne(bean);
		if (bean != null) {//登入成功
			bean.setPassword(null);
			session.setAttribute("stuSession", bean);
			switch (strType) {
			case "0"://讲义信息
				modelAndView.setViewName("redirect:/rjiangyi/jiangyishow" +CommonConstant.sufStr);
				break;
			case "1"://学生档案
				modelAndView.setViewName("redirect:/rdangan/danganmanage" + CommonConstant.sufStr);
				break;
			case "2"://考试信息
				modelAndView.setViewName("risingsunedu/kaoshi/kaoshimanage");
				break;
			case "3"://教师信息
				modelAndView.setViewName("risingsunedu/jiaoshi/jiaoshimanage");
				break;
			}
		}
		else {//登入失败
			modelAndView.addObject("error","用户名或密码错误!" );
			modelAndView.setViewName("risingsunedu/login");
		}
		
		return modelAndView;
	}
	
	//退出
	@RequestMapping("/quit")
	public ModelAndView quit(HttpSession session) {
		ModelAndView modelAndView = new ModelAndView();
		session.removeAttribute("stuSession");
		modelAndView.setViewName("redirect:/client/index" + CommonConstant.sufStr);
		return modelAndView;
	}

}
