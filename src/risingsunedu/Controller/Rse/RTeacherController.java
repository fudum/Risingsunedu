package risingsunedu.Controller.Rse;

import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.JiangyiBean;
import risingsunedu.Bean.TeacherAdditionBean;
import risingsunedu.Bean.TeacherBean;
import risingsunedu.Common.Impl.CommonConstant;
import risingsunedu.Service.IJiangyiService;
import risingsunedu.Service.ITeacherAdditionService;
import risingsunedu.Service.ITeacherService;
import framework.base.common.Pager;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("rteacher")
public class RTeacherController  extends BaseController{

	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	ITeacherService teacherService;
	
	@Autowired
	IJiangyiService jiangyiService;
	
	@Autowired
	ITeacherAdditionService teacherAdditionService;

	@Override
	public void setLogger() {
		logger = LoggerFactory.getLogger(getClass());
	}
	
	@RequestMapping("/teachermanage")
	public ModelAndView teacherManage() {
		ModelAndView modelAndView = new ModelAndView();
		
		//教师列表
		List<TeacherBean> teacherList = teacherService.selectList(null);
		modelAndView.addObject("teaList", teacherList);
		
		modelAndView.setViewName("risingsunedu/jiaoshi/jiaoshimanage");
		modelAndView.addObject(CommonConstant.navOpt, CommonConstant.jiaoshi);
		return modelAndView;
	}
	
	@RequestMapping("/teachersearch")
	public ModelAndView teacherSearch(TeacherBean bean) {
		ModelAndView modelAndView = new ModelAndView();
		
		if ("全部".equals(bean.getGrade())) {
			bean.setGrade(null);
		}
		
		if ("全部".equals(bean.getSubject())) {
			bean.setSubject(null);
		}
		
		//教师列表
		List<TeacherBean> teacherList = teacherService.selectList(bean);
		modelAndView.addObject("teaList", teacherList);
		
		modelAndView.setViewName("risingsunedu/jiaoshi/jiaoshisearch");
		return modelAndView;
	}
	
	@RequestMapping("/teacherinfo")
	public ModelAndView teacherInfo(TeacherBean bean) {
		ModelAndView modelAndView = new ModelAndView();
		//教师信息
		bean = teacherService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		
		//讲义个数
		int jyNum = jiangyiService.selectCountByTeaId(bean.getId());
		
		//教师附加信息
		TeacherAdditionBean teacherAdditionBean = new TeacherAdditionBean();
		teacherAdditionBean.setTea_id(bean.getId());
		teacherAdditionBean = teacherAdditionService.selectOne(teacherAdditionBean);
		modelAndView.addObject("teaaddition", teacherAdditionBean);
		
		//讲义信息
		Pager<JiangyiBean> pager = new Pager<JiangyiBean>();
		JiangyiBean jybBean = new JiangyiBean();
		
		Pager<JiangyiBean> jyPage = jiangyiService.selectPage(jybBean, pager);
		modelAndView.addObject("jyPage", jyPage);
		
		modelAndView.addObject("jynum", jyNum);
		modelAndView.setViewName("risingsunedu/jiaoshi/jiaoshiinfo");
		modelAndView.addObject(CommonConstant.navOpt, CommonConstant.jiaoshi);
		return modelAndView;
	}
}
