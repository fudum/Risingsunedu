package risingsunedu.Controller.Rse;


import java.util.List;

import javax.servlet.http.HttpSession;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.JiangyiBean;
import risingsunedu.Bean.JiangyiParseBean;
import risingsunedu.Bean.StudentAdditionBean;
import risingsunedu.Bean.StudentBean;
import risingsunedu.Common.Impl.CommonConstant;
import risingsunedu.Service.IJiangyiParseService;
import risingsunedu.Service.IJiangyiService;
import risingsunedu.Service.IStudentAdditionService;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("rdangan")
public class RDanganController extends BaseController {

	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	IJiangyiService jiangyiService;
	
	@Autowired
	IJiangyiParseService jiangyiParseService;
	
	@Autowired
	IStudentAdditionService studentAdditionService;

	@Override
	public void setLogger() {
		logger = LoggerFactory.getLogger(getClass());
	}
	
	@RequestMapping("/danganmanage")
	public ModelAndView danganManage(HttpSession session) {
		ModelAndView modelAndView = new ModelAndView();
		
		//没有登入情况下
		if (session.getAttribute("stuSession") == null) {
			modelAndView.setViewName("redirect:/client/login"+CommonConstant.sufStr+"?type=1");
		}
		else {
			//获取用户的session
			StudentBean stuBean = (StudentBean)session.getAttribute("stuSession");
			
			//用户附加信息添加到页面
			StudentAdditionBean stuAddnBean = new StudentAdditionBean();
			stuAddnBean.setStu_id(stuBean.getId());
			stuAddnBean = studentAdditionService.selectOne(stuAddnBean);
			modelAndView.addObject("stuAddnBean", stuAddnBean);
			
			//我的讲义信息添加到页面
			List<JiangyiBean> jyList = jiangyiService.selectByStuId(stuBean.getId());
			modelAndView.addObject("jyList", jyList);
			
			modelAndView.addObject(CommonConstant.navOpt, CommonConstant.dangan);
			modelAndView.setViewName("risingsunedu/dangan/danganmanage");
		}
		
		return modelAndView;
	}

	//讲义解析详情
	@RequestMapping("/jyparseinfo")
	public ModelAndView jyParseInfo(JiangyiBean bean) {
		ModelAndView modelAndView = new ModelAndView();
		
		//讲义解析信息
		JiangyiParseBean jyParseBean = new JiangyiParseBean();
		jyParseBean.setJy_id(bean.getId());
		List<JiangyiParseBean> jyParseList = jiangyiParseService.selectList(jyParseBean);
		
		bean = jiangyiService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		
		if (jyParseList.size() == 0) {//如果没有解析信息
			modelAndView.setViewName("risingsunedu/dangan/jyinfo");
		}
		else {//如果有解析信息
			modelAndView.addObject("jyParseList", jyParseList);
			modelAndView.setViewName("risingsunedu/dangan/jyparseinfo");
		}
		modelAndView.addObject(CommonConstant.navOpt, CommonConstant.dangan);
		return modelAndView;
	}

}
