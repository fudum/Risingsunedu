package risingsunedu.Controller.Rse;

import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Common.Impl.CommonConstant;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/rkaoshi")
public class RKaoshiController extends BaseController{

	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void setLogger() {
		logger = LoggerFactory.getLogger(getClass());
	}
	
	/**
	 * 考试信息页面
	 * @return
	 */
	@RequestMapping("/kaoshimanage")
	public ModelAndView kaoshimanage() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("risingsunedu/kaoshi/kaoshimanage");
		modelAndView.addObject(CommonConstant.navOpt, CommonConstant.zhongkao);
		return modelAndView;
	}
	
	//2015年中考
	@RequestMapping("/zhongkao")
	public ModelAndView zhongkao() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("risingsunedu/kaoshi/zhongkao");
		return modelAndView;
	}
	
	//分数线
	@RequestMapping("/fenshuxian")
	public ModelAndView fenshuxian() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("risingsunedu/kaoshi/fenshuxian");
		return modelAndView;
	}
	
	//招生信息
	@RequestMapping("/zhaosheng")
	public ModelAndView zhaosheng() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("risingsunedu/kaoshi/zhaosheng");
		return modelAndView;
	}
	
	//试题库
	@RequestMapping("/shitiku")
	public ModelAndView shitiku() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("risingsunedu/kaoshi/shitiku");
		return modelAndView;
	}
	
	//模拟录取
	@RequestMapping("/moniluqu")
	public ModelAndView moniluqu() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("risingsunedu/kaoshi/moniluqu");
		return modelAndView;
	}
	
	
	
	

}
