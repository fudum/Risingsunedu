package risingsunedu.Controller.Rse;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.JiangyiBean;
import risingsunedu.Bean.TeacherBean;
import risingsunedu.Common.Impl.CommonConstant;
import risingsunedu.Common.Impl.FileOperateUtilImpl;
import risingsunedu.Service.IJiangyiService;
import risingsunedu.Service.ITeacherService;
import framework.base.controller.BaseController;

/**
 * 讲义前台
 * @author fudum
 * @date 2014年11月3日
 */
@Controller
@RequestMapping("rjiangyi")
public class RJiangyiController extends BaseController{

	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;
	
	@Autowired
	ITeacherService teacherService;
	
	@Autowired
	IJiangyiService jiangyiService;

	@Override
	public void setLogger() {
		logger = LoggerFactory.getLogger(getClass());
	}
	
	/*
	 * 讲义显示
	 */
	@RequestMapping("/jiangyishow")
	public ModelAndView jiangyiShow() {
		ModelAndView modelAndView = new ModelAndView("risingsunedu/jiangyi/jiangyimanage");
		//教师信息
		List<TeacherBean> teacherList = teacherService.selectList(null);
		modelAndView.addObject("teacherList", teacherList);
		//讲义信息
		List<JiangyiBean> jyList = jiangyiService.selectNewest(null);
		modelAndView.addObject("jyList", jyList);
		modelAndView.addObject(CommonConstant.navOpt, CommonConstant.jiangyi);
		return modelAndView;
	}
	
	/*
	 * 讲义查询
	 */
	@RequestMapping("/jiangyisearch")
	public ModelAndView jiangyiSearch(JiangyiBean jybBean) {
		ModelAndView modelAndView = new ModelAndView("risingsunedu/jiangyi/jiangyisearch");
		
		//教师信息
		List<TeacherBean> teacherList = teacherService.selectList(null);
		modelAndView.addObject("teacherList", teacherList);
		//讲义信息
		List<JiangyiBean> jyList = jiangyiService.selectNewest(jybBean);
		modelAndView.addObject("jyList", jyList);
		modelAndView.addObject(CommonConstant.navOpt, CommonConstant.jiangyi);
		return modelAndView;
	}
	
	//讲义详情
	@RequestMapping("/info")
	public ModelAndView info(JiangyiBean bean) {
		ModelAndView modelAndView = new ModelAndView();
		bean = jiangyiService.selectOne(bean);
		modelAndView.addObject("bean", bean);
		modelAndView.setViewName("risingsunedu/jiangyi/jiangyiinfo");
		modelAndView.addObject(CommonConstant.navOpt, CommonConstant.jiangyi);
		return modelAndView;
	}
	
	//下载文件
	@RequestMapping("/download")
	public ModelAndView download(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String storeName = "201205051340364510870879724.zip";
		String realName = "Java设计模式.zip";
		String contentType = "application/octet-stream";
		
		FileOperateUtilImpl.download(request, response, storeName, contentType,
				realName);
		
		return null;
	}
	
}
