package risingsunedu.Controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import risingsunedu.Bean.SchoolBean;
import risingsunedu.Bean.SchoolLevelBean;
import risingsunedu.Common.Impl.CommonFunImpl;
import risingsunedu.Service.IMedTestCatgService;
import risingsunedu.Service.ISchoolLevelService;
import risingsunedu.Service.ISchoolService;
import framework.base.common.Pager;
import framework.base.controller.BaseController;

@Controller
@RequestMapping("/school")
public class SchoolController extends BaseController {

	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	IMedTestCatgService medTestCatgService;
	
	@Autowired
	ISchoolService schoolService;
	
	@Autowired
	ISchoolLevelService schoolLevelService;
	
	@Override
	public void setLogger() {
		logger = LoggerFactory.getLogger(getClass());
	}
	
	@RequestMapping("/manage")
	public ModelAndView manage() {
		ModelAndView modelAndView = new ModelAndView("background/school/schoolmanage");
		
		//中考信息类别
		Pager<SchoolBean> pager = new Pager<SchoolBean>();
		Pager<SchoolBean> list = schoolService.selectPage(null, pager);
		modelAndView.addObject("list", list);
		
		return modelAndView;
	}
	
	@RequestMapping("/add")
	public ModelAndView add() {
		ModelAndView modelAndView = new ModelAndView("background/school/schooladd");
		//学校级别信息
		List<SchoolLevelBean> list = schoolLevelService.selectList(null);
		modelAndView.addObject("list", list);
		
		return modelAndView;
	}
	
	@RequestMapping("/insert")
	public ModelAndView insert(SchoolBean schoolBean) {
		ModelAndView modelAndView = new ModelAndView("background/school/schooledit");
		schoolBean.setId(CommonFunImpl.uuid("SC"));
		schoolBean.setInserttime(CommonFunImpl.currentTime(1));
		String retVal;
		if (schoolService.insert(schoolBean) > 0) {
			retVal = "添加成功！";
		}
		else {
			retVal = "添加失败！";
		}
		modelAndView.addObject("retVal", retVal);
		modelAndView.addObject("bean", schoolBean);
		
		//学校级别信息
		List<SchoolLevelBean> list = schoolLevelService.selectList(null);
		modelAndView.addObject("list", list);
		
		return modelAndView;
	}
	
	@RequestMapping("/update")
	public ModelAndView update(SchoolBean schoolBean) {
		ModelAndView modelAndView = new ModelAndView("background/school/schooledit");
		String retVal;
		if (schoolService.update(schoolBean) > 0) {
			retVal = "编辑成功！";
		}
		else {
			retVal = "编辑失败！";
		}
		modelAndView.addObject("retVal", retVal);
		modelAndView.addObject("bean", schoolBean);
		
		//学校级别信息
		List<SchoolLevelBean> list = schoolLevelService.selectList(null);
		modelAndView.addObject("list", list);
				
		return modelAndView;
	}
	
	//删除
	@RequestMapping("/delete")
	public ModelAndView delete(SchoolBean schoolBean,HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		List<String> list = new ArrayList<String>();
		int ret = 1;
		ret = schoolService.delete(schoolBean);
		if (ret > 0) {
			list.add("1");
			list.add("删除成功!");
		} else {
			list.add("0");
			list.add("删除失败!");
		}
		// 以流的形式返回
		try {
			PrintWriter out = response.getWriter();
			String s = JSONArray.fromObject(list).toString();
			out.print(s);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	//编辑页面
	@RequestMapping("/edit")
	public ModelAndView edit(SchoolBean schoolBean) {
		ModelAndView modelAndView = new ModelAndView("background/school/schooledit");
		schoolBean = schoolService.selectOne(schoolBean);
		modelAndView.addObject("bean", schoolBean);
		//学校级别信息
		List<SchoolLevelBean> list = schoolLevelService.selectList(null);
		modelAndView.addObject("list", list);
		return modelAndView;
	}
	
	// 下一页
	@RequestMapping("/nextpage")
	public ModelAndView nextPage(int pageNum, int totalPage) {
		ModelAndView modelAndView = new ModelAndView("background/school/schoolmanage");
		
		Pager<SchoolBean> pager = new Pager<SchoolBean>();
		if (pageNum < totalPage) {// 如果小于总页数
			pageNum++;
		} else {// 返回首页
			pageNum = 1;
		}
		pager.setCurtPage(pageNum);
		Pager<SchoolBean> list = schoolService.selectPage(null, pager);
		modelAndView.addObject("list", list);
		
		return modelAndView;
	}

	// 上一页
	@RequestMapping("/frontpage")
	public ModelAndView frontPage(int pageNum, int totalPage) {
		ModelAndView modelAndView = new ModelAndView("background/school/schoolmanage");

		Pager<SchoolBean> pager = new Pager<SchoolBean>();
		if (pageNum > 1) {// 如果小于总页数
			pageNum--;
		} else {// 返回首页
			pageNum = totalPage;
		}
		pager.setCurtPage(pageNum);
		Pager<SchoolBean> list = schoolService.selectPage(null, pager);
		modelAndView.addObject("list", list);
		
		return modelAndView;
	}

	// 最后一页
	@RequestMapping("/lastpage")
	public ModelAndView lastPage(int totalPage) {
		ModelAndView modelAndView = new ModelAndView("background/school/schoolmanage");
		
		Pager<SchoolBean> pager = new Pager<SchoolBean>();
		pager.setCurtPage(totalPage);
		Pager<SchoolBean> list = schoolService.selectPage(null, pager);
		modelAndView.addObject("list", list);

		return modelAndView;
	}

}
