package risingsunedu.Bean;

import framework.base.bean.BaseBean;

public class XuebabaobanBean extends BaseBean {
	/**
	 * 学霸档案-我的培优课程（我要报班）
	 */
	private static final long serialVersionUID = 1L;
	private String id;// 主键
	private String stuid;// 学生主键
	private String kechengid;// 课程编号
	private String kechengname;// 课程名称
	private String teaid;// 教师编号
	private String teaname;// 教师名称
	private String inserttime;// 插入时间
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getStuid() {
		return stuid;
	}
	public void setStuid(String stuid) {
		this.stuid = stuid;
	}
	public String getKechengid() {
		return kechengid;
	}
	public void setKechengid(String kechengid) {
		this.kechengid = kechengid;
	}
	public String getKechengname() {
		return kechengname;
	}
	public void setKechengname(String kechengname) {
		this.kechengname = kechengname;
	}
	public String getTeaid() {
		return teaid;
	}
	public void setTeaid(String teaid) {
		this.teaid = teaid;
	}
	public String getTeaname() {
		return teaname;
	}
	public void setTeaname(String teaname) {
		this.teaname = teaname;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}

}