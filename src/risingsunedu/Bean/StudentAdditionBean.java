package risingsunedu.Bean;

import framework.base.bean.BaseBean;

public class StudentAdditionBean extends BaseBean {
	
	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;
	
	private String id;//id
	private String stu_id;//学生id 
	private String kechengsheji;//课程设计 
	private String shangkejilu;// 上课记录
	private String tiku;// 题库
	private String inserttime;// 插入时间
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getStu_id() {
		return stu_id;
	}
	public void setStu_id(String stu_id) {
		this.stu_id = stu_id;
	}
	public String getKechengsheji() {
		return kechengsheji;
	}
	public void setKechengsheji(String kechengsheji) {
		this.kechengsheji = kechengsheji;
	}
	public String getShangkejilu() {
		return shangkejilu;
	}
	public void setShangkejilu(String shangkejilu) {
		this.shangkejilu = shangkejilu;
	}
	public String getTiku() {
		return tiku;
	}
	public void setTiku(String tiku) {
		this.tiku = tiku;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	
	

}