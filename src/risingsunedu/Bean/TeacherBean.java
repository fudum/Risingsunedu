package risingsunedu.Bean;

import framework.base.bean.BaseBean;

public class TeacherBean extends BaseBean {
	/**
	 * 教师bean
	 */
	private static final long serialVersionUID = 1L;
	private String id;// 主键
	private String name;// 姓名
	private String sex;// 性别
	private String tealevel;// 教师级别
	private String grade;// 
	private String subject;// 教学科目
	private String jiaoling;// 实际教龄
	private String headpath;// 
	private String picpath;// 头像路径
	private String videopath;// 视频路径
	private String phone;// 电话
	private String inserttime;// 
	private String address;// 地址
	private String birthday;// 出生年月
	private String techang;// 班次类型
	private String pingfen;// 教学评分
	private String stunumber;// 学生人数
	private String keshinumber;// 课时数
	private String torder;// 教师排序
	private String danwei;// 单位
	private String zhicheng;// 职称
	private String zhijiaokemu;// 职教科目
	private String yuyin;// 教师有话说
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getTealevel() {
		return tealevel;
	}
	public void setTealevel(String tealevel) {
		this.tealevel = tealevel;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getJiaoling() {
		return jiaoling;
	}
	public void setJiaoling(String jiaoling) {
		this.jiaoling = jiaoling;
	}
	public String getHeadpath() {
		return headpath;
	}
	public void setHeadpath(String headpath) {
		this.headpath = headpath;
	}
	public String getPicpath() {
		return picpath;
	}
	public void setPicpath(String picpath) {
		this.picpath = picpath;
	}
	public String getVideopath() {
		return videopath;
	}
	public void setVideopath(String videopath) {
		this.videopath = videopath;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getTechang() {
		return techang;
	}
	public void setTechang(String techang) {
		this.techang = techang;
	}
	public String getPingfen() {
		return pingfen;
	}
	public void setPingfen(String pingfen) {
		this.pingfen = pingfen;
	}
	public String getStunumber() {
		return stunumber;
	}
	public void setStunumber(String stunumber) {
		this.stunumber = stunumber;
	}
	public String getKeshinumber() {
		return keshinumber;
	}
	public void setKeshinumber(String keshinumber) {
		this.keshinumber = keshinumber;
	}
	public String getTorder() {
		return torder;
	}
	public void setTorder(String torder) {
		this.torder = torder;
	}
	public String getDanwei() {
		return danwei;
	}
	public void setDanwei(String danwei) {
		this.danwei = danwei;
	}
	public String getZhicheng() {
		return zhicheng;
	}
	public void setZhicheng(String zhicheng) {
		this.zhicheng = zhicheng;
	}
	public String getZhijiaokemu() {
		return zhijiaokemu;
	}
	public void setZhijiaokemu(String zhijiaokemu) {
		this.zhijiaokemu = zhijiaokemu;
	}
	public String getYuyin() {
		return yuyin;
	}
	public void setYuyin(String yuyin) {
		this.yuyin = yuyin;
	}

}