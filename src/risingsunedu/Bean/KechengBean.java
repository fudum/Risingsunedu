package risingsunedu.Bean;

import framework.base.bean.BaseBean;

public class KechengBean extends BaseBean {
	/**
	 * 课程管理
	 */
	private static final long serialVersionUID = 1L;
	private String id;// 主键编号
	private String name;// 课程名称
	private String year;// 年份
	private String grade;// 年级
	private String subject;// 科目
	private String banci;// 班次
	private String bjtype;// 班级类型
	private String timeframe;// 时段
	private String lessonday;// 上课天 周日
	private String address;// 上课地点
	private String startdate;// 开始日期
	private String enddate;// 结束日期
	private String starttime;// 开始时间
	private String endtime;// 结束时间
	private String tea_id;// 教师编号
	private String tea_name;// 教师姓名
	private String inserttime;// 插入时间
	private String playnum;// 展示次数
	private String price;// 价格
	private String catalog;// 目录
	private TeacherBean teacherBean;//教师信息
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getBanci() {
		return banci;
	}
	public void setBanci(String banci) {
		this.banci = banci;
	}
	public String getBjtype() {
		return bjtype;
	}
	public void setBjtype(String bjtype) {
		this.bjtype = bjtype;
	}
	public String getTimeframe() {
		return timeframe;
	}
	public void setTimeframe(String timeframe) {
		this.timeframe = timeframe;
	}
	public String getLessonday() {
		return lessonday;
	}
	public void setLessonday(String lessonday) {
		this.lessonday = lessonday;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	public String getStarttime() {
		return starttime;
	}
	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}
	public String getEndtime() {
		return endtime;
	}
	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}
	public String getTea_id() {
		return tea_id;
	}
	public void setTea_id(String tea_id) {
		this.tea_id = tea_id;
	}
	public String getTea_name() {
		return tea_name;
	}
	public void setTea_name(String tea_name) {
		this.tea_name = tea_name;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	public String getPlaynum() {
		return playnum;
	}
	public void setPlaynum(String playnum) {
		this.playnum = playnum;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getCatalog() {
		return catalog;
	}
	public void setCatalog(String catalog) {
		this.catalog = catalog;
	}
	public TeacherBean getTeacherBean() {
		return teacherBean;
	}
	public void setTeacherBean(TeacherBean teacherBean) {
		this.teacherBean = teacherBean;
	}
	
}