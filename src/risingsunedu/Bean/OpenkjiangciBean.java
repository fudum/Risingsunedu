package risingsunedu.Bean;

import framework.base.bean.BaseBean;

public class OpenkjiangciBean extends BaseBean {
	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;
	
	private String id;// 主键
	private String openk_id;// 公开课id
	private String name;// 讲次名称
	private String jorder;// 讲词顺序
	private String inserttime;// 插入时间
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOpenk_id() {
		return openk_id;
	}
	public void setOpenk_id(String openk_id) {
		this.openk_id = openk_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getJorder() {
		return jorder;
	}
	public void setJorder(String jorder) {
		this.jorder = jorder;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
}