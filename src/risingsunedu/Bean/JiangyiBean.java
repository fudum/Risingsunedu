package risingsunedu.Bean;

import framework.base.bean.BaseBean;

public class JiangyiBean extends BaseBean {
	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;
	private String id;// 主键
	private String name;// 讲义名称
	private String bj_id;// 班级编号
	private String bj_name;// 班级名称
	private String jiangyipath;// 讲义路径
	private String inserttime;// 插入时间
	private String updatetime;// 更新时间
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBj_id() {
		return bj_id;
	}
	public void setBj_id(String bj_id) {
		this.bj_id = bj_id;
	}
	public String getBj_name() {
		return bj_name;
	}
	public void setBj_name(String bj_name) {
		this.bj_name = bj_name;
	}
	public String getJiangyipath() {
		return jiangyipath;
	}
	public void setJiangyipath(String jiangyipath) {
		this.jiangyipath = jiangyipath;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	public String getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(String updatetime) {
		this.updatetime = updatetime;
	}
}