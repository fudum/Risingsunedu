package risingsunedu.Bean;

import framework.base.bean.BaseBean;

public class SchoolLevelBean extends BaseBean {
	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;
	private String id;// 
	private String name;// 
	private String inserttime;// 
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	
	

}