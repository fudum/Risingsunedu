package risingsunedu.Bean;

import framework.base.bean.BaseBean;

public class ZhangjieBean extends BaseBean {
	/**
	 * 章节表
	 */
	private static final long serialVersionUID = 1L;
	private String id;// 主键
	private String grade;// 年级
	private String subject;// 科目
	private String name;// 名称
	private String inserttime;// 插入时间
	private String zorder;//排序
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	public String getZorder() {
		return zorder;
	}
	public void setZorder(String zorder) {
		this.zorder = zorder;
	}
}