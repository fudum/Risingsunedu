package risingsunedu.Bean;

import framework.base.bean.BaseBean;

public class KechengjiangciBean extends BaseBean {
	/**
	 * 课程讲次Bean
	 */
	private static final long serialVersionUID = 1L;
	private String id;// 
	private String kecheng_id;// 课程编号
	private String name;// 讲次名称
	private String jorder;// 
	private String inserttime;// 
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getKecheng_id() {
		return kecheng_id;
	}
	public void setKecheng_id(String kecheng_id) {
		this.kecheng_id = kecheng_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getJorder() {
		return jorder;
	}
	public void setJorder(String jorder) {
		this.jorder = jorder;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
}