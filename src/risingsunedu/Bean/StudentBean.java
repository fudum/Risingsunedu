package risingsunedu.Bean;

import framework.base.bean.BaseBean;

public class StudentBean extends BaseBean {
	/**
	 * 学生bean
	 */
	private static final long serialVersionUID = 1L;
	private String id;//
	private String stuid;// 用户编号
	private String name;// 姓名
	private String password;// 密码
	private String sex;// 性别
	private String school;// 学校
	private String phone;// 电话
	private String address;// 地址
	private String picture;// 用户头像
	private String inserttime;// 插入时间
	private String zhuangtai;// 登入状态1为可以登入，2为不可登入
	private String kaiqidangan;// 学霸档案是否开启 1为开启，其他为不开启
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getStuid() {
		return stuid;
	}
	public void setStuid(String stuid) {
		this.stuid = stuid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getSchool() {
		return school;
	}
	public void setSchool(String school) {
		this.school = school;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	public String getZhuangtai() {
		return zhuangtai;
	}
	public void setZhuangtai(String zhuangtai) {
		this.zhuangtai = zhuangtai;
	}
	public String getKaiqidangan() {
		return kaiqidangan;
	}
	public void setKaiqidangan(String kaiqidangan) {
		this.kaiqidangan = kaiqidangan;
	}

}