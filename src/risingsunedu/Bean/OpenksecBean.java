package risingsunedu.Bean;

import framework.base.bean.BaseBean;

public class OpenksecBean extends BaseBean {
	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;
	private String id;// 
	private String openk_id;// 
	private String secname;// 第几讲
	private String thirdname;// 第几节
	private String picpath;// 
	private String videopath;// 视频路径
	private String inserttime;// 插入时间
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOpenk_id() {
		return openk_id;
	}
	public void setOpenk_id(String openk_id) {
		this.openk_id = openk_id;
	}
	public String getSecname() {
		return secname;
	}
	public void setSecname(String secname) {
		this.secname = secname;
	}
	public String getThirdname() {
		return thirdname;
	}
	public void setThirdname(String thirdname) {
		this.thirdname = thirdname;
	}
	public String getPicpath() {
		return picpath;
	}
	public void setPicpath(String picpath) {
		this.picpath = picpath;
	}
	public String getVideopath() {
		return videopath;
	}
	public void setVideopath(String videopath) {
		this.videopath = videopath;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
}