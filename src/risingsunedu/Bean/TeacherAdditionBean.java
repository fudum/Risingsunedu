package risingsunedu.Bean;

import framework.base.bean.BaseBean;

public class TeacherAdditionBean extends BaseBean {
	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;
	private String id;// 
	private String tea_id;// 
	private String teainfo;// 
	private String teachengguo;// 
	private String teaweigui;// 
	private String inserttime;// 
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTea_id() {
		return tea_id;
	}
	public void setTea_id(String tea_id) {
		this.tea_id = tea_id;
	}
	public String getTeainfo() {
		return teainfo;
	}
	public void setTeainfo(String teainfo) {
		this.teainfo = teainfo;
	}
	public String getTeachengguo() {
		return teachengguo;
	}
	public void setTeachengguo(String teachengguo) {
		this.teachengguo = teachengguo;
	}
	public String getTeaweigui() {
		return teaweigui;
	}
	public void setTeaweigui(String teaweigui) {
		this.teaweigui = teaweigui;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	
	

}