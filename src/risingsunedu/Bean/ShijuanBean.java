package risingsunedu.Bean;

import framework.base.bean.BaseBean;

public class ShijuanBean extends BaseBean {
	/**
	 * 试卷bean
	 */
	private static final long serialVersionUID = 1L;
	private String id;// 主键
	private String name;// 试卷名称
	private String grade;// 年级
	private String subject;// 科目
	private String stype;// 试卷类型
	private String year;// 试卷年份
	private String sjpath;// 试卷存放路径
	private String wordpath;// 
	private String xueqi;// 学期
	private String downnum;// 下载次数
	private String inserttime;// 插入时间
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getStype() {
		return stype;
	}
	public void setStype(String stype) {
		this.stype = stype;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getSjpath() {
		return sjpath;
	}
	public void setSjpath(String sjpath) {
		this.sjpath = sjpath;
	}
	public String getWordpath() {
		return wordpath;
	}
	public void setWordpath(String wordpath) {
		this.wordpath = wordpath;
	}
	public String getXueqi() {
		return xueqi;
	}
	public void setXueqi(String xueqi) {
		this.xueqi = xueqi;
	}
	public String getDownnum() {
		return downnum;
	}
	public void setDownnum(String downnum) {
		this.downnum = downnum;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	
}