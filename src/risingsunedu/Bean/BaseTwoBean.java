package risingsunedu.Bean;

import framework.base.bean.BaseBean;

public class BaseTwoBean extends BaseBean {
	/**
	 * 基本信息二级
	 */
	private static final long serialVersionUID = 1L;
	private String id;// 主键
	private String botype;// 一级类别
	private String name;//  名称
	private Integer border;// 排序
	private String inserttime;// 插入时间
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBotype() {
		return botype;
	}
	public void setBotype(String botype) {
		this.botype = botype;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getBorder() {
		return border;
	}
	public void setBorder(Integer border) {
		this.border = border;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	
	

}