package risingsunedu.Bean;

import framework.base.bean.BaseBean;

public class ZuoyeBean extends BaseBean {
	/**
	 * 培优作业bean
	 */
	private static final long serialVersionUID = 1L;
	private String id;// 主键
	private String name;// 作业名称
	private String subject;// 科目
	private String originalpath;// 原始作业路径
	private String finishpath;// 完成作业路径
	private String inserttime;// 插入时间
	private String stuid;// 学生主键
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getOriginalpath() {
		return originalpath;
	}
	public void setOriginalpath(String originalpath) {
		this.originalpath = originalpath;
	}
	public String getFinishpath() {
		return finishpath;
	}
	public void setFinishpath(String finishpath) {
		this.finishpath = finishpath;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	public String getStuid() {
		return stuid;
	}
	public void setStuid(String stuid) {
		this.stuid = stuid;
	}
	
	

}