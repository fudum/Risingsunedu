package risingsunedu.Bean;

import framework.base.bean.BaseBean;

public class BanjijieciBean extends BaseBean {
	/**
	 * 班级节次表
	 */
	private static final long serialVersionUID = 1L;
	private String id;// 主键
	private String jiangci_id;// 节次主键
	private String jiangci_name;// 讲次名称
	private String jieci_name;// 节次名称
	private String videopath;// 视频地址
	private String inserttime;// 插入时间
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getJiangci_id() {
		return jiangci_id;
	}
	public void setJiangci_id(String jiangci_id) {
		this.jiangci_id = jiangci_id;
	}
	public String getJiangci_name() {
		return jiangci_name;
	}
	public void setJiangci_name(String jiangci_name) {
		this.jiangci_name = jiangci_name;
	}
	public String getJieci_name() {
		return jieci_name;
	}
	public void setJieci_name(String jieci_name) {
		this.jieci_name = jieci_name;
	}
	public String getVideopath() {
		return videopath;
	}
	public void setVideopath(String videopath) {
		this.videopath = videopath;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}

}