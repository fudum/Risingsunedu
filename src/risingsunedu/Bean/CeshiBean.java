package risingsunedu.Bean;

import framework.base.bean.BaseBean;

public class CeshiBean extends BaseBean {
	/**
	 * 培优测试
	 */
	private static final long serialVersionUID = 1L;
	private String id;// 主键
	private String name;// 测试名称
	private String subject;// 测试科目
	private String originalpath;// 原始测试试卷
	private String finishpath;// 完成测试试卷
	private String inserttime;// 插入时间
	private String stuid;// 学生主键
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getOriginalpath() {
		return originalpath;
	}
	public void setOriginalpath(String originalpath) {
		this.originalpath = originalpath;
	}
	public String getFinishpath() {
		return finishpath;
	}
	public void setFinishpath(String finishpath) {
		this.finishpath = finishpath;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	public String getStuid() {
		return stuid;
	}
	public void setStuid(String stuid) {
		this.stuid = stuid;
	}

}