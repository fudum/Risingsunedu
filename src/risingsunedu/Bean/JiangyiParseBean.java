package risingsunedu.Bean;

import framework.base.bean.BaseBean;

public class JiangyiParseBean extends BaseBean {
	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;
	
	private String id;// 
	private String jy_id;// 
	private String timu;// 
	private String parse;// 
	private String videopath;// 
	private String inserttime;// 
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getJy_id() {
		return jy_id;
	}
	public void setJy_id(String jy_id) {
		this.jy_id = jy_id;
	}
	public String getTimu() {
		return timu;
	}
	public void setTimu(String timu) {
		this.timu = timu;
	}
	public String getParse() {
		return parse;
	}
	public void setParse(String parse) {
		this.parse = parse;
	}
	public String getVideopath() {
		return videopath;
	}
	public void setVideopath(String videopath) {
		this.videopath = videopath;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	
	

}