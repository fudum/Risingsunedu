package risingsunedu.Bean;

import framework.base.bean.BaseBean;

public class XuebaBean extends BaseBean {
	/**
	 * 学霸bean
	 */
	private static final long serialVersionUID = 1L;
	private String id;// 主键
	private String picpath;// 图片路径
	private String porder;// 显示顺序
	private String flag;// 是否显示
	private String inserttime;// 插入时间
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPicpath() {
		return picpath;
	}
	public void setPicpath(String picpath) {
		this.picpath = picpath;
	}
	public String getPorder() {
		return porder;
	}
	public void setPorder(String porder) {
		this.porder = porder;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	

}