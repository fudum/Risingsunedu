package risingsunedu.Bean;

import framework.base.bean.BaseBean;

public class OpenkBean extends BaseBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;// 
	private String name;// 公开课名称
	private String grade;// 年级
	private String subject;// 科目
	private String tea_id;// 教师编号
	private String tea_name;// 教师姓名
	private String info;// 公开课详情
	private Integer playnum;// 播放次数
	private String inserttime;// 
	private String kctype;//课程类别
	private String catalog;//目录详情
	private TeacherBean teacherBean;//教师
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getTea_id() {
		return tea_id;
	}
	public void setTea_id(String tea_id) {
		this.tea_id = tea_id;
	}
	public String getTea_name() {
		return tea_name;
	}
	public void setTea_name(String tea_name) {
		this.tea_name = tea_name;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	public TeacherBean getTeacherBean() {
		return teacherBean;
	}
	public void setTeacherBean(TeacherBean teacherBean) {
		this.teacherBean = teacherBean;
	}
	public Integer getPlaynum() {
		return playnum;
	}
	public void setPlaynum(Integer playnum) {
		this.playnum = playnum;
	}
	public String getKctype() {
		return kctype;
	}
	public void setKctype(String kctype) {
		this.kctype = kctype;
	}
	public String getCatalog() {
		return catalog;
	}
	public void setCatalog(String catalog) {
		this.catalog = catalog;
	}	
}