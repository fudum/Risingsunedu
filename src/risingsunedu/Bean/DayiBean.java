package risingsunedu.Bean;

import framework.base.bean.BaseBean;

public class DayiBean extends BaseBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;// 主键
	private String name;// 名称
	private String subject;// 科目
	private String grade;// 年级
	private String tea_id;// 教师编号
	private String tea_name;// 教师名称
	private String info;// 详情
	private String picpath;// 照片地址
	private String videopath;// 视频地址
	private Integer playnum;// 播放次数
	private String inserttime;// 插入时间
	private String nanyidu;//难易度
	private String chapter;//章节
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public String getTea_id() {
		return tea_id;
	}
	public void setTea_id(String tea_id) {
		this.tea_id = tea_id;
	}
	public String getTea_name() {
		return tea_name;
	}
	public void setTea_name(String tea_name) {
		this.tea_name = tea_name;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public String getPicpath() {
		return picpath;
	}
	public void setPicpath(String picpath) {
		this.picpath = picpath;
	}
	public String getVideopath() {
		return videopath;
	}
	public void setVideopath(String videopath) {
		this.videopath = videopath;
	}
	
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	public Integer getPlaynum() {
		return playnum;
	}
	public void setPlaynum(Integer playnum) {
		this.playnum = playnum;
	}
	public String getNanyidu() {
		return nanyidu;
	}
	public void setNanyidu(String nanyidu) {
		this.nanyidu = nanyidu;
	}
	public String getChapter() {
		return chapter;
	}
	public void setChapter(String chapter) {
		this.chapter = chapter;
	}	
	
	
	
}