package risingsunedu.Bean;

import framework.base.bean.BaseBean;

public class StudentbanjiBean extends BaseBean {
	/**
	 * 学生报班表
	 */
	private static final long serialVersionUID = 1L;
	private String id;// 
	private String stu_id;// 
	private String banji_id;// 
	private String banjiname;// 培优课堂同步班级名称
	private String teaid;// 教师编号
	private String teaname;// 教师姓名
	private String inserttime;// 
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getStu_id() {
		return stu_id;
	}
	public void setStu_id(String stu_id) {
		this.stu_id = stu_id;
	}
	public String getBanji_id() {
		return banji_id;
	}
	public void setBanji_id(String banji_id) {
		this.banji_id = banji_id;
	}
	public String getBanjiname() {
		return banjiname;
	}
	public void setBanjiname(String banjiname) {
		this.banjiname = banjiname;
	}
	public String getTeaid() {
		return teaid;
	}
	public void setTeaid(String teaid) {
		this.teaid = teaid;
	}
	public String getTeaname() {
		return teaname;
	}
	public void setTeaname(String teaname) {
		this.teaname = teaname;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	
	

}