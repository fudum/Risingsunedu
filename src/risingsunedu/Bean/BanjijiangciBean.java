package risingsunedu.Bean;

import framework.base.bean.BaseBean;

public class BanjijiangciBean extends BaseBean {
	/**
	 * 班级讲次表
	 */
	private static final long serialVersionUID = 1L;
	private String id;// 
	private String banji_id;// 
	private String name;// 
	private Integer jorder;
	private String inserttime;// 
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBanji_id() {
		return banji_id;
	}
	public void setBanji_id(String banji_id) {
		this.banji_id = banji_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	public Integer getJorder() {
		return jorder;
	}
	public void setJorder(Integer jorder) {
		this.jorder = jorder;
	}
	
}