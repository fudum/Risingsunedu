package risingsunedu.Bean;

import framework.base.bean.BaseBean;

public class StudentKechengBean extends BaseBean {
	/**
	 * 学生的课程
	 */
	private static final long serialVersionUID = 1L;
	private String id;// 主键
	private String name;// 名称
	private String grade;// 年级
	private String subject;// 科目
	private String catalog;// 目录
	private String inserttime;// 插入时间
	private String stuid;// 学生主键
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getCatalog() {
		return catalog;
	}
	public void setCatalog(String catalog) {
		this.catalog = catalog;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	public String getStuid() {
		return stuid;
	}
	public void setStuid(String stuid) {
		this.stuid = stuid;
	}
}