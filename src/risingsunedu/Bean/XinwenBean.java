package risingsunedu.Bean;

import framework.base.bean.BaseBean;

public class XinwenBean extends BaseBean {
	/**
	 * @author fudum
	 */
	private static final long serialVersionUID = 1L;
	private String id;// 
	private String medtesttype;// 新闻类别
	private String title;// 标题
	private String neirong;// 内容
	private String riqi;// 日期
	private String hits;// 点击量
	private String inserttime;// 插入时间
	private String school;// 
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMedtesttype() {
		return medtesttype;
	}
	public void setMedtesttype(String medtesttype) {
		this.medtesttype = medtesttype;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getNeirong() {
		return neirong;
	}
	public void setNeirong(String neirong) {
		this.neirong = neirong;
	}
	public String getRiqi() {
		return riqi;
	}
	public void setRiqi(String riqi) {
		this.riqi = riqi;
	}
	public String getHits() {
		return hits;
	}
	public void setHits(String hits) {
		this.hits = hits;
	}
	public String getInserttime() {
		return inserttime;
	}
	public void setInserttime(String inserttime) {
		this.inserttime = inserttime;
	}
	public String getSchool() {
		return school;
	}
	public void setSchool(String school) {
		this.school = school;
	}
	
}