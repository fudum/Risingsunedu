package risingsunedu.DAO;

import java.util.List;

import risingsunedu.Bean.JiangyiBean;
import framework.base.dao.IBaseDAO;

public interface IJiangyiDAO extends IBaseDAO {

	//搜寻最新讲义
	public List<JiangyiBean> selectNewest(JiangyiBean jiangyiBean);
	
	public int selectCountByTeaId(String teaId);
	
	/**
	 * 根据学生编号查找讲义
	 * @param stu_id
	 * @return
	 */
	public List<JiangyiBean> selectByStuId(String stu_id);
	
	/**
	 * 根据教师id查找讲义信息
	 * @param tea_id
	 * @return
	 */
	public List<JiangyiBean> selectByTeaId(String tea_id);

}
