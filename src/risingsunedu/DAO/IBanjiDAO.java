package risingsunedu.DAO;

import risingsunedu.Bean.BanjiBean;
import framework.base.common.Pager;
import framework.base.dao.IBaseDAO;

public interface IBanjiDAO extends IBaseDAO {

	public String selectMaxId(BanjiBean bean);
	
	public Pager<BanjiBean> selectPagerWithTeacher(BanjiBean bean,Pager<BanjiBean> pager);
}
