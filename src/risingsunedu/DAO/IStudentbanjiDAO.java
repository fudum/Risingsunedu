package risingsunedu.DAO;

import risingsunedu.Bean.StudentbanjiBean;
import framework.base.dao.IBaseDAO;

public interface IStudentbanjiDAO extends IBaseDAO {

	/**
	 * 删除by stuid
	 * @return
	 */
	public int deleteByStuid(StudentbanjiBean studentbanjiBean);
	
}
