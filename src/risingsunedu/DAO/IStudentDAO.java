package risingsunedu.DAO;

import framework.base.dao.IBaseDAO;

public interface IStudentDAO extends IBaseDAO {

	/*
	 * 获取学生最大id
	 */
	public String getMaxStuId();
	
	
}
