package risingsunedu.DAO;


import java.util.List;

import risingsunedu.Bean.OpenksecBean;
import framework.base.dao.IBaseDAO;

public interface IOpenksecDAO extends IBaseDAO {
	
	public List<OpenksecBean> selectListByOpenkId(String openkId);
}
