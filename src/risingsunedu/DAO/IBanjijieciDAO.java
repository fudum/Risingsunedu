package risingsunedu.DAO;

import java.util.List;

import risingsunedu.Bean.BanjijieciBean;
import framework.base.dao.IBaseDAO;

public interface IBanjijieciDAO extends IBaseDAO {
	
	public List<BanjijieciBean> selectListByBanjiId(String banjiId);
	
	public int updateByJiangciId(BanjijieciBean bean);
}
