package risingsunedu.DAO.Impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import framework.base.common.Pager;
import framework.base.dao.IBaseDAOSupport;
import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.Bean.OpenkBean;
import risingsunedu.DAO.IOpenkDAO;

@Repository
public class OpenkDAOImpl extends BaseDAOImpl implements IOpenkDAO{

	@Autowired
	private IBaseDAOSupport support;
	
	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.Openk.";
	}

	@Override
	public Pager<OpenkBean> selectOpenkTeacher(OpenkBean bean,
			Pager<OpenkBean> pager) {
		return support.selectPage(getNamespace() + "selectopenandteacher", bean, pager);
	}

	@Override
	public int update(Map<String, String> map) {
		return support.update(getNamespace() + "updateByKctype", map);
	}	

}
