package risingsunedu.DAO.Impl;

import org.springframework.stereotype.Repository;

import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.DAO.IKechengjiangciDAO;

@Repository
public class KechengjiangciDAOImpl extends BaseDAOImpl implements IKechengjiangciDAO{

	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.Kechengjiangci.";
	}	

}
