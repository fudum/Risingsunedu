package risingsunedu.DAO.Impl;

import org.springframework.stereotype.Repository;

import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.DAO.IJiangyiParseDAO;

@Repository
public class JiangyiParseDAOImpl extends BaseDAOImpl implements IJiangyiParseDAO{
	
	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.JiangyiParse.";
	}

}
