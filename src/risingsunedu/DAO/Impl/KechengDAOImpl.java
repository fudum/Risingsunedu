package risingsunedu.DAO.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import framework.base.common.Pager;
import framework.base.dao.IBaseDAOSupport;
import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.Bean.KechengBean;
import risingsunedu.DAO.IKechengDAO;

@Repository
public class KechengDAOImpl extends BaseDAOImpl implements IKechengDAO{
	
	@Autowired
	IBaseDAOSupport daoSupport;
	
	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.Kecheng.";
	}

	@Override
	public Pager<KechengBean> selectPagerWithTeacher(KechengBean bean,
			Pager<KechengBean> pager) {
		return daoSupport.selectPage(getNamespace() + "selectpagerwithteacher", bean, pager);
	}
}
