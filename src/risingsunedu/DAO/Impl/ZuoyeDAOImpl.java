package risingsunedu.DAO.Impl;

import org.springframework.stereotype.Repository;

import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.DAO.IZuoyeDAO;

@Repository
public class ZuoyeDAOImpl extends BaseDAOImpl implements IZuoyeDAO{

	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.Zuoye.";
	}
	
}
