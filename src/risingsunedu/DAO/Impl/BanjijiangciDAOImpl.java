package risingsunedu.DAO.Impl;

import org.springframework.stereotype.Repository;

import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.DAO.IBanjijiangciDAO;

@Repository
public class BanjijiangciDAOImpl extends BaseDAOImpl implements IBanjijiangciDAO{

	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.Banjijiangci.";
	}	

}
