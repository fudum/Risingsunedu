package risingsunedu.DAO.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import framework.base.dao.IBaseDAOSupport;
import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.Bean.OpenksecBean;
import risingsunedu.DAO.IOpenksecDAO;

@Repository
public class OpenksecDAOImpl extends BaseDAOImpl implements IOpenksecDAO{

	@Autowired
	private IBaseDAOSupport support;
	
	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.Openksec.";
	}

	@Override
	public List<OpenksecBean> selectListByOpenkId(String openkId) {
		return support.selectList(getNamespace() + "selectListByOpenkId", openkId);
	}	

}
