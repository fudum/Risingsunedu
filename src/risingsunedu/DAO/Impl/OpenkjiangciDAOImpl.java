package risingsunedu.DAO.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import framework.base.dao.IBaseDAOSupport;
import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.Bean.OpenkjiangciBean;
import risingsunedu.DAO.IOpenkjiangciDAO;

@Repository
public class OpenkjiangciDAOImpl extends BaseDAOImpl implements IOpenkjiangciDAO{

	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.Openkjiangci.";
	}
	
	@Autowired
	private IBaseDAOSupport support;

	@Override
	public int updateJorder(OpenkjiangciBean bean) {
		return support.update(getNamespace()+ "updateJorder" , bean);
	}	

}
