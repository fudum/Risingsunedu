package risingsunedu.DAO.Impl;

import org.springframework.stereotype.Repository;

import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.DAO.IStudentKechengDAO;

@Repository
public class StudentKechengDAOImpl extends BaseDAOImpl implements IStudentKechengDAO{

	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.StudentKecheng.";
	}
	
}
