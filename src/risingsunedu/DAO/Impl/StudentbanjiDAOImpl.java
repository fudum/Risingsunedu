package risingsunedu.DAO.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import framework.base.dao.IBaseDAOSupport;
import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.Bean.StudentbanjiBean;
import risingsunedu.DAO.IStudentbanjiDAO;

@Repository
public class StudentbanjiDAOImpl extends BaseDAOImpl implements IStudentbanjiDAO{
	
	@Autowired
	private IBaseDAOSupport support;
	
	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.Studentbanji.";
	}

	@Override
	public int deleteByStuid(StudentbanjiBean studentbanjiBean) {
		return support.delete(getNamespace() + "deletebystuid", studentbanjiBean);
	}
	

}
