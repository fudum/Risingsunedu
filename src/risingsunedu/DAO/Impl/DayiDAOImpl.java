package risingsunedu.DAO.Impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import framework.base.dao.IBaseDAOSupport;
import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.DAO.IDayiDAO;

@Repository
public class DayiDAOImpl extends BaseDAOImpl implements IDayiDAO{

	@Autowired
	IBaseDAOSupport daoSupport;
	
	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.Dayi.";
	}

	@Override
	public int update(Map<String, String> map) {
		return daoSupport.update(getNamespace() + "updateByChapter", map);
	}
	
}
