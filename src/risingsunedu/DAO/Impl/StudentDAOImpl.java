package risingsunedu.DAO.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import framework.base.dao.IBaseDAOSupport;
import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.DAO.IStudentDAO;

@Repository
public class StudentDAOImpl extends BaseDAOImpl implements IStudentDAO{

	@Autowired
	private IBaseDAOSupport support;
	
	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.Student.";
	}

	@Override
	public String getMaxStuId() {
		return support.selectOne(getNamespace() + "getMaxStuId", null);
	}
	
	

}
