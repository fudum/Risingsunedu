package risingsunedu.DAO.Impl;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import framework.base.dao.IBaseDAOSupport;
import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.Bean.KechengjieciBean;
import risingsunedu.DAO.IKechengjieciDAO;

@Repository
public class KechengjieciDAOImpl extends BaseDAOImpl implements IKechengjieciDAO{

	@Autowired
	private IBaseDAOSupport support;
	
	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.Kechengjieci.";
	}

	@Override
	public List<KechengjieciBean> selectListKechengId(String kechengId) {
		return support.selectList(getNamespace() + "selectlistbykechengid", kechengId);
	}

	@Override
	public int updateByJiangciId(KechengjieciBean bean) {
		return support.update(getNamespace() + "updatebyjiangciid", bean);
	}	
}
