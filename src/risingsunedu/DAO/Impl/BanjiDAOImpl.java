package risingsunedu.DAO.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import framework.base.common.Pager;
import framework.base.dao.IBaseDAOSupport;
import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.Bean.BanjiBean;
import risingsunedu.DAO.IBanjiDAO;

@Repository
public class BanjiDAOImpl extends BaseDAOImpl implements IBanjiDAO{

	@Autowired
	private IBaseDAOSupport banjiDAOSupport;
	
	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.Banji.";
	}

	//获取班级最大id
	@Override
	public String selectMaxId(BanjiBean bean) {
		return banjiDAOSupport.selectOne(getNamespace() + "selectmaxid", bean);
	}

	@Override
	public Pager<BanjiBean> selectPagerWithTeacher(BanjiBean bean,
			Pager<BanjiBean> pager) {
		return banjiDAOSupport.selectPage(getNamespace() + "selectbanjiwithteacher", bean, pager);
	}

}
