package risingsunedu.DAO.Impl;

import org.springframework.stereotype.Repository;

import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.DAO.IXuebaDAO;

@Repository
public class XuebaDAOImpl extends BaseDAOImpl implements IXuebaDAO{

	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.Xueba.";
	}
	
}
