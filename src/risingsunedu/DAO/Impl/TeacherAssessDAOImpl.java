package risingsunedu.DAO.Impl;

import org.springframework.stereotype.Repository;

import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.DAO.ITeacherAssessDAO;

@Repository
public class TeacherAssessDAOImpl extends BaseDAOImpl implements ITeacherAssessDAO{

	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.TeacherAssess.";
	}

}
