package risingsunedu.DAO.Impl;

import org.springframework.stereotype.Repository;

import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.DAO.IBaseTwoDAO;

@Repository
public class BaseTwoDAOImpl extends BaseDAOImpl implements IBaseTwoDAO{

	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.BaseTwo.";
	}
	
}
