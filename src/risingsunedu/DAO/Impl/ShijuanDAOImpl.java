package risingsunedu.DAO.Impl;

import org.springframework.stereotype.Repository;

import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.DAO.IShijuanDAO;

@Repository
public class ShijuanDAOImpl extends BaseDAOImpl implements IShijuanDAO{

	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.Shijuan.";
	}
	
}
