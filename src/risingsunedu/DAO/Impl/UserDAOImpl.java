package risingsunedu.DAO.Impl;

import org.springframework.stereotype.Repository;

import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.DAO.IUserDAO;

@Repository
public class UserDAOImpl extends BaseDAOImpl implements IUserDAO{

	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.User.";
	}

}
