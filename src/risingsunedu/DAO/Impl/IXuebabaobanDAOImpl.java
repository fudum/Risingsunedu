package risingsunedu.DAO.Impl;

import org.springframework.stereotype.Repository;

import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.DAO.IXuebabaobanDAO;

@Repository
public class IXuebabaobanDAOImpl extends BaseDAOImpl implements IXuebabaobanDAO{

	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.Xuebabaoban.";
	}
	
}
