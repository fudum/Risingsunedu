package risingsunedu.DAO.Impl;

import org.springframework.stereotype.Repository;

import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.DAO.IBaseOneDAO;

@Repository
public class BaseOneDAOImpl extends BaseDAOImpl implements IBaseOneDAO{

	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.BaseOne.";
	}
	
}
