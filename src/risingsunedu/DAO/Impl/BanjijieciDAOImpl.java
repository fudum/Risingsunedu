package risingsunedu.DAO.Impl;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import framework.base.dao.IBaseDAOSupport;
import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.Bean.BanjijieciBean;
import risingsunedu.DAO.IBanjijieciDAO;

@Repository
public class BanjijieciDAOImpl extends BaseDAOImpl implements IBanjijieciDAO{

	@Autowired
	private IBaseDAOSupport support;
	
	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.Banjijieci.";
	}
	
	@Override
	public List<BanjijieciBean> selectListByBanjiId(String banjiId) {
		return support.selectList(getNamespace() + "selectlistbybanjiid", banjiId);
	}

	@Override
	public int updateByJiangciId(BanjijieciBean bean) {
		return support.update(getNamespace() + "updatebyjiangciid", bean);
	}	
}
