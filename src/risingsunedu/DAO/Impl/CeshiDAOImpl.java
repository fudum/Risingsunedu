package risingsunedu.DAO.Impl;

import org.springframework.stereotype.Repository;

import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.DAO.ICeshiDAO;

@Repository
public class CeshiDAOImpl extends BaseDAOImpl implements ICeshiDAO{

	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.Ceshi.";
	}
	
}
