package risingsunedu.DAO.Impl;

import org.springframework.stereotype.Repository;

import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.DAO.ITeacherAdditionDAO;

@Repository
public class TeacherAdditionDAOImpl extends BaseDAOImpl implements ITeacherAdditionDAO {

	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.TeacherAddition.";
	}	

}
