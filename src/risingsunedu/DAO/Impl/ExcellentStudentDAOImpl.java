package risingsunedu.DAO.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import framework.base.common.Pager;
import framework.base.dao.IBaseDAOSupport;
import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.Bean.ExcellentStudentBean;
import risingsunedu.DAO.IExcellentStudentDAO;

@Repository
public class ExcellentStudentDAOImpl extends BaseDAOImpl implements IExcellentStudentDAO{

	@Autowired
	private IBaseDAOSupport support;
	
	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.ExcellentStudent.";
	}

	@Override
	public Pager<ExcellentStudentBean> selectPageWithStudent(
			ExcellentStudentBean bean, Pager<ExcellentStudentBean> pager) {
		return support.selectPage(getNamespace() + "selectPageWithStudent",bean, pager);
	}

}
