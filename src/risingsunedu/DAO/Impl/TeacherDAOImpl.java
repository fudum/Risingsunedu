package risingsunedu.DAO.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import framework.base.common.Pager;
import framework.base.dao.IBaseDAOSupport;
import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.Bean.TeacherBean;
import risingsunedu.DAO.ITeacherDAO;

@Repository
public class TeacherDAOImpl extends BaseDAOImpl implements ITeacherDAO{
	
	@Autowired
	private IBaseDAOSupport support;
	
	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.Teacher.";
	}

	@Override
	public String getMaxTeaId() {
		return support.selectOne(getNamespace() + "getMaxTeaId", null);
	}

	@Override
	public Pager<TeacherBean> selectPageLike(TeacherBean bean,
			Pager<TeacherBean> page) {
		return support.selectPage(getNamespace() + "selectPageLike",bean, page);
	}

}
