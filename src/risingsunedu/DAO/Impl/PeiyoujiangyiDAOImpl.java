package risingsunedu.DAO.Impl;

import org.springframework.stereotype.Repository;

import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.DAO.IPeiyoujiangyiDAO;

@Repository
public class PeiyoujiangyiDAOImpl extends BaseDAOImpl implements IPeiyoujiangyiDAO{

	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.Peiyoujiangyi.";
	}
	
}
