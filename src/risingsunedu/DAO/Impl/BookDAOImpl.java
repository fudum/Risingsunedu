package risingsunedu.DAO.Impl;

import org.springframework.stereotype.Repository;

import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.DAO.IBookDAO;

@Repository
public class BookDAOImpl extends BaseDAOImpl implements IBookDAO{

	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.Book.";
	}
	
}
