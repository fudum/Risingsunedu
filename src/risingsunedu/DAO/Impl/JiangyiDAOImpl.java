package risingsunedu.DAO.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import framework.base.dao.IBaseDAOSupport;
import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.Bean.JiangyiBean;
import risingsunedu.DAO.IJiangyiDAO;

@Repository
public class JiangyiDAOImpl extends BaseDAOImpl implements IJiangyiDAO{
	
	@Autowired
	IBaseDAOSupport support;
	
	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.Jiangyi.";
	}

	@Override
	public List<JiangyiBean> selectNewest(JiangyiBean jiangyiBean) {
		return support.selectList(getNamespace() + "selectnewest", jiangyiBean);
	}

	@Override
	public int selectCountByTeaId(String teaId) {
		return support.selectCount("jiangyi", " where tea_id = ?", teaId);
	}

	@Override
	public List<JiangyiBean> selectByStuId(String stu_id) {
		return support.selectList(getNamespace() + "selectbystuid", stu_id);
	}

	/**
	 * 根据教师id搜寻讲义信息
	 */
	@Override
	public List<JiangyiBean> selectByTeaId(String tea_id) {
		return support.selectList(getNamespace() + "selectbyteaid", tea_id);
	}


}
