package risingsunedu.DAO.Impl;

import org.springframework.stereotype.Repository;

import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.DAO.IZhangjieDAO;

@Repository
public class ZhangjieDAOImpl extends BaseDAOImpl implements IZhangjieDAO{

	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.Zhangjie.";
	}
	
}
