package risingsunedu.DAO.Impl;

import org.springframework.stereotype.Repository;

import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.DAO.ISchoolLevelDAO;

@Repository
public class SchoolLevelDAOImpl extends BaseDAOImpl implements ISchoolLevelDAO{

	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.SchoolLevel.";
	}

}
