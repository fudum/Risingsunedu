package risingsunedu.DAO.Impl;

import org.springframework.stereotype.Repository;

import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.DAO.IMedTestCatgDAO;

@Repository
public class MedTestCatgDAOImpl extends BaseDAOImpl implements IMedTestCatgDAO{

	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.MedTestCatg.";
	}

}
