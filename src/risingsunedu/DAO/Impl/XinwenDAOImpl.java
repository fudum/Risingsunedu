package risingsunedu.DAO.Impl;

import org.springframework.stereotype.Repository;

import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.DAO.IXinwenDAO;

@Repository
public class XinwenDAOImpl extends BaseDAOImpl implements IXinwenDAO{

	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.Xinwen.";
	}

}
