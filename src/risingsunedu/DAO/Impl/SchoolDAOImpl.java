package risingsunedu.DAO.Impl;

import org.springframework.stereotype.Repository;
import framework.base.dao.impl.BaseDAOImpl;
import risingsunedu.DAO.ISchoolDAO;

@Repository
public class SchoolDAOImpl extends BaseDAOImpl implements ISchoolDAO{

	@Override
	public String getNamespace() {
		return "risingsunedu.mapper.School.";
	}

}
