package risingsunedu.DAO;

import risingsunedu.Bean.ExcellentStudentBean;
import framework.base.common.Pager;
import framework.base.dao.IBaseDAO;

/**
 * 优秀学生dao层
 * @author fudum
 * @date 2014年11月10日
 */
public interface IExcellentStudentDAO extends IBaseDAO {
	
	public Pager<ExcellentStudentBean> selectPageWithStudent(ExcellentStudentBean bean,Pager<ExcellentStudentBean> pager);

}
