package risingsunedu.DAO;

import java.util.Map;

import framework.base.dao.IBaseDAO;

public interface IDayiDAO extends IBaseDAO {
	
	/**
	 * 根据章节修改答疑
	 * @param map
	 * @return
	 */
	public int update(Map<String, String> map);
}
