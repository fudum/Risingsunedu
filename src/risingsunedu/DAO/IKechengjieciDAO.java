package risingsunedu.DAO;

import java.util.List;

import risingsunedu.Bean.KechengjieciBean;
import framework.base.dao.IBaseDAO;

public interface IKechengjieciDAO extends IBaseDAO {
	
	public List<KechengjieciBean> selectListKechengId(String kechengId);
	
	public int updateByJiangciId(KechengjieciBean bean);
}
