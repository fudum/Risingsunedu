package risingsunedu.DAO;

import java.util.Map;

import risingsunedu.Bean.OpenkBean;
import framework.base.common.Pager;
import framework.base.dao.IBaseDAO;

public interface IOpenkDAO extends IBaseDAO {
	
	/**
	 * 搜索带有教师信息的公开课
	 * @param bean
	 * @param pager
	 * @return
	 */
	public Pager<OpenkBean> selectOpenkTeacher(OpenkBean bean,Pager<OpenkBean> pager);
	
	/**
	 * 根据修改公开课的课程信息
	 * @param map
	 * @return
	 */
	public int update(Map<String, String> map);
}
