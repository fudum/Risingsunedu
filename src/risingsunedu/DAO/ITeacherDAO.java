package risingsunedu.DAO;

import risingsunedu.Bean.TeacherBean;
import framework.base.common.Pager;
import framework.base.dao.IBaseDAO;

public interface ITeacherDAO extends IBaseDAO {
	/*
	 * 获取学生最大id
	 */
	public String getMaxTeaId();
	
	public Pager<TeacherBean> selectPageLike(TeacherBean bean,Pager<TeacherBean> page);
}
