package risingsunedu.DAO;

import risingsunedu.Bean.KechengBean;
import framework.base.common.Pager;
import framework.base.dao.IBaseDAO;

public interface IKechengDAO extends IBaseDAO {
	
	public Pager<KechengBean> selectPagerWithTeacher(KechengBean bean,Pager<KechengBean> pager);
	
}
